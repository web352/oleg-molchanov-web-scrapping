"""
Target: `https://coinmarketcap.com/` all pages
1. title
2. symbol
3. currency
"""
import requests
from bs4 import BeautifulSoup
import re
import csv
import sys


def get_html(url):
    r = requests.get(url)
    if r.ok:
        return r.text
    print(r.status_code)


def write_csv(data):
    with open('cmc.csv', 'a') as f:
        writer = csv.writer(f)

        writer.writerow((data['title'],
                         data['symbol'],
                         data['link'],
                         data['currency']))


def handle_paragraph(tr, url='https://coinmarketcap.com/'):
    # td - row, td - column
    tds = tr.find_all('td')
    paragraps = tds[2].find_all('p')
    #     print('\n', type(paragraps), paragraps, paragraps == True, sep='\n', end='\n') # debug
    if len(paragraps) > 0:
        title = paragraps[0].text
        symbol = paragraps[1].text
        link = url + tds[2].find('a').get('href')

        currency_raw = tds[3].find('a').text  # format could be $50,103.74
        try:
            currency = float(re.sub('\$|,', '', currency_raw))
        except ValueError:
            currency = ''

        return title, symbol, link, currency


def handle_span(tr, url='https://coinmarketcap.com/'):
    # td - row, td - column
    tds = tr.find_all('td')
    spans = tds[2].find_all('span')

    #     print(10*'#', tds, 10*'#', sep='\n') # debug

    # we don't need to check instance here
    title = spans[1].text
    symbol = spans[2].text
    link = url + tds[2].find('a').get('href')

    currency_raw = tds[3].text  # format could be $50,103.74
    currency = float(re.sub('\$|,', '', currency_raw))

    # print(tds, title, symbol, link, currency, sep='\n') # debug

    return title, symbol, link, currency


def get_page_data(html):
    try:
        soup = BeautifulSoup(html, 'lxml')
    except TypeError:
        sys.exit('No page')
    # HTML table rows
    trs = soup.find('table').find('tbody').find_all('tr')
    # for row in rows
    for tr in (trs):
        try:
            result = handle_paragraph(tr)
            if not result:
                result = handle_span(tr)
        except IndexError:
            continue
        else:
            title, symbol, link, currency = result

        data = {'title': title, 'symbol': symbol, 'link': link, 'currency': currency}
        write_csv(data)


def main():
    pattern = 'https://coinmarketcap.com/?page={page_num}'
    page = 1
    while True:
        html = get_html(pattern.format(page_num=page))
        get_page_data(html)
        page += 1


if __name__ == '__main__':
    main()



