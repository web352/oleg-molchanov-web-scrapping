import requests
from bs4 import BeautifulSoup
import re
import csv


def get_html(url):
    r = requests.get(url)
    return r.text


def write_csv(data):
    with open('plugins.csv', 'a') as f:
        writer = csv.writer(f)

        writer.writerow((data['name'],
                         data['url'],
                         data['reviews']))


def refined(s):
    """1,573 total ratings to -> 1573"""
    return re.sub('\D', '', s)


def get_data(html):
    soup = BeautifulSoup(html, 'lxml')
    popular = soup.find_all('section')[1]  # We're looking for second element of <bs4.element.ResultSet>
    plugins = popular.find_all('article')

    for plugin in plugins:
        name = plugin.find('h3').text  # .text unpack the data from a tag
        url = plugin.find('h3').find('a').get('href')  # 'a' unpacked tag <a>, and get('<attribute>')

        r = plugin.find('span', class_='rating-count').find('a').text  # find(<tag>, class_=<class>)
        rating = refined(r)

        data = {
            'name': name,
            'url': url,
            'reviews': rating
        }

        write_csv(data)


def main():
    url = 'https://wordpress.org/plugins/'
    print(get_data(get_html(url)))


if __name__ == '__main__':
    main()
