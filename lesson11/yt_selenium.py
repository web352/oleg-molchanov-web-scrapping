from selenium import webdriver
import time
import csv

URL = 'https://www.youtube.com/channel/UC9MK8SybZcrHR3CUV4NMy2g/videos'  # Диджитализируй
CSV = 'digitize.csv'
FIELDS = ('title', 'link', 'duration', 'views', 'posted')
webdrive_path = '/Users/ah/Desktop/chromedriver'
SCROLL_PAUSE_TIME = 0.5


def csv_init(csv_name=CSV, fields_names=FIELDS):
    with open(csv_name, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fields_names)
        writer.writeheader()


def write_csv(data, csv_name=CSV, fields_names=FIELDS):
    with open(csv_name, 'a') as file:
        writer = csv.DictWriter(file, fieldnames=fields_names)

        writer.writerow(data)


def scroll_page_to_bottom(url, driver_path):
    # Открываем канал
    driver = webdriver.Chrome(driver_path)
    driver.get(url)

    # Скролим канал
    scroll_height = driver.execute_script("return document.documentElement.scrollHeight")  # длина скрола
    top, bottom = 0, scroll_height
    while True:
        y_before_scroll = driver.execute_script("return document.documentElement.scrollHeight")

        driver.execute_script(f"window.scrollTo({top}, {bottom});")
        time.sleep(SCROLL_PAUSE_TIME)

        y_after_scroll = driver.execute_script("return document.documentElement.scrollHeight")

        if y_before_scroll == y_after_scroll:  # page bottom
            break

        top += scroll_height
        bottom += scroll_height

    # парсим вывод
    channel_videos = driver.find_elements_by_tag_name('ytd-grid-video-renderer')
    for video in channel_videos:
        title = video.find_element_by_tag_name('h3').text
        link = video.find_element_by_id('video-title').get_attribute('href')
        spans = [el.text for el in video.find_elements_by_tag_name('span') if len(el.text) > 0]
        duration = spans[0]
        views = spans[1]
        posted = spans[2]
        data = dict(zip(FIELDS, [title, link, duration, views, posted]))
        write_csv(data)


if __name__ == '__main__':
    csv_init()
    scroll_page_to_bottom(URL, webdrive_path)
