"""
catertrax
"""
import requests
from bs4 import BeautifulSoup
import csv
import sys

FIELDS = ('author', 'client_since', 'client_kind', 'content_title', 'content_description', 'email', 'phone_number')
CSV = 'testimonial.csv'
URL = 'https://catertrax.com/why-catertrax/traxers/page/{}/'  # pattern


def get_html(url):
    r = requests.get(url)
    return r.text


def csv_init(csv_name=CSV, fields_names=FIELDS):
    with open(csv_name, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fields_names)
        writer.writeheader()


def write_csv(data, csv_name=CSV, fields_names=FIELDS):
    with open(csv_name, 'a') as file:
        writer = csv.DictWriter(file, fieldnames=fields_names)

        writer.writerow(data)


def web_scrapper(html):
    soup = BeautifulSoup(html, 'lxml')
    testimonial = soup.find('div', id='testimonial-2364-3-0-0')
    articles = testimonial.find_all('article')

    if not articles:
        sys.exit('Blank page')

    for article in articles:

        author_details = article.find('div', class_='author-details')
        author = author_details.find('p', class_='testimonial-author').text.strip()
        client_since = author_details.find('p', class_='traxer-since').text.strip()

        content = article.find('div', class_='testimonial-content')
        client_kind = content.find('span', class_='testimonial-category').text.strip()
        content_title = content.find('h2').text.strip()
        content_description = content.find('p').text.strip()

        author_contacts = author_details.find('ul', class_='testimonial-meta')
        if author_contacts:
            email = author_contacts.find('li', class_='email').text.strip()
            phone_number = author_contacts.find('li', class_='tel').text.strip()
        else:
            email = None
            phone_number = None

        data = dict(zip(FIELDS, [author, client_since, client_kind, content_title, content_description,
                                 email, phone_number]))

        write_csv(data)


def main():
    csv_init()
    page = 1
    while True:
        html = get_html(URL.format(str(page)))
        web_scrapper(html)
        page += 1


if __name__ == '__main__':
    main()
