import requests
from bs4 import BeautifulSoup
from random import choice


def get_proxies():
    http = []
    https = []

    url = 'https://free-proxy-list.net/'
    html = requests.get(url).text

    soup = BeautifulSoup(html, 'lxml')
    table = soup.find('table', id='proxylisttable')
    trs = table.find_all('tr')[1:-1]
    for tr in trs:
        tds = tr.find_all('td')
        ip = tds[0].text.strip()
        port = tds[1].text.strip()
        protocol = 'https' if tds[6].text.strip() == 'yes' else 'http'
        data = {'addr': f'{ip}:{port}', 'protocol': protocol}

        if protocol == 'http':
            http.append(data)
        elif protocol == 'https':
            https.append(data)

    return http, https


def get_html(url, proxy_data):
    # proxy_data - {'addr': '185.128.136.252:3128', 'protocol': 'https'}
    p = {proxy_data['protocol']: proxy_data['addr']}
    r = requests.get(url, proxies=p, timeout=5)
    return r.json()['origin']


if __name__ == '__main__':
    http, https = get_proxies()
    random_proxy = choice(http)

    answer = get_html('http://httpbin.org/ip', random_proxy)
    print(answer)
    # 153.122.60.209 139.180.130.108
