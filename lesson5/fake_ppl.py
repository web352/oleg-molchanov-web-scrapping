from faker import Faker
import random
import csv


def csv_init(fields_names):
    """Problem here is that we need to add file_name and fields_name in bunch of methods."""
    with open('fake_humans.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fields_names)
        writer.writeheader()


def write_csv(data, fields_names):
    with open('fake_humans.csv', 'a') as file:
        writer = csv.DictWriter(file, fieldnames=fields_names)

        writer.writerow(data)


def fake_human():
    fake = Faker(['ru_RU'])
    return {'name': fake.first_name(),
            'surname': fake.last_name(),
            'age': random.randint(0, 101),
            'job_title': fake.job()}


def write():
    order = ['name', 'surname', 'age', 'job_title']
    csv_init(order)
    # creating fake humans
    for _ in range(25):
        write_csv(fake_human(), order)


def read():
    with open('fake_humans.csv') as file:
        order = ['name', 'surname', 'age', 'job_title']
        reader = csv.DictReader(file, fieldnames=order)

        for row in reader:
            print(row)


if __name__ == '__main__':
    write()
    read()




