from sqlalchemy import Column, String, Float, Integer
from sqlalchemy.ext.declarative import declarative_base
import os
import csv

Base = declarative_base()


class Coin(Base):
    __tablename__ = 'coin'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    symbol = Column(String)
    link = Column(String)
    currency = Column(Float)

## !! ** SQLITE3 ** !! ##
db_name = '7_cmc.sqlite'
if os.path.exists(db_name):
    os.remove(db_name)

from sqlalchemy import create_engine

engine = create_engine(f'sqlite:///{db_name}')

from sqlalchemy.orm import sessionmaker

session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)


def read():
    s = session()
    with open('cmc.csv') as file:
        order = ['title', 'symbol', 'link', 'currency']
        reader = csv.DictReader(file, fieldnames=order)

        # CMC = Coin(name='CMC')
        # s.add(Coin())

        for row in reader:
            try:
                row['currency'] = float(row['currency'])

            # ValueError: could not convert string to float: ''

            except ValueError:
                row['currency'] = 0
            s.add(Coin(**row))
            s.commit()
        s.close()


if __name__ == '__main__':
    read()


"""
sqlite> select * from coin where currency > 50 order by currency DESC limit 15;
3090|Punk|PUNK|https://coinmarketcap.com//currencies/punk/|156276.49
1058|42-coin|42|https://coinmarketcap.com//currencies/42-coin/|122325.69
644|Unisocks|SOCKS|https://coinmarketcap.com//currencies/unisocks/|84959.63
2354|3X Long Bitcoin Token|BULL|https://coinmarketcap.com//currencies/3x-long-bitcoin-token/|77142.93
3754|Robonomics Web Services|RWS|https://coinmarketcap.com//currencies/robonomics-web-services/|59927.87
4336|mStable BTC|MBTC|https://coinmarketcap.com//currencies/mstable-btc/|57167.87
2746|tBTC|TBTC|https://coinmarketcap.com//currencies/tbtc/|56521.9
4029|pTokens BTC|PBTC|https://coinmarketcap.com//currencies/ptokens-btc/|56220.92
595|RSK Smart Bitcoin|RBTC|https://coinmarketcap.com//currencies/rsk-smart-bitcoin/|56085.8
89|renBTC|RENBTC|https://coinmarketcap.com//currencies/renbtc/|55812.76
1|Bitcoin|BTC|https://coinmarketcap.com//currencies/bitcoin/|55713.15
205|Huobi BTC|HBTC|https://coinmarketcap.com//currencies/huobi-btc/|55682.56
16|Wrapped Bitcoin|WBTC|https://coinmarketcap.com//currencies/wrapped-bitcoin/|55540.83
4019|The Tokenized Bitcoin|imBTC|https://coinmarketcap.com//currencies/the-tokenized-bitcoin/|55422.71
4056|sBTC|SBTC|https://coinmarketcap.com//currencies/sbtc/|54698.99
"""