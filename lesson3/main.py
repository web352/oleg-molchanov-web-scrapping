"""
Target: `https://coinmarketcap.com/`
1. title
2. symbol
3. currency
"""
import requests
from bs4 import BeautifulSoup
import csv
import re


def get_html(url):
    r = requests.get(url)
    return r.text


def write_csv(data):
    with open('cmc.csv', 'a') as f:
        writer = csv.writer(f)

        writer.writerow((data['title'],
                         data['symbol'],
                         data['link'],
                         data['currency']))


def handle_paragraph(tr, url='https://coinmarketcap.com/'):
    # td - row, td - column
    tds = tr.find_all('td')
    paragraps = tds[2].find_all('p')
    if len(paragraps) > 0:
        title = paragraps[0].text
        symbol = paragraps[1].text
        link = url + tds[2].find('a').get('href')
        
        currency_raw = tds[3].find('a').text # format could be $50,103.74
        currency = float(re.sub('\$|,', '', currency_raw))
        
        return title, symbol, link, currency

def handle_span(tr, url='https://coinmarketcap.com/'):
    # td - row, td - column
    tds = tr.find_all('td')
    spans = tds[2].find_all('span')
    # we don't need to check instance len here
    title = spans[1].text
    symbol = spans[2].text
    link = url + tds[2].find('a').get('href')

    currency_raw = tds[3].text # format could be $50,103.74
    currency = float(re.sub('\$|,', '', currency_raw))

    return title, symbol, link, currency

def get_page_data(html):
    url = 'https://coinmarketcap.com/'
    soup = BeautifulSoup(html, 'lxml') 
    # HTML table rows
    trs = soup.find('table').find('tbody').find_all('tr')
    # for row in rows
    for tr in (trs):
        try:
            result = handle_paragraph(tr)
            if not result:
                result = handle_span(tr)
        except IndexError:
            continue
        else:
            title, symbol, link, currency = result
        
        data = {'title': title, 'symbol': symbol, 'link': link, 'currency': currency}
        write_csv(data)


def main():
    html = get_html('https://coinmarketcap.com/')
    get_page_data(html) 


if __name__ == '__main__':
    main()
