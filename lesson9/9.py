import requests
import csv
from multiprocessing import Pool


def get_answer(url):
    r = requests.get(url)
    return r.text


def write_csv(data):
    with open('liveinet.csv', 'a') as f:
        order = ['site_title', 'url', 'description', 'count']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)


def get_page_data(text):
    data = text.split('\n')[1:-2]
    for row in data:
        row = row.split('\t')
        site_title = row[0]
        url = row[1]
        description = row[2].strip()
        count = row[3]
        data = {'site_title': site_title, 'url': url, 'description': description, 'count': count}
        write_csv(data)


def task(url):
    html_dom = get_answer(url)
    return get_page_data(html_dom)


def main():
    #task = lambda x: get_page_data(get_answer(x))

    url = 'https://www.liveinternet.ru/rating/ru//today.tsv?page={}'
    urls = [url.format(str(page_number)) for page_number in range(1, 6756+1)]
    
    with Pool(20) as p:
        p.map(task, urls)


if __name__ == '__main__':
    main()
