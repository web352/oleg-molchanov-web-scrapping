--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: coin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.coin (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    symbol character varying(255) NOT NULL,
    link text NOT NULL,
    currency real NOT NULL
);


ALTER TABLE public.coin OWNER TO postgres;

--
-- Name: coin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.coin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coin_id_seq OWNER TO postgres;

--
-- Name: coin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.coin_id_seq OWNED BY public.coin.id;


--
-- Name: coin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coin ALTER COLUMN id SET DEFAULT nextval('public.coin_id_seq'::regclass);


--
-- Data for Name: coin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.coin (id, title, symbol, link, currency) FROM stdin;
1	Bitcoin	BTC	https://coinmarketcap.com//currencies/bitcoin/	55713.15
2	Ethereum	ETH	https://coinmarketcap.com//currencies/ethereum/	1800.57
3	Binance Coin	BNB	https://coinmarketcap.com//currencies/binance-coin/	257.16
4	Tether	USDT	https://coinmarketcap.com//currencies/tether/	1
5	Cardano	ADA	https://coinmarketcap.com//currencies/cardano/	1.1
6	Polkadot	DOT	https://coinmarketcap.com//currencies/polkadot-new/	34.9
7	XRP	XRP	https://coinmarketcap.com//currencies/xrp/	0.4753
8	Uniswap	UNI	https://coinmarketcap.com//currencies/uniswap/	29.33
9	Litecoin	LTC	https://coinmarketcap.com//currencies/litecoin/	202.47
10	Chainlink	LINK	https://coinmarketcap.com//currencies/chainlink/	27.75
11	Bitcoin Cash	BCH	https://coinmarketcap.com//currencies/bitcoin-cash/	526.66
12	USD Coin	USDC	https://coinmarketcap.com//currencies/usd-coin/	1
13	Stellar	XLM	https://coinmarketcap.com//currencies/stellar/	0.4
14	Terra	LUNA	https://coinmarketcap.com//currencies/terra-luna/	20.53
15	Dogecoin	DOGE	https://coinmarketcap.com//currencies/dogecoin/	0.06
16	Wrapped Bitcoin	WBTC	https://coinmarketcap.com//currencies/wrapped-bitcoin/	55540.83
17	THETA	THETA	https://coinmarketcap.com//currencies/theta/	7.35
18	VeChain	VET	https://coinmarketcap.com//currencies/vechain/	0.08
19	Crypto.com Coin	CRO	https://coinmarketcap.com//currencies/crypto-com-coin/	0.19
20	Aave	AAVE	https://coinmarketcap.com//currencies/aave/	372.28
21	Cosmos	ATOM	https://coinmarketcap.com//currencies/cosmos/	21.07
22	Monero	XMR	https://coinmarketcap.com//currencies/monero/	225.2
23	Avalanche	AVAX	https://coinmarketcap.com//currencies/avalanche/	30.75
24	EOS	EOS	https://coinmarketcap.com//currencies/eos/	4.08
25	FTX Token	FTT	https://coinmarketcap.com//currencies/ftx-token/	40.21
26	IOTA	MIOTA	https://coinmarketcap.com//currencies/iota/	1.35
27	TRON	TRX	https://coinmarketcap.com//currencies/tron/	0.05
28	Solana	SOL	https://coinmarketcap.com//currencies/solana/	13.73
29	Bitcoin SV	BSV	https://coinmarketcap.com//currencies/bitcoin-sv/	194.44
30	NEM	XEM	https://coinmarketcap.com//currencies/nem/	0.4
31	Filecoin	FIL	https://coinmarketcap.com//currencies/filecoin/	59.13
32	Algorand	ALGO	https://coinmarketcap.com//currencies/algorand/	1.22
33	Binance USD	BUSD	https://coinmarketcap.com//currencies/binance-usd/	1
34	Tezos	XTZ	https://coinmarketcap.com//currencies/tezos/	4.03
35	Chiliz	CHZ	https://coinmarketcap.com//currencies/chiliz/	0.54
36	Neo	NEO	https://coinmarketcap.com//currencies/neo/	40.44
37	Dai	DAI	https://coinmarketcap.com//currencies/multi-collateral-dai/	1
38	Kusama	KSM	https://coinmarketcap.com//currencies/kusama/	326.86
39	Huobi Token	HT	https://coinmarketcap.com//currencies/huobi-token/	14.82
40	Hedera Hashgraph	HBAR	https://coinmarketcap.com//currencies/hedera-hashgraph/	0.35
41	SushiSwap	SUSHI	https://coinmarketcap.com//currencies/sushiswap/	20.19
42	Elrond	EGLD	https://coinmarketcap.com//currencies/elrond-egld/	132.46
43	Dash	DASH	https://coinmarketcap.com//currencies/dash/	222.63
44	Synthetix	SNX	https://coinmarketcap.com//currencies/synthetix-network-token/	19.03
45	Enjin Coin	ENJ	https://coinmarketcap.com//currencies/enjin-coin/	2.6
46	Ravencoin	RVN	https://coinmarketcap.com//currencies/ravencoin/	0.26
47	The Graph	GRT	https://coinmarketcap.com//currencies/the-graph/	1.75
48	NEAR Protocol	NEAR	https://coinmarketcap.com//currencies/near-protocol/	6.69
49	Decred	DCR	https://coinmarketcap.com//currencies/decred/	166.2
50	UNUS SED LEO	LEO	https://coinmarketcap.com//currencies/unus-sed-leo/	2.06
51	Compound	COMP	https://coinmarketcap.com//currencies/compound/	438.2
52	Maker	MKR	https://coinmarketcap.com//currencies/maker/	2035.33
53	Polygon	MATIC	https://coinmarketcap.com//currencies/polygon/	0.38
54	Theta Fuel	TFUEL	https://coinmarketcap.com//currencies/theta-fuel/	0.36
55	Zilliqa	ZIL	https://coinmarketcap.com//currencies/zilliqa/	0.16
56	BitTorrent	BTT	https://coinmarketcap.com//currencies/bittorrent/	0
57	Zcash	ZEC	https://coinmarketcap.com//currencies/zcash/	137.56
58	Decentraland	MANA	https://coinmarketcap.com//currencies/decentraland/	0.97
59	Nexo	NEXO	https://coinmarketcap.com//currencies/nexo/	2.69
60	Ethereum Classic	ETC	https://coinmarketcap.com//currencies/ethereum-classic/	12.37
61	Bitcoin BEP2	BTCB	https://coinmarketcap.com//currencies/bitcoin-bep2/	54661.86
62	UMA	UMA	https://coinmarketcap.com//currencies/uma/	23.9
63	PancakeSwap	CAKE	https://coinmarketcap.com//currencies/pancakeswap/	10.17
64	THORChain	RUNE	https://coinmarketcap.com//currencies/thorchain/	5.62
65	Voyager Token	VGX	https://coinmarketcap.com//currencies/voyager-token/	5.77
66	yearn.finance	YFI	https://coinmarketcap.com//currencies/yearn-finance/	34379.32
67	SwissBorg	CHSB	https://coinmarketcap.com//currencies/swissborg/	1.33
68	Bancor	BNT	https://coinmarketcap.com//currencies/bancor/	7.56
69	Basic Attention Token	BAT	https://coinmarketcap.com//currencies/basic-attention-token/	0.82
70	Stacks	STX	https://coinmarketcap.com//currencies/stacks/	1.14
71	Celsius	CEL	https://coinmarketcap.com//currencies/celsius/	4.83
72	ICON	ICX	https://coinmarketcap.com//currencies/icon/	1.91
73	Holo	HOT	https://coinmarketcap.com//currencies/holo/	0.01
74	Ren	REN	https://coinmarketcap.com//currencies/ren/	1.11
75	TerraUSD	UST	https://coinmarketcap.com//currencies/terrausd/	1
76	Revain	REV	https://coinmarketcap.com//currencies/revain/	0.01
77	0x	ZRX	https://coinmarketcap.com//currencies/0x/	1.37
78	Reserve Rights	RSR	https://coinmarketcap.com//currencies/reserve-rights/	0.08
79	Waves	WAVES	https://coinmarketcap.com//currencies/waves/	9.9
80	Flow	FLOW	https://coinmarketcap.com//currencies/flow/	33.07
81	Fantom	FTM	https://coinmarketcap.com//currencies/fantom/	0.39
82	Ontology	ONT	https://coinmarketcap.com//currencies/ontology/	1.19
83	DigiByte	DGB	https://coinmarketcap.com//currencies/digibyte/	0.07
84	Siacoin	SC	https://coinmarketcap.com//currencies/siacoin/	0.02
85	OKB	OKB	https://coinmarketcap.com//currencies/okb/	14.69
86	Harmony	ONE	https://coinmarketcap.com//currencies/harmony/	0.09
87	Pundi X	NPXS	https://coinmarketcap.com//currencies/pundi-x/	0
88	Paxos Standard	PAX	https://coinmarketcap.com//currencies/paxos-standard/	1
89	renBTC	RENBTC	https://coinmarketcap.com//currencies/renbtc/	55812.76
90	OMG Network	OMG	https://coinmarketcap.com//currencies/omg/	5.12
91	Celo	CELO	https://coinmarketcap.com//currencies/celo/	3.69
92	IOST	IOST	https://coinmarketcap.com//currencies/iostoken/	0.04
93	Loopring	LRC	https://coinmarketcap.com//currencies/loopring/	0.54
94	Nano	NANO	https://coinmarketcap.com//currencies/nano/	4.93
95	Orbs	ORBS	https://coinmarketcap.com//currencies/orbs/	0.28
96	Curve DAO Token	CRV	https://coinmarketcap.com//currencies/curve-dao-token/	2.47
97	Ocean Protocol	OCEAN	https://coinmarketcap.com//currencies/ocean-protocol/	1.45
98	Qtum	QTUM	https://coinmarketcap.com//currencies/qtum/	6.12
99	1inch	1INCH	https://coinmarketcap.com//currencies/1inch/	4.11
100	Helium	HNT	https://coinmarketcap.com//currencies/helium/	7.72
101	Ankr	ANKR	https://coinmarketcap.com//currencies/ankr/	0.0805
102	HUSD	HUSD	https://coinmarketcap.com//currencies/husd/	0.9997
103	Horizen	ZEN	https://coinmarketcap.com//currencies/horizen/	51.6
104	ZKSwap	ZKS	https://coinmarketcap.com//currencies/zkswap/	2.67
105	Bitcoin Gold	BTG	https://coinmarketcap.com//currencies/bitcoin-gold/	30.04
106	Verge	XVG	https://coinmarketcap.com//currencies/verge/	0.03101
107	SKALE Network	SKL	https://coinmarketcap.com//currencies/skale-network/	0.7635
108	Reef	REEF	https://coinmarketcap.com//currencies/reef/	0.04061
109	Quant	QNT	https://coinmarketcap.com//currencies/quant/	37.6
110	Kyber Network	KNC	https://coinmarketcap.com//currencies/kyber-network/	2.18
111	Fetch.ai	FET	https://coinmarketcap.com//currencies/fetch/	0.6
112	DODO	DODO	https://coinmarketcap.com//currencies/dodo/	4.42
113	Golem	GLM	https://coinmarketcap.com//currencies/golem-network-tokens/	0.43
114	Energy Web Token	EWT	https://coinmarketcap.com//currencies/energy-web-token/	14.14
115	Arweave	AR	https://coinmarketcap.com//currencies/arweave/	12.65
116	Lisk	LSK	https://coinmarketcap.com//currencies/lisk/	3.28
117	IoTeX	IOTX	https://coinmarketcap.com//currencies/iotex/	0.04
118	Alpha Finance Lab	ALPHA	https://coinmarketcap.com//currencies/alpha-finance-lab/	1.64
119	FunFair	FUN	https://coinmarketcap.com//currencies/funfair/	0.04
120	WAX	WAXP	https://coinmarketcap.com//currencies/wax/	0.26
121	HedgeTrade	HEDG	https://coinmarketcap.com//currencies/hedgetrade/	1.14
122	Nervos Network	CKB	https://coinmarketcap.com//currencies/nervos-network/	0.02
123	Venus	XVS	https://coinmarketcap.com//currencies/venus/	43.52
124	VeThor Token	VTHO	https://coinmarketcap.com//currencies/vethor-token/	0.01
125	Balancer	BAL	https://coinmarketcap.com//currencies/balancer/	54.48
126	KuCoin Token	KCS	https://coinmarketcap.com//currencies/kucoin-token/	4.66
127	Kava.io	KAVA	https://coinmarketcap.com//currencies/kava/	6.23
128	Telcoin	TEL	https://coinmarketcap.com//currencies/telcoin/	0.01
129	MVL	MVL	https://coinmarketcap.com//currencies/mvl/	0.03
130	Polymath	POLY	https://coinmarketcap.com//currencies/polymath-network/	0.55
131	Augur	REP	https://coinmarketcap.com//currencies/augur/	29.76
132	Status	SNT	https://coinmarketcap.com//currencies/status/	0.09
133	Badger DAO	BADGER	https://coinmarketcap.com//currencies/badger-dao/	42.89
134	OriginTrail	TRAC	https://coinmarketcap.com//currencies/origintrail/	0.86
135	Celer Network	CELR	https://coinmarketcap.com//currencies/celer-network/	0.06
136	Oasis Network	ROSE	https://coinmarketcap.com//currencies/oasis-network/	0.2
137	TrueUSD	TUSD	https://coinmarketcap.com//currencies/trueusd/	1
138	Utrust	UTK	https://coinmarketcap.com//currencies/utrust/	0.66
139	Dent	DENT	https://coinmarketcap.com//currencies/dent/	0
140	BORA	BORA	https://coinmarketcap.com//currencies/bora/	0.34
141	Polkastarter	POLS	https://coinmarketcap.com//currencies/polkastarter/	4.87
142	MyNeighborAlice	ALICE	https://coinmarketcap.com//currencies/myneighboralice/	16.28
143	QuarkChain	QKC	https://coinmarketcap.com//currencies/quarkchain/	0.04
144	Civic	CVC	https://coinmarketcap.com//currencies/civic/	0.42
145	Swipe	SXP	https://coinmarketcap.com//currencies/swipe/	3.19
146	Band Protocol	BAND	https://coinmarketcap.com//currencies/band-protocol/	13.42
147	Aragon	ANT	https://coinmarketcap.com//currencies/aragon/	6.93
148	MaidSafeCoin	MAID	https://coinmarketcap.com//currencies/maidsafecoin/	0.61
149	Origin Protocol	OGN	https://coinmarketcap.com//currencies/origin-protocol/	1.03
150	Serum	SRM	https://coinmarketcap.com//currencies/serum/	5.39
151	COTI	COTI	https://coinmarketcap.com//currencies/coti/	0.39
152	Wanchain	WAN	https://coinmarketcap.com//currencies/wanchain/	1.53
153	Ampleforth	AMPL	https://coinmarketcap.com//currencies/ampleforth/	0.89
154	Everipedia	IQ	https://coinmarketcap.com//currencies/everipedia/	0.02
155	NuCypher	NU	https://coinmarketcap.com//currencies/nucypher/	0.59
156	SingularityNET	AGI	https://coinmarketcap.com//currencies/singularitynet/	0.28
157	Storj	STORJ	https://coinmarketcap.com//currencies/storj/	0.94
158	Ardor	ARDR	https://coinmarketcap.com//currencies/ardor/	0.23
159	BitShares	BTS	https://coinmarketcap.com//currencies/bitshares/	0.08
160	Gnosis	GNO	https://coinmarketcap.com//currencies/gnosis-gno/	150.24
161	RSK Infrastructure Framework	RIF	https://coinmarketcap.com//currencies/rsk-infrastructure-framework/	0.31
162	Numeraire	NMR	https://coinmarketcap.com//currencies/numeraire/	46.42
163	Orchid	OXT	https://coinmarketcap.com//currencies/orchid/	0.6
164	Bitcoin Standard Hashrate Token	BTCST	https://coinmarketcap.com//currencies/bitcoin-standard-hashrate-token/	370.39
165	Travala.com	AVA	https://coinmarketcap.com//currencies/travala/	4.15
166	Secret	SCRT	https://coinmarketcap.com//currencies/secret/	3.03
167	TrustSwap	SWAP	https://coinmarketcap.com//currencies/trustswap/	2.6
168	IRISnet	IRIS	https://coinmarketcap.com//currencies/irisnet/	0.22
169	Litentry	LIT	https://coinmarketcap.com//currencies/litentry/	11.13
170	Bitcoin Diamond	BCD	https://coinmarketcap.com//currencies/bitcoin-diamond/	1.08
171	iExec RLC	RLC	https://coinmarketcap.com//currencies/rlc/	2.5
172	Stratis	STRAX	https://coinmarketcap.com//currencies/stratis/	1.53
173	Metadium	META	https://coinmarketcap.com//currencies/metadium/	0.16
174	ZB Token	ZB	https://coinmarketcap.com//currencies/zb-token/	0.42
175	TomoChain	TOMO	https://coinmarketcap.com//currencies/tomochain/	2.34
176	Hifi Finance	MFT	https://coinmarketcap.com//currencies/mainframe/	0.02
177	Ultra	UOS	https://coinmarketcap.com//currencies/ultra/	0.66
178	Unibright	UBT	https://coinmarketcap.com//currencies/unibright/	1.22
179	Chromia	CHR	https://coinmarketcap.com//currencies/chromia/	0.43
180	Velas	VLX	https://coinmarketcap.com//currencies/velas/	0.09
181	Injective Protocol	INJ	https://coinmarketcap.com//currencies/injective-protocol/	11.56
182	Haven Protocol	XHV	https://coinmarketcap.com//currencies/haven-protocol/	12.81
183	Komodo	KMD	https://coinmarketcap.com//currencies/komodo/	1.4
184	Syscoin	SYS	https://coinmarketcap.com//currencies/syscoin/	0.28
185	Steem	STEEM	https://coinmarketcap.com//currencies/steem/	0.46
186	Sora	XOR	https://coinmarketcap.com//currencies/sora/	489.66
187	Ark	ARK	https://coinmarketcap.com//currencies/ark/	1.32
188	aelf	ELF	https://coinmarketcap.com//currencies/aelf/	0.31
189	Phala Network	PHA	https://coinmarketcap.com//currencies/phala-network/	0.94
190	StormX	STMX	https://coinmarketcap.com//currencies/stormx/	0.02
191	district0x	DNT	https://coinmarketcap.com//currencies/district0x/	0.28
192	Divi	DIVI	https://coinmarketcap.com//currencies/divi/	0.07
193	Kin	KIN	https://coinmarketcap.com//currencies/kin/	0
194	Syntropy	NOIA	https://coinmarketcap.com//currencies/syntropy/	0.48
195	Thunder Token	TT	https://coinmarketcap.com//currencies/thunder-token/	0.02
196	LTO Network	LTO	https://coinmarketcap.com//currencies/lto-network/	0.57
197	JUST	JST	https://coinmarketcap.com//currencies/just/	0.07
198	CyberVein	CVT	https://coinmarketcap.com//currencies/cybervein/	0.15
199	ABBC Coin	ABBC	https://coinmarketcap.com//currencies/abbc-coin/	0.19
200	Loom Network	LOOM	https://coinmarketcap.com//currencies/loom-network/	0.16
201	Creditcoin	CTC	https://coinmarketcap.com//currencies/creditcoin/	6.95
202	Bridge Oracle	BRG	https://coinmarketcap.com//currencies/bridge-oracle/	0.2808
203	HEX	HEX	https://coinmarketcap.com//currencies/hex/	0.01198
204	Counos X	CCXX	https://coinmarketcap.com//currencies/counos-x/	92.61
205	Huobi BTC	HBTC	https://coinmarketcap.com//currencies/huobi-btc/	55682.56
206	WhiteCoin	XWC	https://coinmarketcap.com//currencies/whitecoin/	2.03
207	DeFiChain	DFI	https://coinmarketcap.com//currencies/defichain/	3.49
208	Wrapped BNB	WBNB	https://coinmarketcap.com//currencies/wbnb/	253.77
209	INO COIN	INO	https://coinmarketcap.com//currencies/ino-coin/	6.87
210	ThoreCoin	THR	https://coinmarketcap.com//currencies/thorecoin/	10833.06
211	BitMax Token	BTMX	https://coinmarketcap.com//currencies/bitmax-token/	1.34
212	The Transfer Token	TTT	https://coinmarketcap.com//currencies/the-transfer-token/	6.95
213	Orbit Chain	ORC	https://coinmarketcap.com//currencies/orbit-chain/	1.01
214	XinFin Network	XDC	https://coinmarketcap.com//currencies/xinfin-network/	0.04
215	Bitpanda Ecosystem Token	BEST	https://coinmarketcap.com//currencies/bitpanda-ecosystem-token/	1.08
216	Mixin	XIN	https://coinmarketcap.com//currencies/mixin/	840.24
217	NXM	NXM	https://coinmarketcap.com//currencies/nxm/	68.7
218	Venus BNB	vBNB	https://coinmarketcap.com//currencies/venus-bnb/	5.17
219	The Sandbox	SAND	https://coinmarketcap.com//currencies/the-sandbox/	0.62
220	Conflux Network	CFX	https://coinmarketcap.com//currencies/conflux-network/	0.59
221	Orion Protocol	ORN	https://coinmarketcap.com//currencies/orion-protocol/	18.93
222	Klever	KLV	https://coinmarketcap.com//currencies/klever/	0.1
223	Zelwin	ZLW	https://coinmarketcap.com//currencies/zelwin/	4.86
224	stETH (Lido)	STETH	https://coinmarketcap.com//currencies/steth/	1748.1
225	GNY	GNY	https://coinmarketcap.com//currencies/gny/	1.69
226	Mirror Protocol	MIR	https://coinmarketcap.com//currencies/mirror-protocol/	7.32
227	Electroneum	ETN	https://coinmarketcap.com//currencies/electroneum/	0.02
228	Hathor	HTR	https://coinmarketcap.com//currencies/hathor/	1.67
229	Bitcoin Cash ABC	BCHA	https://coinmarketcap.com//currencies/bitcoin-cash-abc-2/	16.14
230	Rewardiqa	REW	https://coinmarketcap.com//currencies/rewardiqa/	29.87
231	Mdex	MDX	https://coinmarketcap.com//currencies/mdex/	3.9
232	Keep Network	KEEP	https://coinmarketcap.com//currencies/keep-network/	0.59
233	SafePal	SFP	https://coinmarketcap.com//currencies/safepal/	2.7
234	dKargo	DKA	https://coinmarketcap.com//currencies/dkargo/	0.36
235	Axie Infinity	AXS	https://coinmarketcap.com//currencies/axie-infinity/	5.93
236	Sologenic	SOLO	https://coinmarketcap.com//currencies/sologenic/	1.26
237	MATH	MATH	https://coinmarketcap.com//currencies/math/	2.18
238	Idea Chain Coin	ICH	https://coinmarketcap.com//currencies/idea-chain-coin/	4.7
239	botXcoin	BOTX	https://coinmarketcap.com//currencies/botxcoin/	0.15
240	Linear	LINA	https://coinmarketcap.com//currencies/linear/	0.09
241	Amp	AMP	https://coinmarketcap.com//currencies/amp/	0.02
242	Venus BTC	vBTC	https://coinmarketcap.com//currencies/venus-btc/	1103.91
243	Eauric	EAURIC	https://coinmarketcap.com//currencies/eauric/	8.33
244	RedFOX Labs	RFOX	https://coinmarketcap.com//currencies/redfox-labs/	0.16
245	Audius	AUDIO	https://coinmarketcap.com//currencies/audius/	1.75
246	Gala	GALA	https://coinmarketcap.com//currencies/gala/	0.03
247	Akash Network	AKT	https://coinmarketcap.com//currencies/akash-network/	5.11
248	WHALE	WHALE	https://coinmarketcap.com//currencies/whale/	37.75
249	Venus XVS	vXVS	https://coinmarketcap.com//currencies/venus-xvs/	0.87
250	Dego Finance	DEGO	https://coinmarketcap.com//currencies/dego-finance/	22.46
251	TROY	TROY	https://coinmarketcap.com//currencies/troy/	0.02
252	SuperFarm	SUPER	https://coinmarketcap.com//currencies/superfarm/	1.83
253	BakeryToken	BAKE	https://coinmarketcap.com//currencies/bakerytoken/	1.27
254	MimbleWimbleCoin	MWC	https://coinmarketcap.com//currencies/mimblewimblecoin/	16.63
255	DerivaDAO	DDX	https://coinmarketcap.com//currencies/derivadao/	6.68
256	Elitium	EUM	https://coinmarketcap.com//currencies/elitium/	5.79
257	Neutrino USD	USDN	https://coinmarketcap.com//currencies/neutrino-usd/	1
258	Doctors Coin	DRS	https://coinmarketcap.com//currencies/doctors-coin/	0.74
259	SparkPoint	SRK	https://coinmarketcap.com//currencies/sparkpoint/	0.02
260	Beefy.Finance	BIFI	https://coinmarketcap.com//currencies/beefy-finance/	2307.31
261	Rocket Pool	RPL	https://coinmarketcap.com//currencies/rocket-pool/	16.03
262	Rari Governance Token	RGT	https://coinmarketcap.com//currencies/rari-governance-token/	18.73
263	Trust Wallet Token	TWT	https://coinmarketcap.com//currencies/trust-wallet-token/	0.63
264	KardiaChain	KAI	https://coinmarketcap.com//currencies/kardiachain/	0.08
265	MANTRA DAO	OM	https://coinmarketcap.com//currencies/mantra-dao/	0.57
266	Akropolis	AKRO	https://coinmarketcap.com//currencies/akropolis/	0.06
267	Livepeer	LPT	https://coinmarketcap.com//currencies/livepeer/	7.12
268	Tokenlon Network Token	LON	https://coinmarketcap.com//currencies/tokenlon-network-token/	6.87
269	Populous	PPT	https://coinmarketcap.com//currencies/populous/	2.8
270	NewYork Exchange	NYE	https://coinmarketcap.com//currencies/newyork-exchange/	20.93
271	Power Ledger	POWR	https://coinmarketcap.com//currencies/power-ledger/	0.34
272	Vai	VAI	https://coinmarketcap.com//currencies/vai/	0.9
273	Hive	HIVE	https://coinmarketcap.com//currencies/hive-blockchain/	0.37
274	CoinMetro Token	XCM	https://coinmarketcap.com//currencies/coinmetro-token/	0.48
275	Streamr	DATA	https://coinmarketcap.com//currencies/streamr/	0.17
276	sUSD	SUSD	https://coinmarketcap.com//currencies/susd/	1.01
277	Perpetual Protocol	PERP	https://coinmarketcap.com//currencies/perpetual-protocol/	6.54
278	PAX Gold	PAXG	https://coinmarketcap.com//currencies/pax-gold/	1741.32
279	EFFORCE	WOZX	https://coinmarketcap.com//currencies/efforce/	2.18
280	PIBBLE	PIB	https://coinmarketcap.com//currencies/pibble/	0.01
281	Uquid Coin	UQC	https://coinmarketcap.com//currencies/uquid-coin/	14
282	Aion	AION	https://coinmarketcap.com//currencies/aion/	0.28
283	Hellenic Coin	HNC	https://coinmarketcap.com//currencies/helleniccoin/	1.99
284	Bloomzed Loyalty Club Ticket	BLCT	https://coinmarketcap.com//currencies/bloomzed-token/	3.64
285	PARSIQ	PRQ	https://coinmarketcap.com//currencies/parsiq/	1.26
286	saffron.finance	SFI	https://coinmarketcap.com//currencies/saffron-finance/	1767.59
287	Mask Network	MASK	https://coinmarketcap.com//currencies/mask-network/	15.19
288	Carry	CRE	https://coinmarketcap.com//currencies/carry/	0.02
289	Bytom	BTM	https://coinmarketcap.com//currencies/bytom/	0.09
290	Edgeware	EDG	https://coinmarketcap.com//currencies/edgeware/	0.02
291	Spendcoin	SPND	https://coinmarketcap.com//currencies/spendcoin/	0.05
292	Centrality	CENNZ	https://coinmarketcap.com//currencies/centrality/	0.11
293	Attila	ATT	https://coinmarketcap.com//currencies/attila/	0.28
294	GreenPower	GRN	https://coinmarketcap.com//currencies/greenpower/	0.04
295	12Ships	TSHP	https://coinmarketcap.com//currencies/12ships/	0.03
296	REVV	REVV	https://coinmarketcap.com//currencies/revv/	0.45
297	MediBloc	MED	https://coinmarketcap.com//currencies/medibloc/	0.05
298	Big Data Protocol	BDP	https://coinmarketcap.com//currencies/big-data-protocol/	3.89
299	Dusk Network	DUSK	https://coinmarketcap.com//currencies/dusk-network/	0.34
300	Wootrade	WOO	https://coinmarketcap.com//currencies/wootrade/	0.32
301	Request	REQ	https://coinmarketcap.com//currencies/request/	0.1222
302	MonaCoin	MONA	https://coinmarketcap.com//currencies/monacoin/	1.85
303	Sapphire	SAPP	https://coinmarketcap.com//currencies/sapphire/	0.2361
304	HARD Protocol	HARD	https://coinmarketcap.com//currencies/hard-protocol/	2.37
305	GateToken	GT	https://coinmarketcap.com//currencies/gatetoken/	1.55
306	WazirX	WRX	https://coinmarketcap.com//currencies/wazirx/	0.4939
307	Gemini Dollar	GUSD	https://coinmarketcap.com//currencies/gemini-dollar/	0.9961
308	Chimpion	BNANA	https://coinmarketcap.com//currencies/chimpion/	3.66
309	Gas	GAS	https://coinmarketcap.com//currencies/gas/	11.48
310	LBRY Credits	LBC	https://coinmarketcap.com//currencies/library-credit/	0.2228
311	BarnBridge	BOND	https://coinmarketcap.com//currencies/barnbridge/	66.8
312	NFTX	NFTX	https://coinmarketcap.com//currencies/nftx/	259.11
313	Radix	EXRD	https://coinmarketcap.com//currencies/radix/	0.15
314	Bella Protocol	BEL	https://coinmarketcap.com//currencies/bella-protocol/	4.03
315	Frax	FRAX	https://coinmarketcap.com//currencies/frax/	1
316	AdEx Network	ADX	https://coinmarketcap.com//currencies/adx-net/	0.96
317	yOUcash	YOUC	https://coinmarketcap.com//currencies/youcash/	0.06
318	Raydium	RAY	https://coinmarketcap.com//currencies/raydium/	9.75
319	Handshake	HNS	https://coinmarketcap.com//currencies/handshake/	0.3
320	PIXEL	PXL	https://coinmarketcap.com//currencies/pixel/	0.19
321	ShareToken	SHR	https://coinmarketcap.com//currencies/sharetoken/	0.05
322	AllianceBlock	ALBT	https://coinmarketcap.com//currencies/allianceblock/	0.77
323	Atari Token	ATRI	https://coinmarketcap.com//currencies/atari-token/	0.27
324	Unifi Protocol DAO	UNFI	https://coinmarketcap.com//currencies/unifi-protocol-dao/	31.3
325	KLAYswap Protocol	KSP	https://coinmarketcap.com//currencies/klayswap-protocol/	18.06
326	Darwinia Network	RING	https://coinmarketcap.com//currencies/darwinia-network/	0.23
327	ReddCoin	RDD	https://coinmarketcap.com//currencies/redd/	0
328	SOLVE	SOLVE	https://coinmarketcap.com//currencies/solve/	0.28
329	Bluzelle	BLZ	https://coinmarketcap.com//currencies/bluzelle/	0.38
330	Waltonchain	WTC	https://coinmarketcap.com//currencies/waltonchain/	1.43
331	DIA	DIA	https://coinmarketcap.com//currencies/dia/	3.63
332	Mobilian Coin	MBN	https://coinmarketcap.com//currencies/mobilian-coin/	0.72
333	xDai	STAKE	https://coinmarketcap.com//currencies/xdai/	24.04
334	XeniosCoin	XNC	https://coinmarketcap.com//currencies/xenioscoin/	1.32
335	Zenon	ZNN	https://coinmarketcap.com//currencies/zenon/	18.9
336	Cartesi	CTSI	https://coinmarketcap.com//currencies/cartesi/	0.33
337	SUN	SUN	https://coinmarketcap.com//currencies/sun/	20.48
338	Ethernity Chain	ERN	https://coinmarketcap.com//currencies/ethernity-chain/	22.67
339	bZx Protocol	BZRX	https://coinmarketcap.com//currencies/bzx-protocol/	0.57
340	Massnet	MASS	https://coinmarketcap.com//currencies/massnet/	1.04
341	Lambda	LAMB	https://coinmarketcap.com//currencies/lambda/	0.07
342	Firo	FIRO	https://coinmarketcap.com//currencies/firo/	8.34
343	Venus USDC	vUSDC	https://coinmarketcap.com//currencies/venus-usdc/	0.02
344	Zap	ZAP	https://coinmarketcap.com//currencies/zap/	0.41
345	Frax Share	FXS	https://coinmarketcap.com//currencies/frax-share/	12.61
346	Kylin	KYL	https://coinmarketcap.com//currencies/kylin/	1.92
347	Enzyme	MLN	https://coinmarketcap.com//currencies/enzyme/	53.14
348	Hegic	HEGIC	https://coinmarketcap.com//currencies/hegic/	0.24
349	Contentos	COS	https://coinmarketcap.com//currencies/contentos/	0.03
350	Ignis	IGNIS	https://coinmarketcap.com//currencies/ignis/	0.12
351	Bridge Mutual	BMI	https://coinmarketcap.com//currencies/bridge-mutual/	3.83
352	RAMP	RAMP	https://coinmarketcap.com//currencies/ramp/	0.47
353	Morpheus.Network	MRPH	https://coinmarketcap.com//currencies/morpheus-network/	1.94
354	Frontier	FRONT	https://coinmarketcap.com//currencies/frontier/	2.49
355	PAC Global	PAC	https://coinmarketcap.com//currencies/pac-global/	0.01
356	CertiK	CTK	https://coinmarketcap.com//currencies/certik/	2.5
357	Dragonchain	DRGN	https://coinmarketcap.com//currencies/dragonchain/	0.24
358	NKN	NKN	https://coinmarketcap.com//currencies/nkn/	0.15
359	MovieBloc	MBL	https://coinmarketcap.com//currencies/moviebloc/	0.01
360	Metal	MTL	https://coinmarketcap.com//currencies/metal/	1.35
361	DuckDaoDime	DDIM	https://coinmarketcap.com//currencies/duckdaodime/	85.29
362	Terra Virtua Kolect	TVK	https://coinmarketcap.com//currencies/terra-virtua-kolect/	0.99
363	Energi	NRG	https://coinmarketcap.com//currencies/energi/	2.25
364	Constellation	DAG	https://coinmarketcap.com//currencies/constellation/	0.07
365	Perlin	PERL	https://coinmarketcap.com//currencies/perlin/	0.18
366	ARPA Chain	ARPA	https://coinmarketcap.com//currencies/arpa-chain/	0.08
367	Ferrum Network	FRM	https://coinmarketcap.com//currencies/ferrum-network/	0.58
368	ChainX	PCX	https://coinmarketcap.com//currencies/chainx/	11.2
369	Harvest Finance	FARM	https://coinmarketcap.com//currencies/harvest-finance/	170.5
370	Verasity	VRA	https://coinmarketcap.com//currencies/verasity/	0.02
371	NULS	NULS	https://coinmarketcap.com//currencies/nuls/	0.86
372	WaykiChain	WICC	https://coinmarketcap.com//currencies/waykichain/	0.41
373	DFI.Money	YFII	https://coinmarketcap.com//currencies/yearn-finance-ii/	2203.41
374	Bytecoin	BCN	https://coinmarketcap.com//currencies/bytecoin-bcn/	0
375	WINk	WIN	https://coinmarketcap.com//currencies/wink/	0
376	Nexus	NXS	https://coinmarketcap.com//currencies/nexus/	1.19
377	JustLiquidity	JUL	https://coinmarketcap.com//currencies/justliquidity/	185.68
378	Beam	BEAM	https://coinmarketcap.com//currencies/beam/	0.98
379	BioPassport Token	BIOT	https://coinmarketcap.com//currencies/biopassport-token/	0.04
380	Beldex	BDX	https://coinmarketcap.com//currencies/beldex/	0.08
381	Meme	MEME	https://coinmarketcap.com//currencies/degenerator-meme/	2902.24
382	Huobi Pool Token	HPT	https://coinmarketcap.com//currencies/huobi-pool-token/	0.01
383	Aavegotchi	GHST	https://coinmarketcap.com//currencies/aavegotchi/	1.6
384	Aergo	AERGO	https://coinmarketcap.com//currencies/aergo/	0.3
385	FC Barcelona Fan Token	BAR	https://coinmarketcap.com//currencies/fc-barcelona-fan-token/	49.61
386	Cashaa	CAS	https://coinmarketcap.com//currencies/cashaa/	0.12
387	Wrapped NXM	WNXM	https://coinmarketcap.com//currencies/wrapped-nxm/	47.23
388	Decentral Games	DG	https://coinmarketcap.com//currencies/decentral-games/	442.97
389	DxChain Token	DX	https://coinmarketcap.com//currencies/dxchain-token/	0
390	YIELD App	YLD	https://coinmarketcap.com//currencies/yield-app/	0.86
391	Radicle	RAD	https://coinmarketcap.com//currencies/radicle/	15.87
392	Nash	NEX	https://coinmarketcap.com//currencies/nash-exchange/	2.66
393	Marlin	POND	https://coinmarketcap.com//currencies/marlin/	0.17
394	SUKU	SUKU	https://coinmarketcap.com//currencies/suku/	0.67
395	Switcheo	SWTH	https://coinmarketcap.com//currencies/switcheo/	0.06
396	Hxro	HXRO	https://coinmarketcap.com//currencies/hxro/	0.34
397	DeXe	DEXE	https://coinmarketcap.com//currencies/dexe/	26.73
398	NEST Protocol	NEST	https://coinmarketcap.com//currencies/nest-protocol/	0.04
399	MX Token	MX	https://coinmarketcap.com//currencies/mx-token/	0.63
400	LUKSO	LYXe	https://coinmarketcap.com//currencies/lukso/	19.84
401	LUKSO	LYXe	https://coinmarketcap.com//currencies/lukso/	19.84
402	inSure DeFi	SURE	https://coinmarketcap.com//currencies/insure/	0.004852
403	DEXTools	DEXT	https://coinmarketcap.com//currencies/dextools/	0.7769
404	Swingby	SWINGBY	https://coinmarketcap.com//currencies/swingby/	0.632
405	RChain	REV	https://coinmarketcap.com//currencies/rchain/	0.1545
406	API3	API3	https://coinmarketcap.com//currencies/api3/	5.39
407	AXEL	AXEL	https://coinmarketcap.com//currencies/axel/	0.2801
408	Vitae	VITAE	https://coinmarketcap.com//currencies/vitae/	3.81
409	Venus ETH	vETH	https://coinmarketcap.com//currencies/venus-eth/	36.05
410	Standard Tokenization Protocol	STPT	https://coinmarketcap.com//currencies/standard-tokenization-protocol/	0.0716
411	v.systems	VSYS	https://coinmarketcap.com//currencies/v-systems/	0.03
412	PIVX	PIVX	https://coinmarketcap.com//currencies/pivx/	1.1
413	BOSAGORA	BOA	https://coinmarketcap.com//currencies/bosagora/	0.24
414	KARMA	KARMA	https://coinmarketcap.com//currencies/karma-eos/	0.01
415	MAPS	MAPS	https://coinmarketcap.com//currencies/maps/	1.12
416	LCX	LCX	https://coinmarketcap.com//currencies/lcx/	0.11
417	Selfkey	KEY	https://coinmarketcap.com//currencies/selfkey/	0.02
418	Qcash	QC	https://coinmarketcap.com//currencies/qcash/	0.15
419	Prometeus	PROM	https://coinmarketcap.com//currencies/prometeus/	11.47
420	Flamingo	FLM	https://coinmarketcap.com//currencies/flamingo/	0.47
421	DigitalBits	XDB	https://coinmarketcap.com//currencies/digitalbits/	0.09
422	Oxen	OXEN	https://coinmarketcap.com//currencies/oxen/	1.29
423	Render Token	RNDR	https://coinmarketcap.com//currencies/render-token/	0.48
424	Quantstamp	QSP	https://coinmarketcap.com//currencies/quantstamp/	0.1
425	Elastos	ELA	https://coinmarketcap.com//currencies/elastos/	3.96
426	VideoCoin	VID	https://coinmarketcap.com//currencies/videocoin/	0.46
427	DREP	DREP	https://coinmarketcap.com//currencies/drep/	0.02
428	BEPRO Network	BEPRO	https://coinmarketcap.com//currencies/bepro-network/	0.04
429	Sport and Leisure	SNL	https://coinmarketcap.com//currencies/sport-and-leisure/	0.1
430	Bondly	BONDLY	https://coinmarketcap.com//currencies/bondly/	0.64
431	Spartan Protocol	SPARTA	https://coinmarketcap.com//currencies/spartan-protocol/	1.04
432	Dock	DOCK	https://coinmarketcap.com//currencies/dock/	0.1
433	Cindicator	CND	https://coinmarketcap.com//currencies/cindicator/	0.03
434	Vesper	VSP	https://coinmarketcap.com//currencies/vesper/	35.55
435	Everest	ID	https://coinmarketcap.com//currencies/everest/	0.53
436	Nimiq	NIM	https://coinmarketcap.com//currencies/nimiq/	0.01
437	VIDT Datalink	VIDT	https://coinmarketcap.com//currencies/vidt-datalink/	1.3
438	Einsteinium	EMC2	https://coinmarketcap.com//currencies/einsteinium/	0.29
439	Fusion	FSN	https://coinmarketcap.com//currencies/fusion/	1.29
440	Lympo	LYM	https://coinmarketcap.com//currencies/lympo/	0.08
441	MiL.k	MLK	https://coinmarketcap.com//currencies/milk-alliance/	0.83
442	TrueFi	TRU	https://coinmarketcap.com//currencies/truefi-token/	0.46
443	Cream Finance	CREAM	https://coinmarketcap.com//currencies/cream-finance/	103.16
444	Maro	MARO	https://coinmarketcap.com//currencies/maro/	0.13
445	Ripio Credit Network	RCN	https://coinmarketcap.com//currencies/ripio-credit-network/	0.12
446	Stakenet	XSN	https://coinmarketcap.com//currencies/stakenet/	0.54
447	Mithril	MITH	https://coinmarketcap.com//currencies/mithril/	0.06
448	Tellor	TRB	https://coinmarketcap.com//currencies/tellor/	51.33
449	Kleros	PNK	https://coinmarketcap.com//currencies/kleros/	0.1
450	Multiplier	MXX	https://coinmarketcap.com//currencies/multiplier/	0.33
451	Endor Protocol	EDR	https://coinmarketcap.com//currencies/endor-protocol/	0.04
452	MXC	MXC	https://coinmarketcap.com//currencies/mxc/	0.02
453	Duck DAO (DLP Duck Token)	DUCK	https://coinmarketcap.com//currencies/duck-dao/	1.93
454	Aeternity	AE	https://coinmarketcap.com//currencies/aeternity/	0.18
455	Pirate Chain	ARRR	https://coinmarketcap.com//currencies/pirate-chain/	0.34
456	Ambrosus	AMB	https://coinmarketcap.com//currencies/amber/	0.15
457	Cocos-BCX	COCOS	https://coinmarketcap.com//currencies/cocos-bcx/	1.42
458	TEMCO	TEMCO	https://coinmarketcap.com//currencies/temco/	0.02
459	SIRIN LABS Token	SRN	https://coinmarketcap.com//currencies/sirin-labs-token/	0.12
460	Phantasma	SOUL	https://coinmarketcap.com//currencies/phantasma/	0.6
461	YUSRA	YUSRA	https://coinmarketcap.com//currencies/yusra/	1.56
462	Tornado Cash	TORN	https://coinmarketcap.com//currencies/torn/	179.31
463	Empty Set Dollar	ESD	https://coinmarketcap.com//currencies/empty-set-dollar/	0.13
464	FIO Protocol	FIO	https://coinmarketcap.com//currencies/fio-protocol/	0.26
465	Cortex	CTXC	https://coinmarketcap.com//currencies/cortex/	0.23
466	dForce	DF	https://coinmarketcap.com//currencies/dforce/	0.51
467	Sentinel	SENT	https://coinmarketcap.com//currencies/sentinel/	0.03
468	Sentinel Protocol	UPP	https://coinmarketcap.com//currencies/sentinel-protocol/	0.12
469	IDEX	IDEX	https://coinmarketcap.com//currencies/idex/	0.1
470	STASIS EURO	EURS	https://coinmarketcap.com//currencies/stasis-euro/	1.2
471	BSCPAD	BSCPAD	https://coinmarketcap.com//currencies/bscpad/	3.84
472	PowerPool	CVP	https://coinmarketcap.com//currencies/powerpool/	2.76
473	Nexalt	XLT	https://coinmarketcap.com//currencies/nexalt/	2.72
474	USDX [Kava]	USDX	https://coinmarketcap.com//currencies/usdx-kava/	0.92
475	GoChain	GO	https://coinmarketcap.com//currencies/gochain/	0.05
476	Crust	CRU	https://coinmarketcap.com//currencies/crust/	33.86
477	Venus BUSD	vBUSD	https://coinmarketcap.com//currencies/venus-busd/	0.02
478	Phoenix Global	PHB	https://coinmarketcap.com//currencies/phoenix-global/	0.02
479	Dovu	DOV	https://coinmarketcap.com//currencies/dovu/	0.07
480	Basid Coin	BASID	https://coinmarketcap.com//currencies/basid-coin/	0.09
481	Groestlcoin	GRS	https://coinmarketcap.com//currencies/groestlcoin/	0.69
482	Anyswap	ANY	https://coinmarketcap.com//currencies/anyswap/	2.84
483	DIGG	DIGG	https://coinmarketcap.com//currencies/digg/	34190.31
484	Measurable Data Token	MDT	https://coinmarketcap.com//currencies/measurable-data-token/	0.08
485	suterusu	SUTER	https://coinmarketcap.com//currencies/suterusu/	0.01
486	Ergo	ERG	https://coinmarketcap.com//currencies/ergo/	1.72
487	Poseidon Network	QQQ	https://coinmarketcap.com//currencies/poseidon-network/	0.02
488	MCO	MCO	https://coinmarketcap.com//currencies/crypto-com/	3.29
489	LockTrip	LOC	https://coinmarketcap.com//currencies/lockchain/	3.52
490	TitanSwap	TITAN	https://coinmarketcap.com//currencies/titanswap/	0.97
491	GXChain	GXC	https://coinmarketcap.com//currencies/gxchain/	0.74
492	BTU Protocol	BTU	https://coinmarketcap.com//currencies/btu-protocol/	0.57
493	VITE	VITE	https://coinmarketcap.com//currencies/vite/	0.11
494	pNetwork	PNT	https://coinmarketcap.com//currencies/pnetwork/	1.89
495	Galatasaray Fan Token	GAL	https://coinmarketcap.com//currencies/galatasaray-fan-token/	14.68
496	Goose Finance	EGG	https://coinmarketcap.com//currencies/goose-finance/	53.49
497	ankrETH	aEth	https://coinmarketcap.com//currencies/ankreth/	1714.31
498	cVault.finance	CORE	https://coinmarketcap.com//currencies/cvault-finance/	4982.87
499	DSLA Protocol	DSLA	https://coinmarketcap.com//currencies/dsla-protocol/	0.01
500	Nebulas	NAS	https://coinmarketcap.com//currencies/nebulas-token/	0.84
501	DSLA Protocol	DSLA	https://coinmarketcap.com//currencies/dsla-protocol/	0.007714
502	Keep3rV1	KP3R	https://coinmarketcap.com//currencies/keep3rv1/	246.71
503	Nebulas	NAS	https://coinmarketcap.com//currencies/nebulas-token/	0.8433
504	Bitrue Coin	BTR	https://coinmarketcap.com//currencies/bitrue-coin/	0.3738
505	BitZ Token	BZ	https://coinmarketcap.com//currencies/bit-z-token/	0.3952
506	TNC Coin	TNC	https://coinmarketcap.com//currencies/tnc-coin/	0.02101
507	Bounce Token	AUCTION	https://coinmarketcap.com//currencies/bounce-token/	23.28
508	The Midas Touch Gold	TMTG	https://coinmarketcap.com//currencies/the-midas-touch-gold/	0.006196
509	HyperCash	HC	https://coinmarketcap.com//currencies/hypercash/	1.07
510	VerusCoin	VRSC	https://coinmarketcap.com//currencies/veruscoin/	0.7851
511	Shopping	SPI	https://coinmarketcap.com//currencies/shopping/	50.14
512	GET Protocol	GET	https://coinmarketcap.com//currencies/get-protocol/	4.2
513	Observer	OBSR	https://coinmarketcap.com//currencies/observer/	0.02
514	UniLend	UFT	https://coinmarketcap.com//currencies/unilend/	3.32
515	Burger Swap	BURGER	https://coinmarketcap.com//currencies/burger-swap/	3.96
516	Muse	MUSE	https://coinmarketcap.com//currencies/muse/	91.92
517	Quiztok	QTCON	https://coinmarketcap.com//currencies/quiztok/	0.07
518	Wirex Token	WXT	https://coinmarketcap.com//currencies/wirex-token/	0.01
519	Shroom.Finance	SHROOM	https://coinmarketcap.com//currencies/shroom-finance/	0.9
520	Skycoin	SKY	https://coinmarketcap.com//currencies/skycoin/	2.3
521	Wing	WING	https://coinmarketcap.com//currencies/wing/	43.97
522	Bonfida	FIDA	https://coinmarketcap.com//currencies/bonfida/	1.75
523	Function X	FX	https://coinmarketcap.com//currencies/function-x/	0.2
524	Etherisc DIP Token	DIP	https://coinmarketcap.com//currencies/etherisc/	0.25
525	Darma Cash	DMCH	https://coinmarketcap.com//currencies/darma-cash/	0.21
526	Crowns	CWS	https://coinmarketcap.com//currencies/crowns/	56.39
527	TerraKRW	KRT	https://coinmarketcap.com//currencies/terra-krw/	0
528	Humanscape	HUM	https://coinmarketcap.com//currencies/humanscape/	0.09
529	Auto	AUTO	https://coinmarketcap.com//currencies/auto/	3034.48
530	Hacken Token	HAI	https://coinmarketcap.com//currencies/hackenai/	0.18
531	mStable USD	MUSD	https://coinmarketcap.com//currencies/mstable-usd/	1
532	Veritaseum	VERI	https://coinmarketcap.com//currencies/veritaseum/	20.41
533	RING X PLATFORM	RINGX	https://coinmarketcap.com//currencies/ring-x-platform/	0.2
534	Rally	RLY	https://coinmarketcap.com//currencies/rally/	0.71
535	AirSwap	AST	https://coinmarketcap.com//currencies/airswap/	0.29
536	Sentivate	SNTVT	https://coinmarketcap.com//currencies/sentivate/	0.01
537	EasyFi	EASY	https://coinmarketcap.com//currencies/easyfi/	22.78
538	Steem Dollars	SBD	https://coinmarketcap.com//currencies/steem-dollars/	6.6
539	Celo Dollar	CUSD	https://coinmarketcap.com//currencies/celo-dollar/	1
540	Neblio	NEBL	https://coinmarketcap.com//currencies/neblio/	2.43
541	Moss Coin	MOC	https://coinmarketcap.com//currencies/moss-coin/	0.11
542	Nxt	NXT	https://coinmarketcap.com//currencies/nxt/	0.04
543	Wabi	WABI	https://coinmarketcap.com//currencies/wabi/	0.42
544	All Sports	SOC	https://coinmarketcap.com//currencies/all-sports/	0.03
545	Apollo Currency	APL	https://coinmarketcap.com//currencies/apollo-currency/	0
546	BitKan	KAN	https://coinmarketcap.com//currencies/bitkan/	0
547	Stafi	FIS	https://coinmarketcap.com//currencies/stafi/	3.6
548	RioDeFi	RFUEL	https://coinmarketcap.com//currencies/rio-defi/	0.16
549	DMarket	DMT	https://coinmarketcap.com//currencies/dmarket/	0.71
550	BoringDAO	BOR	https://coinmarketcap.com//currencies/boringdao/	493.63
551	OptionRoom	ROOM	https://coinmarketcap.com//currencies/optionroom/	3.26
552	APY.Finance	APY	https://coinmarketcap.com//currencies/apy-finance/	1.66
553	Super Zero Protocol	SERO	https://coinmarketcap.com//currencies/super-zero-protocol/	0.13
554	Alpha Quark Token	AQT	https://coinmarketcap.com//currencies/alpha-quark-token/	7.76
555	Bitball Treasure	BTRS	https://coinmarketcap.com//currencies/bitball-treasure/	87.46
556	Telos	TLOS	https://coinmarketcap.com//currencies/telos/	0.15
557	OctoFi	OCTO	https://coinmarketcap.com//currencies/octofi/	68.23
558	Refereum	RFR	https://coinmarketcap.com//currencies/refereum/	0.01
559	Meta	MTA	https://coinmarketcap.com//currencies/meta/	2.25
560	BigONE Token	ONE	https://coinmarketcap.com//currencies/bigone-token/	0
561	LATOKEN	LA	https://coinmarketcap.com//currencies/latoken/	0.1
562	Proton	XPR	https://coinmarketcap.com//currencies/proton/	0.01
563	DAD	DAD	https://coinmarketcap.com//currencies/dad/	0.29
564	0Chain	ZCN	https://coinmarketcap.com//currencies/0chain/	0.79
565	Whiteheart	WHITE	https://coinmarketcap.com//currencies/whiteheart/	4286.6
566	88mph	MPH	https://coinmarketcap.com//currencies/88mph/	126.16
567	TE-FOOD	TONE	https://coinmarketcap.com//currencies/te-food/	0.07
568	Grin	GRIN	https://coinmarketcap.com//currencies/grin/	0.55
569	Kadena	KDA	https://coinmarketcap.com//currencies/kadena/	0.35
570	Robonomics.network	XRT	https://coinmarketcap.com//currencies/robonomics-network/	39.07
571	DigixDAO	DGD	https://coinmarketcap.com//currencies/digixdao/	346.74
572	Bifrost	BFC	https://coinmarketcap.com//currencies/bifrost/	0.07
573	Oraichain Token	ORAI	https://coinmarketcap.com//currencies/oraichain-token/	40.69
574	Vertcoin	VTC	https://coinmarketcap.com//currencies/vertcoin/	0.61
575	ZeroSwap	ZEE	https://coinmarketcap.com//currencies/zeroswap/	1.09
576	Project Pai	PAI	https://coinmarketcap.com//currencies/project-pai/	0.02
577	Navcoin	NAV	https://coinmarketcap.com//currencies/nav-coin/	0.5
578	Propy	PRO	https://coinmarketcap.com//currencies/propy/	0.51
579	Raiden Network Token	RDN	https://coinmarketcap.com//currencies/raiden-network-token/	0.7
580	Effect.AI	EFX	https://coinmarketcap.com//currencies/effect-ai/	0.14
581	dHedge DAO	DHT	https://coinmarketcap.com//currencies/dhedge-dao/	4.06
582	TOP	TOP	https://coinmarketcap.com//currencies/top/	0.01
583	Aurora	AOA	https://coinmarketcap.com//currencies/aurora/	0
584	Bankera	BNK	https://coinmarketcap.com//currencies/bankera/	0
585	Minter Network	BIP	https://coinmarketcap.com//currencies/minter-network/	0.01
586	Molecular Future	MOF	https://coinmarketcap.com//currencies/molecular-future/	0.4
587	Neutrino Token	NSBT	https://coinmarketcap.com//currencies/neutrino-system-base-token/	18.6
588	Finxflo	FXF	https://coinmarketcap.com//currencies/finxflo/	0.56
589	Presearch	PRE	https://coinmarketcap.com//currencies/presearch/	0.1
590	COVER Protocol	COVER	https://coinmarketcap.com//currencies/cover-protocol-new/	555.46
591	PumaPay	PMA	https://coinmarketcap.com//currencies/pumapay/	0
592	Newscrypto	NWC	https://coinmarketcap.com//currencies/newscrypto/	0.35
593	Bao Finance	BAO	https://coinmarketcap.com//currencies/bao-finance/	0
594	Aidos Kuneen	ADK	https://coinmarketcap.com//currencies/aidos-kuneen/	1.32
595	RSK Smart Bitcoin	RBTC	https://coinmarketcap.com//currencies/rsk-smart-bitcoin/	56085.8
596	CasinoCoin	CSC	https://coinmarketcap.com//currencies/casinocoin/	0
597	Bounce Token (OLD)	BOT	https://coinmarketcap.com//currencies/bounce-token-old/	2595.01
598	Gifto	GTO	https://coinmarketcap.com//currencies/gifto/	0.04
599	BASIC	BASIC	https://coinmarketcap.com//currencies/basic/	0.01
600	Tixl	TXL	https://coinmarketcap.com//currencies/tixl-new/	0.59
601	BASIC	BASIC	https://coinmarketcap.com//currencies/basic/	0.007237
602	Tixl	TXL	https://coinmarketcap.com//currencies/tixl-new/	0.5897
603	Genesis Vision	GVT	https://coinmarketcap.com//currencies/genesis-vision/	7.03
604	LGO Token	LGO	https://coinmarketcap.com//currencies/lgo-token/	0.5361
605	AGA Token	AGA	https://coinmarketcap.com//currencies/aga/	4.2
606	BUX Token	BUX	https://coinmarketcap.com//currencies/bux-token/	0.5308
607	Aleph.im	ALEPH	https://coinmarketcap.com//currencies/aleph-im/	0.3658
608	Ultiledger	ULT	https://coinmarketcap.com//currencies/ultiledger/	0.01289
609	Opium	OPIUM	https://coinmarketcap.com//currencies/opium/	7.34
610	PEAKDEFI	PEAK	https://coinmarketcap.com//currencies/peakdefi/	0.2758
611	Venus USDT	vUSDT	https://coinmarketcap.com//currencies/venus-usdt/	0.02
612	Bitcoin 2	BTC2	https://coinmarketcap.com//currencies/bitcoin2/	1.72
613	BnkToTheFuture	BFT	https://coinmarketcap.com//currencies/bnktothefuture/	0.04
614	LGCY Network	LGCY	https://coinmarketcap.com//currencies/lgcy-network/	0
615	Blocknet	BLOCK	https://coinmarketcap.com//currencies/blocknet/	3.96
616	Decentralized Vulnerability Platform	DVP	https://coinmarketcap.com//currencies/decentralized-vulnerability-platform/	0.1
617	Rubic	RBC	https://coinmarketcap.com//currencies/rubic/	0.3
618	Metronome	MET	https://coinmarketcap.com//currencies/metronome/	2.55
619	NFT	NFT	https://coinmarketcap.com//currencies/nft/	0.79
620	Morpheus Labs	MITX	https://coinmarketcap.com//currencies/morpheus-labs/	0.07
621	AntiMatter	MATTER	https://coinmarketcap.com//currencies/antimatter/	3.04
622	Rarible	RARI	https://coinmarketcap.com//currencies/rarible/	33.74
623	Invictus Hyperion Fund	IHF	https://coinmarketcap.com//currencies/invictus-hyperion-fund/	0.24
624	OAX	OAX	https://coinmarketcap.com//currencies/oax/	0.38
625	QASH	QASH	https://coinmarketcap.com//currencies/qash/	0.08
626	USDK	USDK	https://coinmarketcap.com//currencies/usdk/	1
627	AppCoins	APPC	https://coinmarketcap.com//currencies/appcoins/	0.11
628	Dynamic	DYN	https://coinmarketcap.com//currencies/dynamic/	1.83
629	YFDAI.FINANCE	YF-DAI	https://coinmarketcap.com//currencies/yfdai-finance/	7027.26
782	Ubiq	UBQ	https://coinmarketcap.com//currencies/ubiq/	0.36
630	Anchor Neural World	ANW	https://coinmarketcap.com//currencies/anchor-neural-world/	0.1
631	Smartlands Network	SLT	https://coinmarketcap.com//currencies/smartlands-network/	5.47
632	Freeway Token	FWT	https://coinmarketcap.com//currencies/freeway-token/	0.01
633	ForTube	FOR	https://coinmarketcap.com//currencies/the-force-protocol/	0.06
634	ASTA	ASTA	https://coinmarketcap.com//currencies/asta/	0.03
635	Paris Saint-Germain Fan Token	PSG	https://coinmarketcap.com//currencies/paris-saint-germain-fan-token/	21.34
636	HUNT	HUNT	https://coinmarketcap.com//currencies/hunt/	0.25
637	TokenClub	TCT	https://coinmarketcap.com//currencies/tokenclub/	0.04
638	FirmaChain	FCT	https://coinmarketcap.com//currencies/firmachain/	0.15
639	POA	POA	https://coinmarketcap.com//currencies/poa/	0.1
640	WOM Protocol	WOM	https://coinmarketcap.com//currencies/wom-protocol/	0.27
641	WePower	WPR	https://coinmarketcap.com//currencies/wepower/	0.04
642	TenX	PAY	https://coinmarketcap.com//currencies/tenx/	0.18
643	Obyte	GBYTE	https://coinmarketcap.com//currencies/obyte/	35.83
644	Unisocks	SOCKS	https://coinmarketcap.com//currencies/unisocks/	84959.63
645	Davinci Coin	DAC	https://coinmarketcap.com//currencies/davinci-coin/	0.01
646	StableXSwap	STAX	https://coinmarketcap.com//currencies/stablexswap/	1.93
647	ExNetwork Token	EXNT	https://coinmarketcap.com//currencies/exnetwork-token/	0.42
648	1irstcoin	FST	https://coinmarketcap.com//currencies/1irstcoin/	0.75
649	Elamachain	ELAMA	https://coinmarketcap.com//currencies/elamachain/	0.04
650	Decentr	DEC	https://coinmarketcap.com//currencies/decentr/	0.35
651	Blox	CDT	https://coinmarketcap.com//currencies/blox/	0.04
652	Hermez Network	HEZ	https://coinmarketcap.com//currencies/hermez-network/	5.52
653	OST	OST	https://coinmarketcap.com//currencies/ost/	0.03
654	Mirrored Tesla	mTSLA	https://coinmarketcap.com//currencies/mirrored-tesla/	708.92
655	Bread	BRD	https://coinmarketcap.com//currencies/bread/	0.28
656	Venus SXP	vSXP	https://coinmarketcap.com//currencies/vsxp/	0.06
657	AC Milan Fan Token	ACM	https://coinmarketcap.com//currencies/ac-milan-fan-token/	12.67
658	CyberMiles	CMT	https://coinmarketcap.com//currencies/cybermiles/	0.03
659	PCHAIN	PI	https://coinmarketcap.com//currencies/pchain/	0.03
660	CUTcoin	CUT	https://coinmarketcap.com//currencies/cutcoin/	0.19
661	Safex Token	SFT	https://coinmarketcap.com//currencies/safex-token/	0.01
662	Nerve Finance	NRV	https://coinmarketcap.com//currencies/nerve-finance/	0.54
663	Helmet.insure	HELMET	https://coinmarketcap.com//currencies/helmet-insure/	1
664	ICHI	ICHI	https://coinmarketcap.com//currencies/ichi/	9.64
665	Safe Haven	SHA	https://coinmarketcap.com//currencies/safe-haven/	0.01
666	CryptalDash	CRD	https://coinmarketcap.com//currencies/cryptaldash/	0.02
667	Newton	NEW	https://coinmarketcap.com//currencies/newton/	0
668	Umbrella Network	UMB	https://coinmarketcap.com//currencies/umbrella-network/	1.34
669	Razor Network	RAZOR	https://coinmarketcap.com//currencies/razor-network/	0.5
670	Bonded Finance	BOND	https://coinmarketcap.com//currencies/bonded-finance/	0.06
671	Quantum Resistant Ledger	QRL	https://coinmarketcap.com//currencies/quantum-resistant-ledger/	0.33
672	Moeda Loyalty Points	MDA	https://coinmarketcap.com//currencies/moeda-loyalty-points/	1.21
673	Lattice Token	LTX	https://coinmarketcap.com//currencies/lattice-token/	0.83
674	UnlimitedIP	UIP	https://coinmarketcap.com//currencies/unlimitedip/	0.02
675	Crypterium	CRPT	https://coinmarketcap.com//currencies/crpt/	0.28
676	MurAll	PAINT	https://coinmarketcap.com//currencies/murall/	0
677	Rakon	RKN	https://coinmarketcap.com//currencies/rakon/	0.1
678	Egretia	EGT	https://coinmarketcap.com//currencies/egretia/	0.01
679	SALT	SALT	https://coinmarketcap.com//currencies/salt/	0.29
680	PowerTrade Fuel	PTF	https://coinmarketcap.com//currencies/powertrade-fuel/	1.13
681	Ducato Protocol Token	DUCATO	https://coinmarketcap.com//currencies/ducato-protocol-token/	17.98
682	MediShares	MDS	https://coinmarketcap.com//currencies/medishares/	0.02
683	Homeros	HMR	https://coinmarketcap.com//currencies/homeros/	0.05
684	MixMarvel	MIX	https://coinmarketcap.com//currencies/mixmarvel/	0.01
685	Mirrored Netflix	mNFLX	https://coinmarketcap.com//currencies/mirrored-netflix/	531.25
686	Metaverse Dualchain Network Architecture	DNA	https://coinmarketcap.com//currencies/metaverse-dualchain-network-architecture/	0
687	EUNO	EUNO	https://coinmarketcap.com//currencies/euno/	0
688	Mirrored iShares Silver Trust	mSLV	https://coinmarketcap.com//currencies/mirrored-ishares-silver-trust/	24.62
689	QLC Chain	QLC	https://coinmarketcap.com//currencies/qlink/	0.09
690	Gameswap	GSWAP	https://coinmarketcap.com//currencies/gameswap/	2.64
691	Tokamak Network	TON	https://coinmarketcap.com//currencies/tokamak-network/	8.19
692	Shadows	DOWS	https://coinmarketcap.com//currencies/shadows/	2.95
693	Gleec	GLEEC	https://coinmarketcap.com//currencies/gleec/	1.04
694	Mirrored Amazon	mAMZN	https://coinmarketcap.com//currencies/mirrored-amazon/	3213.74
695	Dero	DERO	https://coinmarketcap.com//currencies/dero/	2.07
696	Mirrored Invesco QQQ Trust	mQQQ	https://coinmarketcap.com//currencies/mirrored-invesco-qqq-trust/	328.69
697	ArdCoin	ARDX	https://coinmarketcap.com//currencies/ardcoin/	0.08
698	Mirrored Apple	mAAPL	https://coinmarketcap.com//currencies/mirrored-apple/	129.33
699	Benchmark Protocol	MARK	https://coinmarketcap.com//currencies/benchmark-protocol/	1.32
700	Kryll	KRL	https://coinmarketcap.com//currencies/kryll/	0.67
701	ZEON	ZEON	https://coinmarketcap.com//currencies/zeon/	0.0007055
702	Namecoin	NMC	https://coinmarketcap.com//currencies/namecoin/	1.4
703	Mirrored Twitter	mTWTR	https://coinmarketcap.com//currencies/mirrored-twitter/	70.9
704	Hashgard	GARD	https://coinmarketcap.com//currencies/hashgard/	0.0002061
705	Cobak Token	CBK	https://coinmarketcap.com//currencies/cobak-token/	7.26
706	Factom	FCT	https://coinmarketcap.com//currencies/factom/	2.09
707	Prosper	PROS	https://coinmarketcap.com//currencies/prosper/	4.48
708	Non-Fungible Yearn	NFY	https://coinmarketcap.com//currencies/non-fungible-yearn/	316.9
709	UniLayer	LAYER	https://coinmarketcap.com//currencies/unilayer/	1
710	TrueChain	TRUE	https://coinmarketcap.com//currencies/truechain/	0.2505
711	AnRKey X	$ANRX	https://coinmarketcap.com//currencies/anrkey-x/	0.21
712	Aryacoin	AYA	https://coinmarketcap.com//currencies/aryacoin/	0.12
713	Metaverse ETP	ETP	https://coinmarketcap.com//currencies/metaverse/	0.25
714	BitcoinHD	BHD	https://coinmarketcap.com//currencies/bitcoinhd/	3.25
715	Mirrored Microsoft	mMSFT	https://coinmarketcap.com//currencies/mirrored-microsoft/	240.24
716	STEM CELL COIN	SCC	https://coinmarketcap.com//currencies/stem-cell-coin/	0.06
717	Juventus Fan Token	JUV	https://coinmarketcap.com//currencies/juventus-fan-token/	14.82
718	Agrello	DLT	https://coinmarketcap.com//currencies/agrello-delta/	0.19
719	BitcoinPoS	BPS	https://coinmarketcap.com//currencies/bitcoinpos/	4.69
720	BLOCKv	VEE	https://coinmarketcap.com//currencies/blockv/	0.01
721	ELYSIA	EL	https://coinmarketcap.com//currencies/elysia/	0.01
722	Autonio	NIOX	https://coinmarketcap.com//currencies/autonio/	0.27
723	Bird.Money	BIRD	https://coinmarketcap.com//currencies/bird-money/	219.7
724	YOYOW	YOYOW	https://coinmarketcap.com//currencies/yoyow/	0.04
725	Everex	EVX	https://coinmarketcap.com//currencies/everex/	0.84
726	CUDOS	CUDOS	https://coinmarketcap.com//currencies/cudos/	0.05
727	Centaur	CNTR	https://coinmarketcap.com//currencies/centaur/	0.02
728	#MetaHash	MHC	https://coinmarketcap.com//currencies/metahash/	0.01
729	XSGD	XSGD	https://coinmarketcap.com//currencies/xsgd/	0.76
730	Mirrored Alibaba	mBABA	https://coinmarketcap.com//currencies/mirrored-alibaba/	231.72
731	Mirrored iShares Gold Trust	mIAU	https://coinmarketcap.com//currencies/mirrored-ishares-gold-trust/	17.06
732	FOAM	FOAM	https://coinmarketcap.com//currencies/foam/	0.06
733	Basis Cash	BAC	https://coinmarketcap.com//currencies/basis-cash/	0.32
734	Cardstack	CARD	https://coinmarketcap.com//currencies/cardstack/	0.01
735	SakeToken	SAKE	https://coinmarketcap.com//currencies/sake-token/	0.24
736	Yield Optimization Platform & Protocol	YOP	https://coinmarketcap.com//currencies/yop/	1.82
737	X-CASH	XCASH	https://coinmarketcap.com//currencies/x-cash/	0
738	Ruff	RUFF	https://coinmarketcap.com//currencies/ruff/	0.02
739	Nucleus Vision	NCASH	https://coinmarketcap.com//currencies/nucleus-vision/	0
740	GameCredits	GAME	https://coinmarketcap.com//currencies/gamecredits/	0.13
741	Mirrored United States Oil Fund	mUSO	https://coinmarketcap.com//currencies/mirrored-united-states-oil-fund/	44.29
742	Kira Network	KEX	https://coinmarketcap.com//currencies/kira-network/	1.44
743	SYNC Network	SYNC	https://coinmarketcap.com//currencies/sync-network/	0.16
744	Props Token	PROPS	https://coinmarketcap.com//currencies/props/	0.06
745	Unistake	UNISTAKE	https://coinmarketcap.com//currencies/unistake/	0.13
746	Polyient Games Governance Token	PGT	https://coinmarketcap.com//currencies/polyient-games-governance-token/	1007.55
747	Dentacoin	DCN	https://coinmarketcap.com//currencies/dentacoin/	0
748	CONUN	CON	https://coinmarketcap.com//currencies/conun/	0.01
749	DOS Network	DOS	https://coinmarketcap.com//currencies/dos-network/	0.13
750	PRIZM	PZM	https://coinmarketcap.com//currencies/prizm/	0.01
751	Viacoin	VIA	https://coinmarketcap.com//currencies/viacoin/	0.75
752	PolySwarm	NCT	https://coinmarketcap.com//currencies/polyswarm/	0.01
753	carVertical	CV	https://coinmarketcap.com//currencies/carvertical/	0
754	Trittium	TRTT	https://coinmarketcap.com//currencies/trittium/	0.13
755	Venus LINK	vLINK	https://coinmarketcap.com//currencies/venus-link/	0.56
756	Viberate	VIB	https://coinmarketcap.com//currencies/viberate/	0.09
757	NerveNetwork	NVT	https://coinmarketcap.com//currencies/nervenetwork/	0.06
758	Permission Coin	ASK	https://coinmarketcap.com//currencies/permission-coin/	0
759	DeepBrain Chain	DBC	https://coinmarketcap.com//currencies/deepbrain-chain/	0.01
760	Pickle Finance	PICKLE	https://coinmarketcap.com//currencies/pickle-finance/	12.85
761	DATA	DTA	https://coinmarketcap.com//currencies/data/	0
762	THEKEY	TKY	https://coinmarketcap.com//currencies/thekey/	0
763	Venus LTC	vLTC	https://coinmarketcap.com//currencies/venus-ltc/	4.04
764	eBoost	EBST	https://coinmarketcap.com//currencies/eboostcoin/	0.17
765	Monetha	MTH	https://coinmarketcap.com//currencies/monetha/	0.04
766	Zano	ZANO	https://coinmarketcap.com//currencies/zano/	1.58
767	Emirex Token	EMRX	https://coinmarketcap.com//currencies/emirex-token/	0.61
768	Arcblock	ABT	https://coinmarketcap.com//currencies/arcblock/	0.17
769	BitForex Token	BF	https://coinmarketcap.com//currencies/bitforex-token/	0
770	Leverj Gluon	L2	https://coinmarketcap.com//currencies/leverj-gluon/	0.06
771	Public Mint	MINT	https://coinmarketcap.com//currencies/public-mint/	0.94
772	King DAG	KDAG	https://coinmarketcap.com//currencies/king-dag/	0.3
773	Folgory Coin	FLG	https://coinmarketcap.com//currencies/folgory-coin/	0.94
774	SOMESING	SSX	https://coinmarketcap.com//currencies/somesing/	0.11
775	Doki Doki Finance	DOKI	https://coinmarketcap.com//currencies/doki-doki-finance/	323.92
776	SingularDTV	SNGLS	https://coinmarketcap.com//currencies/singulardtv/	0.03
777	CoinEx Token	CET	https://coinmarketcap.com//currencies/coinex-token/	0.02
778	Bithao	BHAO	https://coinmarketcap.com//currencies/bithao/	0.35
779	SONM	SNM	https://coinmarketcap.com//currencies/sonm/	0.04
780	SpaceChain	SPC	https://coinmarketcap.com//currencies/spacechain/	0.05
781	Nestree	EGG	https://coinmarketcap.com//currencies/nestree/	0.01
783	Mirrored ProShares VIX	mVIXY	https://coinmarketcap.com//currencies/mirrored-proshares-vix-short-term-futures-etf/	10.9
784	GeoDB	GEO	https://coinmarketcap.com//currencies/geodb/	0.71
785	VIDY	VIDY	https://coinmarketcap.com//currencies/vidy/	0
786	VestChain	VEST	https://coinmarketcap.com//currencies/vestchain/	0
787	Tachyon Protocol	IPX	https://coinmarketcap.com//currencies/tachyon-protocol/	0.06
788	Achain	ACT	https://coinmarketcap.com//currencies/achain/	0.02
789	USDJ	USDJ	https://coinmarketcap.com//currencies/usdj/	1.03
790	Idle	IDLE	https://coinmarketcap.com//currencies/idle/	12.36
791	Atomic Wallet Coin	AWC	https://coinmarketcap.com//currencies/atomic-wallet-coin/	1.41
792	S4FE	S4F	https://coinmarketcap.com//currencies/s4fe/	0.02
793	Gem Exchange And Trading	GXT	https://coinmarketcap.com//currencies/gem-exchange-and-trading/	0.34
794	xRhodium	XRC	https://coinmarketcap.com//currencies/xrhodium/	11.96
795	Credits	CS	https://coinmarketcap.com//currencies/credits/	0.06
796	Juggernaut	JGN	https://coinmarketcap.com//currencies/juggernaut/	1.45
797	SnowSwap	SNOW	https://coinmarketcap.com//currencies/snowswap/	48.75
798	Hedget	HGET	https://coinmarketcap.com//currencies/hedget/	8.43
799	Swerve	SWRV	https://coinmarketcap.com//currencies/swerve/	1.29
800	CONTRACOIN	CTCN	https://coinmarketcap.com//currencies/contracoin/	0.46
801	MoonSwap	MOON	https://coinmarketcap.com//currencies/moonswap/	0.8766
802	SEEN	SEEN	https://coinmarketcap.com//currencies/seen/	19.56
803	Dracula Token	DRC	https://coinmarketcap.com//currencies/dracula-token/	1.01
804	ProximaX	XPX	https://coinmarketcap.com//currencies/proximax/	0.002312
805	CargoX	CXO	https://coinmarketcap.com//currencies/cargox/	0.08511
806	Genaro Network	GNX	https://coinmarketcap.com//currencies/genaro-network/	0.05157
807	Falcon Project	FNT	https://coinmarketcap.com//currencies/falcon-project/	0.003498
808	Credits	CS	https://coinmarketcap.com//currencies/credits/	0.06682
809	TrueFeedBack	TFB	https://coinmarketcap.com//currencies/truefeedback/	0.005918
810	IQeon	IQN	https://coinmarketcap.com//currencies/iqeon/	2.54
811	Restart Energy MWAT	MWAT	https://coinmarketcap.com//currencies/restart-energy-mwat/	0.03
812	Hydro Protocol	HOT	https://coinmarketcap.com//currencies/hydro-protocol/	0.02
813	Fuse Network	FUSE	https://coinmarketcap.com//currencies/fuse-network/	0.25
814	Atletico De Madrid Fan Token	ATM	https://coinmarketcap.com//currencies/atletico-de-madrid-fan-token/	11.02
815	BaaSid	BAAS	https://coinmarketcap.com//currencies/baasid/	0
816	LiquidApps	DAPP	https://coinmarketcap.com//currencies/liquid-apps/	0.02
817	Poolz Finance	POOLZ	https://coinmarketcap.com//currencies/poolz-finance/	19.79
818	Jupiter	JUP	https://coinmarketcap.com//currencies/jupiter/	0.11
819	Berry Data	BRY	https://coinmarketcap.com//currencies/berry-data/	6.7
820	Unido EP	UDO	https://coinmarketcap.com//currencies/unido/	0.57
821	Abyss	ABYSS	https://coinmarketcap.com//currencies/abyss/	0.06
822	BHPCoin	BHP	https://coinmarketcap.com//currencies/bhp-coin/	0.66
823	Xensor	XSR	https://coinmarketcap.com//currencies/xensor/	0
824	Beowulf	BWF	https://coinmarketcap.com//currencies/beowulf/	0.01
825	Blockzero Labs	XIO	https://coinmarketcap.com//currencies/blockzerolabs/	0.4
826	Phore	PHR	https://coinmarketcap.com//currencies/phore/	0.56
827	ARMOR	ARMOR	https://coinmarketcap.com//currencies/armor/	0.63
828	TrustVerse	TRV	https://coinmarketcap.com//currencies/trustverse/	0.03
829	Geeq	GEEQ	https://coinmarketcap.com//currencies/geeq/	1.54
830	Peercoin	PPC	https://coinmarketcap.com//currencies/peercoin/	0.48
831	Dawn Protocol	DAWN	https://coinmarketcap.com//currencies/dawn-protocol/	0.28
832	AS Roma Fan Token	ASR	https://coinmarketcap.com//currencies/as-roma-fan-token/	10.45
833	Circuits of Value	COVAL	https://coinmarketcap.com//currencies/circuits-of-value/	0.01
834	BIKI	BIKI	https://coinmarketcap.com//currencies/biki/	0.04
835	Aluna.Social	ALN	https://coinmarketcap.com//currencies/aluna-social/	1.16
836	Casino Betting Coin	CBC	https://coinmarketcap.com//currencies/casino-betting-coin/	0.08
837	Alchemy Pay	ACH	https://coinmarketcap.com//currencies/alchemy-pay/	0.01
838	Emercoin	EMC	https://coinmarketcap.com//currencies/emercoin/	0.26
839	reflect.finance	RFI	https://coinmarketcap.com//currencies/reflect-finance/	1.29
840	KeeperDAO	ROOK	https://coinmarketcap.com//currencies/keeperdao/	335.31
841	LinkEye	LET	https://coinmarketcap.com//currencies/linkeye/	0.01
842	Waifu Token	WAIF	https://coinmarketcap.com//currencies/waifu-token/	0.01
843	PolkaBridge	PBR	https://coinmarketcap.com//currencies/polkabridge/	0.38
844	Parachute	PAR	https://coinmarketcap.com//currencies/parachute/	0.02
845	High Performance Blockchain	HPB	https://coinmarketcap.com//currencies/high-performance-blockchain/	0.17
846	Defis Network	DFS	https://coinmarketcap.com//currencies/defis-network/	32.72
847	SENSO	SENSO	https://coinmarketcap.com//currencies/senso/	0.98
848	Banano	BAN	https://coinmarketcap.com//currencies/banano/	0.01
849	Santiment Network Token	SAN	https://coinmarketcap.com//currencies/santiment/	0.19
850	Kcash	KCASH	https://coinmarketcap.com//currencies/kcash/	0.03
851	OG Fan Token	OG	https://coinmarketcap.com//currencies/og-fan-token/	9.37
852	OIN Finance	OIN	https://coinmarketcap.com//currencies/oin-finance/	0.47
853	Nord Finance	NORD	https://coinmarketcap.com//currencies/nord-finance/	13.1
854	Aragon Court	ANJ	https://coinmarketcap.com//currencies/aragon-court/	0.1
855	AMEPAY	AME	https://coinmarketcap.com//currencies/amepay/	0.05
856	Venus DOT	vDOT	https://coinmarketcap.com//currencies/venus-dot/	0.7
857	XYO	XYO	https://coinmarketcap.com//currencies/xyo/	0
858	Furucombo	COMBO	https://coinmarketcap.com//currencies/furucombo/	2.52
859	Zynecoin	ZYN	https://coinmarketcap.com//currencies/zynecoin/	0.52
860	FLETA	FLETA	https://coinmarketcap.com//currencies/fleta/	0.01
861	NIX	NIX	https://coinmarketcap.com//currencies/nix/	0.23
862	SwftCoin	SWFTC	https://coinmarketcap.com//currencies/swftcoin/	0
863	PLATINCOIN	PLC	https://coinmarketcap.com//currencies/platincoin/	2.29
864	SmartMesh	SMT	https://coinmarketcap.com//currencies/smartmesh/	0.01
865	Mettalex	MTLX	https://coinmarketcap.com//currencies/mettalex/	9.96
866	Darwinia Commitment Token	KTON	https://coinmarketcap.com//currencies/darwinia-commitment-token/	232.01
867	Callisto Network	CLO	https://coinmarketcap.com//currencies/callisto-network/	0
868	Monavale	MONA	https://coinmarketcap.com//currencies/monavale/	1845.6
869	DePay	DEPAY	https://coinmarketcap.com//currencies/depay/	3.62
870	PlatonCoin	PLTC	https://coinmarketcap.com//currencies/platoncoin/	0.43
871	DigitalNote	XDN	https://coinmarketcap.com//currencies/digitalnote/	0
872	EOS Force	EOSC	https://coinmarketcap.com//currencies/eos-force/	0.01
873	Realio Network	RIO	https://coinmarketcap.com//currencies/realio-network/	1.65
874	U Network	UUU	https://coinmarketcap.com//currencies/u-network/	0
875	DeFi Yield Protocol	DYP	https://coinmarketcap.com//currencies/defi-yield-protocol/	3.27
876	Monolith	TKN	https://coinmarketcap.com//currencies/monolith/	0.32
877	APYSwap	APYS	https://coinmarketcap.com//currencies/apyswap/	1.9
878	IoT Chain	ITC	https://coinmarketcap.com//currencies/iot-chain/	0.13
879	APIX	APIX	https://coinmarketcap.com//currencies/apix/	0.09
880	PressOne	PRS	https://coinmarketcap.com//currencies/pressone/	0.04
881	DragonVein	DVC	https://coinmarketcap.com//currencies/dragonvein/	0.07
882	Name Changing Token	NCT	https://coinmarketcap.com//currencies/name-changing-token/	0.28
883	Offshift	XFT	https://coinmarketcap.com//currencies/offshift/	4.05
884	Archer DAO Governance Token	ARCH	https://coinmarketcap.com//currencies/archer-dao-governance-token/	1.3
885	SharedStake	SGT	https://coinmarketcap.com//currencies/sharedstake/	70.34
886	Valor Token	VALOR	https://coinmarketcap.com//currencies/valor-token/	0.52
887	Levolution	LEVL	https://coinmarketcap.com//currencies/levolution/	0.13
888	UniMex Network	UMX	https://coinmarketcap.com//currencies/unimex-network/	1.86
889	YF Link	YFL	https://coinmarketcap.com//currencies/yflink/	204.92
890	TERA	TERA	https://coinmarketcap.com//currencies/tera/	0.01
891	Hakka.Finance	HAKKA	https://coinmarketcap.com//currencies/hakka-finance/	0.07
892	SIX	SIX	https://coinmarketcap.com//currencies/six/	0.04
893	Dev Protocol	DEV	https://coinmarketcap.com//currencies/dev-protocol/	9.43
894	ROOBEE	ROOBEE	https://coinmarketcap.com//currencies/roobee/	0
895	Airbloc	ABL	https://coinmarketcap.com//currencies/airbloc/	0.04
896	Anchor	ANCT	https://coinmarketcap.com//currencies/anchor/	0.8
897	Tap	XTP	https://coinmarketcap.com//currencies/tap/	0.01
898	BABB	BAX	https://coinmarketcap.com//currencies/babb/	0
899	ZBG Token	ZT	https://coinmarketcap.com//currencies/zbg-token/	0.04
900	FNB Protocol	FNB	https://coinmarketcap.com//currencies/fnb-protocol/	0
901	yieldwatch	WATCH	https://coinmarketcap.com//currencies/yieldwatch/	0.9731
902	Multi Channel Influencer Creater Cloud Funding Platform	MCI	https://coinmarketcap.com//currencies/multi-channel-influencer-creater-cloud-funding-platform/	0.01764
903	Eminer	EM	https://coinmarketcap.com//currencies/eminer/	0.006899
904	FIBOS	FO	https://coinmarketcap.com//currencies/fibos/	0.009319
905	apM Coin	APM	https://coinmarketcap.com//currencies/apm-coin/	0.02744
906	MahaDAO	MAHA	https://coinmarketcap.com//currencies/mahadao/	8.54
907	ASKO	ASKO	https://coinmarketcap.com//currencies/askobar-network/	0.08405
908	Validity	VAL	https://coinmarketcap.com//currencies/validity/	2.32
909	DAOstack	GEN	https://coinmarketcap.com//currencies/daostack/	0.2019
910	Strong	STRONG	https://coinmarketcap.com//currencies/strong/	70.81
911	BeatzCoin	BTZC	https://coinmarketcap.com//currencies/beatzcoin/	0
912	Crypto Village Accelerator	CVA	https://coinmarketcap.com//currencies/crypto-village-accelerator/	0.1
913	FLO	FLO	https://coinmarketcap.com//currencies/flo/	0.06
914	Odyssey	OCN	https://coinmarketcap.com//currencies/odyssey/	0
915	FinNexus	FNX	https://coinmarketcap.com//currencies/finnexus/	0.37
916	DMScript	DMST	https://coinmarketcap.com//currencies/dmscript/	0.17
917	Royale Finance	ROYA	https://coinmarketcap.com//currencies/royale-finance/	0.95
918	Unitrade	TRADE	https://coinmarketcap.com//currencies/unitrade/	0.35
919	Time New Bank	TNB	https://coinmarketcap.com//currencies/time-new-bank/	0
920	Seele-N	SEELE	https://coinmarketcap.com//currencies/seele/	0.01
921	Aitra	AITRA	https://coinmarketcap.com//currencies/aitra/	6.33
922	Atlas Protocol	ATP	https://coinmarketcap.com//currencies/atlas-protocol/	0
923	Zel	ZEL	https://coinmarketcap.com//currencies/zel/	0.08
924	Bibox Token	BIX	https://coinmarketcap.com//currencies/bibox-token/	0.11
925	TriumphX	TRIX	https://coinmarketcap.com//currencies/triumphx/	0.75
926	XMax	XMX	https://coinmarketcap.com//currencies/xmax/	0
927	Insured Finance	INFI	https://coinmarketcap.com//currencies/insured-finance/	0.65
928	bAlpha	BALPHA	https://coinmarketcap.com//currencies/balpha/	1591.62
929	Fyooz	FYZ	https://coinmarketcap.com//currencies/fyooz/	1.53
930	Node Runners	NDR	https://coinmarketcap.com//currencies/node-runners/	395.9
931	Hyve	HYVE	https://coinmarketcap.com//currencies/hyve/	0.36
932	VIBE	VIBE	https://coinmarketcap.com//currencies/vibe/	0.03
933	Smart MFG	MFG	https://coinmarketcap.com//currencies/smart-mfg/	0.03
934	Lead Wallet	LEAD	https://coinmarketcap.com//currencies/lead-wallet/	0.02
935	smARTOFGIVING	AOG	https://coinmarketcap.com//currencies/smartofgiving/	0.12
936	Tokenomy	TEN	https://coinmarketcap.com//currencies/tokenomy/	0.04
937	FREE Coin	FREE	https://coinmarketcap.com//currencies/free-coin/	0
938	Pluton	PLU	https://coinmarketcap.com//currencies/pluton/	10.41
939	Insureum	ISR	https://coinmarketcap.com//currencies/insureum/	0.04
940	Pundi X NEM	NPXSXEM	https://coinmarketcap.com//currencies/pundi-x-nem/	0
941	Waves Enterprise	WEST	https://coinmarketcap.com//currencies/waves-enterprise/	0.21
942	Nsure.Network	NSURE	https://coinmarketcap.com//currencies/nsure-network/	1.54
943	SmartCash	SMART	https://coinmarketcap.com//currencies/smartcash/	0.01
944	Falconswap	FSW	https://coinmarketcap.com//currencies/fsw-token/	0.23
945	PlayFuel	PLF	https://coinmarketcap.com//currencies/playfuel/	0.02
946	NaPoleonX	NPX	https://coinmarketcap.com//currencies/napoleonx/	0.34
947	Primecoin	XPM	https://coinmarketcap.com//currencies/primecoin/	0.24
948	Particl	PART	https://coinmarketcap.com//currencies/particl/	0.88
949	Tokes	TKS	https://coinmarketcap.com//currencies/tokes/	0.04
950	Open Platform	OPEN	https://coinmarketcap.com//currencies/open-platform/	0.01
951	UCA Coin	UCA	https://coinmarketcap.com//currencies/uca-coin/	0.01
952	Lightning Bitcoin	LBTC	https://coinmarketcap.com//currencies/lightning-bitcoin/	2.17
953	Zenfuse	ZEFU	https://coinmarketcap.com//currencies/zenfuse/	0.15
954	Chrono.tech	TIME	https://coinmarketcap.com//currencies/chrono-tech/	11.72
955	Swapcoinz	SPAZ	https://coinmarketcap.com//currencies/swapcoinz/	0.78
956	Burst	BURST	https://coinmarketcap.com//currencies/burst/	0
957	Aeon	AEON	https://coinmarketcap.com//currencies/aeon/	0.52
958	Azuki	AZUKI	https://coinmarketcap.com//currencies/azuki/	0.95
959	Monero Classic	XMC	https://coinmarketcap.com//currencies/monero-classic/	0.42
960	STATERA	STA	https://coinmarketcap.com//currencies/statera/	0.1
961	Peculium	PCL	https://coinmarketcap.com//currencies/peculium/	0
962	Caspian	CSP	https://coinmarketcap.com//currencies/caspian/	0.02
963	WeShow Token	WET	https://coinmarketcap.com//currencies/weshow-token/	0.01
964	Enecuum	ENQ	https://coinmarketcap.com//currencies/enecuum/	0.05
965	Evedo	EVED	https://coinmarketcap.com//currencies/evedo/	0.55
966	Precium	PCM	https://coinmarketcap.com//currencies/precium/	0.01
967	HitChain	HIT	https://coinmarketcap.com//currencies/hitchain/	0
968	MultiVAC	MTV	https://coinmarketcap.com//currencies/multivac/	0
969	QuickX Protocol	QCX	https://coinmarketcap.com//currencies/quickx-protocol/	0.02
970	Grid+	GRID	https://coinmarketcap.com//currencies/grid/	0.2
971	Wings	WINGS	https://coinmarketcap.com//currencies/wings/	0.08
972	DEAPcoin	DEP	https://coinmarketcap.com//currencies/deapcoin/	0.01
973	PlotX	PLOT	https://coinmarketcap.com//currencies/plotx/	0.22
974	PieDAO DOUGH v2	DOUGH	https://coinmarketcap.com//currencies/piedao-dough-v2/	1.14
975	GoCrypto Token	GOC	https://coinmarketcap.com//currencies/gocrypto-token/	0.04
976	Sylo	SYLO	https://coinmarketcap.com//currencies/sylo/	0
977	QuadrantProtocol	EQUAD	https://coinmarketcap.com//currencies/quadrantprotocol/	0.02
978	Team Heretics Fan Token	TH	https://coinmarketcap.com//currencies/team-heretics-fan-token/	17.89
979	Project WITH	WIKEN	https://coinmarketcap.com//currencies/project-with/	0.02
980	Growth DeFi	GRO	https://coinmarketcap.com//currencies/growthdefi/	21.19
981	Mysterium	MYST	https://coinmarketcap.com//currencies/mysterium/	0.38
982	Xaya	CHI	https://coinmarketcap.com//currencies/xaya/	0.16
983	Auctus	AUC	https://coinmarketcap.com//currencies/auctus/	0.23
984	Feathercoin	FTC	https://coinmarketcap.com//currencies/feathercoin/	0.03
985	Covesting	COV	https://coinmarketcap.com//currencies/covesting/	0.41
986	MicroBitcoin	MBC	https://coinmarketcap.com//currencies/microbitcoin/	0
987	LikeCoin	LIKE	https://coinmarketcap.com//currencies/likecoin/	0.01
988	Pillar	PLR	https://coinmarketcap.com//currencies/pillar/	0.03
989	BTSE	BTSE	https://coinmarketcap.com//currencies/btse/	1.66
990	Origin Dollar	OUSD	https://coinmarketcap.com//currencies/origin-dollar/	0.99
991	Ethverse	ETHV	https://coinmarketcap.com//currencies/ethverse/	0.51
992	Matrix AI Network	MAN	https://coinmarketcap.com//currencies/matrix-ai-network/	0.03
993	HAPI	HAPI	https://coinmarketcap.com//currencies/hapi-one/	91.19
994	Lamden	TAU	https://coinmarketcap.com//currencies/lamden/	0.05
995	Gulden	NLG	https://coinmarketcap.com//currencies/gulden/	0.01
996	e-Gulden	EFL	https://coinmarketcap.com//currencies/e-gulden/	0.42
997	VNX Exchange	VNXLU	https://coinmarketcap.com//currencies/vnx-exchange/	0.25
998	ScPrime	SCP	https://coinmarketcap.com//currencies/scprime/	0.21
999	BitMart Token	BMX	https://coinmarketcap.com//currencies/bitmart-token/	0.04
1000	OneLedger	OLT	https://coinmarketcap.com//currencies/oneledger/	0.02
1001	Orient Walt	HTDF	https://coinmarketcap.com//currencies/orient-walt/	0.07446
1002	Jobchain	JOB	https://coinmarketcap.com//currencies/jobchain/	0.001097
1003	Myriad	XMY	https://coinmarketcap.com//currencies/myriad/	0.003892
1004	Cryptocean	CRON	https://coinmarketcap.com//currencies/cryptocean/	0.8401
1005	Kambria	KAT	https://coinmarketcap.com//currencies/kambria/	0.002953
1006	CWV Chain	CWV	https://coinmarketcap.com//currencies/cwv-chain/	0.001932
1007	Skrumble Network	SKM	https://coinmarketcap.com//currencies/skrumble-network/	0.006688
1008	Idena	IDNA	https://coinmarketcap.com//currencies/idena/	0.1763
1009	TurtleCoin	TRTL	https://coinmarketcap.com//currencies/turtlecoin/	6.985e-05
1010	IG Gold	IGG	https://coinmarketcap.com//currencies/ig-gold/	0.0007588
1011	PotCoin	POT	https://coinmarketcap.com//currencies/potcoin/	0.03
1012	Litecoin Cash	LCC	https://coinmarketcap.com//currencies/litecoin-cash/	0.01
1013	EDUCare	EKT	https://coinmarketcap.com//currencies/educare/	0.01
1014	Fatcoin	FAT	https://coinmarketcap.com//currencies/fatcoin/	0.06
1015	SOAR.FI	SOAR	https://coinmarketcap.com//currencies/soar-fi/	0.75
1016	Blockchain Certified Data Token	BCDT	https://coinmarketcap.com//currencies/blockchain-certified-data-token/	0.17
1017	Safe	SAFE	https://coinmarketcap.com//currencies/safe/	0.31
1018	Opacity	OPCT	https://coinmarketcap.com//currencies/opacity/	0.08
1019	Penta	PNT	https://coinmarketcap.com//currencies/penta/	0
1020	Game.com	GTC	https://coinmarketcap.com//currencies/game/	0.01
1021	Router Protocol	ROUTE	https://coinmarketcap.com//currencies/router-protocol/	3.73
1022	SmartCredit Token	SMARTCREDIT	https://coinmarketcap.com//currencies/smartcredit-token/	4.73
1023	Stealth	XST	https://coinmarketcap.com//currencies/stealth/	0.16
1024	Chonk	CHONK	https://coinmarketcap.com//currencies/chonk/	159.01
1025	BIDR	BIDR	https://coinmarketcap.com//currencies/binance-idr/	0
1026	SunContract	SNC	https://coinmarketcap.com//currencies/suncontract/	0.05
1027	HyperDAO	HDAO	https://coinmarketcap.com//currencies/hyperdao/	0.02
1028	ZIMBOCASH	ZASH	https://coinmarketcap.com//currencies/zimbocash/	0.01
1029	Apollon Limassol	APL	https://coinmarketcap.com//currencies/apollon-limassol/	24.03
1030	Urus	URUS	https://coinmarketcap.com//currencies/urus/	12.65
1031	DEX	DEX	https://coinmarketcap.com//currencies/dex/	0.03
1032	Tripio	TRIO	https://coinmarketcap.com//currencies/tripio/	0
1033	ChatCoin	CHAT	https://coinmarketcap.com//currencies/chatcoin/	0.01
1034	YOU COIN	YOU	https://coinmarketcap.com//currencies/you-coin/	0.01
1035	Iconic Token	ICNQ	https://coinmarketcap.com//currencies/iconic-token/	0.71
1036	MAP Protocol	MAP	https://coinmarketcap.com//currencies/map-protocol/	0.02
1037	HTMLCOIN	HTML	https://coinmarketcap.com//currencies/html-coin/	0
1038	HollyGold	HGOLD	https://coinmarketcap.com//currencies/hollygold/	2.23
1039	Add.xyz	PLT	https://coinmarketcap.com//currencies/add-xyz/	1
1040	Pepemon Pepeballs	PPBLZ	https://coinmarketcap.com//currencies/pepemon-pepeballs/	402.01
1041	Litex	LXT	https://coinmarketcap.com//currencies/litex/	0
1042	Lido DAO Token	LDO	https://coinmarketcap.com//currencies/lido-dao/	0.63
1043	Venus DAI	vDAI	https://coinmarketcap.com//currencies/venus-dai/	0.02
1044	Mobius	MOBI	https://coinmarketcap.com//currencies/mobius/	0.01
1045	Filecash	FIC	https://coinmarketcap.com//currencies/filecash/	0.21
1046	Blocery	BLY	https://coinmarketcap.com//currencies/blocery/	0.1
1047	Cred	LBA	https://coinmarketcap.com//currencies/libra-credit/	0.01
1048	CPChain	CPC	https://coinmarketcap.com//currencies/cpchain/	0.01
1049	Origo	OGO	https://coinmarketcap.com//currencies/origo/	0.01
1050	Gourmet Galaxy	GUM	https://coinmarketcap.com//currencies/gourmet-galaxy/	1.6
1051	Valobit	VBIT	https://coinmarketcap.com//currencies/valobit/	0.01
1052	Raven Protocol	RAVEN	https://coinmarketcap.com//currencies/raven-protocol/	0
1053	Insights Network	INSTAR	https://coinmarketcap.com//currencies/insights-network/	0.03
1054	Global Social Chain	GSC	https://coinmarketcap.com//currencies/global-social-chain/	0.01
1055	BOLT	BOLT	https://coinmarketcap.com//currencies/bolt/	0.01
1056	XMON	XMON	https://coinmarketcap.com//currencies/xmon/	3494.86
1057	ILCOIN	ILC	https://coinmarketcap.com//currencies/ilcoin/	0.01
1058	42-coin	42	https://coinmarketcap.com//currencies/42-coin/	122325.69
1059	Vidya	VIDYA	https://coinmarketcap.com//currencies/vidya/	0.14
1060	Jarvis Network	JRT	https://coinmarketcap.com//currencies/jarvis-network/	0.18
1061	Lunes	LUNES	https://coinmarketcap.com//currencies/lunes/	0.03
1062	Open Governance Token	OPEN	https://coinmarketcap.com//currencies/open-governance-token/	1.4
1063	VeriDocGlobal	VDG	https://coinmarketcap.com//currencies/veridocglobal/	0
1064	Remme	REM	https://coinmarketcap.com//currencies/remme/	0.01
1065	SolarCoin	SLR	https://coinmarketcap.com//currencies/solarcoin/	0.08
1066	OTOCASH	OTO	https://coinmarketcap.com//currencies/otocash/	0.14
1067	Swarm	SWM	https://coinmarketcap.com//currencies/swarm-network/	0.06
1068	PANTHEON X	XPN	https://coinmarketcap.com//currencies/pantheon-x/	0.01
1069	Indexed Finance	NDX	https://coinmarketcap.com//currencies/indexed-finance/	5.77
1070	Scala	XLA	https://coinmarketcap.com//currencies/scala/	0
1071	INT	INT	https://coinmarketcap.com//currencies/int-chain/	0.01
1072	Earneo	RNO	https://coinmarketcap.com//currencies/earneo/	0.02
1073	Stake DAO	SDT	https://coinmarketcap.com//currencies/stake-dao/	6.85
1074	DECOIN	DTEP	https://coinmarketcap.com//currencies/decoin/	0.09
1075	MIR COIN	MIR	https://coinmarketcap.com//currencies/mir-coin/	0.01
1076	Internxt	INXT	https://coinmarketcap.com//currencies/internxt/	7.63
1077	Footballcoin	XFC	https://coinmarketcap.com//currencies/footballcoin/	0.01
1078	Content Neutrality Network	CNN	https://coinmarketcap.com//currencies/content-neutrality-network/	0
1079	Digitex	DGTX	https://coinmarketcap.com//currencies/digitex/	0.01
1080	Nyzo	NYZO	https://coinmarketcap.com//currencies/nyzo/	0.4
1081	Panda Yield	BBOO	https://coinmarketcap.com//currencies/panda-yield/	5.07
1082	DeFiner	FIN	https://coinmarketcap.com//currencies/definer/	0.42
1083	SaTT	SATT	https://coinmarketcap.com//currencies/satt/	0
1084	GridCoin	GRC	https://coinmarketcap.com//currencies/gridcoin/	0.01
1085	Cubiex	CBIX	https://coinmarketcap.com//currencies/cubiex/	0.07
1086	FairCoin	FAIR	https://coinmarketcap.com//currencies/faircoin/	0.09
1087	FedoraCoin	TIPS	https://coinmarketcap.com//currencies/fedoracoin/	0
1088	BIZZCOIN	BIZZ	https://coinmarketcap.com//currencies/bizzcoin/	0.63
1089	SignatureChain	SIGN	https://coinmarketcap.com//currencies/signature-chain/	0.01
1090	Vexanium	VEX	https://coinmarketcap.com//currencies/vexanium/	0.01
1091	Polis	POLIS	https://coinmarketcap.com//currencies/polis/	0.46
1092	WeOwn	CHX	https://coinmarketcap.com//currencies/we-own/	0.05
1093	TENT	TENT	https://coinmarketcap.com//currencies/tent/	0.14
1094	Beaxy	BXY	https://coinmarketcap.com//currencies/beaxy/	0.02
1095	YGGDRASH	YEED	https://coinmarketcap.com//currencies/yeed/	0
1096	NextDAO	NAX	https://coinmarketcap.com//currencies/nextdao/	0
1097	Swace	SWACE	https://coinmarketcap.com//currencies/swace/	0.01
1098	Acute Angle Cloud	AAC	https://coinmarketcap.com//currencies/acute-angle-cloud/	0.02
1099	BitGreen	BITG	https://coinmarketcap.com//currencies/bitgreen/	0.37
1100	Cryptopay	CPAY	https://coinmarketcap.com//currencies/cryptopay/	0.06
1101	1World	1WO	https://coinmarketcap.com//currencies/1world/	0.1184
1102	Linker Coin	LNC	https://coinmarketcap.com//currencies/linker-coin/	0.03895
1103	Pantos	PAN	https://coinmarketcap.com//currencies/pantos/	0.1542
1104	Chi Gastoken	CHI	https://coinmarketcap.com//currencies/chi-gastoken/	5.08
1105	OceanEx Token	OCE	https://coinmarketcap.com//currencies/oceanex-token/	0.002424
1106	bitCNY	BITCNY	https://coinmarketcap.com//currencies/bitcny/	0.1533
1107	Alpaca Finance	ALPACA	https://coinmarketcap.com//currencies/alpaca-finance/	0.6516
1108	fyeth.finance	YETH	https://coinmarketcap.com//currencies/fyeth-finance/	7.72
1109	ERC20	ERC20	https://coinmarketcap.com//currencies/erc20/	0.00378
1110	Digix Gold Token	DGX	https://coinmarketcap.com//currencies/digix-gold-token/	55.53
1111	Pizza	PIZZA	https://coinmarketcap.com//currencies/pizza/	0.14
1112	Kebab Token	KEBAB	https://coinmarketcap.com//currencies/kebab-token/	3.91
1113	GAMB	GMB	https://coinmarketcap.com//currencies/gamb/	0
1114	ContentBox	BOX	https://coinmarketcap.com//currencies/contentbox/	0
1115	Crown	CRW	https://coinmarketcap.com//currencies/crown/	0.16
1116	Thisoption	TONS	https://coinmarketcap.com//currencies/thisoption/	0.75
1117	Bittwatt	BWT	https://coinmarketcap.com//currencies/bittwatt/	0.01
1118	UGAS	UGAS	https://coinmarketcap.com//currencies/ugas/	0.02
1119	Netbox Coin	NBX	https://coinmarketcap.com//currencies/netbox-coin/	0.05
1120	Xiotri	XIOT	https://coinmarketcap.com//currencies/xiotri/	914.47
1121	adbank	ADB	https://coinmarketcap.com//currencies/adbank/	0
1122	Carbon	CRBN	https://coinmarketcap.com//currencies/carbon/	0.16
1123	Machi X	MCX	https://coinmarketcap.com//currencies/machix/	0.01
1124	CHADS VC	CHADS	https://coinmarketcap.com//currencies/chads-vc/	0.1
1125	Small Love Potion	SLP	https://coinmarketcap.com//currencies/small-love-potion/	0.09
1126	Ideaology	IDEA	https://coinmarketcap.com//currencies/ideaology/	0.32
1127	MASQ	MASQ	https://coinmarketcap.com//currencies/masq/	0.26
1128	BlackCoin	BLK	https://coinmarketcap.com//currencies/blackcoin/	0.06
1129	Cajutel	CAJ	https://coinmarketcap.com//currencies/cajutel/	2.87
1130	Essentia	ESS	https://coinmarketcap.com//currencies/essentia/	0
1131	Club Atletico Independiente	CAI	https://coinmarketcap.com//currencies/club-atletico-independiente/	7.01
1132	TronEuropeRewardCoin	TERC	https://coinmarketcap.com//currencies/troneuroperewardcoin/	0.02
1133	AIDUS TOKEN	AIDUS	https://coinmarketcap.com//currencies/aidus-token/	0.01
1134	ALLY	ALY	https://coinmarketcap.com//currencies/ally/	0
1135	QunQun	QUN	https://coinmarketcap.com//currencies/qunqun/	0.01
1136	YEE	YEE	https://coinmarketcap.com//currencies/yee/	0
1137	Datum	DAT	https://coinmarketcap.com//currencies/datum/	0
1138	Peerplays	PPY	https://coinmarketcap.com//currencies/peerplays-ppy/	0.84
1139	REPO	REPO	https://coinmarketcap.com//currencies/repo/	0.17
1140	HalalChain	HLC	https://coinmarketcap.com//currencies/halalchain/	0.01
1141	Rupiah Token	IDRT	https://coinmarketcap.com//currencies/rupiah-token/	0
1142	AstroTools	ASTRO	https://coinmarketcap.com//currencies/astrotools/	1.25
1143	EXRNchain	EXRN	https://coinmarketcap.com//currencies/exrnchain/	0
1144	GHOST	GHOST	https://coinmarketcap.com//currencies/ghost/	0.25
1145	DMM: Governance	DMG	https://coinmarketcap.com//currencies/dmm-governance/	0.07
1146	BiFi	BIFI	https://coinmarketcap.com//currencies/bifi/	0.04
1147	OKCash	OK	https://coinmarketcap.com//currencies/okcash/	0.05
1148	Coin Artist	COIN	https://coinmarketcap.com//currencies/coin-artist/	3.44
1149	Pigeoncoin	PGN	https://coinmarketcap.com//currencies/pigeoncoin/	0
1150	PhoenixDAO	PHNX	https://coinmarketcap.com//currencies/phoenixdao/	0.06
1151	Qitmeer	PMEER	https://coinmarketcap.com//currencies/qitmeer/	0.02
1152	Hyperion	HYN	https://coinmarketcap.com//currencies/hyperion/	0.02
1153	Blackmoon	BMC	https://coinmarketcap.com//currencies/blackmoon/	0.07
1154	Playcent	PCNT	https://coinmarketcap.com//currencies/playcent/	0.51
1155	DAEX	DAX	https://coinmarketcap.com//currencies/daex/	0.01
1156	Omni	OMNI	https://coinmarketcap.com//currencies/omni/	6.45
1157	Pivot Token	PVT	https://coinmarketcap.com//currencies/pivot-token/	0
1158	BackPacker Coin	BPC	https://coinmarketcap.com//currencies/backpacker-coin/	0.57
1159	Amon	AMN	https://coinmarketcap.com//currencies/amon/	0.01
1160	Ravencoin Classic	RVC	https://coinmarketcap.com//currencies/ravencoin-classic/	0
1161	Thugs Finance	THUGS	https://coinmarketcap.com//currencies/thugs-fi/	5.33
1162	GAPS	GAP	https://coinmarketcap.com//currencies/gaps/	0.35
1163	Seigniorage Shares	SHARE	https://coinmarketcap.com//currencies/seigniorage-shares/	0.17
1164	Catex Token	CATT	https://coinmarketcap.com//currencies/catex-token/	0
1165	MoonTools	MOONS	https://coinmarketcap.com//currencies/moontools/	127.74
1166	CoTrader	COT	https://coinmarketcap.com//currencies/cotrader/	0
1167	HOMIHELP	HOMI	https://coinmarketcap.com//currencies/homihelp/	2.97
1168	Noku	NOKU	https://coinmarketcap.com//currencies/noku/	0.11
1169	BLink	BLINK	https://coinmarketcap.com//currencies/blink/	0.02
1170	Tierion	TNT	https://coinmarketcap.com//currencies/tierion/	0.01
1171	KuboCoin	KUBO	https://coinmarketcap.com//currencies/kubocoin/	0
1172	Pinkcoin	PINK	https://coinmarketcap.com//currencies/pinkcoin/	0.01
1173	xBTC	xBTC	https://coinmarketcap.com//currencies/xbtc/	0.58
1174	Sumokoin	SUMO	https://coinmarketcap.com//currencies/sumokoin/	0.12
1175	AXPR	AXPR	https://coinmarketcap.com//currencies/axpr-token/	0.01
1176	BarterTrade	BART	https://coinmarketcap.com//currencies/bartertrade/	0.05
1177	CEEK VR	CEEK	https://coinmarketcap.com//currencies/ceek-vr/	0
1178	Minereum	MNE	https://coinmarketcap.com//currencies/minereum/	0.28
1179	Folder Protocol	FOL	https://coinmarketcap.com//currencies/folder-protocol/	0.91
1180	xSigma	SIG	https://coinmarketcap.com//currencies/xsigma/	3.47
1181	Phantasma Energy	KCAL	https://coinmarketcap.com//currencies/phantasma-energy/	0.08
1182	Vid	VI	https://coinmarketcap.com//currencies/vid/	0.13
1183	MenaPay	MPAY	https://coinmarketcap.com//currencies/menapay/	0.02
1184	Graviocoin	GIO	https://coinmarketcap.com//currencies/graviocoin/	0.04
1185	VNT Chain	VNT	https://coinmarketcap.com//currencies/vnt-chain/	0
1186	Bitswift	BITS	https://coinmarketcap.com//currencies/bitswift/	0.8
1187	NFTLootBox	LOOT	https://coinmarketcap.com//currencies/nftlootbox/	136.12
1188	Enigma	ENG	https://coinmarketcap.com//currencies/enigma/	0.22
1189	OneRoot Network	RNT	https://coinmarketcap.com//currencies/oneroot-network/	0.01
1190	WPP TOKEN	WPP	https://coinmarketcap.com//currencies/wpp-token/	0
1191	KanadeCoin	KNDC	https://coinmarketcap.com//currencies/kanadecoin/	0
1192	AMLT	AMLT	https://coinmarketcap.com//currencies/amlt/	0.01
1193	Pascal	PASC	https://coinmarketcap.com//currencies/pascal/	0.09
1194	CryptoPing	PING	https://coinmarketcap.com//currencies/cryptoping/	0.41
1195	4THPILLAR TECHNOLOGIES	FOUR	https://coinmarketcap.com//currencies/4thpillar-technologies/	0.02
1196	Breezecoin	BRZE	https://coinmarketcap.com//currencies/breezecoin/	0.03
1197	Heart Number	HTN	https://coinmarketcap.com//currencies/heartnumber/	0
1198	NativeCoin	N8V	https://coinmarketcap.com//currencies/native-coin/	0.12
1199	HashNet BitEco	HNB	https://coinmarketcap.com//currencies/hashnet-biteco/	0.1
1200	VINchain	VIN	https://coinmarketcap.com//currencies/vinchain/	0
1201	hybrix	HY	https://coinmarketcap.com//currencies/hybrix/	1.25
1202	Golden Goose	GOLD	https://coinmarketcap.com//currencies/golden-goose/	0.01948
1203	Meter Governance	MTRG	https://coinmarketcap.com//currencies/meter-governance/	1.24
1204	Cube	AUTO	https://coinmarketcap.com//currencies/cube/	0.0004228
1205	Hyprr (Howdoo)	UDOO	https://coinmarketcap.com//currencies/hyprr/	0.02913
1206	Nexty	NTY	https://coinmarketcap.com//currencies/nexty/	1.797e-05
1207	Nuco.cloud	NCDT	https://coinmarketcap.com//currencies/nuco-cloud/	0.4333
1208	ThreeFold	TFT	https://coinmarketcap.com//currencies/threefold/	0.03455
1209	BitcoinZ	BTCZ	https://coinmarketcap.com//currencies/bitcoinz/	0.0003324
1210	2key.network	2KEY	https://coinmarketcap.com//currencies/2key-network/	0.05321
1211	OWNDATA	OWN	https://coinmarketcap.com//currencies/owndata/	0
1212	MyWish	WISH	https://coinmarketcap.com//currencies/mywish/	0.29
1213	BitCapitalVendor	BCV	https://coinmarketcap.com//currencies/bitcapitalvendor/	0
1214	BonFi	BNF	https://coinmarketcap.com//currencies/bonfi/	0.01
1215	Denarius	D	https://coinmarketcap.com//currencies/denarius-d/	0.36
1216	CACHE Gold	CGT	https://coinmarketcap.com//currencies/cache-gold/	55.53
1217	Eden	EDN	https://coinmarketcap.com//currencies/eden/	0
1218	Mushroom	MUSH	https://coinmarketcap.com//currencies/mushroom/	13.55
1219	FortKnoxster	FKX	https://coinmarketcap.com//currencies/fortknoxster/	0.02
1220	EOSDT	EOSDT	https://coinmarketcap.com//currencies/eosdt/	1
1221	ZClassic	ZCL	https://coinmarketcap.com//currencies/zclassic/	0.29
1222	BOX Token	BOX	https://coinmarketcap.com//currencies/box-token/	0.03
1223	MobileGo	MGO	https://coinmarketcap.com//currencies/mobilego/	0.03
1224	Zero Utility Token	ZUT	https://coinmarketcap.com//currencies/zero-utility-token/	2698.39
1225	VeriCoin	VRC	https://coinmarketcap.com//currencies/vericoin/	0.08
1226	Rotharium	RTH	https://coinmarketcap.com//currencies/rotharium/	0.78
1227	Expanse	EXP	https://coinmarketcap.com//currencies/expanse/	0.14
1228	Merculet	MVP	https://coinmarketcap.com//currencies/merculet/	0
1229	Wownero	WOW	https://coinmarketcap.com//currencies/wownero/	0.07
1230	DiFy.Finance	YFIII	https://coinmarketcap.com//currencies/dify-finance/	321.41
1231	FYDcoin	FYD	https://coinmarketcap.com//currencies/fydcoin/	0
1232	Ether-1	ETHO	https://coinmarketcap.com//currencies/ether-1/	0.05
1233	Aventus	AVT	https://coinmarketcap.com//currencies/aventus/	0.43
1234	Databroker	DTX	https://coinmarketcap.com//currencies/databroker/	0.03
1235	Epic Cash	EPIC	https://coinmarketcap.com//currencies/epic-cash/	0.23
1236	WinStars.live	WNL	https://coinmarketcap.com//currencies/winstars-live/	0.13
1237	DACSEE	DACS	https://coinmarketcap.com//currencies/dacsee/	0
1238	Base Protocol	BASE	https://coinmarketcap.com//currencies/base-protocol/	1.66
1239	ODUWA	OWC	https://coinmarketcap.com//currencies/oduwa/	0.6
1240	Themis	GET	https://coinmarketcap.com//currencies/themis/	0
1241	DOC.COM	MTC	https://coinmarketcap.com//currencies/doc-com/	0
1242	PayPie	PPP	https://coinmarketcap.com//currencies/paypie/	0.03
1243	DeFinition	DZI	https://coinmarketcap.com//currencies/definition/	1.07
1244	CVCoin	CVN	https://coinmarketcap.com//currencies/cvcoin/	0.2
1245	HempCoin	THC	https://coinmarketcap.com//currencies/hempcoin/	0.01
1246	HiCoin	XHI	https://coinmarketcap.com//currencies/hicoin/	0
1247	Bonk	BONK	https://coinmarketcap.com//currencies/bonk/	0.82
1248	Xfinance	XFI	https://coinmarketcap.com//currencies/xfinance/	52.15
1249	NOW Token	NOW	https://coinmarketcap.com//currencies/now-token/	0.03
1250	Snetwork	SNET	https://coinmarketcap.com//currencies/snetwork/	0.01
1251	Curecoin	CURE	https://coinmarketcap.com//currencies/curecoin/	0.1
1252	BillionHappiness	BHC	https://coinmarketcap.com//currencies/billionhappiness/	88.53
1253	Rapidz	RPZX	https://coinmarketcap.com//currencies/rapidz/	0
1254	Open Predict Token	OPT	https://coinmarketcap.com//currencies/open-predict-token/	2.39
1255	Education Ecosystem	LEDU	https://coinmarketcap.com//currencies/education-ecosystem/	0.01
1256	Ditto	DITTO	https://coinmarketcap.com//currencies/ditto/	0.93
1257	Cappasity	CAPP	https://coinmarketcap.com//currencies/cappasity/	0
1258	Kuende	KUE	https://coinmarketcap.com//currencies/kuende/	0
1259	AICHAIN	AIT	https://coinmarketcap.com//currencies/aichain/	0
1260	JUIICE	JUI	https://coinmarketcap.com//currencies/juiice/	0
1261	ChartEx	CHART	https://coinmarketcap.com//currencies/chartex/	0.12
1262	Bismuth	BIS	https://coinmarketcap.com//currencies/bismuth/	0.12
1263	Honest	HNST	https://coinmarketcap.com//currencies/honest/	0.03
1264	HEROcoin	PLAY	https://coinmarketcap.com//currencies/herocoin/	0.01
1265	Dimecoin	DIME	https://coinmarketcap.com//currencies/dimecoin/	0
1266	Naka Bodhi Token	NBOT	https://coinmarketcap.com//currencies/naka-bodhi-token/	0.03
1267	Uptrennd	1UP	https://coinmarketcap.com//currencies/uptrennd/	0.01
1268	TransferCoin	TX	https://coinmarketcap.com//currencies/transfercoin/	0.28
1269	GHOSTPRISM	GHOST	https://coinmarketcap.com//currencies/ghostprism/	0.61
1270	SafeCoin	SAFE	https://coinmarketcap.com//currencies/safecoin/	0.08
1271	Rapids	RPD	https://coinmarketcap.com//currencies/rapids/	0
1272	Boosted Finance	BOOST	https://coinmarketcap.com//currencies/boosted-finance/	38.84
1273	Maecenas	ART	https://coinmarketcap.com//currencies/maecenas/	0.03
1274	Experty	EXY	https://coinmarketcap.com//currencies/experty/	0.09
1275	Tadpole Finance	TAD	https://coinmarketcap.com//currencies/tadpole-finance/	10.02
1276	PirateCash	PIRATE	https://coinmarketcap.com//currencies/piratecash/	0.09
1277	IHT Real Estate Protocol	IHT	https://coinmarketcap.com//currencies/iht-real-estate-protocol/	0
1278	HYCON	HYC	https://coinmarketcap.com//currencies/hycon/	0
1279	Bean Cash	BITB	https://coinmarketcap.com//currencies/bean-cash/	0
1280	KickToken	KICK	https://coinmarketcap.com//currencies/kick-token/	0
1281	Zoracles	ZORA	https://coinmarketcap.com//currencies/zoracles/	408.75
1282	Gene Source Code Chain	GENE	https://coinmarketcap.com//currencies/gene-source-code-chain/	0.01
1283	Bitcoin Confidential	BC	https://coinmarketcap.com//currencies/bitcoin-confidential/	0
1284	IntelliShare	INE	https://coinmarketcap.com//currencies/intellishare/	0.01
1285	Kingdom Game 4.0	KDG	https://coinmarketcap.com//currencies/kingdom-game-4/	0.07
1286	NuBits	USNBT	https://coinmarketcap.com//currencies/nubits/	0.2
1287	Medicalchain	MTN	https://coinmarketcap.com//currencies/medical-chain/	0.01
1288	CryptoFranc	XCHF	https://coinmarketcap.com//currencies/cryptofranc/	1.08
1289	EOS TRUST	EOST	https://coinmarketcap.com//currencies/eos-trust/	0
1290	ECOSC	ECU	https://coinmarketcap.com//currencies/ecosc/	2.48
1291	Coinlancer	CL	https://coinmarketcap.com//currencies/coinlancer/	0.03
1292	Plair	PLA	https://coinmarketcap.com//currencies/plair/	0
1293	Lobstex	LOBS	https://coinmarketcap.com//currencies/lobstex/	0.1
1294	Blue Whale EXchange	BWX	https://coinmarketcap.com//currencies/blue-whale-exchange/	0.05
1295	PWR Coin	PWR	https://coinmarketcap.com//currencies/powercoin/	0
1296	Membrana	MBN	https://coinmarketcap.com//currencies/membrana/	0.01
1297	SonoCoin	SONO	https://coinmarketcap.com//currencies/sonocoin/	0.07
1298	Karbo	KRB	https://coinmarketcap.com//currencies/karbo/	0.23
1299	YVS.Finance	YVS	https://coinmarketcap.com//currencies/yvs-finance/	5.82
1300	Vites	VITES	https://coinmarketcap.com//currencies/vites/	0
1301	MonetaryUnit	MUE	https://coinmarketcap.com//currencies/monetaryunit/	0.009452
1302	I/O Coin	IOC	https://coinmarketcap.com//currencies/iocoin/	0.109
1303	Mochimo	MCM	https://coinmarketcap.com//currencies/mochimo/	0.2428
1304	LINKA	LINKA	https://coinmarketcap.com//currencies/linka/	0.002915
1305	Vanilla Network	VNLA	https://coinmarketcap.com//currencies/vanilla-network/	2.77
1306	Infinitus Token	INF	https://coinmarketcap.com//currencies/infinitus-token/	0.03477
1307	Ulord	UT	https://coinmarketcap.com//currencies/ulord/	0.02597
1308	GMB	GMB	https://coinmarketcap.com//currencies/gmb/	0.003875
1309	Donut	DONUT	https://coinmarketcap.com//currencies/donut/	0.01664
1310	Patientory	PTOY	https://coinmarketcap.com//currencies/patientory/	0.02854
1311	Conceal	CCX	https://coinmarketcap.com//currencies/conceal/	0.2
1312	Humaniq	HMQ	https://coinmarketcap.com//currencies/humaniq/	0.01
1313	Ormeus Cash	OMC	https://coinmarketcap.com//currencies/ormeus-cash/	0.01
1314	Lunyr	LUN	https://coinmarketcap.com//currencies/lunyr/	0.85
1315	DAOBet	BET	https://coinmarketcap.com//currencies/daobet/	0.01
1316	BetterBetting	BETR	https://coinmarketcap.com//currencies/betterbetting/	0.01
1317	Cryptaur	CPT	https://coinmarketcap.com//currencies/cryptaur/	0
1318	Stobox Token	STBU	https://coinmarketcap.com//currencies/stobox-token/	0.33
1319	XcelToken Plus	XLAB	https://coinmarketcap.com//currencies/xceltoken-plus/	0
1320	CannabisCoin	CANN	https://coinmarketcap.com//currencies/cannabiscoin/	0.03
1321	XTRABYTES	XBY	https://coinmarketcap.com//currencies/xtrabytes/	0
1322	Alphacat	ACAT	https://coinmarketcap.com//currencies/alphacat/	0
1323	TrueFlip	TFL	https://coinmarketcap.com//currencies/trueflip/	0.31
1324	SeChain	SNN	https://coinmarketcap.com//currencies/sechain/	0
1325	Primas	PST	https://coinmarketcap.com//currencies/primas/	0.04
1326	Vipstar Coin	VIPS	https://coinmarketcap.com//currencies/vipstar-coin/	0
1327	Libertas Token	LIBERTAS	https://coinmarketcap.com//currencies/libertas-token/	0.02
1328	NeoWorld Cash	NASH	https://coinmarketcap.com//currencies/neoworld-cash/	0
1329	Scry.info	DDD	https://coinmarketcap.com//currencies/scryinfo/	0
1330	SoMee.Social	ONG	https://coinmarketcap.com//currencies/ongsocial/	0.01
1331	BOOM	BOOM	https://coinmarketcap.com//currencies/boom/	0
1332	The ChampCoin	TCC	https://coinmarketcap.com//currencies/the-champcoin/	0.01
1333	Tolar	TOL	https://coinmarketcap.com//currencies/tolar/	0.01
1334	0xBitcoin	0xBTC	https://coinmarketcap.com//currencies/0xbtc/	0.32
1335	FujiCoin	FJC	https://coinmarketcap.com//currencies/fujicoin/	0
1336	Dragon Coins	DRG	https://coinmarketcap.com//currencies/dragon-coins/	0.01
1337	Rope	$ROPE	https://coinmarketcap.com//currencies/rope/	65.23
1338	BOScoin	BOS	https://coinmarketcap.com//currencies/boscoin/	0
1339	Eureka Coin	ERK	https://coinmarketcap.com//currencies/eureka-coin/	0.03
1340	SpectrumCash	XSM	https://coinmarketcap.com//currencies/spectrumcash/	0
1341	PLANET	PLA	https://coinmarketcap.com//currencies/planet/	0.01
1342	Asch	XAS	https://coinmarketcap.com//currencies/asch/	0.02
1343	Qwertycoin	QWC	https://coinmarketcap.com//currencies/qwertycoin/	0
1344	Quark	QRK	https://coinmarketcap.com//currencies/quark/	0.01
1345	CryptoFlow	CFL	https://coinmarketcap.com//currencies/cryptoflow/	0.02
1346	Ormeus Ecosystem	ECO	https://coinmarketcap.com//currencies/ormeus-ecosystem/	0
1347	MetaMorph	METM	https://coinmarketcap.com//currencies/metamorph/	0.01
1348	PieDAO DEFI++	DEFI++	https://coinmarketcap.com//currencies/piedao-defi/	5.27
1349	Megacoin	MEC	https://coinmarketcap.com//currencies/megacoin/	0.04
1350	Rise	RISE	https://coinmarketcap.com//currencies/rise/	0.01
1351	Infinitecoin	IFC	https://coinmarketcap.com//currencies/infinitecoin/	0
1352	Upfiring	UFR	https://coinmarketcap.com//currencies/upfiring/	0.07
1353	DeepOnion	ONION	https://coinmarketcap.com//currencies/deeponion/	0.08
1354	Matryx	MTX	https://coinmarketcap.com//currencies/matryx/	0.07
1355	Pakcoin	PAK	https://coinmarketcap.com//currencies/pakcoin/	0.03
1356	Kuai Token	KT	https://coinmarketcap.com//currencies/kuai-token/	0.15
1357	Tokoin	TOKO	https://coinmarketcap.com//currencies/tokoin/	0.01
1358	Vetri	VLD	https://coinmarketcap.com//currencies/vetri/	0
1359	Nuggets	NUG	https://coinmarketcap.com//currencies/nuggets/	0
1360	StrongHands	SHND	https://coinmarketcap.com//currencies/stronghands/	0
1361	Centric Cash	CNS	https://coinmarketcap.com//currencies/centric-cash/	0
1362	StaysBASE	SBS	https://coinmarketcap.com//currencies/staysbase/	0.51
1363	Zero	ZER	https://coinmarketcap.com//currencies/zero/	0.17
1364	VIG	VIG	https://coinmarketcap.com//currencies/vig/	0
1365	AI Doctor	AIDOC	https://coinmarketcap.com//currencies/aidoc/	0
1366	Xaurum	XAUR	https://coinmarketcap.com//currencies/xaurum/	0.02
1367	eosDAC	EOSDAC	https://coinmarketcap.com//currencies/eosdac/	0
1368	Shield Protocol	SHIELD	https://coinmarketcap.com//currencies/shield-protocol/	7.68
1369	EDC Blockchain	EDC	https://coinmarketcap.com//currencies/edc-blockchain/	0.04
1370	Modern Investment Coin	MODIC	https://coinmarketcap.com//currencies/modern-investment-coin/	0.56
1371	Bottos	BTO	https://coinmarketcap.com//currencies/bottos/	0
1372	OLXA	OLXA	https://coinmarketcap.com//currencies/olxa/	0
1373	Unification	FUND	https://coinmarketcap.com//currencies/unification/	0.05
1374	Venus BCH	vBCH	https://coinmarketcap.com//currencies/venus-bch/	10.53
1375	Energo	TSL	https://coinmarketcap.com//currencies/energo/	0
1376	Ycash	YEC	https://coinmarketcap.com//currencies/ycash/	0.15
1377	Japan Content Token	JCT	https://coinmarketcap.com//currencies/japan-content-token/	0.01
1378	ZumCoin	ZUM	https://coinmarketcap.com//currencies/zumcoin/	0
1379	SmileyCoin	SMLY	https://coinmarketcap.com//currencies/smileycoin/	0
1380	Tidex Token	TDX	https://coinmarketcap.com//currencies/tidex-token/	0.17
1381	Safex Cash	SFX	https://coinmarketcap.com//currencies/safex-cash/	0.02
1382	Sharder	SS	https://coinmarketcap.com//currencies/sharder/	0.01
1383	Substratum	SUB	https://coinmarketcap.com//currencies/substratum/	0
1384	Connectome	CNTM	https://coinmarketcap.com//currencies/connectome/	1.3
1385	Stabilize	STBZ	https://coinmarketcap.com//currencies/stabilize/	11.84
1386	SINOVATE	SIN	https://coinmarketcap.com//currencies/sinovate/	0
1387	Playgroundz	IOG	https://coinmarketcap.com//currencies/playgroundz/	0.05
1388	COVA	COVA	https://coinmarketcap.com//currencies/cova/	0
1389	GlobalBoost-Y	BSTY	https://coinmarketcap.com//currencies/globalboost-y/	0.09
1390	Renewable Electronic Energy Coin	REEC	https://coinmarketcap.com//currencies/electronic-energy-coin/	0
1391	Perth Mint Gold Token	PMGT	https://coinmarketcap.com//currencies/perth-mint-gold-token/	1770.49
1392	DexKit	KIT	https://coinmarketcap.com//currencies/dexkit/	1.91
1393	Insight Protocol	INX	https://coinmarketcap.com//currencies/insight-protocol/	0.01
1394	Banca	BANCA	https://coinmarketcap.com//currencies/banca/	0
1395	Adshares	ADS	https://coinmarketcap.com//currencies/adshares/	0.04
1396	Shard	SHARD	https://coinmarketcap.com//currencies/shard/	0.08
1397	CoinPoker	CHP	https://coinmarketcap.com//currencies/coinpoker/	0.01
1398	Axis DeFi	AXIS	https://coinmarketcap.com//currencies/axis-defi/	0.72
1399	TrezarCoin	TZC	https://coinmarketcap.com//currencies/trezarcoin/	0.01
1400	ADAMANT Messenger	ADM	https://coinmarketcap.com//currencies/adamant-messenger/	0.02
1401	Teloscoin	TELOS	https://coinmarketcap.com//currencies/teloscoin/	0.008908
1402	EXMO Coin	EXM	https://coinmarketcap.com//currencies/exmo-coin/	0.01336
1403	CloakCoin	CLOAK	https://coinmarketcap.com//currencies/cloakcoin/	0.2655
1404	0xcert	ZXC	https://coinmarketcap.com//currencies/0xcert/	0.004274
1405	Titan Coin	TTN	https://coinmarketcap.com//currencies/titan-coin/	0.001624
1406	Zen Protocol	ZP	https://coinmarketcap.com//currencies/zen-protocol/	0.06529
1407	BitBall	BTB	https://coinmarketcap.com//currencies/bitball/	0.002301
1408	LUXCoin	LUX	https://coinmarketcap.com//currencies/luxcoin/	0.1353
1409	Max Property Group	MPG	https://coinmarketcap.com//currencies/max-property-group/	0.01037
1410	FansTime	FTI	https://coinmarketcap.com//currencies/fanstime/	0.0005155
1411	Isiklar Coin	ISIKC	https://coinmarketcap.com//currencies/isiklar-coin/	0.62
1412	CryptoTask	CTASK	https://coinmarketcap.com//currencies/cryptotask/	1
1413	DeltaChain	DELTA	https://coinmarketcap.com//currencies/delta-chain/	0
1414	Havy	HAVY	https://coinmarketcap.com//currencies/havy/	0
1415	pEOS	PEOS	https://coinmarketcap.com//currencies/peos/	0
1416	8X8 PROTOCOL	EXE	https://coinmarketcap.com//currencies/8x8-protocol/	0
1417	Mercury	MER	https://coinmarketcap.com//currencies/mercury/	0.01
1418	Birake	BIR	https://coinmarketcap.com//currencies/birake/	0.02
1419	REAL	REAL	https://coinmarketcap.com//currencies/real/	0.14
1420	Memetic / PepeCoin	MEME	https://coinmarketcap.com//currencies/memetic/	0.05
1421	EchoLink	EKO	https://coinmarketcap.com//currencies/echolink/	0
1422	Ixcoin	IXC	https://coinmarketcap.com//currencies/ixcoin/	0.07
1423	BiblePay	BBP	https://coinmarketcap.com//currencies/biblepay/	0
1424	Phoenixcoin	PXC	https://coinmarketcap.com//currencies/phoenixcoin/	0.02
1425	Coinsbit Token	CNB	https://coinmarketcap.com//currencies/coinsbit-token/	0
1426	Global Cryptocurrency	GCC	https://coinmarketcap.com//currencies/global-cryptocurrency/	0
1427	CorionX	CORX	https://coinmarketcap.com//currencies/corionx/	0.01
1428	Bitcoin Plus	XBC	https://coinmarketcap.com//currencies/bitcoin-plus/	8.98
1429	WebDollar	WEBD	https://coinmarketcap.com//currencies/webdollar/	0
1430	Neurotoken	NTK	https://coinmarketcap.com//currencies/neurotoken/	0.02
1431	Fusible	FUSII	https://coinmarketcap.com//currencies/fusible/	3.34
1432	PetroDollar	XPD	https://coinmarketcap.com//currencies/petrodollar/	0.02
1433	Bezant	BZNT	https://coinmarketcap.com//currencies/bezant/	0
1434	Zebi Token	ZEBI	https://coinmarketcap.com//currencies/zebi-token/	0
1435	Origin Sport	ORS	https://coinmarketcap.com//currencies/origin-sport/	0
1436	DogeCash	DOGEC	https://coinmarketcap.com//currencies/dogecash/	0.1
1437	NFTX Hashmasks Index	MASK	https://coinmarketcap.com//currencies/nftx-hashmasks-index/	2254.7
1438	Q DAO Governance token v1.0	QDAO	https://coinmarketcap.com//currencies/q-dao-governance-token/	8.37
1439	CREA	CREA	https://coinmarketcap.com//currencies/crea/	0.07
1440	Azbit	AZ	https://coinmarketcap.com//currencies/azbit/	0
1441	Trollcoin	TROLL	https://coinmarketcap.com//currencies/trollcoin/	0
1442	Typerium	TYPE	https://coinmarketcap.com//currencies/typerium/	0
1443	SPINDLE	SPD	https://coinmarketcap.com//currencies/spindle/	0
1444	PUBLISH	NEWS	https://coinmarketcap.com//currencies/publish/	0.01
1445	CoinDeal Token	CDL	https://coinmarketcap.com//currencies/coindeal-token/	0.03
1446	SynchroBitcoin	SNB	https://coinmarketcap.com//currencies/synchrobitcoin/	0.02
1447	GeoCoin	GEO	https://coinmarketcap.com//currencies/geocoin/	0.41
1448	x42 Protocol	X42	https://coinmarketcap.com//currencies/x42-protocol/	0.06
1449	Bitcoin Private	BTCP	https://coinmarketcap.com//currencies/bitcoin-private/	0.27
1450	ebirah	EBRH	https://coinmarketcap.com//currencies/ebirah/	2.99
1451	NFT Index	NFTI	https://coinmarketcap.com//currencies/nft-index/	569.36
1452	Rivetz	RVT	https://coinmarketcap.com//currencies/rivetz/	0.05
1453	ZrCoin	ZRC	https://coinmarketcap.com//currencies/zrcoin/	0.25
1454	DeFi Bids	BID	https://coinmarketcap.com//currencies/defi-bids/	0.09
1455	LocalCoinSwap	LCS	https://coinmarketcap.com//currencies/local-coin-swap/	0.02
1456	Dether	DTH	https://coinmarketcap.com//currencies/dether/	0.01
1457	DAPS Coin	DAPS	https://coinmarketcap.com//currencies/daps-coin/	0
1458	Freicoin	FRC	https://coinmarketcap.com//currencies/freicoin/	0.02
1459	Eternity	ENT	https://coinmarketcap.com//currencies/eternity/	0.13
1460	Rise Protocol	RISE	https://coinmarketcap.com//currencies/rise-protocol/	17.74
1461	AAX Token	AAB	https://coinmarketcap.com//currencies/aax-token/	0.16
1462	DDKoin	DDK	https://coinmarketcap.com//currencies/ddkoin/	0.71
1463	Stox	STX	https://coinmarketcap.com//currencies/stox/	0.02
1464	CrowdWiz	WIZ	https://coinmarketcap.com//currencies/crowdwiz/	0.41
1465	ParkinGo	GOT	https://coinmarketcap.com//currencies/parkingo/	0.06
1466	DECENT	DCT	https://coinmarketcap.com//currencies/decent/	0.02
1467	Incent	INCNT	https://coinmarketcap.com//currencies/incent/	0.03
1468	AmonD	AMON	https://coinmarketcap.com//currencies/amond/	0
1469	SAFE DEAL	SFD	https://coinmarketcap.com//currencies/safe-deal/	3.11
1470	Garlicoin	GRLC	https://coinmarketcap.com//currencies/garlicoin/	0.02
1471	Blockpass	PASS	https://coinmarketcap.com//currencies/blockpass/	0.01
1472	IXT	IXT	https://coinmarketcap.com//currencies/ixledger/	0.03
1473	ATLANT	ATL	https://coinmarketcap.com//currencies/atlant/	0.02
1474	Color Platform	CLR	https://coinmarketcap.com//currencies/color-platform/	0.01
1475	TokenPay	TPAY	https://coinmarketcap.com//currencies/tokenpay/	0.05
1476	Nexxo	NEXXO	https://coinmarketcap.com//currencies/nexxo/	0.01
1477	BlockMesh	BMH	https://coinmarketcap.com//currencies/blockmesh/	0
1478	Cashbery Coin	CBC	https://coinmarketcap.com//currencies/cashbery-coin/	0.02
1479	On.Live	ONL	https://coinmarketcap.com//currencies/on-live/	0.05
1480	ZPER	ZPR	https://coinmarketcap.com//currencies/zper/	0
1481	Bitcoin Atom	BCA	https://coinmarketcap.com//currencies/bitcoin-atom/	0.06
1482	SHAKE	SHAKE	https://coinmarketcap.com//currencies/shake/	1979
1483	LanaCoin	LANA	https://coinmarketcap.com//currencies/lanacoin/	0
1484	Mcashchain	MCASH	https://coinmarketcap.com//currencies/mcashchain/	0
1485	HEAT	HEAT	https://coinmarketcap.com//currencies/heat-ledger/	0.02
1486	Decentrahub Coin	DCNTR	https://coinmarketcap.com//currencies/decentrahub-coin/	0.85
1487	Cosmo Coin	COSM	https://coinmarketcap.com//currencies/cosmo-coin/	0
1488	HashBX 	HBX	https://coinmarketcap.com//currencies/hashsbx/	0
1489	ZENZO	ZNZ	https://coinmarketcap.com//currencies/zenzo/	0.08
1490	Fesschain	FESS	https://coinmarketcap.com//currencies/fesschain/	0.01
1491	Meridian Network	LOCK	https://coinmarketcap.com//currencies/meridian-network/	0.1
1492	BUZZCoin	BUZZ	https://coinmarketcap.com//currencies/buzzcoin/	0
1493	Maincoin	MNC	https://coinmarketcap.com//currencies/maincoin/	0
1494	FUD.finance	FUD	https://coinmarketcap.com//currencies/fudfinance/	46.98
1495	Etherparty	FUEL	https://coinmarketcap.com//currencies/etherparty/	0
1496	SOTA Finance	SOTA	https://coinmarketcap.com//currencies/sota-finance/	0.44
1497	STK	STK	https://coinmarketcap.com//currencies/stk/	0
1498	Wrapped Origin Axie	WOA	https://coinmarketcap.com//currencies/wrapped-origin-axie/	4780.27
1499	Patron	PAT	https://coinmarketcap.com//currencies/patron/	0
1500	Smartshare	SSP	https://coinmarketcap.com//currencies/smartshare/	0
1501	DubaiCoin	DBIX	https://coinmarketcap.com//currencies/dubaicoin-dbix/	0.2519
1502	USDX [Lighthouse]	USDX	https://coinmarketcap.com//currencies/usdx-lighthouse/	0.2838
1503	Fera	FERA	https://coinmarketcap.com//currencies/fera/	0.005757
1504	Rate3	RTE	https://coinmarketcap.com//currencies/rate3/	0.001193
1505	Tendies	TEND	https://coinmarketcap.com//currencies/tendies/	0.1408
1506	BlitzPredict	XBP	https://coinmarketcap.com//currencies/blitzpredict/	0.002765
1507	Waves Community Token	WCT	https://coinmarketcap.com//currencies/waves-community-token/	0.1045
1508	Peony	PNY	https://coinmarketcap.com//currencies/peony/	0.1474
1509	Liquidity Network	LQD	https://coinmarketcap.com//currencies/liquidity-network/	0.01942
1510	BonusCloud	BXC	https://coinmarketcap.com//currencies/bonuscloud/	0.0003305
1511	Clash Token	SCT	https://coinmarketcap.com//currencies/clash-token/	0.69
1512	SIBCoin	SIB	https://coinmarketcap.com//currencies/sibcoin/	0.05
1513	NeuroChain	NCC	https://coinmarketcap.com//currencies/neurochain/	0
1514	PegNet	PEG	https://coinmarketcap.com//currencies/pegnet/	0
1515	EBCoin	EBC	https://coinmarketcap.com//currencies/ebcoin/	0
1516	ShipChain	SHIP	https://coinmarketcap.com//currencies/shipchain/	0
1517	NuShares	NSR	https://coinmarketcap.com//currencies/nushares/	0
1518	Squirrel Finance	NUTS	https://coinmarketcap.com//currencies/squirrel-finance/	2.05
1519	NEXT.coin	NEXT	https://coinmarketcap.com//currencies/next-coin/	0.16
1520	Cornichon	CORN	https://coinmarketcap.com//currencies/cornichon/	0.05
1521	Omnitude	ECOM	https://coinmarketcap.com//currencies/omnitude/	0.02
1522	Earnbase	ENB	https://coinmarketcap.com//currencies/earnbase/	8.05
1523	Ubex	UBEX	https://coinmarketcap.com//currencies/ubex/	0
1524	Fountain	FTN	https://coinmarketcap.com//currencies/fountain/	0.01
1525	Winco	WCO	https://coinmarketcap.com//currencies/winco/	0
1526	StrongHands Masternode	SHMN	https://coinmarketcap.com//currencies/stronghands-masternode/	0.34
1527	WeTrust	TRST	https://coinmarketcap.com//currencies/trust/	0.01
1528	Moneynet	MNC	https://coinmarketcap.com//currencies/moneynet/	0
1529	QANplatform	QARK	https://coinmarketcap.com//currencies/qanplatform/	0.05
1530	Flixxo	FLIXX	https://coinmarketcap.com//currencies/flixxo/	0.01
1531	Olyseum	OLY	https://coinmarketcap.com//currencies/olyseum/	0.47
1532	BitScreener Token	BITX	https://coinmarketcap.com//currencies/bitscreener-token/	0.01
1533	QChi	QCH	https://coinmarketcap.com//currencies/qchi/	0.03
1534	Veil	VEIL	https://coinmarketcap.com//currencies/veil/	0.01
1535	RigoBlock	GRG	https://coinmarketcap.com//currencies/rigoblock/	0.69
1536	Bitgear	GEAR	https://coinmarketcap.com//currencies/bitgear/	0.01
1537	Doge Token	DOGET	https://coinmarketcap.com//currencies/doge-token/	0
1538	Krios	GIG	https://coinmarketcap.com//currencies/krios/	0
1539	Cat Token	CAT	https://coinmarketcap.com//currencies/cat-token/	0.14
1540	Atmos	ATMOS	https://coinmarketcap.com//currencies/atmos/	0.01
1541	Artfinity	AT	https://coinmarketcap.com//currencies/artfinity/	0.01
1542	Ethereum Stake	ETHYS	https://coinmarketcap.com//currencies/ethereum-stake/	4.54
1543	Spaceswap	MILK2	https://coinmarketcap.com//currencies/spaceswap/	0.23
1544	HyperExchange	HX	https://coinmarketcap.com//currencies/hyperexchange/	0.02
1545	Hiveterminal Token	HVN	https://coinmarketcap.com//currencies/hiveterminal-token/	0
1546	Curio	CUR	https://coinmarketcap.com//currencies/curio/	0.52
1547	MARK.SPACE	MRK	https://coinmarketcap.com//currencies/mark-space/	0
1548	Beacon	BECN	https://coinmarketcap.com//currencies/beacon/	1.71
1549	BOMB	BOMB	https://coinmarketcap.com//currencies/bomb/	0.98
1550	Carboncoin	CARBON	https://coinmarketcap.com//currencies/carboncoin/	0
1551	Antiample	XAMP	https://coinmarketcap.com//currencies/antiample/	0
1552	Arbidex	ABX	https://coinmarketcap.com//currencies/arbidex/	0.04
1553	ONOToken	ONOT	https://coinmarketcap.com//currencies/onotoken/	0
1554	Jarvis+	JAR	https://coinmarketcap.com//currencies/jarvis/	0.01
1555	Friendz	FDZ	https://coinmarketcap.com//currencies/friends/	0
1556	Maxcoin	MAX	https://coinmarketcap.com//currencies/maxcoin/	0.01
1557	Bounty0x	BNTY	https://coinmarketcap.com//currencies/bounty0x/	0
1558	DEXA COIN	DEXA	https://coinmarketcap.com//currencies/dexa-coin/	0
1559	PolypuX	PUX	https://coinmarketcap.com//currencies/polypux/	0.02
1560	EventChain	EVC	https://coinmarketcap.com//currencies/eventchain/	0.03
1561	Orbitcoin	ORB	https://coinmarketcap.com//currencies/orbitcoin/	0.27
1562	Cryptobuyer	XPT	https://coinmarketcap.com//currencies/cryptobuyer/	0.01
1563	FuzeX	FXT	https://coinmarketcap.com//currencies/fuzex/	0
1564	Sapien	SPN	https://coinmarketcap.com//currencies/sapien/	0
1565	PRIA	PRIA	https://coinmarketcap.com//currencies/pria/	12.18
1566	Ink Protocol	XNK	https://coinmarketcap.com//currencies/ink-protocol/	0
1567	Rublix	RBLX	https://coinmarketcap.com//currencies/rublix/	0.04
1568	EveryCoin 	EVY	https://coinmarketcap.com//currencies/everycoin/	0
1569	TraDove B2BCoin	BBC	https://coinmarketcap.com//currencies/b2bcoin/	0
1570	Webflix Token	WFX	https://coinmarketcap.com//currencies/webflix-token/	0
1571	Spectre.ai Utility Token	SXUT	https://coinmarketcap.com//currencies/spectre-utility/	0.03
1572	Wrapped Gen-0 CryptoKitties	WG0	https://coinmarketcap.com//currencies/wrapped-gen-0-cryptokitties/	426.43
1573	Rotten	ROT	https://coinmarketcap.com//currencies/rotten/	0.01
1574	DATx	DATX	https://coinmarketcap.com//currencies/datx/	0
1575	iDealCash	DEAL	https://coinmarketcap.com//currencies/idealcash/	0
1576	TurtleNetwork	TN	https://coinmarketcap.com//currencies/turtlenetwork/	0.01
1577	Pawtocol	UPI	https://coinmarketcap.com//currencies/pawtocol/	0.01
1578	ATN	ATN	https://coinmarketcap.com//currencies/atn/	0.01
1579	ICE ROCK MINING	ROCK2	https://coinmarketcap.com//currencies/ice-rock-mining/	0.05
1580	FintruX Network	FTX	https://coinmarketcap.com//currencies/fintrux-network/	0.01
1581	POPCHAIN	PCH	https://coinmarketcap.com//currencies/popchain/	0
1582	Manna	MANNA	https://coinmarketcap.com//currencies/manna/	0
1583	Power Index Pool Token	PIPT	https://coinmarketcap.com//currencies/power-index-pool-token/	3.91
1584	ExclusiveCoin	EXCL	https://coinmarketcap.com//currencies/exclusivecoin/	0.13
1585	AidCoin	AID	https://coinmarketcap.com//currencies/aidcoin/	0.02
1586	Wolves of Wall Street	WOWS	https://coinmarketcap.com//currencies/wolves-of-wall-street/	175.75
1587	Ruler Protocol	RULER	https://coinmarketcap.com//currencies/ruler-protocol/	206.79
1588	Alpha Token	A	https://coinmarketcap.com//currencies/alpha-token/	0.02
1589	Gravity	GZRO	https://coinmarketcap.com//currencies/gravity/	0
1590	TrustDAO	TRUST	https://coinmarketcap.com//currencies/trustdao/	0.04
1591	Niobium Coin	NBC	https://coinmarketcap.com//currencies/niobium-coin/	0.01
1592	CyberFM	CYFM	https://coinmarketcap.com//currencies/cyberfm/	0
1593	NestEGG Coin	EGG	https://coinmarketcap.com//currencies/nestegg-coin/	0.02
1594	Vox.Finance	VOX	https://coinmarketcap.com//currencies/vox-finance/	142.03
1595	BITTO	BITTO	https://coinmarketcap.com//currencies/bitto/	0.25
1596	Pylon Network	PYLNT	https://coinmarketcap.com//currencies/pylon-network/	1.23
1597	Yearn Secure	YSEC	https://coinmarketcap.com//currencies/yearn-secure/	1.41
1598	Pesetacoin	PTC	https://coinmarketcap.com//currencies/pesetacoin/	0.01
1599	Noir	NOR	https://coinmarketcap.com//currencies/noir/	0.04
1600	MidasProtocol	MAS	https://coinmarketcap.com//currencies/midasprotocol/	0
1601	eXPerience Chain	XPC	https://coinmarketcap.com//currencies/experience-chain/	8.094e-06
1602	Digital Gold	GOLD	https://coinmarketcap.com//currencies/digital-gold/	54.15
1603	Woodcoin	LOG	https://coinmarketcap.com//currencies/woodcoin/	0.09102
1604	Indorse Token	IND	https://coinmarketcap.com//currencies/indorse-token/	0.01945
1605	VikkyToken	VIKKY	https://coinmarketcap.com//currencies/vikkytoken/	0.000193
1606	GoWithMi	GMAT	https://coinmarketcap.com//currencies/gowithmi/	0.0002636
1607	Debitum	DEB	https://coinmarketcap.com//currencies/debitum-network/	0.003588
1608	Ink	INK	https://coinmarketcap.com//currencies/ink/	0.001541
1609	Ormeus Coin	ORMEUS	https://coinmarketcap.com//currencies/ormeus-coin/	0.01988
1610	Almace Shards	ALMX	https://coinmarketcap.com//currencies/almace-shards/	71.07
1611	RED	RED	https://coinmarketcap.com//currencies/red/	0.01
1612	MIB Coin	MIB	https://coinmarketcap.com//currencies/mib-coin/	0.01
1613	GokuMarket Credit	GMC	https://coinmarketcap.com//currencies/gokumarket-credit/	0.23
1614	BZEdge	BZE	https://coinmarketcap.com//currencies/bzedge/	0
1615	Limitless VIP	VIP	https://coinmarketcap.com//currencies/limitless-vip/	0
1616	CryptoSoul	SOUL	https://coinmarketcap.com//currencies/cryptosoul/	0
1617	ESBC	ESBC	https://coinmarketcap.com//currencies/esbc/	0.03
1618	Trinity Network Credit	TNC	https://coinmarketcap.com//currencies/trinity-network-credit/	0
1619	Okschain	OKS	https://coinmarketcap.com//currencies/okschain/	0
1620	CaluraCoin	CLC	https://coinmarketcap.com//currencies/caluracoin/	0.05
1621	TigerCash	TCH	https://coinmarketcap.com//currencies/tigercash/	0.01
1622	MyBit	MYB	https://coinmarketcap.com//currencies/mybit/	0
1623	Deutsche eMark	DEM	https://coinmarketcap.com//currencies/deutsche-emark/	0.01
1624	FlypMe	FYP	https://coinmarketcap.com//currencies/flypme/	0.04
1625	LYNC Network	LYNC	https://coinmarketcap.com//currencies/lync-network/	0.71
1626	Electrify.Asia	ELEC	https://coinmarketcap.com//currencies/electrifyasia/	0
1627	RMPL	RMPL	https://coinmarketcap.com//currencies/rmpl/	0.98
1628	Lisk Machine Learning	LML	https://coinmarketcap.com//currencies/lisk-machine-learning/	0.01
1629	Yap Stone	YAP	https://coinmarketcap.com//currencies/yap-stone/	0
1630	Infinity Esaham	INFS	https://coinmarketcap.com//currencies/infinity-esaham/	1.02
1631	MEET.ONE	MEETONE	https://coinmarketcap.com//currencies/meetone/	0
1632	Delphy	DPY	https://coinmarketcap.com//currencies/delphy/	0.01
1633	CoinFi	COFI	https://coinmarketcap.com//currencies/coinfi/	0
1634	Stably USD	USDS	https://coinmarketcap.com//currencies/stableusd/	0.99
1635	Primalbase Token	PBT	https://coinmarketcap.com//currencies/primalbase/	494.54
1636	Blue Protocol	BLUE	https://coinmarketcap.com//currencies/ethereum-blue/	0.02
1637	Spiking	SPIKE	https://coinmarketcap.com//currencies/spiking/	0
1638	Ethereum Yield	ETHY	https://coinmarketcap.com//currencies/ethereum-yield/	6.11
1639	Trias	TRY	https://coinmarketcap.com//currencies/trias/	0
1640	CryptoBonusMiles	CBM	https://coinmarketcap.com//currencies/cryptobonusmiles/	0
1641	ZCore	ZCR	https://coinmarketcap.com//currencies/zcore/	0.07
1642	Internet of People	IOP	https://coinmarketcap.com//currencies/internet-of-people/	0.04
1643	Rentberry	BERRY	https://coinmarketcap.com//currencies/rentberry/	0
1644	OracleChain	OCT	https://coinmarketcap.com//currencies/oraclechain/	0.02
1645	Zeusshield	ZSC	https://coinmarketcap.com//currencies/zeusshield/	0
1646	Bitblocks	BBK	https://coinmarketcap.com//currencies/bitblocks/	0
1647	Sakura Bloom	SKB	https://coinmarketcap.com//currencies/sakura-bloom/	0
1648	Shadow Token	SHDW	https://coinmarketcap.com//currencies/shadow-token/	0.08
1649	Opus	OPT	https://coinmarketcap.com//currencies/opus/	0
1650	Transcodium	TNS	https://coinmarketcap.com//currencies/transcodium/	0.01
1651	Elementeum	ELET	https://coinmarketcap.com//currencies/elementeum/	0.11
1652	Sociall	SCL	https://coinmarketcap.com//currencies/sociall/	0.03
1653	TON Token	TON	https://coinmarketcap.com//currencies/tontoken/	0.01
1654	Crypton	CRP	https://coinmarketcap.com//currencies/utopia/	0.17
1655	PiplCoin	PIPL	https://coinmarketcap.com//currencies/piplcoin/	0
1656	Edgeless	EDG	https://coinmarketcap.com//currencies/edgeless/	0
1657	ZeuxCoin	ZUC	https://coinmarketcap.com//currencies/zeuxcoin/	0
1658	Insula	ISLA	https://coinmarketcap.com//currencies/insula/	0.71
1659	Merebel	MERI	https://coinmarketcap.com//currencies/merebel/	0.09
1660	HelloGold	HGT	https://coinmarketcap.com//currencies/hellogold/	0
1661	Sessia	KICKS	https://coinmarketcap.com//currencies/sessia/	0.06
1662	NFX Coin	NFXC	https://coinmarketcap.com//currencies/nfx-coin/	0
1663	Golos Blockchain	GLS	https://coinmarketcap.com//currencies/golos-blockchain/	0
1664	Digital Insurance Token	DIT	https://coinmarketcap.com//currencies/digital-insurance-token/	0
1665	Birdchain	BIRD	https://coinmarketcap.com//currencies/birdchain/	0
1666	X8X Token	X8X	https://coinmarketcap.com//currencies/x8x-token/	0.01
1667	Zennies	ZENI	https://coinmarketcap.com//currencies/zennies/	0
1668	Neutron	NTRN	https://coinmarketcap.com//currencies/neutron/	0.01
1669	Nerva	XNV	https://coinmarketcap.com//currencies/nerva/	0.03
1670	AudioCoin	ADC	https://coinmarketcap.com//currencies/audiocoin/	0
1671	Bob's Repair	BOB	https://coinmarketcap.com//currencies/bobs-repair/	0
1672	FantasyGold	FGC	https://coinmarketcap.com//currencies/fantasygold/	0
1673	Posscoin	POSS	https://coinmarketcap.com//currencies/posscoin/	0
1674	Master Contract Token	MCT	https://coinmarketcap.com//currencies/master-contract-token/	0
1675	PTON	PTON	https://coinmarketcap.com//currencies/pton/	0
1676	MAX Exchange Token	MAX	https://coinmarketcap.com//currencies/max-exchange-token/	0.3
1677	Bridge Protocol	BRDG	https://coinmarketcap.com//currencies/bridge-protocol/	0
1678	UpToken	UP	https://coinmarketcap.com//currencies/uptoken/	0
1679	SBank	STS	https://coinmarketcap.com//currencies/sbank/	0.09
1680	Block-Logic	BLTG	https://coinmarketcap.com//currencies/block-logic/	0.02
1681	Devery	EVE	https://coinmarketcap.com//currencies/devery/	0.01
1682	Tornado	TCORE	https://coinmarketcap.com//currencies/tornado/	87.94
1683	Lendingblock	LND	https://coinmarketcap.com//currencies/lendingblock/	0
1684	Lightstreams	PHT	https://coinmarketcap.com//currencies/lightstreams/	0.01
1685	MintMe.com Coin	MINTME	https://coinmarketcap.com//currencies/mintme-com-coin/	0
1686	RealTract	RET	https://coinmarketcap.com//currencies/realtract/	0
1687	ChessCoin	CHESS	https://coinmarketcap.com//currencies/chesscoin/	0.01
1688	Moneytoken	IMT	https://coinmarketcap.com//currencies/moneytoken/	0
1689	Bidesk	BDK	https://coinmarketcap.com//currencies/bidesk/	0.06
1690	ROAD	ROAD	https://coinmarketcap.com//currencies/road/	0.01
1691	Escroco Emerald	ESCE	https://coinmarketcap.com//currencies/escroco-emerald/	0
1692	DAV Coin	DAV	https://coinmarketcap.com//currencies/dav-coin/	0
1693	Actinium	ACM	https://coinmarketcap.com//currencies/actinium/	0.02
1694	DPRating	RATING	https://coinmarketcap.com//currencies/dprating/	0
1695	extraDNA	XDNA	https://coinmarketcap.com//currencies/extradna/	0
1696	Crypto Sports	CSPN	https://coinmarketcap.com//currencies/crypto-sports/	0.18
1697	Blocktix	TIX	https://coinmarketcap.com//currencies/blocktix/	0.01
1698	Nework	NKC	https://coinmarketcap.com//currencies/nework/	0
1699	Ethereum Meta	ETHM	https://coinmarketcap.com//currencies/ethereum-meta/	0
1700	Bitcloud	BTDX	https://coinmarketcap.com//currencies/bitcloud/	0.01
1701	CustomContractNetwork	CCN	https://coinmarketcap.com//currencies/customcontractnetwork/	0.000296
1702	VouchForMe	IPL	https://coinmarketcap.com//currencies/insurepal/	0.001956
1703	GoldenPyrex	GPYX	https://coinmarketcap.com//currencies/goldenpyrex/	0.04777
1704	GoldMint	MNTP	https://coinmarketcap.com//currencies/goldmint/	0.247
1705	Playkey	PKT	https://coinmarketcap.com//currencies/playkey/	0.02854
1706	VegaWallet Token	VGW	https://coinmarketcap.com//currencies/vegawallet-token/	0.02889
1707	BitDegree	BDG	https://coinmarketcap.com//currencies/bitdegree/	0.001205
1708	Alt.Estate token	ALT	https://coinmarketcap.com//currencies/alt-estate-token/	0.0006394
1709	KnoxFS (new)	KFX	https://coinmarketcap.com//currencies/knoxfs-new/	1.22
1710	SophiaTX	SPHTX	https://coinmarketcap.com//currencies/sophiatx/	0.001396
1711	Swap	XWP	https://coinmarketcap.com//currencies/swap/	0.04
1712	AMATEN	AMA	https://coinmarketcap.com//currencies/amaten/	0.06
1713	Wrapped Basic CryptoKitties	WCK	https://coinmarketcap.com//currencies/wrapped-cryptokitties/	6.98
1714	Equal	EQL	https://coinmarketcap.com//currencies/equal/	0
1715	Blockmason Credit Protocol	BCPT	https://coinmarketcap.com//currencies/blockmason/	0
1716	Solaris	XLR	https://coinmarketcap.com//currencies/solaris/	0.31
1717	Startcoin	START	https://coinmarketcap.com//currencies/startcoin/	0.01
1718	JustBet	WINR	https://coinmarketcap.com//currencies/justbet/	0
1719	PUBLYTO Token	PUB	https://coinmarketcap.com//currencies/publyto-token/	0
1720	CryptoVerificationCoin	CVCC	https://coinmarketcap.com//currencies/cryptoverificationcoin/	0.77
1721	Keep4r	KP4R	https://coinmarketcap.com//currencies/keep4r/	5.68
1722	Lition	LIT	https://coinmarketcap.com//currencies/lition/	0.01
1723	Lethean	LTHN	https://coinmarketcap.com//currencies/lethean/	0
1724	Auxilium	AUX	https://coinmarketcap.com//currencies/auxilium/	0
1725	Switch	ESH	https://coinmarketcap.com//currencies/switch/	0.04
1726	PENG	PENG	https://coinmarketcap.com//currencies/peng/	0
1727	Stipend	SPD	https://coinmarketcap.com//currencies/stipend/	0.04
1728	Ethereum Gold	ETG	https://coinmarketcap.com//currencies/ethereum-gold/	0.02
1729	Synergy	SNRG	https://coinmarketcap.com//currencies/synergy/	0.09
1730	uPlexa	UPX	https://coinmarketcap.com//currencies/uplexa/	0
1731	Rigel Finance	RIGEL	https://coinmarketcap.com//currencies/rigel-finance/	101.67
1732	Fundamenta	FMTA	https://coinmarketcap.com//currencies/fundamenta/	0.57
1733	CryptoAds Marketplace	CRAD	https://coinmarketcap.com//currencies/cryptoads-marketplace/	0.03
1734	Po.et	POE	https://coinmarketcap.com//currencies/poet/	0
1735	TrueDeck	TDP	https://coinmarketcap.com//currencies/truedeck/	0.01
1736	OptiToken	OPTI	https://coinmarketcap.com//currencies/optitoken/	0.01
1737	AtromG8	AG8	https://coinmarketcap.com//currencies/atromg8/	0.03
1738	ARAW	ARAW	https://coinmarketcap.com//currencies/araw/	0
1739	KIMCHI.finance	KIMCHI	https://coinmarketcap.com//currencies/kimchi-finance/	0
1740	SovranoCoin	SVR	https://coinmarketcap.com//currencies/sovranocoin/	0.29
1741	ImageCoin	IMG	https://coinmarketcap.com//currencies/imagecoin/	0.04
1742	NPCoin	NPC	https://coinmarketcap.com//currencies/npcoin/	0.01
1743	Masari	MSR	https://coinmarketcap.com//currencies/masari/	0.03
1744	AiLink Token	ALI	https://coinmarketcap.com//currencies/ailink-token/	0
1745	BidiPass	BDP	https://coinmarketcap.com//currencies/bidipass/	0
1746	Cryptonite	XCN	https://coinmarketcap.com//currencies/cryptonite/	0
1747	HashCoin	HSC	https://coinmarketcap.com//currencies/hashcoin/	0
1748	LunchMoney	LMY	https://coinmarketcap.com//currencies/lunchmoney/	0
1749	Qbao	QBT	https://coinmarketcap.com//currencies/qbao/	0.01
1750	Datamine	DAM	https://coinmarketcap.com//currencies/datamine/	0.09
1751	TerraCredit	CREDIT	https://coinmarketcap.com//currencies/credit/	0
1752	Faceter	FACE	https://coinmarketcap.com//currencies/faceter/	0
1753	Streamity	STM	https://coinmarketcap.com//currencies/streamity/	0.01
1754	TENA	TENA	https://coinmarketcap.com//currencies/tena/	0.13
1755	Starbase	STAR	https://coinmarketcap.com//currencies/starbase/	0
1756	Vidulum	VDL	https://coinmarketcap.com//currencies/vidulum/	0.05
1757	EtherGem	EGEM	https://coinmarketcap.com//currencies/ethergem/	0.02
1758	SwiftCash	SWIFT	https://coinmarketcap.com//currencies/swiftcash/	0
1759	SafeInsure	SINS	https://coinmarketcap.com//currencies/safeinsure/	0.02
1760	SHIELD	XSH	https://coinmarketcap.com//currencies/shield-xsh/	0
1761	nYFI	N0031	https://coinmarketcap.com//currencies/nyfi/	0.14
1762	Alchemint Standards	SDS	https://coinmarketcap.com//currencies/alchemint-standards/	0
1763	Scorum Coins	SCR	https://coinmarketcap.com//currencies/scorum-coins/	0.01
1764	Hashshare	HSS	https://coinmarketcap.com//currencies/hashshare/	0
1765	Zeepin	ZPT	https://coinmarketcap.com//currencies/zeepin/	0
1766	Swing	SWING	https://coinmarketcap.com//currencies/swing/	0.07
1767	Gems 	GEM	https://coinmarketcap.com//currencies/gems-protocol/	0
1768	Datawallet	DXT	https://coinmarketcap.com//currencies/datawallet/	0
1769	ION	ION	https://coinmarketcap.com//currencies/ion/	0.03
1770	iEthereum	IETH	https://coinmarketcap.com//currencies/iethereum/	0.02
1771	QUINADS	QUIN	https://coinmarketcap.com//currencies/quinads/	0
1772	NANJCOIN	NANJ	https://coinmarketcap.com//currencies/nanjcoin/	0
1773	Ether Zero	ETZ	https://coinmarketcap.com//currencies/ether-zero/	0
1774	Kora Network Token	KNT	https://coinmarketcap.com//currencies/kora-network-token/	0
1775	Coinsuper Ecosystem Network	CEN	https://coinmarketcap.com//currencies/coinsuper-ecosystem-network/	0
1776	Axe	AXE	https://coinmarketcap.com//currencies/axe/	0.07
1777	True Seigniorage Dollar	TSD	https://coinmarketcap.com//currencies/true-seigniorage-dollar/	0.01
1778	AllSafe	ASAFE	https://coinmarketcap.com//currencies/allsafe/	0.04
1779	FLIP	FLP	https://coinmarketcap.com//currencies/flip/	0.01
1780	Energycoin	ENRG	https://coinmarketcap.com//currencies/energycoin/	0
1781	Bitsdaq	BQQQ	https://coinmarketcap.com//currencies/bitsdaq/	0
1782	Sentinel Chain	SENC	https://coinmarketcap.com//currencies/sentinel-chain/	0
1783	Tokenbox	TBX	https://coinmarketcap.com//currencies/tokenbox/	0.03
1784	Felixo Coin	FLX	https://coinmarketcap.com//currencies/felixo-coin/	0
1785	ZelaaPayAE	ZPAE	https://coinmarketcap.com//currencies/zelaapayae/	0
1786	Bitstar	BITS	https://coinmarketcap.com//currencies/bitstar/	0.02
1787	Graft	GRFT	https://coinmarketcap.com//currencies/graft/	0
1788	WandX	WAND	https://coinmarketcap.com//currencies/wandx/	0.03
1789	DopeCoin	DOPE	https://coinmarketcap.com//currencies/dopecoin/	0
1790	Xeonbit	XNB	https://coinmarketcap.com//currencies/xeonbit/	0
1791	InvestDigital	IDT	https://coinmarketcap.com//currencies/investdigital/	0
1792	BlueCoin	BLU	https://coinmarketcap.com//currencies/bluecoin/	0
1793	InvestFeed	IFT	https://coinmarketcap.com//currencies/investfeed/	0
1794	Bigbom	BBO	https://coinmarketcap.com//currencies/bigbom/	0
1795	HOQU	HQX	https://coinmarketcap.com//currencies/hoqu/	0
1796	Parkgene	GENE	https://coinmarketcap.com//currencies/parkgene/	0
1797	Innovative Bioresearch Coin	INNBC	https://coinmarketcap.com//currencies/innovative-bioresearch-coin/	0
1798	Bitether	BTR	https://coinmarketcap.com//currencies/bitether/	0.01
1799	SynLev	SYN	https://coinmarketcap.com//currencies/synlev/	0.03
1800	adToken	ADT	https://coinmarketcap.com//currencies/adtoken/	0
1801	Aeron	ARNX	https://coinmarketcap.com//currencies/aeron/	0.009069
1802	MEXC Token	MEXC	https://coinmarketcap.com//currencies/mexc-token/	0.0003
1803	Scrypta	LYRA	https://coinmarketcap.com//currencies/scrypta/	0.01863
1804	Asura Coin	ASA	https://coinmarketcap.com//currencies/asura-coin/	0.001366
1805	Maverick Chain	MVC	https://coinmarketcap.com//currencies/maverick-chain/	0.00278
1806	GoNetwork	GOT	https://coinmarketcap.com//currencies/gonetwork/	0.004111
1807	2GIVE	2GIVE	https://coinmarketcap.com//currencies/2give/	0.000556
1808	pulltherug.finance	RUGZ	https://coinmarketcap.com//currencies/rugz/	30.86
1809	IQ.cash	IQ	https://coinmarketcap.com//currencies/iqcash/	0.02396
1810	GoByte	GBX	https://coinmarketcap.com//currencies/gobyte/	0.03003
1811	Ellaism	ELLA	https://coinmarketcap.com//currencies/ellaism/	0.01
1812	Decentralized Crypto Token	DCTO	https://coinmarketcap.com//currencies/decentralized-crypto-token/	0
1813	Beetle Coin	BEET	https://coinmarketcap.com//currencies/beetle-coin/	0
1814	Matrexcoin	MAC	https://coinmarketcap.com//currencies/matrexcoin/	0.01
1815	Bezop	BEZ	https://coinmarketcap.com//currencies/bezop/	0.01
1816	Italian Lira	ITL	https://coinmarketcap.com//currencies/italian-lira/	0
1817	HeroNode	HER	https://coinmarketcap.com//currencies/heronode/	0
1818	BitCash	BITC	https://coinmarketcap.com//currencies/bitcash/	0.01
1819	IONChain	IONC	https://coinmarketcap.com//currencies/ionchain/	0
1820	Phoneum	PHT	https://coinmarketcap.com//currencies/phoneum/	0
1821	AC3	AC3	https://coinmarketcap.com//currencies/ac3/	0
1822	Aigang	AIX	https://coinmarketcap.com//currencies/aigang/	0.01
1823	Castweet	CTT	https://coinmarketcap.com//currencies/castweet/	0.15
1824	MODEL-X-coin	MODX	https://coinmarketcap.com//currencies/model-x-coin/	0.02
1825	PDATA	PDATA	https://coinmarketcap.com//currencies/pdata/	0
1826	Zilla	ZLA	https://coinmarketcap.com//currencies/zilla/	0
1827	Jetcoin	JET	https://coinmarketcap.com//currencies/jetcoin/	0.03
1828	Ouroboros	OURO	https://coinmarketcap.com//currencies/ouroboros/	0.01
1829	CryptoCarbon	CCRB	https://coinmarketcap.com//currencies/cryptocarbon/	0.01
1830	PluraCoin	PLURA	https://coinmarketcap.com//currencies/pluracoin/	0
1831	Kuverit	KUV	https://coinmarketcap.com//currencies/kuverit/	0
1832	Beer Money	BEER	https://coinmarketcap.com//currencies/beer-money/	0.01
1833	ParallelCoin	DUO	https://coinmarketcap.com//currencies/parallelcoin/	0.8
1834	Dogeswap	DOGES	https://coinmarketcap.com//currencies/dogeswap/	12.58
1835	Yfscience	YFSI	https://coinmarketcap.com//currencies/yfscience/	18.2
1836	Fortuna	FOTA	https://coinmarketcap.com//currencies/fortuna/	0
1837	HollyWoodCoin	HWC	https://coinmarketcap.com//currencies/hollywoodcoin/	0.01
1838	Sharpay	S	https://coinmarketcap.com//currencies/sharpay/	0
1839	PoSW Coin	POSW	https://coinmarketcap.com//currencies/posw-coin/	0.01
1840	MicroMoney	AMM	https://coinmarketcap.com//currencies/micromoney/	0.02
1841	Advanced Technology Coin	ARC	https://coinmarketcap.com//currencies/arcticcoin/	0.01
1842	CROAT	CROAT	https://coinmarketcap.com//currencies/croat/	0
1843	HeartBout	HB	https://coinmarketcap.com//currencies/heartbout/	0
1844	DOGEFI	DOGEFI	https://coinmarketcap.com//currencies/dogefi/	0.24
1845	Wrapped Virgin Gen-0 CryptoKitties	WVG0	https://coinmarketcap.com//currencies/wrapped-virgin-gen-0-cryptokitties/	690.14
1846	CommunityGeneration	CGEN	https://coinmarketcap.com//currencies/communitygeneration/	0
1847	Kobocoin	KOBO	https://coinmarketcap.com//currencies/kobocoin/	0.01
1848	Baz Token	BAZT	https://coinmarketcap.com//currencies/baz-token/	0.21
1849	Terracoin	TRC	https://coinmarketcap.com//currencies/terracoin/	0.01
1850	AceD	ACED	https://coinmarketcap.com//currencies/aced/	0
1851	Cobinhood	COB	https://coinmarketcap.com//currencies/cobinhood/	0
1852	DecentBet	DBET	https://coinmarketcap.com//currencies/decent-bet/	0
1853	ATC Coin	ATCC	https://coinmarketcap.com//currencies/atc-coin/	0
1854	MMOCoin	MMO	https://coinmarketcap.com//currencies/mmocoin/	0
1855	Cryptojacks	CJ	https://coinmarketcap.com//currencies/cryptojacks/	0
1856	Dollars	USDX	https://coinmarketcap.com//currencies/dollars/	0.12
1857	PAYCENT	PYN	https://coinmarketcap.com//currencies/paycent/	0
1858	Innova	INN	https://coinmarketcap.com//currencies/innova/	0.03
1859	CryptoEnergy	CNRG	https://coinmarketcap.com//currencies/cryptoenergy/	8
1860	Utrum	OOT	https://coinmarketcap.com//currencies/utrum/	0
1861	Deri Protocol	DERI	https://coinmarketcap.com//currencies/deri-protocol/	0.2
1862	Bonpay	BON	https://coinmarketcap.com//currencies/bonpay/	0.02
1863	Fiii	FIII	https://coinmarketcap.com//currencies/fiii/	0
1864	1Million Token	1MT	https://coinmarketcap.com//currencies/1million-token/	0.26
1865	Ryo Currency	RYO	https://coinmarketcap.com//currencies/ryo-currency/	0.01
1866	EveriToken	EVT	https://coinmarketcap.com//currencies/everitoken/	0.01
1867	Helix	HLIX	https://coinmarketcap.com//currencies/helix/	0.01
1868	FLUX	FLUX	https://coinmarketcap.com//currencies/flux/	0.61
1869	Simple Software Solutions	SSS	https://coinmarketcap.com//currencies/simple-software-solutions/	0.08
1870	Opal	OPAL	https://coinmarketcap.com//currencies/opal/	0.01
1871	Ubricoin	UBN	https://coinmarketcap.com//currencies/ubricoin/	0
1872	Netko	NETKO	https://coinmarketcap.com//currencies/netko/	0.02
1873	MorCrypto Coin	MOR	https://coinmarketcap.com//currencies/morcrypto-coin/	0.01
1874	Commercium	CMM	https://coinmarketcap.com//currencies/commercium/	0
1875	Hush	HUSH	https://coinmarketcap.com//currencies/hush/	0.02
1876	SuperCoin	SUPER	https://coinmarketcap.com//currencies/supercoin/	0
1877	TenUp	TUP	https://coinmarketcap.com//currencies/tenup/	0.01
1878	Digitalcoin	DGC	https://coinmarketcap.com//currencies/digitalcoin/	0.01
1879	Martkist	MARTK	https://coinmarketcap.com//currencies/martkist/	0.01
1880	Leadcoin	LDC	https://coinmarketcap.com//currencies/leadcoin/	0
1881	BBSCoin	BBS	https://coinmarketcap.com//currencies/bbscoin/	0
1882	DraftCoin	DFT	https://coinmarketcap.com//currencies/draftcoin/	0.02
1883	SHPING	SHPING	https://coinmarketcap.com//currencies/shping/	0
1884	Storiqa	STQ	https://coinmarketcap.com//currencies/storiqa/	0
1885	Inex Project	INEX	https://coinmarketcap.com//currencies/inex-project/	0
1886	TagCoin	TAG	https://coinmarketcap.com//currencies/tagcoin/	0.03
1887	Baguette Token	BGTT	https://coinmarketcap.com//currencies/baguette-token/	0.02
1888	YMPL	YMPL	https://coinmarketcap.com//currencies/ympl/	4.4
1889	TCASH	TCASH	https://coinmarketcap.com//currencies/tcash/	0.01
1890	Proton Token	PTT	https://coinmarketcap.com//currencies/proton-token/	0
1891	Agora	VOTE	https://coinmarketcap.com//currencies/agora/	0.01
1892	LHT	LHT	https://coinmarketcap.com//currencies/lighthouse-token/	0
1893	MotaCoin	MOTA	https://coinmarketcap.com//currencies/motacoin/	0.01
1894	MesChain	MES	https://coinmarketcap.com//currencies/meschain/	0
1895	Bela	BELA	https://coinmarketcap.com//currencies/belacoin/	0
1896	Citadel	CTL	https://coinmarketcap.com//currencies/citadel/	0.02
1897	Pyrk	PYRK	https://coinmarketcap.com//currencies/pyrk/	0.01
1898	Pirl	PIRL	https://coinmarketcap.com//currencies/pirl/	0
1899	Decentralized Machine Learning	DML	https://coinmarketcap.com//currencies/decentralized-machine-learning/	0
1900	Bitgesell	BGL	https://coinmarketcap.com//currencies/bitgesell/	0.02
1901	Bolivarcoin	BOLI	https://coinmarketcap.com//currencies/bolivarcoin/	0.01212
1902	Motocoin	MOTO	https://coinmarketcap.com//currencies/motocoin/	0.009452
1903	OSA Token	OSA	https://coinmarketcap.com//currencies/osa-token/	0.0001334
1904	TOKPIE	TKP	https://coinmarketcap.com//currencies/tokpie/	0.006454
1905	AquariusCoin	ARCO	https://coinmarketcap.com//currencies/aquariuscoin/	0.05343
1906	renDOGE	RENDOGE	https://coinmarketcap.com//currencies/rendoge/	0.05802
1907	Gentarium	GTM	https://coinmarketcap.com//currencies/gentarium/	0.03447
1908	Bitcoin Token	BTCT	https://coinmarketcap.com//currencies/bitcoin-token/	0.0139
1909	imbrex	REX	https://coinmarketcap.com//currencies/imbrex/	0.02051
1910	Arionum	ARO	https://coinmarketcap.com//currencies/arionum/	0.001112
1911	WinCash	WCC	https://coinmarketcap.com//currencies/wincash/	0.12
1912	Fire Lotto	FLOT	https://coinmarketcap.com//currencies/fire-lotto/	0.01
1913	Semux	SEM	https://coinmarketcap.com//currencies/semux/	0.01
1914	Chronologic	DAY	https://coinmarketcap.com//currencies/chronologic/	0.15
1915	PosEx	PEX	https://coinmarketcap.com//currencies/posex/	0.07
1916	GINcoin	GIN	https://coinmarketcap.com//currencies/gincoin/	0.02
1917	Taklimakan Network	TAN	https://coinmarketcap.com//currencies/taklimakan-network/	0
1918	SkinCoin	SKIN	https://coinmarketcap.com//currencies/skincoin/	0
1919	Thrive Token	THRT	https://coinmarketcap.com//currencies/thrive-token/	0
1920	More Coin	MORE	https://coinmarketcap.com//currencies/more-coin/	0.08
1921	Artis Turba	ARTIS	https://coinmarketcap.com//currencies/artis-turba/	0.01
1922	Adzcoin	ADZ	https://coinmarketcap.com//currencies/adzcoin/	0
1923	Fantasy Sports	DFS	https://coinmarketcap.com//currencies/fantasy-sports/	0.03
1924	EnterCoin	ENTRC	https://coinmarketcap.com//currencies/entercoin/	0.03
1925	wave edu coin	WEC	https://coinmarketcap.com//currencies/wave-edu-coin/	0
1926	Defis	XGM	https://coinmarketcap.com//currencies/defis/	0
1927	Photon	PHO	https://coinmarketcap.com//currencies/photon/	0
1928	Cashhand	CHND	https://coinmarketcap.com//currencies/cashhand/	0
1929	ITO Utility Token	IUT	https://coinmarketcap.com//currencies/ito-utility-token/	0
1930	CRDT	CRDT	https://coinmarketcap.com//currencies/crdt/	0.03
1931	Evil Coin	EVIL	https://coinmarketcap.com//currencies/evil-coin/	0.01
1932	TouchCon	TOC	https://coinmarketcap.com//currencies/touchcon/	0
1933	PopularCoin	POP	https://coinmarketcap.com//currencies/popularcoin/	0
1934	vSlice	VSL	https://coinmarketcap.com//currencies/vslice/	0
1935	GravityCoin	GXX	https://coinmarketcap.com//currencies/gravitycoin/	0.03
1936	Anoncoin	ANC	https://coinmarketcap.com//currencies/anoncoin/	0.07
1937	Donu	DONU	https://coinmarketcap.com//currencies/donu/	0.03
1938	PEPS Coin	PEPS	https://coinmarketcap.com//currencies/peps-coin/	0
1939	Bata	BTA	https://coinmarketcap.com//currencies/bata/	0.03
1940	Nasdacoin	NSD	https://coinmarketcap.com//currencies/nasdacoin/	0.01
1941	Litecoin Plus	LCP	https://coinmarketcap.com//currencies/litecoin-plus/	0.05
1942	TEAM (TokenStars)	TEAM	https://coinmarketcap.com//currencies/tokenstars/	0.01
1943	BitSend	BSD	https://coinmarketcap.com//currencies/bitsend/	0
1944	Emerald Crypto	EMD	https://coinmarketcap.com//currencies/emerald/	0.01
1945	Rebased	REB2	https://coinmarketcap.com//currencies/rebased/	0.95
1946	Monkey Project	MONK	https://coinmarketcap.com//currencies/monkey-project/	0.01
1947	Worldcore	WRC	https://coinmarketcap.com//currencies/worldcore/	0
1948	Mainstream For The Underground	MFTU	https://coinmarketcap.com//currencies/mainstream-for-the-underground/	0
1949	INRToken	INRT	https://coinmarketcap.com//currencies/inrtoken/	0
1950	Crowd Machine	CMCT	https://coinmarketcap.com//currencies/crowd-machine/	0
1951	HorusPay	HORUS	https://coinmarketcap.com//currencies/horuspay/	0
1952	SYB Coin	SYBC	https://coinmarketcap.com//currencies/syb-coin/	0.01
1953	VULCANO	VULC	https://coinmarketcap.com//currencies/vulcano/	0
1954	ZINC	ZINC	https://coinmarketcap.com//currencies/zinc/	0.02
1955	TRAXIA	TM2	https://coinmarketcap.com//currencies/traxia/	0
1956	Yocoin	YOC	https://coinmarketcap.com//currencies/yocoin/	0
1957	GCN Coin	GCN	https://coinmarketcap.com//currencies/gcn-coin/	0
1958	BitWhite	BTW	https://coinmarketcap.com//currencies/bitwhite/	0
1959	Boolberry	BBR	https://coinmarketcap.com//currencies/boolberry/	0.01
1960	MojoCoin	MOJO	https://coinmarketcap.com//currencies/mojocoin/	0.01
1961	Bitcoin Interest	BCI	https://coinmarketcap.com//currencies/bitcoin-interest/	0.01
1962	Aditus	ADI	https://coinmarketcap.com//currencies/aditus/	0
1963	Raise	RAISE	https://coinmarketcap.com//currencies/raise/	0.01
1964	W Green Pay	WGP	https://coinmarketcap.com//currencies/w-green-pay/	0.01
1965	Datarius Credit	DTRC	https://coinmarketcap.com//currencies/datarius-credit/	0
1966	Narrative	NRVE	https://coinmarketcap.com//currencies/narrative/	0
1967	PengolinCoin	PGO	https://coinmarketcap.com//currencies/pengolincoin/	0.01
1968	Global Currency Reserve	GCR	https://coinmarketcap.com//currencies/global-currency-reserve/	0
1969	Rupee	RUP	https://coinmarketcap.com//currencies/rupee/	0
1970	EtherInc	ETI	https://coinmarketcap.com//currencies/etherinc/	0
1971	Cheesecoin	CHEESE	https://coinmarketcap.com//currencies/cheesecoin/	0
1972	Zetacoin	ZET	https://coinmarketcap.com//currencies/zetacoin/	0
1973	BTC Lite	BTCL	https://coinmarketcap.com//currencies/btc-lite/	0.01
1974	InterValue	INVE	https://coinmarketcap.com//currencies/intervalue/	0
1975	BitCoen	BEN	https://coinmarketcap.com//currencies/bitcoen/	0.02
1976	Bitcoin Zero	BZX	https://coinmarketcap.com//currencies/bitcoin-zero/	0
1977	Silverway	SLV	https://coinmarketcap.com//currencies/silverway/	0
1978	Cyber Movie Chain	CMCT	https://coinmarketcap.com//currencies/cyber-movie-chain/	0
1979	Rimbit	RBT	https://coinmarketcap.com//currencies/rimbit/	0
1980	Ignition	IC	https://coinmarketcap.com//currencies/ignition/	0.08
1981	Dynamite	DYNMT	https://coinmarketcap.com//currencies/dynamite/	0.29
1982	Bitcoin Incognito	XBI	https://coinmarketcap.com//currencies/bitcoin-incognito/	0.01
1983	EUNOMIA	ENTS	https://coinmarketcap.com//currencies/eunomia/	0
1984	DABANKING	DAB	https://coinmarketcap.com//currencies/dabanking/	0.02
1985	MktCoin	MLM	https://coinmarketcap.com//currencies/mktcoin/	0
1986	Elysian	ELY	https://coinmarketcap.com//currencies/elysian/	0
1987	Xuez	XUEZ	https://coinmarketcap.com//currencies/xuez/	0.03
1988	Veros	VRS	https://coinmarketcap.com//currencies/veros/	0.09
1989	Rubies	RBIES	https://coinmarketcap.com//currencies/rubies/	0.01
1990	Micromines	MICRO	https://coinmarketcap.com//currencies/micromines/	0
1991	Eroscoin	ERO	https://coinmarketcap.com//currencies/eroscoin/	0
1992	Fivebalance	FBN	https://coinmarketcap.com//currencies/fivebalance/	0
1993	Kurrent	KURT	https://coinmarketcap.com//currencies/kurrent/	0
1994	Ratecoin	XRA	https://coinmarketcap.com//currencies/ratecoin/	0
1995	SteepCoin	STEEP	https://coinmarketcap.com//currencies/steepcoin/	0
1996	ProxyNode	PRX	https://coinmarketcap.com//currencies/proxynode/	0
1997	ShowHand	HAND	https://coinmarketcap.com//currencies/showhand/	0
1998	MFCoin	MFC	https://coinmarketcap.com//currencies/mfcoin/	0
1999	Bintex Futures	BNTX	https://coinmarketcap.com//currencies/bintex-futures/	0.98
2000	PlayGame	PXG	https://coinmarketcap.com//currencies/playgame/	0
2001	Era Swap	ES	https://coinmarketcap.com//currencies/era-swap/	0.005035
2002	Abitshadow Token	ABST	https://coinmarketcap.com//currencies/abitshadow-token/	0.0002086
2003	Helex	HLX	https://coinmarketcap.com//currencies/helex/	1.4
2004	YENTEN	YTN	https://coinmarketcap.com//currencies/yenten/	0.004025
2005	Civitas	CIV	https://coinmarketcap.com//currencies/civitas/	0.01223
2006	FirstCoin	FRST	https://coinmarketcap.com//currencies/firstcoin/	0.003122
2007	Plus-Coin	NPLC	https://coinmarketcap.com//currencies/plus-coin/	0.0002063
2008	Truegame	TGAME	https://coinmarketcap.com//currencies/tgame/	0.001168
2009	SiaCashCoin	SCC	https://coinmarketcap.com//currencies/siacashcoin/	6.29e-06
2010	STRAKS	STAK	https://coinmarketcap.com//currencies/straks/	0.004003
2011	Waletoken	WTN	https://coinmarketcap.com//currencies/waletoken/	0
2012	Neuromorphic.io	NMP	https://coinmarketcap.com//currencies/neuromorphic-io/	0.02
2013	WeBlock	WON	https://coinmarketcap.com//currencies/weblock/	0.02
2014	Bitcoin CZ	BCZ	https://coinmarketcap.com//currencies/bitcoin-cz/	0.03
2015	Unify	UNIFY	https://coinmarketcap.com//currencies/unify/	0
2016	CBDAO	BREE	https://coinmarketcap.com//currencies/cbdao/	0.09
2017	Zloadr	ZDR	https://coinmarketcap.com//currencies/zloadr/	0.02
2018	Provoco Token	VOCO	https://coinmarketcap.com//currencies/provoco-token/	0
2019	Paytomat	PTI	https://coinmarketcap.com//currencies/paytomat/	0
2020	Coin2.1	C2	https://coinmarketcap.com//currencies/coin2-1/	0
2021	Thingschain	TIC	https://coinmarketcap.com//currencies/thingschain/	0
2022	GoldFund	GFUN	https://coinmarketcap.com//currencies/goldfund/	0
2023	SafeCapital	SCAP	https://coinmarketcap.com//currencies/safecapital/	0.05
2024	SF Capital	SFCP	https://coinmarketcap.com//currencies/sf-capital/	0
2025	Veles	VLS	https://coinmarketcap.com//currencies/veles/	0.06
2026	FSBT API Token	FSBT	https://coinmarketcap.com//currencies/fsbt-api-token/	0.02
2027	YFFII Finance	YFFII	https://coinmarketcap.com//currencies/yffii-finance/	2.64
2028	StarDEX	XSTAR	https://coinmarketcap.com//currencies/stardex/	0.01
2029	BERNcash	BERN	https://coinmarketcap.com//currencies/berncash/	0
2030	GuccioneCoin	GCC	https://coinmarketcap.com//currencies/guccionecoin/	0
2031	Niobio Cash	NBR	https://coinmarketcap.com//currencies/niobio-cash/	0
2032	IGToken	IG	https://coinmarketcap.com//currencies/igtoken/	0
2033	MalwareChain	MALW	https://coinmarketcap.com//currencies/malwarechain/	0.01
2034	Bitcoin Free Cash	BFC	https://coinmarketcap.com//currencies/bitcoin-free-cash/	0.02
2035	yTSLA Finance	yTSLA	https://coinmarketcap.com//currencies/ytsla-finance/	1.13
2036	Kush Finance	KSEED	https://coinmarketcap.com//currencies/kush-finance/	0.27
2037	CPUchain	CPU	https://coinmarketcap.com//currencies/cpuchain/	0
2038	ebakus	EBK	https://coinmarketcap.com//currencies/ebakus/	0
2039	Biotron	BTRN	https://coinmarketcap.com//currencies/biotron/	0
2040	WABnetwork	WAB	https://coinmarketcap.com//currencies/wabnetwork/	0
2041	Acoin	ACOIN	https://coinmarketcap.com//currencies/acoin/	0.06
2042	BoutsPro	BOUTS	https://coinmarketcap.com//currencies/boutspro/	0
2043	Universal Currency	UNIT	https://coinmarketcap.com//currencies/universal-currency/	0
2044	Fox Trading	FOXT	https://coinmarketcap.com//currencies/fox-trading/	0.01
2045	Swarm City	SWT	https://coinmarketcap.com//currencies/swarm-city/	0.01
2046	Webcoin	WEB	https://coinmarketcap.com//currencies/webcoin/	0
2047	S.Finance	SFG	https://coinmarketcap.com//currencies/s-finance/	0.83
2048	ATBCoin	ATB	https://coinmarketcap.com//currencies/atbcoin/	0
2049	PureVidz	VIDZ	https://coinmarketcap.com//currencies/purevidz/	0
2050	Kind Ads Token	KIND	https://coinmarketcap.com//currencies/kind-ads-token/	0
2051	Giant	GIC	https://coinmarketcap.com//currencies/giant-coin/	0.01
2052	EvenCoin	EVN	https://coinmarketcap.com//currencies/evencoin/	0
2053	Arion	ARION	https://coinmarketcap.com//currencies/arion/	0.01
2054	VoteCoin	VOT	https://coinmarketcap.com//currencies/votecoin/	0
2055	Privatix	PRIX	https://coinmarketcap.com//currencies/privatix/	0.06
2056	GlobalToken	GLT	https://coinmarketcap.com//currencies/globaltoken/	0
2057	Ethereum Gold Project	ETGP	https://coinmarketcap.com//currencies/ethereum-gold-project/	0
2058	CFun	CFUN	https://coinmarketcap.com//currencies/cfun/	0
2059	Blocklancer	LNC	https://coinmarketcap.com//currencies/blocklancer/	0
2060	Yearn Finance Bit	YFBT	https://coinmarketcap.com//currencies/yearn-finance-bit/	60.36
2061	Freyrchain	FREC	https://coinmarketcap.com//currencies/freyrchain/	0
2062	Arqma	ARQ	https://coinmarketcap.com//currencies/arqma/	0.01
2063	PutinCoin	PUT	https://coinmarketcap.com//currencies/putincoin/	0
2064	Cipher	CPR	https://coinmarketcap.com//currencies/cipher/	0
2065	YFIVE FINANCE	YFIVE	https://coinmarketcap.com//currencies/yfive-finance/	3.57
2066	SmartCoin	SMC	https://coinmarketcap.com//currencies/smartcoin/	0
2067	Atheios	ATH	https://coinmarketcap.com//currencies/atheios/	0
2068	MedicCoin	MEDIC	https://coinmarketcap.com//currencies/mediccoin/	0
2069	INDINODE	XIND	https://coinmarketcap.com//currencies/indinode/	0
2070	Universe	UNI	https://coinmarketcap.com//currencies/universe/	0
2071	ELTCOIN	ELTCOIN	https://coinmarketcap.com//currencies/eltcoin/	0
2072	BitRewards	BIT	https://coinmarketcap.com//currencies/bitrewards/	0
2073	Scanetchain	SWC	https://coinmarketcap.com//currencies/scanetchain/	0
2074	GoldBlocks	GB	https://coinmarketcap.com//currencies/goldblocks/	0
2075	CoinUs	CNUS	https://coinmarketcap.com//currencies/coinus/	0
2076	IFX24	IFX24	https://coinmarketcap.com//currencies/ifx24/	0.02
2077	BitGuild PLAT	PLAT	https://coinmarketcap.com//currencies/bitguild-plat/	0
2078	Onix	ONX	https://coinmarketcap.com//currencies/onix/	0
2079	ICOBID	ICOB	https://coinmarketcap.com//currencies/icobid/	0
2080	UNICORN Token	UNI	https://coinmarketcap.com//currencies/unicorn-token/	0
2081	Jade Currency	JADE	https://coinmarketcap.com//currencies/jade-currency/	0
2082	StarterCoin	STAC	https://coinmarketcap.com//currencies/startercoin/	0
2083	Digital Money Bits	DMB	https://coinmarketcap.com//currencies/digital-money-bits/	0
2084	MESEFA	SEFA	https://coinmarketcap.com//currencies/mesefa/	0.14
2085	Tourist Token	TOTO	https://coinmarketcap.com//currencies/tourist-token/	0
2086	Bitcoiin	B2G	https://coinmarketcap.com//currencies/bitcoiin/	0
2087	Zuflo Coin	ZFL	https://coinmarketcap.com//currencies/zuflo-coin/	0
2088	ArbitrageCT	ARCT	https://coinmarketcap.com//currencies/arbitragect/	0
2089	Bitnation	XPAT	https://coinmarketcap.com//currencies/bitnation/	0
2090	Trexcoin	TREX	https://coinmarketcap.com//currencies/trexcoin/	0
2091	ALL BEST ICO	ALLBI	https://coinmarketcap.com//currencies/all-best-ico/	0
2092	Theresa May Coin	MAY	https://coinmarketcap.com//currencies/theresa-may-coin/	0
2093	Peet DeFi	PTE	https://coinmarketcap.com//currencies/peet-defi/	1.39
2094	Cream	CRM	https://coinmarketcap.com//currencies/cream/	0
2095	Blakecoin	BLC	https://coinmarketcap.com//currencies/blakecoin/	0
2096	Bitcoin Adult	BTAD	https://coinmarketcap.com//currencies/bitcoin-adult/	0
2097	Herbalist Token	HERB	https://coinmarketcap.com//currencies/herbalist-token/	0
2098	BLAST	BLAST	https://coinmarketcap.com//currencies/blast/	0
2099	PostCoin	POST	https://coinmarketcap.com//currencies/postcoin/	0
2100	Neural Protocol	NRP	https://coinmarketcap.com//currencies/neural-protocol/	0
2101	Digiwage	WAGE	https://coinmarketcap.com//currencies/digiwage/	0.001112
2102	Knekted	KNT	https://coinmarketcap.com//currencies/knekted/	5.185e-05
2103	EthereumX	ETX	https://coinmarketcap.com//currencies/ethereumx/	0.0005856
2104	INMAX	INX	https://coinmarketcap.com//currencies/inmax/	0.01972
2105	ETHplode	ETHPLO	https://coinmarketcap.com//currencies/ethplode/	0.001149
2106	Compound Coin	COMP	https://coinmarketcap.com//currencies/compound-coin/	9.284e-07
2107	iBTC	IBTC	https://coinmarketcap.com//currencies/ibtc/	0.0009875
2108	Bitradio	BRO	https://coinmarketcap.com//currencies/bitradio/	0.005
2109	Zurcoin	ZUR	https://coinmarketcap.com//currencies/zurcoin/	0.000556
2110	Kemacoin	KEMA	https://coinmarketcap.com//currencies/kemacoin/	0.002002
2111	Decentralized Asset Trading Platform	DATP	https://coinmarketcap.com//currencies/decentralized-asset-trading-platform/	0
2112	FidexToken	FEX	https://coinmarketcap.com//currencies/fidex-token/	0
2113	SnodeCoin	SND	https://coinmarketcap.com//currencies/snodecoin/	0
2114	MultiCoinCasino	MCC	https://coinmarketcap.com//currencies/multicoincasino/	0.01
2115	Cryptrust	CTRT	https://coinmarketcap.com//currencies/cryptrust/	0
2116	FUZE Token	FUZE	https://coinmarketcap.com//currencies/fuze-token/	60.93
2117	FuzzBalls	FUZZ	https://coinmarketcap.com//currencies/fuzzballs/	0.01
2118	Arepacoin	AREPA	https://coinmarketcap.com//currencies/arepacoin/	0
2119	ACE (TokenStars)	ACE	https://coinmarketcap.com//currencies/ace/	0
2120	DistX	DISTX	https://coinmarketcap.com//currencies/distx/	0
2121	Obee Network	OBEE	https://coinmarketcap.com//currencies/obee-network/	0
2122	Dreamcoin	DRM	https://coinmarketcap.com//currencies/dreamcoin/	0.02
2123	HyperQuant	HQT	https://coinmarketcap.com//currencies/hyperquant/	0
2124	PYRO Network	PYRO	https://coinmarketcap.com//currencies/pyro-network/	0
2125	The Currency Analytics	TCAT	https://coinmarketcap.com//currencies/the-currency-analytics/	0
2126	Fireball	FIRE	https://coinmarketcap.com//currencies/fireball/	2.16
2127	Connect Coin	XCON	https://coinmarketcap.com//currencies/connect-coin/	0
2128	Centurion	CNT	https://coinmarketcap.com//currencies/centurion/	0
2129	CyberMusic	CYMT	https://coinmarketcap.com//currencies/cybermusic/	0
2130	Save Environment Token	SET	https://coinmarketcap.com//currencies/save-environment-token/	0.04
2131	SparksPay	SPK	https://coinmarketcap.com//currencies/sparkspay/	0
2132	Bitzeny	ZNY	https://coinmarketcap.com//currencies/bitzeny/	0
2133	Italo	XTA	https://coinmarketcap.com//currencies/italo/	0
2134	Kalkulus	KLKS	https://coinmarketcap.com//currencies/kalkulus/	0
2135	BLOC.MONEY	BLOC	https://coinmarketcap.com//currencies/bloc-money/	0
2136	EDRCoin	EDRC	https://coinmarketcap.com//currencies/edrcoin/	0.01
2137	ZeusNetwork	ZEUS	https://coinmarketcap.com//currencies/zeusnetwork/	0
2138	Joint Ventures	JOINT	https://coinmarketcap.com//currencies/joint-ventures/	0
2139	ANON	ANON	https://coinmarketcap.com//currencies/anon/	0.01
2140	Yield Stake Finance	YI12	https://coinmarketcap.com//currencies/yield-stake-finance/	3.41
2141	Blockburn	BURN	https://coinmarketcap.com//currencies/blockburn/	0
2142	Celeum	CLX	https://coinmarketcap.com//currencies/celeum/	0.01
2143	BowsCoin	BSC	https://coinmarketcap.com//currencies/bowscoin/	0.01
2144	PRiVCY	PRIV	https://coinmarketcap.com//currencies/privcy/	0
2145	Blur	BLUR	https://coinmarketcap.com//currencies/blur/	0.01
2146	DNotes	NOTE	https://coinmarketcap.com//currencies/dnotes/	0
2147	Suretly	SUR	https://coinmarketcap.com//currencies/suretly/	0.15
2148	Gossip Coin	GOSS	https://coinmarketcap.com//currencies/gossipcoin/	0
2149	UChain	UCN	https://coinmarketcap.com//currencies/uchain/	0
2150	Payfair	PFR	https://coinmarketcap.com//currencies/payfair/	0
2151	BitcoiNote	BTCN	https://coinmarketcap.com//currencies/bitcoinote/	0
2152	Save and Gain	SANDG	https://coinmarketcap.com//currencies/save-and-gain/	0.01
2153	Mchain	MAR	https://coinmarketcap.com//currencies/mchain/	0
2154	Iridium	IRD	https://coinmarketcap.com//currencies/iridium/	0
2155	GeyserCoin	GSR	https://coinmarketcap.com//currencies/geysercoin/	0.02
2156	Nekonium	NUKO	https://coinmarketcap.com//currencies/nekonium/	0
2157	PayCoin	XPY	https://coinmarketcap.com//currencies/paycoin2/	0
2158	TrumpCoin	TRUMP	https://coinmarketcap.com//currencies/trumpcoin/	0.01
2159	Apollon	XAP	https://coinmarketcap.com//currencies/apollon/	0
2160	EXMR FDN	EXMR	https://coinmarketcap.com//currencies/exmr-fdn/	0.01
2161	Intelligent Trading Foundation	ITT	https://coinmarketcap.com//currencies/intelligent-trading-foundation/	0
2162	BitCoal	COAL	https://coinmarketcap.com//currencies/bitcoal/	0.01
2163	KWHCoin	KWH	https://coinmarketcap.com//currencies/kwhcoin/	0
2164	Galilel	GALI	https://coinmarketcap.com//currencies/galilel/	0
2165	Zenswap Network Token	ZNT	https://coinmarketcap.com//currencies/zenswap-network-token/	0
2166	Halving Token	HALV	https://coinmarketcap.com//currencies/halving-coin/	0.02
2167	Netrum	NTR	https://coinmarketcap.com//currencies/netrum/	0.01
2168	SCRIV NETWORK	SCRIV	https://coinmarketcap.com//currencies/scriv-network/	0
2169	Global Crypto Alliance	CALL	https://coinmarketcap.com//currencies/global-crypto-alliance/	0
2170	Shivers	SHVR	https://coinmarketcap.com//currencies/shivers/	0
2171	NevaCoin	NEVA	https://coinmarketcap.com//currencies/nevacoin/	0.01
2172	Ultragate	ULG	https://coinmarketcap.com//currencies/ultragate/	0
2173	APR Coin	APR	https://coinmarketcap.com//currencies/apr-coin/	0
2174	SWYFT	SWYFTT	https://coinmarketcap.com//currencies/swyft/	0.04
2175	ParkByte	PKB	https://coinmarketcap.com//currencies/parkbyte/	0.01
2176	Newton Coin Project	NCP	https://coinmarketcap.com//currencies/newton-coin-project/	0
2177	Natmin Pure Escrow	NAT	https://coinmarketcap.com//currencies/natmin-pure-escrow/	0
2178	Thore Cash	TCH	https://coinmarketcap.com//currencies/thore-cash/	0
2179	MustangCoin	MST	https://coinmarketcap.com//currencies/mustangcoin/	0.05
2180	Exosis	EXO	https://coinmarketcap.com//currencies/exosis/	0.05
2181	ROIyal Coin	ROCO	https://coinmarketcap.com//currencies/roiyal-coin/	0.02
2182	AmsterdamCoin	AMS	https://coinmarketcap.com//currencies/amsterdamcoin/	0
2183	Bionic	BNC	https://coinmarketcap.com//currencies/bionic/	0
2184	X-Coin	XCO	https://coinmarketcap.com//currencies/x-coin/	0
2185	BitMoney	BIT	https://coinmarketcap.com//currencies/bitmoney/	0
2186	Spectrum	SPT	https://coinmarketcap.com//currencies/spectrum/	0
2187	Mobile Crypto Pay Coin	MCPC	https://coinmarketcap.com//currencies/mobile-crypto-pay-coin/	0
2188	Zealium	NZL	https://coinmarketcap.com//currencies/zealium/	0
2189	Carebit	CARE	https://coinmarketcap.com//currencies/carebit/	0
2190	Californium	CF	https://coinmarketcap.com//currencies/californium/	0.01
2191	Ether Kingdoms Token	IMP	https://coinmarketcap.com//currencies/ether-kingdoms-token/	0
2192	Vivid Coin	VIVID	https://coinmarketcap.com//currencies/vivid-coin/	0
2193	Guider	GDR	https://coinmarketcap.com//currencies/guider/	0
2194	Dollarcoin	DLC	https://coinmarketcap.com//currencies/dollarcoin/	0
2195	SpreadCoin	SPR	https://coinmarketcap.com//currencies/spreadcoin/	0
2196	ARbit	ARB	https://coinmarketcap.com//currencies/arbit/	0
2197	MarteXcoin	MXT	https://coinmarketcap.com//currencies/martexcoin/	0.01
2198	TravelNote	TVNT	https://coinmarketcap.com//currencies/travelnote/	0.01
2199	Castle	CSTL	https://coinmarketcap.com//currencies/castle/	0
2200	Mining Core Coin	MCC	https://coinmarketcap.com//currencies/mining-core/	0
2201	TajCoin	TAJ	https://coinmarketcap.com//currencies/tajcoin/	0.001112
2202	Desire	DSR	https://coinmarketcap.com//currencies/desire/	0.002224
2203	Squorum	SQR	https://coinmarketcap.com//currencies/squorum/	0.001668
2204	noob.finance	$NOOB	https://coinmarketcap.com//currencies/noob-finance/	1.02
2205	Quantis Network	QUAN	https://coinmarketcap.com//currencies/quantis-network/	0.000556
2206	Datacoin	DTC	https://coinmarketcap.com//currencies/datacoin/	0.000556
2207	Kabberry Coin	KKC	https://coinmarketcap.com//currencies/kabberry-coin/	0.001112
2208	Bitcoin 21	XBTC21	https://coinmarketcap.com//currencies/bitcoin-21/	0.0278
2209	BoatPilot Token	NAVY	https://coinmarketcap.com//currencies/boat-pilot-token/	0.000296
2210	Diligence	IRA	https://coinmarketcap.com//currencies/diligence/	0.00104
2211	Phantomx	PNX	https://coinmarketcap.com//currencies/phantomx/	0
2212	Akroma	AKA	https://coinmarketcap.com//currencies/akroma/	0
2213	onLEXpa	onLEXpa	https://coinmarketcap.com//currencies/onlexpa/	0
2214	Zayedcoin	ZYD	https://coinmarketcap.com//currencies/zayedcoin/	0
2215	Eurocoin	EUC	https://coinmarketcap.com//currencies/eurocoin/	0
2216	Hyper Speed Network	HSN	https://coinmarketcap.com//currencies/hyper-speed-network/	0
2217	Impleum	IMPL	https://coinmarketcap.com//currencies/impleum/	0
2218	KUN	KUN	https://coinmarketcap.com//currencies/kun/	10.2
2219	PlatinumBAR	XPTX	https://coinmarketcap.com//currencies/platinumbar/	0.01
2220	CrevaCoin	CREVA	https://coinmarketcap.com//currencies/crevacoin/	0
2221	XDNA	XDNA	https://coinmarketcap.com//currencies/xdna/	0
2222	PLNcoin	PLNC	https://coinmarketcap.com//currencies/plncoin/	0
2223	Paypex	PAYX	https://coinmarketcap.com//currencies/paypex/	0
2224	EagleX	EGX	https://coinmarketcap.com//currencies/eaglex/	0
2225	Sparkle Loyalty	SPRKL	https://coinmarketcap.com//currencies/sparkle-loyalty/	0
2226	GoHelpFund	HELP	https://coinmarketcap.com//currencies/gohelpfund/	0
2227	FUTURAX	FTXT	https://coinmarketcap.com//currencies/futurax/	0
2228	4NEW	KWATT	https://coinmarketcap.com//currencies/4new/	0
2229	Flowchain	FLC	https://coinmarketcap.com//currencies/flowchain/	0.02
2230	SongCoin	SONG	https://coinmarketcap.com//currencies/songcoin/	0
2231	Nyerium	NYEX	https://coinmarketcap.com//currencies/nyerium/	0
2232	SkyHub Coin	SHB	https://coinmarketcap.com//currencies/skyhub-coin/	0.03
2233	XOVBank	XOV	https://coinmarketcap.com//currencies/xovbank/	0
2234	Bitcoinus	BITS	https://coinmarketcap.com//currencies/bitcoinus/	0
2235	Feellike	FLL	https://coinmarketcap.com//currencies/feellike/	0.01
2236	Galactrum	ORE	https://coinmarketcap.com//currencies/galactrum/	0
2237	ImageCash	IMGC	https://coinmarketcap.com//currencies/imagecash/	0
2238	1X2 COIN	1X2	https://coinmarketcap.com//currencies/1x2-coin/	0
2239	Litecred	LTCR	https://coinmarketcap.com//currencies/litecred/	0
2240	aWSB	aWSB	https://coinmarketcap.com//currencies/awsb/	18.76
2241	Business Credit Alliance Chain	BCAC	https://coinmarketcap.com//currencies/business-credit-alliance-chain/	0
2242	Mirai	MRI	https://coinmarketcap.com//currencies/mirai/	0
2243	Electrum Dark	ELD	https://coinmarketcap.com//currencies/electrum-dark-eld/	0
2244	Klimatas	KTS	https://coinmarketcap.com//currencies/klimatas/	0.02
2245	Quebecoin	QBC	https://coinmarketcap.com//currencies/quebecoin/	0
2246	Streamit Coin	STREAM	https://coinmarketcap.com//currencies/streamit-coin/	0
2247	SpeedCash	SCS	https://coinmarketcap.com//currencies/speedcash/	0.03
2248	Grimm	GRIMM	https://coinmarketcap.com//currencies/grimm/	0
2249	Paparazzi	PAZZI	https://coinmarketcap.com//currencies/paparazzi/	0
2250	LuckySevenToken	LST	https://coinmarketcap.com//currencies/luckyseventoken/	0
2251	GenesisX	XGS	https://coinmarketcap.com//currencies/genesisx/	0
2252	Bulleon	BUL	https://coinmarketcap.com//currencies/bulleon/	0.01
2253	Prime-XI	PXI	https://coinmarketcap.com//currencies/prime-xi/	0
2254	MiloCoin	MILO	https://coinmarketcap.com//currencies/milocoin/	0
2255	IBStoken	IBS	https://coinmarketcap.com//currencies/ibstoken/	0.01
2256	Cabbage	CAB	https://coinmarketcap.com//currencies/cabbage/	0
2257	YoloCash	YLC	https://coinmarketcap.com//currencies/yolocash/	0
2258	CCUniverse	UVU	https://coinmarketcap.com//currencies/ccuniverse/	0
2259	Veltor	VLT	https://coinmarketcap.com//currencies/veltor/	0.02
2260	JavaScript Token	JS	https://coinmarketcap.com//currencies/javascript-token/	0
2261	ETHPlus	ETHP	https://coinmarketcap.com//currencies/ethplus/	0.01
2262	Soverain	SOVE	https://coinmarketcap.com//currencies/soverain/	0
2263	XMCT	XMCT	https://coinmarketcap.com//currencies/xmct/	0
2264	VectorAI	VEC2	https://coinmarketcap.com//currencies/vector/	0
2265	High Voltage	HVCO	https://coinmarketcap.com//currencies/high-voltage/	0.01
2266	Alpha Coin	APC	https://coinmarketcap.com//currencies/alpha-coin/	0
2267	Bitvolt	VOLT	https://coinmarketcap.com//currencies/bitvolt/	0
2268	MDtoken	MDTK	https://coinmarketcap.com//currencies/mdtoken/	0
2269	Bitcoin Classic	BXC	https://coinmarketcap.com//currencies/bitcoin-classic/	0.02
2270	Ragnarok	RAGNA	https://coinmarketcap.com//currencies/ragnarok/	0
2271	Independent Money System	IMS	https://coinmarketcap.com//currencies/independent-money-system/	0
2272	Agrolot	AGLT	https://coinmarketcap.com//currencies/agrolot/	0
2273	Dash Green	DASHG	https://coinmarketcap.com//currencies/dash-green/	0
2274	Luna Coin	LUNA	https://coinmarketcap.com//currencies/luna-coin/	0.01
2275	Ccore	CCO	https://coinmarketcap.com//currencies/ccore/	0
2276	LitecoinToken	LTK	https://coinmarketcap.com//currencies/litecoin-token/	0
2277	MoneroV 	XMV	https://coinmarketcap.com//currencies/monero-v/	0
2278	BriaCoin	BRIA	https://coinmarketcap.com//currencies/briacoin/	0.01
2279	Wild Beast Block	WBB	https://coinmarketcap.com//currencies/wild-beast-block/	0.04
2280	Cannation	CNNC	https://coinmarketcap.com//currencies/cannation/	0
2281	BoostCoin	BOST	https://coinmarketcap.com//currencies/boostcoin/	0
2282	PAXEX	PAXEX	https://coinmarketcap.com//currencies/paxex/	0
2283	Comet	CMT	https://coinmarketcap.com//currencies/comet/	0.01
2284	KZ Cash	KZC	https://coinmarketcap.com//currencies/kz-cash/	0
2285	SONO	SONO	https://coinmarketcap.com//currencies/altcommunity-coin/	0
2286	LiteCoin Ultra	LTCU	https://coinmarketcap.com//currencies/litecoin-ultra/	0.01
2287	iBank	IBANK	https://coinmarketcap.com//currencies/ibank/	0
2288	Storeum	STO	https://coinmarketcap.com//currencies/storeum/	0
2289	Coinonat	CXT	https://coinmarketcap.com//currencies/coinonat/	0
2290	SocialCoin	SOCC	https://coinmarketcap.com//currencies/socialcoin-socc/	0
2291	Abulaba	AAA	https://coinmarketcap.com//currencies/abulaba/	0
2292	Pamp Network	PAMP	https://coinmarketcap.com//currencies/pamp-network/	0
2293	Magic Cube Coin	MCC	https://coinmarketcap.com//currencies/magic-cube-coin/	0
2294	MNPCoin	MNP	https://coinmarketcap.com//currencies/mnpcoin/	0
2295	Crystal Token	CYL	https://coinmarketcap.com//currencies/crystal-token/	0.01
2296	Staker	STR	https://coinmarketcap.com//currencies/staker/	0
2297	Iconic	ICON	https://coinmarketcap.com//currencies/iconic/	0
2298	Gold Poker	GPKR	https://coinmarketcap.com//currencies/gold-poker/	0
2299	Dinero	DIN	https://coinmarketcap.com//currencies/dinero/	0
2300	Stakinglab	LABX	https://coinmarketcap.com//currencies/stakinglab/	0.01
2301	Qbic	QBIC	https://coinmarketcap.com//currencies/qbic/	0.000528
2302	WXCOINS	WXC	https://coinmarketcap.com//currencies/wxcoins/	5.56e-05
2303	HEIDI	HDI	https://coinmarketcap.com//currencies/heidi/	0.001833
2304	Project-X	NANOX	https://coinmarketcap.com//currencies/project-x/	18849.92
2305	Benz	BENZ	https://coinmarketcap.com//currencies/benz/	0.0002442
2306	PonziCoin	PONZI	https://coinmarketcap.com//currencies/ponzicoin/	0.001668
2307	Argus	ARGUS	https://coinmarketcap.com//currencies/argus/	0.001112
2308	Concoin	CONX	https://coinmarketcap.com//currencies/concoin/	0.001111
2309	Flit Token	FLT	https://coinmarketcap.com//currencies/flit-token/	1.9e-07
2310	Money Plant Token	MPT	https://coinmarketcap.com//currencies/money-plant-token/	0.000395
2311	Innovative Bioresearch Classic	INNBCL	https://coinmarketcap.com//currencies/innovative-bioresearch-classic/	0
2312	Helpico	HELP	https://coinmarketcap.com//currencies/helpico/	0.01
2313	Nibble	NBXC	https://coinmarketcap.com//currencies/nibble/	0
2314	WETH	WETH	https://coinmarketcap.com//currencies/weth/	1791.76
2315	Wrapped Huobi Token	WHT	https://coinmarketcap.com//currencies/wrapped-huobi-token/	14.79
2316	Klaytn	KLAY	https://coinmarketcap.com//currencies/klaytn/	3.19
2317	Xeno Token	XNO	https://coinmarketcap.com//currencies/xeno-token/	0.63
2318	PayProtocol	PCI	https://coinmarketcap.com//currencies/payprotocol/	2.27
2319	Oxygen	OXY	https://coinmarketcap.com//currencies/oxygen/	2.97
2320	Global China Cash	CNC	https://coinmarketcap.com//currencies/global-china-cash/	0.15
2321	PlayDapp	PLA	https://coinmarketcap.com//currencies/playdapp/	0.62
2322	Hdac	HDAC	https://coinmarketcap.com//currencies/hdac/	0.06
2323	Amun Bitcoin 3x Daily Long	BTC3L	https://coinmarketcap.com//currencies/amun-bitcoin-3x-daily-long/	11.73
2324	STING	STN	https://coinmarketcap.com//currencies/sting/	37.32
2325	Compound Dai	CDAI	https://coinmarketcap.com//currencies/compound-dai/	0.02
2326	BTCUP	BTCUP	https://coinmarketcap.com//currencies/btcup/	169.28
2327	ECOMI	OMI	https://coinmarketcap.com//currencies/ecomi/	0.01
2328	Compound USD Coin	CUSDC	https://coinmarketcap.com//currencies/compound-usd-coin/	0.02
2329	Ontology Gas	ONG	https://coinmarketcap.com//currencies/ontology-gas/	0.72
2330	Amun Ether 3x Daily Short	ETH3S	https://coinmarketcap.com//currencies/amun-ether-3x-daily-short/	0.01
2331	Amun Ether 3x Daily Long	ETH3L	https://coinmarketcap.com//currencies/amun-ether-3x-daily-long/	2.2
2332	WEMIX	WEMIX	https://coinmarketcap.com//currencies/wemix/	0.46
2333	MOTIV Protocol	MOV	https://coinmarketcap.com//currencies/motiv-protocol/	0.02
2334	CELEBPLUS	CELEB	https://coinmarketcap.com//currencies/celebplus/	0.87
2335	BTCDOWN	BTCDOWN	https://coinmarketcap.com//currencies/btcdown/	0.05
2336	Amun Bitcoin 3x Daily Short	BTC3S	https://coinmarketcap.com//currencies/amun-bitcoin-3x-daily-short/	0
2337	Basis Gold	BAG	https://coinmarketcap.com//currencies/bag/	0.58
2338	OKExChain	OKT	https://coinmarketcap.com//currencies/okexchain/	123.76
2339	PicaArtMoney	PICA	https://coinmarketcap.com//currencies/picaartmoney/	0.12
2340	VEROX	VRX	https://coinmarketcap.com//currencies/verox/	1134.23
2569	TosDis	DIS	https://coinmarketcap.com//currencies/tosdis/	84.95
2341	Electric Vehicle Zone	EVZ	https://coinmarketcap.com//currencies/electric-vehicle-zone/	0.04
2342	KINE	KINE	https://coinmarketcap.com//currencies/kine/	4.27
2343	ETHUP	ETHUP	https://coinmarketcap.com//currencies/ethup/	124.3
2344	EMOGI Network	LOL	https://coinmarketcap.com//currencies/emogi-network/	0
2345	New BitShares	NBS	https://coinmarketcap.com//currencies/new-bitshares/	0.03
2346	SXPUP	SXPUP	https://coinmarketcap.com//currencies/sxpup/	1.85
2347	Hoge Finance	HOGE	https://coinmarketcap.com//currencies/hoge-finance/	0
2348	MobileCoin	MOB	https://coinmarketcap.com//currencies/mobilecoin/	5.62
2349	bDollar	BDO	https://coinmarketcap.com//currencies/bdollar/	1.14
2350	ETHDOWN	ETHDOWN	https://coinmarketcap.com//currencies/ethdown/	0.01
2351	UnFederalReserve	ERSDL	https://coinmarketcap.com//currencies/unfederalreserve/	0.33
2352	3X Long Cardano Token	ADABULL	https://coinmarketcap.com//currencies/3x-long-cardano-token/	30462.35
2353	OASISBloc	OSB	https://coinmarketcap.com//currencies/oasisbloc/	0.55
2354	3X Long Bitcoin Token	BULL	https://coinmarketcap.com//currencies/3x-long-bitcoin-token/	77142.93
2355	sKLAY	SKLAY	https://coinmarketcap.com//currencies/sklay/	3.29
2356	Value Liquidity	VALUE	https://coinmarketcap.com//currencies/value-defi-protocol/	4.6
2357	XRPUP	XRPUP	https://coinmarketcap.com//currencies/xrpup/	3.19
2358	Hiblocks	HIBS	https://coinmarketcap.com//currencies/hiblocks/	0.01
2359	Polka City	POLC	https://coinmarketcap.com//currencies/polka-city/	1.42
2360	Australian Dollar Token	AUDT	https://coinmarketcap.com//currencies/australian-dollar-token/	0.77
2361	Basis Gold Share	BAGS	https://coinmarketcap.com//currencies/basis-gold-share/	226.63
2362	Polaris Share	POLA	https://coinmarketcap.com//currencies/polaris-share/	0.33
2363	SafeMoon	SAFEMOON	https://coinmarketcap.com//currencies/safemoon/	0
2364	Global AEX Token	GAT	https://coinmarketcap.com//currencies/global-aex-token/	0.02
2365	Dapp Token	DAPPT	https://coinmarketcap.com//currencies/dapp-token/	0.01
2366	Paypolitan Token	EPAN	https://coinmarketcap.com//currencies/paypolitan-token/	0.2
2367	ITAM Games	ITAM	https://coinmarketcap.com//currencies/itam-games/	0.07
2368	DigiCol	DGCL	https://coinmarketcap.com//currencies/digicol/	0.89
2369	Wifi Coin	WIFI	https://coinmarketcap.com//currencies/wifi-coin/	0.65
2370	Bitcoin Vault	BTCV	https://coinmarketcap.com//currencies/bitcoin-vault/	65.6
2371	Compound USDT	CUSDT	https://coinmarketcap.com//currencies/compound-usdt/	0.02
2372	300FIT NETWORK	FIT	https://coinmarketcap.com//currencies/300fit-network/	0
2373	BSCEX	BSCX	https://coinmarketcap.com//currencies/bscex/	13.76
2374	XRPDOWN	XRPDOWN	https://coinmarketcap.com//currencies/xrpdown/	0
2375	EduMetrix Coin	EMC	https://coinmarketcap.com//currencies/edumetrix/	16.9
2376	bDollar Share	SBDO	https://coinmarketcap.com//currencies/bdollar-share/	7686.05
2377	SHIBA INU	SHIB	https://coinmarketcap.com//currencies/shiba-inu/	0
2378	Beacon ETH	BETH	https://coinmarketcap.com//currencies/beacon-eth/	1720.03
2379	BNBUP	BNBUP	https://coinmarketcap.com//currencies/bnbup/	543.86
2380	Gold Coin Reserve	GCR	https://coinmarketcap.com//currencies/gold-coin-reserve/	1728.96
2381	FinexboxToken	FNB	https://coinmarketcap.com//currencies/finexboxtoken/	4.27
2382	PancakeBunny	BUNNY	https://coinmarketcap.com//currencies/pancakebunny/	128.81
2383	Mineral	MNR	https://coinmarketcap.com//currencies/mineral/	0.02
2384	Chain Guardians	CGG	https://coinmarketcap.com//currencies/chain-guardians/	3.24
2385	Dvision Network	DVI	https://coinmarketcap.com//currencies/dvision-network/	0.4
2386	LINKDOWN	LINKDOWN	https://coinmarketcap.com//currencies/linkdown/	0
2387	Rai Reflex Index	RAI	https://coinmarketcap.com//currencies/rai/	2.97
2388	Midas Dollar Share	MDS	https://coinmarketcap.com//currencies/midas-dollar-share/	17689.59
2389	Adappter Token	ADP	https://coinmarketcap.com//currencies/adappter-token/	0.04
2390	TRXDOWN	TRXDOWN	https://coinmarketcap.com//currencies/trxdown/	0.12
2391	Argon	ARGON	https://coinmarketcap.com//currencies/argon/	0.15
2392	Alchemix	ALCX	https://coinmarketcap.com//currencies/alchemix/	1655.72
2393	LINKUP	LINKUP	https://coinmarketcap.com//currencies/linkup/	11.77
2394	YFIII	YFIII	https://coinmarketcap.com//currencies/yfiii/	9.02
2395	DAOventures	DVG	https://coinmarketcap.com//currencies/daoventures/	3.51
2396	MinePlex	PLEX	https://coinmarketcap.com//currencies/mineplex/	0.46
2397	FILUP	FILUP	https://coinmarketcap.com//currencies/filup/	11.05
2398	QUEENBEE	QBZ	https://coinmarketcap.com//currencies/queenbee/	0.01
2399	Gera Coin	GERA	https://coinmarketcap.com//currencies/gera-coin/	0.7134
2400	ADAUP	ADAUP	https://coinmarketcap.com//currencies/adaup/	88.62
2401	SifChain	erowan	https://coinmarketcap.com//currencies/sifchain/	1.25
2402	DMD	DMD	https://coinmarketcap.com//currencies/dmd/	40.93
2403	LTCUP	LTCUP	https://coinmarketcap.com//currencies/ltcup/	27.22
2404	Global Utility Smart Digital Token	GUSDT	https://coinmarketcap.com//currencies/gusd-token/	25.32
2405	ADADOWN	ADADOWN	https://coinmarketcap.com//currencies/adadown/	0.000569
2406	SXPDOWN	SXPDOWN	https://coinmarketcap.com//currencies/sxpdown/	0.01109
2407	Tower token	TOWER	https://coinmarketcap.com//currencies/tower-token/	0.04242
2408	XTZUP	XTZUP	https://coinmarketcap.com//currencies/xtzup/	0.596
2409	Innovation Blockchain Payment	IBP	https://coinmarketcap.com//currencies/innovation-blockchain-payment/	0.07
2410	MeconCash	MCH	https://coinmarketcap.com//currencies/meconcash/	0.17
2411	BigBoys Industry	BBI	https://coinmarketcap.com//currencies/bigboys-industry/	852.48
2412	Idavoll Network	IDV	https://coinmarketcap.com//currencies/idavoll-network/	0.04
2413	Bearn	BFI	https://coinmarketcap.com//currencies/bearn/	0.44
2414	Soda Coin	SOC	https://coinmarketcap.com//currencies/soda-coin/	0.01
2415	OVR	OVR	https://coinmarketcap.com//currencies/ovr/	0.9
2416	FairGame	FAIR	https://coinmarketcap.com//currencies/fairgame/	0.01
2417	DeFi Pulse Index	DPI	https://coinmarketcap.com//currencies/defi-pulse-index/	433.78
2418	Golff	GOF	https://coinmarketcap.com//currencies/golff/	1.68
2419	WiBX	WBX	https://coinmarketcap.com//currencies/wibx/	0.04
2420	Pearl	PEARL	https://coinmarketcap.com//currencies/pearl/	240.73
2421	3X Long Ethereum Token	ETHBULL	https://coinmarketcap.com//currencies/3x-long-ethereum-token/	4576.57
2422	Trabzonspor Fan Token	TRA	https://coinmarketcap.com//currencies/trabzonspor-fan-token/	5.66
2423	DOTUP	DOTUP	https://coinmarketcap.com//currencies/dotup/	143.6
2424	Velo	VELO	https://coinmarketcap.com//currencies/velo/	1.26
2425	FILDOWN	FILDOWN	https://coinmarketcap.com//currencies/fildown/	0.11
2426	HOPR	HOPR	https://coinmarketcap.com//currencies/hopr/	0.78
2427	XLMUP	XLMUP	https://coinmarketcap.com//currencies/xlmup/	10.75
2428	Trading Membership Community	TMC	https://coinmarketcap.com//currencies/trading-membership-community/	0.01
2429	EDDASwap	EDDA	https://coinmarketcap.com//currencies/eddaswap/	4833.63
2430	Gather	GTH	https://coinmarketcap.com//currencies/gather/	0.57
2431	Compound Wrapped BTC	CWBTC	https://coinmarketcap.com//currencies/compound-wrapped-btc/	1120.83
2432	DOTDOWN	DOTDOWN	https://coinmarketcap.com//currencies/dotdown/	0
2433	UNIUP	UNIUP	https://coinmarketcap.com//currencies/uniup/	26.13
2434	3X Short Bitcoin Token	BEAR	https://coinmarketcap.com//currencies/3x-short-bitcoin-token/	0
2435	Modefi	MOD	https://coinmarketcap.com//currencies/modefi/	2.8
2436	MilliMeter	MM	https://coinmarketcap.com//currencies/millimeter/	1
2437	G999	G999	https://coinmarketcap.com//currencies/g999/	0.01
2438	Bat True Dollar	BTD	https://coinmarketcap.com//currencies/bat-true-dollar/	1.15
2439	MIMOSA	MIMO	https://coinmarketcap.com//currencies/mimosa/	1.69
2440	Wise	WISE	https://coinmarketcap.com//currencies/wise/	0.41
2441	FUTUREXCRYPTO	FXC	https://coinmarketcap.com//currencies/futurexcrypto/	1.02
2442	Bilaxy Token	BIA	https://coinmarketcap.com//currencies/bilaxy-token/	0.01
2443	TRXUP	TRXUP	https://coinmarketcap.com//currencies/trxup/	3.13
2444	Bitcoin File	BIFI	https://coinmarketcap.com//currencies/bitcoin-file/	0
2445	B20	B20	https://coinmarketcap.com//currencies/b20/	12.74
2446	Inverse Finance	INV	https://coinmarketcap.com//currencies/inverse-finance/	1220.34
2447	Launchpool	LPOOL	https://coinmarketcap.com//currencies/launchpool/	22.62
2448	Polkacover	CVR	https://coinmarketcap.com//currencies/polkacover/	0.91
2449	Zero Exchange	ZERO	https://coinmarketcap.com//currencies/zero-exchange/	0.29
2450	PAID Network	PAID	https://coinmarketcap.com//currencies/paid-network/	1.66
2451	SUSHIUP	SUSHIUP	https://coinmarketcap.com//currencies/sushiup/	91.11
2452	XTZDOWN	XTZDOWN	https://coinmarketcap.com//currencies/xtzdown/	0.11
2453	7up Finance	7UP	https://coinmarketcap.com//currencies/7up-finance/	4.48
2454	CyberFi Token	CFi	https://coinmarketcap.com//currencies/cyberfi/	24.09
2455	Kindcow Finance	KIND	https://coinmarketcap.com//currencies/kindcow-finance/	2.27
2456	Bat True Share	BTS	https://coinmarketcap.com//currencies/bat-true-share/	515.88
2457	Brother Music Platform	BMP	https://coinmarketcap.com//currencies/brother-music-platform/	0.04
2458	UniCrypt	UNCX	https://coinmarketcap.com//currencies/uncx/	236
2459	SocialGood	SG	https://coinmarketcap.com//currencies/socialgood/	3.78
2460	Yobit Token	YO	https://coinmarketcap.com//currencies/yobit-token/	2133.87
2461	10x.gg	XGG	https://coinmarketcap.com//currencies/10xgg/	0.01
2462	FastSwap	FAST	https://coinmarketcap.com//currencies/fastswap/	12.46
2463	Zeedex	ZDEX	https://coinmarketcap.com//currencies/zeedex/	0.99
2464	Alpha5	A5T	https://coinmarketcap.com//currencies/alpha5/	1.07
2465	AAVEUP	AAVEUP	https://coinmarketcap.com//currencies/aave-up/	102.01
2466	YFIUP	YFIUP	https://coinmarketcap.com//currencies/yfiup/	5.51
2467	Gas Token Two	GST2	https://coinmarketcap.com//currencies/gas-token-two/	373.4
2468	3X Long Chainlink Token	LINKBULL	https://coinmarketcap.com//currencies/3x-long-chainlink-token/	404.97
2469	ALP Coin	ALP	https://coinmarketcap.com//currencies/alp-coin/	0.26
2470	Monster Slayer Cash	MSC	https://coinmarketcap.com//currencies/monster-slayer-finance/	1.11
2471	TopChain	TOPC	https://coinmarketcap.com//currencies/topchain/	0.01
2472	Tessla Coin	TSLA	https://coinmarketcap.com//currencies/tessla-coin/	1.79
2473	3x Long XRP Token	XRPBULL	https://coinmarketcap.com//currencies/3x-long-xrp-token/	0.44
2474	Medican Coin	MCAN	https://coinmarketcap.com//currencies/medican-coin/	0.16
2475	Bitcicoin	BITCI	https://coinmarketcap.com//currencies/bitcicoin/	0.01
2476	BDCC Bitica COIN	BDCC	https://coinmarketcap.com//currencies/bdcc-bitica-coin/	0.52
2477	AAVEDOWN	AAVEDOWN	https://coinmarketcap.com//currencies/aave-down/	0
2478	EOSDOWN	EOSDOWN	https://coinmarketcap.com//currencies/eosdown/	0.25
2479	Dfinity (IOU)	ICP	https://coinmarketcap.com//currencies/dfinity/	96.94
2480	Panda Dao	PDAO	https://coinmarketcap.com//currencies/panda-dao/	0
2481	Hintchain	HINT	https://coinmarketcap.com//currencies/hintchain/	0.02
2482	Blank Wallet	BLANK	https://coinmarketcap.com//currencies/blank-wallet/	0.71
2483	Ethereum Lightning	ETLT	https://coinmarketcap.com//currencies/ethereumlightning/	6.11
2484	AOS	AOS	https://coinmarketcap.com//currencies/aos/	0.04
2485	Bitcoin God	GOD	https://coinmarketcap.com//currencies/bitcoin-god/	11.07
2486	3X Long Sushi Token	SUSHIBULL	https://coinmarketcap.com//currencies/3x-long-sushi-token/	1.16
2487	Portion	PRT	https://coinmarketcap.com//currencies/portion/	0.2
2488	Carnomaly	CARR	https://coinmarketcap.com//currencies/carnomaly/	0.88
2489	UNION Protocol Governance Token	UNN	https://coinmarketcap.com//currencies/union-protocol-governance-token/	0.1
2490	BSCstarter	START	https://coinmarketcap.com//currencies/bscstarter/	19.5
2491	BT.Finance	BT	https://coinmarketcap.com//currencies/bt-finance/	41.04
2492	King Swap	$KING	https://coinmarketcap.com//currencies/kingswap/	0.14
2493	DeltaHub Community	DHC	https://coinmarketcap.com//currencies/deltahub-community/	1.71
2494	Exeedme	XED	https://coinmarketcap.com//currencies/exeedme/	1.35
2495	Unit Protocol Duck	DUCK	https://coinmarketcap.com//currencies/unit-protocol-duck/	0.32
2496	Butterfly Protocol	BFLY	https://coinmarketcap.com//currencies/butterfly-protocol-2/	0.78
2497	HashBridge Oracle	HBO	https://coinmarketcap.com//currencies/hashbridge-oracle/	1.24
2498	THORChain (ERC20)	RUNE	https://coinmarketcap.com//currencies/thorchain-erc20/	5.55
2499	Butterfly Protocol	BFLY	https://coinmarketcap.com//currencies/butterfly-protocol-2/	0.7806
2500	FEGtoken	FEG	https://coinmarketcap.com//currencies/fegtoken/	0
2501	Pando	PANDO	https://coinmarketcap.com//currencies/pando/	2.25
2502	Lendhub	LHB	https://coinmarketcap.com//currencies/lendhub/	0.3939
2503	ROONEX	RNX	https://coinmarketcap.com//currencies/roonex/	0.0006187
2504	UNIDOWN	UNIDOWN	https://coinmarketcap.com//currencies/unidown/	0.00271
2505	ISTARDUST	ISDT	https://coinmarketcap.com//currencies/istardust/	0.0008385
2506	Tether Gold	XAUT	https://coinmarketcap.com//currencies/tether-gold/	1730.42
2507	Jasmy	JASMY	https://coinmarketcap.com//currencies/jasmy/	2.09
2508	All.me	ME	https://coinmarketcap.com//currencies/all-me/	0.01
2509	FolgoryUSD	USDF	https://coinmarketcap.com//currencies/folgoryusd/	1
2510	DEOR	DEOR	https://coinmarketcap.com//currencies/deor/	0.28
2511	Whole Network	NODE	https://coinmarketcap.com//currencies/whole-network/	0
2512	Lotto	LOTTO	https://coinmarketcap.com//currencies/lotto/	0.11
2513	GRAP	GRAP	https://coinmarketcap.com//currencies/grap/	0.45
2514	LTCDOWN	LTCDOWN	https://coinmarketcap.com//currencies/ltcdown/	0.01
2515	NFTL Token	NFTL	https://coinmarketcap.com//currencies/nftl-token/	0.27
2516	Governor DAO	GDAO	https://coinmarketcap.com//currencies/governor-dao/	2.78
2517	Consensus Cell Network	ECELL	https://coinmarketcap.com//currencies/consensus-cell-network/	0.05
2518	Swirge	SWG	https://coinmarketcap.com//currencies/swirge/	0.5
2519	Filda	FILDA	https://coinmarketcap.com//currencies/filda/	0.41
2520	POC Blockchain	POC	https://coinmarketcap.com//currencies/poc-blockchain/	9.17
2521	Monster Slayer Share	MSS	https://coinmarketcap.com//currencies/monster-slayer-share/	863.36
2522	QuickSwap	QUICK	https://coinmarketcap.com//currencies/quickswap/	219
2523	HAPY Coin	HAPY	https://coinmarketcap.com//currencies/hapy-coin/	0.01
2524	Lien	LIEN	https://coinmarketcap.com//currencies/lien/	60.9
2525	Tcbcoin	TCFX	https://coinmarketcap.com//currencies/tcbcoin/	1.29
2526	DxSale Network	SALE	https://coinmarketcap.com//currencies/dxsale-network/	0.22
2527	Betherchip	BEC	https://coinmarketcap.com//currencies/betherchip/	11.55
2528	FNK wallet	FNK	https://coinmarketcap.com//currencies/fnk-wallet/	18.56
2529	CNNS	CNNS	https://coinmarketcap.com//currencies/cnns/	0.01
2530	CafeSwap Token	BREW	https://coinmarketcap.com//currencies/cafeswap-token/	4.2
2531	BNBDOWN	BNBDOWN	https://coinmarketcap.com//currencies/bnbdown/	0
2532	Soteria	WSOTE	https://coinmarketcap.com//currencies/soteria/	2.25
2533	MobiePay	MBX	https://coinmarketcap.com//currencies/mobiepay/	0.02
2534	Freeliquid	FL	https://coinmarketcap.com//currencies/freeliquid/	7.33
2535	ETHA Lend	ETHA	https://coinmarketcap.com//currencies/etha-lend/	2.38
2536	Auric Network	AUSCM	https://coinmarketcap.com//currencies/auric-network/	0.32
2537	Crypto Holding Frank Token	CHFT	https://coinmarketcap.com//currencies/crypto-holding-frank-token/	1.08
2538	ORO	ORO	https://coinmarketcap.com//currencies/oro/	0.41
2539	ViteX Coin	VX	https://coinmarketcap.com//currencies/vitex-coin/	0.8
2540	Bananatok	BNA	https://coinmarketcap.com//currencies/bananatok/	0.01
2541	Peanut	NUX	https://coinmarketcap.com//currencies/peanut/	3.27
2542	Axial Entertainment Digital Asset	AXL	https://coinmarketcap.com//currencies/axial-entertainment-digital-asset/	0.03
2543	Marginswap	MFI	https://coinmarketcap.com//currencies/marginswap/	0.96
2544	Siren	SI	https://coinmarketcap.com//currencies/siren/	2.64
2545	Previse	PRVS	https://coinmarketcap.com//currencies/previse/	0.93
2546	Curate	XCUR	https://coinmarketcap.com//currencies/curate/	2.11
2547	IDK	IDK	https://coinmarketcap.com//currencies/idk/	0.07
2548	PoolTogether	POOL	https://coinmarketcap.com//currencies/pooltogether/	23.6
2549	Wault Finance	WAULT	https://coinmarketcap.com//currencies/wault-finance/	19.41
2550	Bispex	BPX	https://coinmarketcap.com//currencies/bispex/	0
2551	Medium	MDM	https://coinmarketcap.com//currencies/medium/	0.37
2552	DEUS Finance	DEUS	https://coinmarketcap.com//currencies/deus-finance/	9.5
2553	Haze Finance	HAZE	https://coinmarketcap.com//currencies/haze-finance/	8.46
2554	keyTango	TANGO	https://coinmarketcap.com//currencies/keytango/	1.83
2555	YFIDOWN	YFIDOWN	https://coinmarketcap.com//currencies/yfidown/	0
2556	Dexchain	DXC	https://coinmarketcap.com//currencies/mydexpay/	0.01
2557	QuiverX	QRX	https://coinmarketcap.com//currencies/quiverx/	0.08
2558	Fromm Car	FCR	https://coinmarketcap.com//currencies/fromm-car/	0
2559	Basis Share	BAS	https://coinmarketcap.com//currencies/basis-share/	45.86
2560	Fire Protocol	FIRE	https://coinmarketcap.com//currencies/fire-protocol/	1.08
2561	Combo	COMB	https://coinmarketcap.com//currencies/combo/	0.01
2562	Digital Reserve Currency	DRC	https://coinmarketcap.com//currencies/digital-reserve-currency/	0
2563	unilock.network	UNL	https://coinmarketcap.com//currencies/unilock-network/	2428.14
2564	Loon Network	LOON	https://coinmarketcap.com//currencies/loon-network/	0.04
2565	3x Long EOS Token	EOSBULL	https://coinmarketcap.com//currencies/3x-long-eos-token/	0.23
2566	Arianee	ARIA20	https://coinmarketcap.com//currencies/arianee-protocol/	3.64
2567	Coinzo Token	CNZ	https://coinmarketcap.com//currencies/coinzo-token/	0.11
2568	BambooDeFi	BAMBOO	https://coinmarketcap.com//currencies/bamboo-defi/	0.51
2570	KittenFinance	KIF	https://coinmarketcap.com//currencies/kittenfinance/	97.79
2571	Mandala Exchange Token	MDX	https://coinmarketcap.com//currencies/mandala-exchange-token/	0.15
2572	Apiary Fund Coin	AFC	https://coinmarketcap.com//currencies/apiary-fund-coin/	0
2573	pBTC35A	pBTC35A	https://coinmarketcap.com//currencies/pbtc35a/	124.18
2574	VKENAF	VKNF	https://coinmarketcap.com//currencies/vkenaf/	0.2
2575	MixTrust	MXT	https://coinmarketcap.com//currencies/mixtrust/	0.01
2576	Cross Finance	CRP	https://coinmarketcap.com//currencies/cross-finance/	0.01
2577	Axion	AXN	https://coinmarketcap.com//currencies/axion/	0
2578	Libartysharetoken	LST	https://coinmarketcap.com//currencies/libartysharetoken/	0.14
2579	ACryptoS	ACS	https://coinmarketcap.com//currencies/acryptos/	57.14
2580	dFuture	DFT	https://coinmarketcap.com//currencies/dfuture/	0.43
2581	Lightning	LIGHT	https://coinmarketcap.com//currencies/lightning/	0.02
2582	BKEX Token	BKK	https://coinmarketcap.com//currencies/bkex-token/	0.14
2583	e-Money	NGM	https://coinmarketcap.com//currencies/e-money-coin/	1.58
2584	GOGO.finance	GOGO	https://coinmarketcap.com//currencies/gogo-finance/	182.2
2585	Counos Coin	CCA	https://coinmarketcap.com//currencies/counos-coin/	1.28
2586	UBIX.Network	UBX	https://coinmarketcap.com//currencies/ubix-network/	0
2587	Rivex	RVX	https://coinmarketcap.com//currencies/rivex/	0.23
2588	Zyro	ZYRO	https://coinmarketcap.com//currencies/zyro/	0.04
2589	Tenet	TEN	https://coinmarketcap.com//currencies/tenet/	2.29
2590	BotOcean	BOTS	https://coinmarketcap.com//currencies/botocean/	0.29
2591	Elite Swap	ELT	https://coinmarketcap.com//currencies/elite-swap/	0
2592	Hydra	HYDRA	https://coinmarketcap.com//currencies/hydra/	14.43
2593	Mantis	MNTIS	https://coinmarketcap.com//currencies/mantis/	7.3
2594	Metis	MTS	https://coinmarketcap.com//currencies/metis/	1
2595	BeeKan	BKBT	https://coinmarketcap.com//currencies/beekan/	0
2596	MCDEX	MCB	https://coinmarketcap.com//currencies/mcdex/	15.66
2597	Starname	IOV	https://coinmarketcap.com//currencies/starname/	0.07
2598	Stream Protocol	STPL	https://coinmarketcap.com//currencies/stream-protocol/	0.04851
2599	MegaCryptoPolis	MEGA	https://coinmarketcap.com//currencies/megacryptopolis/	48.93
2600	Trism	TRISM	https://coinmarketcap.com//currencies/trism/	0.8988
2601	SaltSwap Finance	SALT	https://coinmarketcap.com//currencies/saltswap-finance/	3.28
2602	DeFi Wizard	DWZ	https://coinmarketcap.com//currencies/defi-wizard/	6.75
2603	ZCore Finance	ZEFI	https://coinmarketcap.com//currencies/zcore-finance/	1.55
2604	Tardigrades Finance	TRDG	https://coinmarketcap.com//currencies/tardigrades-finance/	0
2605	Dextoken	DEXG	https://coinmarketcap.com//currencies/dextoken/	330.4
2606	3X Short Ethereum Token	ETHBEAR	https://coinmarketcap.com//currencies/3x-short-ethereum-token/	5.438e-05
2607	Binance VND	BVND	https://coinmarketcap.com//currencies/binance-vnd/	4.145e-05
2608	YAM v3	YAM	https://coinmarketcap.com//currencies/yamv3/	3.22
2609	WaykiChain Governance Coin	WGRT	https://coinmarketcap.com//currencies/waykichain-governance-coin/	0.01
2610	Bidao	BID	https://coinmarketcap.com//currencies/bidao/	0.02
2611	Lux Bio Cell	LBXC	https://coinmarketcap.com//currencies/lux-bio-cell/	0
2612	StarLink	SLNV2	https://coinmarketcap.com//currencies/starlink/	0.03
2613	ProBit Token	PROB	https://coinmarketcap.com//currencies/probit-token/	0.45
2614	AGOV (ANSWER Governance)	AGOV	https://coinmarketcap.com//currencies/agov-answer-governance/	0.03
2615	The Luxury Coin	TLB	https://coinmarketcap.com//currencies/the-luxury-coin/	51.1
2616	DXdao	DXD	https://coinmarketcap.com//currencies/dxdao/	225.69
2617	XLMDOWN	XLMDOWN	https://coinmarketcap.com//currencies/xlmdown/	0.01
2618	Float Protocol	BANK	https://coinmarketcap.com//currencies/float-protocol/	704.65
2619	Proof Of Liquidity	POL	https://coinmarketcap.com//currencies/proof-of-liquidity/	0.11
2620	B21 Invest	B21	https://coinmarketcap.com//currencies/b21-invest/	0.37
2621	3x Long Bitcoin Cash Token	BCHBULL	https://coinmarketcap.com//currencies/3x-long-bitcoin-cash-token/	3.13
2622	Slime Finance	SLME	https://coinmarketcap.com//currencies/slime-finance/	6.39
2623	LinkArt	LAR	https://coinmarketcap.com//currencies/linkart/	0
2624	REDi	REDI	https://coinmarketcap.com//currencies/redi/	0
2625	Swapfolio	SWFL	https://coinmarketcap.com//currencies/swapfolio/	0.09
2626	HecoFi	HFI	https://coinmarketcap.com//currencies/hecofi/	0.04
2627	Mars	Mars	https://coinmarketcap.com//currencies/mars/	0.06
2628	Whirl Finance	WHIRL	https://coinmarketcap.com//currencies/whirl-finance/	0.01
2629	Themis	MIS	https://coinmarketcap.com//currencies/themis-oracle/	0.53
2630	Compound 0x	CZRX	https://coinmarketcap.com//currencies/compound-0x/	0.03
2631	EthicHub	ETHIX	https://coinmarketcap.com//currencies/ethichub/	0.54
2632	Decentralized Mining Exchange	DMC	https://coinmarketcap.com//currencies/decentralized-mining-exchange/	0.42
2633	RAKUN	RAKU	https://coinmarketcap.com//currencies/rakun/	0.02
2634	stakedXEM	STXEM	https://coinmarketcap.com//currencies/stakedxem/	0.37
2635	Yuan Chain Coin	YCC	https://coinmarketcap.com//currencies/yuan-chain-coin/	0.02
2636	BSC FARM	BSC	https://coinmarketcap.com//currencies/bsc-farm/	1.03
2637	MeetPle	MPT	https://coinmarketcap.com//currencies/meetple/	0.88
2638	Solomon Defi	SLM	https://coinmarketcap.com//currencies/solomon-defi/	0.04
2639	Polkazeck	ZCK	https://coinmarketcap.com//currencies/polkazeck/	0.09
2640	3x Long Litecoin Token	LTCBULL	https://coinmarketcap.com//currencies/3x-long-litecoin-token/	8
2641	Moola	MLA	https://coinmarketcap.com//currencies/moola/	1.35
2642	Marsan Exchange token	MRS	https://coinmarketcap.com//currencies/marsan-exchange-token/	0.21
2643	AllWin DeFi	ALLWIN	https://coinmarketcap.com//currencies/allwin-defi/	0.39
2644	UpBots	UBXT	https://coinmarketcap.com//currencies/upbots/	0.03
2645	Barkis Network	BKS	https://coinmarketcap.com//currencies/barkis-network/	0.81
2646	SmartKey	SKEY	https://coinmarketcap.com//currencies/smartkey/	0.36
2647	MiraQle	MQL	https://coinmarketcap.com//currencies/miraqle/	0.23
2648	DEGEN Index	DEGEN	https://coinmarketcap.com//currencies/degen-index/	6.56
2649	Compound Basic Attention Token	CBAT	https://coinmarketcap.com//currencies/compound-basic-attention-token/	0.02
2650	Brazilian Digital Token	BRZ	https://coinmarketcap.com//currencies/brz/	0.17
2651	Moderna tokenized stock FTX	MRNA	https://coinmarketcap.com//currencies/moderna-tokenized-stock-ftx/	155.9
2652	Mars Network	MARS	https://coinmarketcap.com//currencies/mars-network/	0.02
2653	DEUS Finance DEA	DEA	https://coinmarketcap.com//currencies/deus-finance-dea/	336.6
2654	Cap	CAP	https://coinmarketcap.com//currencies/cap/	38.4
2655	MCOBIT	MCT	https://coinmarketcap.com//currencies/mcobit/	0.14
2656	Alpaca City	ALPA	https://coinmarketcap.com//currencies/alpaca-city/	1.08
2657	Handy	HANDY	https://coinmarketcap.com//currencies/handy/	0.03
2658	Student Coin	STC	https://coinmarketcap.com//currencies/student-coin/	0.02
2659	Firdaos	FDO	https://coinmarketcap.com//currencies/firdaos/	0.2
2660	Katalyo	KTLYO	https://coinmarketcap.com//currencies/katalyo/	0.56
2661	3X Long Algorand Token	ALGOBULL	https://coinmarketcap.com//currencies/3x-long-algorand-token/	0
2662	Cryptoindex.com 100	CIX100	https://coinmarketcap.com//currencies/cryptoindex-com-100/	0.79
2663	UniPower	POWER	https://coinmarketcap.com//currencies/unipower/	1.36
2664	DEXTF Protocol	DEXTF	https://coinmarketcap.com//currencies/dextf-protocol/	1.58
2665	xDAI	xDAI	https://coinmarketcap.com//currencies/xdaistable/	1
2666	CSP DAO	NEBO	https://coinmarketcap.com//currencies/csp-dao/	17.83
2667	MyToken	MT	https://coinmarketcap.com//currencies/mytoken/	0
2668	CoFiX	COFI	https://coinmarketcap.com//currencies/cofix/	0.65
2669	protocol finance	PFI	https://coinmarketcap.com//currencies/protocol-finance/	101.94
2670	Echoin	EC	https://coinmarketcap.com//currencies/echoin/	0
2671	UBU	UBU	https://coinmarketcap.com//currencies/ubu/	0.08
2672	DigiFinexToken	DFT	https://coinmarketcap.com//currencies/digifinextoken/	0.16
2673	Armor NXM	arNXM	https://coinmarketcap.com//currencies/armor-nxm/	44.77
2674	Itchiro Games	JEM	https://coinmarketcap.com//currencies/itchiro-games/	52.98
2675	PROXI	CREDIT	https://coinmarketcap.com//currencies/proxi/	0.04
2676	Misbloc	MSB	https://coinmarketcap.com//currencies/misbloc/	0.11
2677	Zytara dollar	ZUSD	https://coinmarketcap.com//currencies/zytara-dollar/	1
2678	DefiCliq	CLIQ	https://coinmarketcap.com//currencies/deficliq/	0.12
2679	Bogged Finance	BOG	https://coinmarketcap.com//currencies/bogged-finance/	2.42
2680	Multiplier	BMXX	https://coinmarketcap.com//currencies/bmultiplier/	32.64
2681	OnX Finance	ONX	https://coinmarketcap.com//currencies/onx-finance/	2.38
2682	Bitcoiva	BCA	https://coinmarketcap.com//currencies/bitcoiva/	3.11
2683	ATTN	ATTN	https://coinmarketcap.com//currencies/attn/	0.08
2684	ethArt	ARTE	https://coinmarketcap.com//currencies/ethart/	21.24
2685	Scifi Finance	SPICE	https://coinmarketcap.com//currencies/scifi-finance/	1.32
2686	B26 Finance	B26	https://coinmarketcap.com//currencies/b26-finance/	265.75
2687	Blockcloud	BLOC	https://coinmarketcap.com//currencies/blockcloud/	0
2688	Quantbook	QTBK	https://coinmarketcap.com//currencies/quantbook/	0.01
2689	JustLiquidity Binance	JULB	https://coinmarketcap.com//currencies/justliquidity-binance/	179.32
2690	Chain Games	CHAIN	https://coinmarketcap.com//currencies/chain-games/	0.11
2691	Glitch	GLCH	https://coinmarketcap.com//currencies/glitch/	0.09
2692	Maxonrow	MXW	https://coinmarketcap.com//currencies/maxonrow/	0.03
2693	Yfi.mobi	YFIM	https://coinmarketcap.com//currencies/yfi-mobi/	68.37
2694	Govi	GOVI	https://coinmarketcap.com//currencies/govi/	3.31
2695	PayAccept	PAYT	https://coinmarketcap.com//currencies/payaccept/	0.09
2696	Exchange Union	XUC	https://coinmarketcap.com//currencies/exchange-union/	0.6267
2697	ClinTex CTi	CTI	https://coinmarketcap.com//currencies/clintex-cti/	0.04805
2698	JackPool.finance	JFI	https://coinmarketcap.com//currencies/jackpool-finance/	89.57
2699	Minds	MINDS	https://coinmarketcap.com//currencies/minds/	0.7448
2700	3x Short EOS Token	EOSBEAR	https://coinmarketcap.com//currencies/3x-short-eos-token/	0.03682
2701	KVI	KVI	https://coinmarketcap.com//currencies/kvi/	0.001287
2702	QiSwap	QI	https://coinmarketcap.com//currencies/qiswap/	0.6943
2703	Swop	SWOP	https://coinmarketcap.com//currencies/swop/	20.74
2704	Pteria	PTERIA	https://coinmarketcap.com//currencies/pteria/	3.87
2705	Flashstake	FLASH	https://coinmarketcap.com//currencies/flashstake/	0.9944
2706	Interest Bearing ETH	ibETH	https://coinmarketcap.com//currencies/ibeth/	1880.61
2707	CY Finance	CYF	https://coinmarketcap.com//currencies/cy-finance/	0
2708	3X Long Cosmos Token	ATOMBULL	https://coinmarketcap.com//currencies/3x-long-cosmos-token/	40.24
2709	TokenPocket	TPT	https://coinmarketcap.com//currencies/tokenpocket/	0.03
2710	Order of the Black Rose	OBR	https://coinmarketcap.com//currencies/order-of-the-black-rose/	294.69
2711	yAxis	YAX	https://coinmarketcap.com//currencies/yaxis/	17.43
2712	Fairum	FAI	https://coinmarketcap.com//currencies/fairum/	1.63
2713	Largo Coin	LRG	https://coinmarketcap.com//currencies/largo-coin/	9.38
2714	Banana.finance	BANANA	https://coinmarketcap.com//currencies/banana-finance/	374.28
2715	Culture Ticket Chain	CTC	https://coinmarketcap.com//currencies/culture-ticket-chain/	0.01
2716	MangoChain	MGP	https://coinmarketcap.com//currencies/mangochain/	0.07
2717	LBK	LBK	https://coinmarketcap.com//currencies/lbk/	0.01
2718	Connect Financial	CNFI	https://coinmarketcap.com//currencies/connect-financial/	0.39
2719	EXRT Network	EXRT	https://coinmarketcap.com//currencies/exrt-network/	0.02
2720	AC eXchange Token	ACXT	https://coinmarketcap.com//currencies/ac-exchange-token/	1.02
2721	CryptoBank	CBANK	https://coinmarketcap.com//currencies/cryptobank/	3.29
2722	Blockchain Quotations Index Token	BQT	https://coinmarketcap.com//currencies/blockchain-quotations-index-token/	0.01
2723	Kaleido	KAL	https://coinmarketcap.com//currencies/kaleido/	0.01
2724	Two Prime FF1 Token	FF1	https://coinmarketcap.com//currencies/two-prime-ff1-token/	1.09
2725	Buy-Sell	BSE	https://coinmarketcap.com//currencies/buy-sell/	1.4
2726	Behodler	EYE	https://coinmarketcap.com//currencies/beholder/	1.36
2727	ShineChain	SHE	https://coinmarketcap.com//currencies/shinechain/	0
2728	MileVerse	MVC	https://coinmarketcap.com//currencies/mileverse/	0.01
2729	GGDApp	GGTK	https://coinmarketcap.com//currencies/ggdapp/	0.41
2730	ARTH	ARTH	https://coinmarketcap.com//currencies/arth/	0.32
2731	Farmland Protocol	FAR	https://coinmarketcap.com//currencies/farmland-protocol/	0.45
2732	Natural Farm Union Protocol	NFUP	https://coinmarketcap.com//currencies/natural-farm-union-protocol/	0.03
2733	DeFiat	DFT	https://coinmarketcap.com//currencies/defiat/	24.05
2734	TOM Finance	TOM	https://coinmarketcap.com//currencies/tom-finance/	94.62
2735	Energy Ledger	ELX	https://coinmarketcap.com//currencies/energy-ledger/	0.01
2736	Engine	EGCC	https://coinmarketcap.com//currencies/engine/	0
2737	Gains Farm v2	GFARM2	https://coinmarketcap.com//currencies/gains-farm-v2/	260.69
2738	Octree	OCT	https://coinmarketcap.com//currencies/octree/	345.01
2739	Bankcoin	BANK	https://coinmarketcap.com//currencies/the-bank-coin/	0.01
2740	DeFiPie	PIE	https://coinmarketcap.com//currencies/defipie/	0.12
2741	SUP	SUP	https://coinmarketcap.com//currencies/sup/	67.83
2742	3X Long Matic Token	MATICBULL	https://coinmarketcap.com//currencies/3x-long-matic-token/	32.79
2743	Lung Protocol	L2P	https://coinmarketcap.com//currencies/lung-protocol/	0
2744	Tapmydata	TAP	https://coinmarketcap.com//currencies/tapmydata/	0.53
2745	TOZEX	TOZ	https://coinmarketcap.com//currencies/tozex/	0.32
2746	tBTC	TBTC	https://coinmarketcap.com//currencies/tbtc/	56521.9
2747	Unifty	NIF	https://coinmarketcap.com//currencies/unifty/	17.85
2748	Tesla tokenized stock FTX	TSLA	https://coinmarketcap.com//currencies/tesla-tokenized-stock-ftx/	706.2
2749	GameStop tokenized stock FTX	GME	https://coinmarketcap.com//currencies/gamestop-tokenized-stock-ftx/	193.9
2750	Akita Inu	AKITA	https://coinmarketcap.com//currencies/akita-inu/	0
2751	Treat DAO	TREAT	https://coinmarketcap.com//currencies/treat-dao/	1.62
2752	Roxe Cash	ROC	https://coinmarketcap.com//currencies/roxe-cash/	0.1
2753	Coreto	COR	https://coinmarketcap.com//currencies/coreto/	0.01
2754	3X Long BNB Token	BNBBULL	https://coinmarketcap.com//currencies/3x-long-bnb-token/	5745.81
2755	fry.world	FRIES	https://coinmarketcap.com//currencies/fryworld/	58.22
2756	CheesecakeSwap Token	CCAKE	https://coinmarketcap.com//currencies/cheesecakeswap-token/	1.57
2757	Oxbull.tech	OXB	https://coinmarketcap.com//currencies/oxbull-tech/	0.58
2758	nHBTC	N0001	https://coinmarketcap.com//currencies/nhbtc/	1.03
2759	Minty Art	MINTY	https://coinmarketcap.com//currencies/minty-art/	2.37
2760	BlackDragon	BDT	https://coinmarketcap.com//currencies/blackdragon/	14.04
2761	Voice Token	VOICE	https://coinmarketcap.com//currencies/nix-bridge-token/	75.03
2762	Sperax	SPA	https://coinmarketcap.com//currencies/sperax/	0.02
2763	Unifund	IFUND	https://coinmarketcap.com//currencies/unifund/	0.15
2764	HyruleSwap	RUPEE	https://coinmarketcap.com//currencies/hyruleswap/	0.87
2765	OneSwap DAO Token	ONES	https://coinmarketcap.com//currencies/oneswap-dao-token/	1.25
2766	Your Finance Decentralized	YFD	https://coinmarketcap.com//currencies/yfdfi-finance/	95.48
2767	REBIT	KEYT	https://coinmarketcap.com//currencies/rebit/	0.02
2768	Yearn Finance Passive Income	YFPI	https://coinmarketcap.com//currencies/yearn-finance-passive-income/	21.57
2769	CryptEx	CRX	https://coinmarketcap.com//currencies/cryptex/	31.94
2770	Phoswap	PHO	https://coinmarketcap.com//currencies/phoswap/	1.4
2771	Global Human Trust	GHT	https://coinmarketcap.com//currencies/global-human-trust/	43.07
2772	Axia Protocol	AXIA	https://coinmarketcap.com//currencies/axia-protocol/	1.18
2773	Vortex Defi	VTX	https://coinmarketcap.com//currencies/vortex-defi/	0.29
2774	JUST NETWORK	JUS	https://coinmarketcap.com//currencies/just-network/	0.01
2775	Sora Validator Token	VAL	https://coinmarketcap.com//currencies/sora-validator-token/	2.37
2776	UniFi Protocol	UP	https://coinmarketcap.com//currencies/unifi-protocol/	1.38
2777	Blue Baikal	BBC	https://coinmarketcap.com//currencies/blue-baikal/	0
2778	PRCY Coin	PRCY	https://coinmarketcap.com//currencies/prcy-coin/	0.1
2779	Yield	YLD	https://coinmarketcap.com//currencies/yield/	88.11
2780	EIDOS	EIDOS	https://coinmarketcap.com//currencies/eidos/	0
2781	VARC	VARC	https://coinmarketcap.com//currencies/varc/	0
2782	SpiderDAO	SPDR	https://coinmarketcap.com//currencies/spiderdao/	0.04
2783	COIN	$COIN	https://coinmarketcap.com//currencies/coin-defi/	0.29
2784	GIVLY Coin	GIV	https://coinmarketcap.com//currencies/givly-coin/	0.01
2785	YEARNYFI NETWORK	YNI	https://coinmarketcap.com//currencies/yearnyfi-network/	230.59
2786	Wealthlocks	WLT	https://coinmarketcap.com//currencies/wealthlocks/	41.75
2787	Channels	CAN	https://coinmarketcap.com//currencies/channels/	25.13
2788	Yearn Classic Finance	EARN	https://coinmarketcap.com//currencies/yearn-classic-finance/	90.6
2789	DACC	DACC	https://coinmarketcap.com//currencies/dacc/	0
2790	Mogu	MOGX	https://coinmarketcap.com//currencies/mogu/	0.01
2791	VeraSwap	VRAP	https://coinmarketcap.com//currencies/veraswap/	0.06
2792	Sashimi	SASHIMI	https://coinmarketcap.com//currencies/sashimi/	0.08
2793	Premia	PREMIA	https://coinmarketcap.com//currencies/premia/	1.4
2794	BuildUp	BUP	https://coinmarketcap.com//currencies/buildup/	0.53
2795	MoneySwap	MSWAP	https://coinmarketcap.com//currencies/moneyswap/	0.01
2796	Pluto	PLUT	https://coinmarketcap.com//currencies/pluto/	0.1711
3022	Level01	LVX	https://coinmarketcap.com//currencies/level01/	0.02
2797	Wrapped CrescoFin	wCRES	https://coinmarketcap.com//currencies/wrapped-crescofin/	7.59
2798	Burency	BUY	https://coinmarketcap.com//currencies/burency/	0.1228
2799	Koinos	KOIN	https://coinmarketcap.com//currencies/koinos/	0.1365
2800	Intelligent Investment Chain	IIC	https://coinmarketcap.com//currencies/intelligent-investment-chain/	0.0002439
2801	Treecle	TRCL	https://coinmarketcap.com//currencies/treecle/	0.01584
2802	xToken	XTK	https://coinmarketcap.com//currencies/xtoken/	0.2224
2803	YearnAgnostic Finance	YFIAG	https://coinmarketcap.com//currencies/yearnagnostic-finance/	2.78
2804	Berry	BERRY	https://coinmarketcap.com//currencies/berry/	0.006764
2805	Latamcash	LMCH	https://coinmarketcap.com//currencies/latamcash/	0.004204
2806	Font	FONT	https://coinmarketcap.com//currencies/font/	18.03
2807	BitcoinVend	BCVT	https://coinmarketcap.com//currencies/bitcoinvend/	0.15
2808	Unique One	RARE	https://coinmarketcap.com//currencies/unique-one/	4.37
2809	Secret (ERC20)	WSCRT	https://coinmarketcap.com//currencies/secret-erc20/	3.06
2810	Hyper Pay	HPY	https://coinmarketcap.com//currencies/hyper-pay/	0
2811	Sovi Finance	SOVI	https://coinmarketcap.com//currencies/sovi-finance/	8.09
2812	EduCoin	EDU	https://coinmarketcap.com//currencies/edu-coin/	0
2813	TON Crystal	TON	https://coinmarketcap.com//currencies/ton-crystal/	0.67
2814	Warp Finance	WARP	https://coinmarketcap.com//currencies/warp-finance/	797.15
2815	Rake Finance	RAK	https://coinmarketcap.com//currencies/rake-finance/	35.28
2816	YFIONE	YFO	https://coinmarketcap.com//currencies/yfione/	2046.16
2817	Super Bitcoin	SBTC	https://coinmarketcap.com//currencies/super-bitcoin/	1.13
2818	Balloon-X	BLX	https://coinmarketcap.com//currencies/balloon-x/	0.01
2819	EHash	EHASH	https://coinmarketcap.com//currencies/ehash/	1.2
2820	DefiBox	BOX	https://coinmarketcap.com//currencies/defibox/	18.21
2821	WELL	WELL	https://coinmarketcap.com//currencies/well-token/	0.78
2822	UniWorld	UNW	https://coinmarketcap.com//currencies/uniworld/	0.04
2823	CircleSwap	CIR	https://coinmarketcap.com//currencies/circleswap/	1.96
2824	Mirrored Google	mGOOGL	https://coinmarketcap.com//currencies/mirrored-google/	2115.42
2825	Toshimon	TOSHI	https://coinmarketcap.com//currencies/toshimon/	113.55
2826	BlackFisk	BLFI	https://coinmarketcap.com//currencies/blackfisk/	37.3
2827	BSB Token	BSBT	https://coinmarketcap.com//currencies/bsb-token/	0.28
2828	BNS Token	BNS	https://coinmarketcap.com//currencies/bns-token/	0.07
2829	3X Long Dogecoin Token	DOGEBULL	https://coinmarketcap.com//currencies/3x-long-dogecoin-token/	4371
2830	WAXE	WAXE	https://coinmarketcap.com//currencies/waxe/	112.25
2831	BSYS	BSYS	https://coinmarketcap.com//currencies/bsys/	0
2832	FOMPOUND	FOMP	https://coinmarketcap.com//currencies/fompound/	35.96
2833	DefiDollar DAO	DFD	https://coinmarketcap.com//currencies/defidollar-dao/	0.67
2834	BUMO	BU	https://coinmarketcap.com//currencies/bumo/	0.01
2835	3x Long Bitcoin SV Token	BSVBULL	https://coinmarketcap.com//currencies/3x-long-bitcoin-sv-token/	0.03
2836	Tigerfinance	TIGER	https://coinmarketcap.com//currencies/tigerfinance/	0.62
2837	EA Token	EA	https://coinmarketcap.com//currencies/ea-token/	0
2838	MDUKEY	MDU	https://coinmarketcap.com//currencies/mdu/	0.06
2839	D Community	DILI	https://coinmarketcap.com//currencies/d-community/	0
2840	3X Short Cardano Token	ADABEAR	https://coinmarketcap.com//currencies/3x-short-cardano-token/	0
2841	BELIEVER	BLVR	https://coinmarketcap.com//currencies/believer/	0.76
2842	Force For Fast	FFF	https://coinmarketcap.com//currencies/force-for-fast/	0.01
2843	SparkPoint Fuel	SFUEL	https://coinmarketcap.com//currencies/sparkpoint-fuel/	0.16
2844	Biscuit Farm Finance	BCU	https://coinmarketcap.com//currencies/biscuit-farm-finance/	0.67
2845	Dcoin Token	DT	https://coinmarketcap.com//currencies/dcoin-token/	0.02
2846	Degen Protocol	DGN	https://coinmarketcap.com//currencies/degen-protocol/	4.3
2847	Relevant	REL	https://coinmarketcap.com//currencies/relevant/	2.2
2848	Ludos Protocol	LUD	https://coinmarketcap.com//currencies/ludos/	0
2849	Akropolis Delphi	ADEL	https://coinmarketcap.com//currencies/akropolis-delphi/	0.16
2850	Assemble Protocol	ASM	https://coinmarketcap.com//currencies/assemble-protocol/	0.04
2851	YouLive Coin	UC	https://coinmarketcap.com//currencies/youlive-coin/	0
2852	DeFi of Thrones	DOTX	https://coinmarketcap.com//currencies/deli-of-thrones/	0.38
2853	MM Token	MM	https://coinmarketcap.com//currencies/mm-token/	14.23
2854	BiLira	TRYB	https://coinmarketcap.com//currencies/bilira/	0.13
2855	Unique Photo	FOTO	https://coinmarketcap.com//currencies/unique-photo/	0.33
2856	CoinMeet	MEET	https://coinmarketcap.com//currencies/coinmeet/	0.01
2857	Tsunami finance	NAMI	https://coinmarketcap.com//currencies/tsunami/	517.42
2858	XT.com Token	XT	https://coinmarketcap.com//currencies/xtcom-token/	9.36
2859	DefiDollar	DUSD	https://coinmarketcap.com//currencies/defidollar/	1
2860	3x Short Bitcoin SV Token	BSVBEAR	https://coinmarketcap.com//currencies/3x-short-bitcoin-sv-token/	0.01
2861	3x Short XRP Token	XRPBEAR	https://coinmarketcap.com//currencies/3x-short-xrp-token/	0
2862	UniWhales	UWL	https://coinmarketcap.com//currencies/uniwhales/	0.69
2863	Block Duelers NFT Battles	BDT	https://coinmarketcap.com//currencies/block-duelers/	264.98
2864	QURAS	XQC	https://coinmarketcap.com//currencies/quras/	0.02
2865	Buzzshow	GLDY	https://coinmarketcap.com//currencies/buzzshow/	0.09
2866	The Famous Token	TFT	https://coinmarketcap.com//currencies/the-famous-token/	44.46
2867	YUI Token	YUI	https://coinmarketcap.com//currencies/yui-token/	21.91
2868	VROOMGO	VRGX	https://coinmarketcap.com//currencies/vroomgo/	0
2869	Aloha	ALOHA	https://coinmarketcap.com//currencies/aloha/	0.04
2870	Zugacoin	SZC	https://coinmarketcap.com//currencies/zugacoin/	89.74
2871	CoralFarm	CRL	https://coinmarketcap.com//currencies/coralfarm/	1.21
2872	Alaya	ATP	https://coinmarketcap.com//currencies/alaya/	5.53
2873	Hive Dollar	HBD	https://coinmarketcap.com//currencies/hive-dollar/	1.2
2874	Project Inverse	XIV	https://coinmarketcap.com//currencies/project-inverse/	0.22
2875	FINANCIAL INVESTMENT TOKEN	FIT	https://coinmarketcap.com//currencies/financial-investment-token/	11.86
2876	TOKOK	TOK	https://coinmarketcap.com//currencies/tokok/	0.01
2877	Jinbi Token	JNB	https://coinmarketcap.com//currencies/jinbi-token/	352.22
2878	Xrpalike Gene	XAG	https://coinmarketcap.com//currencies/xrpalike-gene/	0.05
2879	Dexfin	DXF	https://coinmarketcap.com//currencies/dexfin/	0.01
2880	ThingsOperatingSystem	TOS	https://coinmarketcap.com//currencies/thingsoperatingsystem/	0
2881	Baroin	BRI	https://coinmarketcap.com//currencies/baroin/	1.77
2882	DexMex	DEXM	https://coinmarketcap.com//currencies/dexmex/	0.1
2883	AINORI	AIN	https://coinmarketcap.com//currencies/ainori/	0
2884	TecraCoin	TCR	https://coinmarketcap.com//currencies/tecracoin/	0.25
2885	3X Long TomoChain Token	TOMOBULL	https://coinmarketcap.com//currencies/3x-long-tomochain-token/	0.18
2886	Black Diamond Rating	HZT	https://coinmarketcap.com//currencies/black-diamond-rating/	0.04
2887	Hey Bitcoin	HYBN	https://coinmarketcap.com//currencies/hey-bitcoin/	0.33
2888	DIESEL	DIESEL	https://coinmarketcap.com//currencies/diesel/	1.22
2889	Kulupu	KLP	https://coinmarketcap.com//currencies/kulupu/	0.22
2890	Glox Finance	GLOX	https://coinmarketcap.com//currencies/glox-finance/	102.47
2891	GSPI Shopping.io Governance	GSPI	https://coinmarketcap.com//currencies/gspi-governance/	9.59
2892	Niubi Swap	NIU	https://coinmarketcap.com//currencies/niubiswap/	0.27
2893	YFII Gold	YFIIG	https://coinmarketcap.com//currencies/yfii-gold/	7.88
2894	OpenAlexa Protocol	OAP	https://coinmarketcap.com//currencies/openalexa-protocol/	2.4
2895	YFII Gold	YFIIG	https://coinmarketcap.com//currencies/yfii-gold/	7.88
2896	Ludena Protocol	LDN	https://coinmarketcap.com//currencies/ludena-protocol/	1.06
2897	Alibaba tokenized stock FTX	BABA	https://coinmarketcap.com//currencies/alibaba-tokenized-stock-ftx/	228.55
2898	ReapChain	REAP	https://coinmarketcap.com//currencies/reapchain/	0.1578
2899	Tranche Finance	SLICE	https://coinmarketcap.com//currencies/tranche-finance/	0.7489
2900	Ternio-ERC20	TERN	https://coinmarketcap.com//currencies/ternio-erc20/	6.17
2901	WhaleRoom	WHL	https://coinmarketcap.com//currencies/whaleroom/	5.84
2902	KOK	KOK	https://coinmarketcap.com//currencies/keystone-of-opportunity-knowledge/	0.5
2903	ETG Finance	ETGF	https://coinmarketcap.com//currencies/etg-finance/	1.07
2904	Akoin	AKN	https://coinmarketcap.com//currencies/akoin/	0.4183
2905	Index Cooperative	INDEX	https://coinmarketcap.com//currencies/index-cooperative/	20.71
2906	Shardus	ULT	https://coinmarketcap.com//currencies/shardus/	0.11
2907	Basis Dollar Share	BSDS	https://coinmarketcap.com//currencies/basis-dollar-share/	1.29
2908	Fisco Coin	FSCC	https://coinmarketcap.com//currencies/fisco/	0.67
2909	ARK Innovation ETF tokenized stock FTX	ARKK	https://coinmarketcap.com//currencies/ark-innovation-etf-tokenized-stock-ftx/	130.18
2910	Tesra	TSR	https://coinmarketcap.com//currencies/tesra/	0.01
2911	BitUP Token	BUT	https://coinmarketcap.com//currencies/bitup-token/	0
2912	Rowan Token	RWN	https://coinmarketcap.com//currencies/rowan-token/	0.01
2913	Dipper Network	DIP	https://coinmarketcap.com//currencies/dipper-network/	0.01
2914	Metrix Coin	MRX	https://coinmarketcap.com//currencies/metrix-coin/	0
2915	YAMv2	YAMV2	https://coinmarketcap.com//currencies/yam-v2/	12.94
2916	Tamy Token	TMT	https://coinmarketcap.com//currencies/tamy-token/	0.03
2917	BlockNoteX	BNOX	https://coinmarketcap.com//currencies/blocknotex/	0.57
2918	Uniris	UCO	https://coinmarketcap.com//currencies/uniris/	0.16
2919	UniDex	UNIDX	https://coinmarketcap.com//currencies/unidex/	0.74
2920	Shrimp Capital	SHRMP	https://coinmarketcap.com//currencies/shrimp-capital/	0.03
2921	MP4	MP4	https://coinmarketcap.com//currencies/mp4/	0.01
2922	Kangal	KANGAL	https://coinmarketcap.com//currencies/kangal/	0
2923	Temtum	TEM	https://coinmarketcap.com//currencies/temtum/	0.83
2924	Vectorspace AI	VXV	https://coinmarketcap.com//currencies/vectorspace-ai/	0.75
2925	FANZY	FX1	https://coinmarketcap.com//currencies/fanzy/	0
2926	Pastel	PSL	https://coinmarketcap.com//currencies/pastel/	0
2927	QFinance	QFI	https://coinmarketcap.com//currencies/qfinance/	1.2
2928	DRC mobility	DRC	https://coinmarketcap.com//currencies/drc-mobility/	0.15
2929	Easticoin	ESTI	https://coinmarketcap.com//currencies/easticoin/	0.02
2930	BULLS	BULLS	https://coinmarketcap.com//currencies/bulls/	0.04
2931	STABLE ASSET	STA	https://coinmarketcap.com//currencies/stable-asset/	0.64
2932	3X Long TRX Token	TRXBULL	https://coinmarketcap.com//currencies/3x-long-trx-token/	4.12
2933	GrafSound	GSMT	https://coinmarketcap.com//currencies/grafsound/	0.33
2934	Chickenkebab Finance	CHIK	https://coinmarketcap.com//currencies/chickenkebab-finance/	0.17
2935	renFIL	RENFIL	https://coinmarketcap.com//currencies/renfil/	57.7
2936	BitcoinX	BCX	https://coinmarketcap.com//currencies/bitcoinx/	0
2937	RamenSwap	RAMEN	https://coinmarketcap.com//currencies/ramenswap/	2.67
2938	Exen Coin	EXEN	https://coinmarketcap.com//currencies/exen-coin/	1.78
2939	Keep3r BSC Network	KP3RB	https://coinmarketcap.com//currencies/keep3r-bsc-network/	5.39
2940	Earn Defi Coin	EDC	https://coinmarketcap.com//currencies/earn-defi/	15.79
2941	ARCS	ARX	https://coinmarketcap.com//currencies/arcs/	0.32
2942	SHD CASH	SHDC	https://coinmarketcap.com//currencies/shd-cash/	0.03
2943	Jetfuel Finance	FUEL	https://coinmarketcap.com//currencies/jetfuel-finance/	189.93
2944	CyberTime Finance Token	CTF	https://coinmarketcap.com//currencies/cybertime-finance-token/	49.04
2945	Stacker Ventures	STACK	https://coinmarketcap.com//currencies/stacker-ventures/	30.72
2946	Water Finance	WTR	https://coinmarketcap.com//currencies/water-finance/	93.1
2947	Oracle System	ORC	https://coinmarketcap.com//currencies/oracle-system/	0
2948	CoinLoan	CLT	https://coinmarketcap.com//currencies/coinloan/	8.35
2949	Kevacoin	KVA	https://coinmarketcap.com//currencies/kevacoin/	0
2950	Crow Finance	CROW	https://coinmarketcap.com//currencies/crow-finance/	274.36
2951	Mithril Share	MIS	https://coinmarketcap.com//currencies/mithril-share/	4.16
2952	Puriever	PURE	https://coinmarketcap.com//currencies/puriever/	0.55
2953	GoldFinX	GIX	https://coinmarketcap.com//currencies/goldfinx/	2.83
2954	Eight Hours	EHRT	https://coinmarketcap.com//currencies/eight-hours/	0
2955	Landbox	LAND	https://coinmarketcap.com//currencies/landbox/	7.55
2956	Dynamic Set Dollar	DSD	https://coinmarketcap.com//currencies/dynamic-set-dollar/	0.07
2957	Ndau	NDAU	https://coinmarketcap.com//currencies/ndau/	18.18
2958	GALAXY NETWORK	GNC	https://coinmarketcap.com//currencies/galaxy-network/	0.01
2959	TokenAsset	NTB	https://coinmarketcap.com//currencies/tokenasset/	0.42
2960	3X Long BitMax Token Token	BTMXBULL	https://coinmarketcap.com//currencies/3x-long-bitmax-token-token/	724.38
2961	PayYoda	YOT	https://coinmarketcap.com//currencies/payyoda/	0.07
2962	UTU Protocol	UTU	https://coinmarketcap.com//currencies/utu-protocol/	0.14
2963	Pofid Dao	PFID	https://coinmarketcap.com//currencies/pofid-dao/	28.24
2964	World Token	WORLD	https://coinmarketcap.com//currencies/world-token/	0.08
2965	SportX	SX	https://coinmarketcap.com//currencies/sportx/	0.37
2966	xFund	XFUND	https://coinmarketcap.com//currencies/xfund/	613.47
2967	Nyan Finance	NYAN	https://coinmarketcap.com//currencies/nyan-finance/	1.87
2968	The Hash Speed	THS	https://coinmarketcap.com//currencies/the-hash-speed/	0.22
2969	ReFork	EFK	https://coinmarketcap.com//currencies/refork/	0.02
2970	3X Short Algorand Token	ALGOBEAR	https://coinmarketcap.com//currencies/3x-short-algorand-token/	0
2971	CoinHe Token	CHT	https://coinmarketcap.com//currencies/coinhe-token/	0.07
2972	Cybereits	CRE	https://coinmarketcap.com//currencies/cybereits/	0
2973	Ballswap	BSP	https://coinmarketcap.com//currencies/ballswap/	0
2974	Mute	MUTE	https://coinmarketcap.com//currencies/mute/	0.28
2975	Index Chain	IDX	https://coinmarketcap.com//currencies/index-chain/	0
2976	FlourMix	FLO	https://coinmarketcap.com//currencies/flourmix/	0.71
2977	Promotion Coin	PC	https://coinmarketcap.com//currencies/promotion-coin/	0
2978	Serum Ecosystem Token	SECO	https://coinmarketcap.com//currencies/serum-ecosystem-token/	4.91
2979	PizzaSwap	PIZZA	https://coinmarketcap.com//currencies/pizzaswap/	8.09
2980	3x Short Litecoin Token	LTCBEAR	https://coinmarketcap.com//currencies/3x-short-litecoin-token/	0.13
2981	Global Gaming	GMNG	https://coinmarketcap.com//currencies/global-gaming/	0
2982	United Bitcoin	UBTC	https://coinmarketcap.com//currencies/united-bitcoin/	0.77
2983	YFi Management	YEFIM	https://coinmarketcap.com//currencies/yfi-management/	18.38
2984	Prophecy	PRY	https://coinmarketcap.com//currencies/prophecy/	0.05
2985	3X Short Cosmos Token	ATOMBEAR	https://coinmarketcap.com//currencies/3x-short-cosmos-token/	0
2986	UBU Finance	UBU	https://coinmarketcap.com//currencies/ubu-finance/	0.55
2987	Ti-Value	TV	https://coinmarketcap.com//currencies/ti-value/	0.01
2988	DFOhub	BUIDL	https://coinmarketcap.com//currencies/dfohub/	4.65
2989	V-Dimension	VOLLAR	https://coinmarketcap.com//currencies/v-dimension/	0.37
2990	Divert Finance	DEVE	https://coinmarketcap.com//currencies/divert-finance/	153.6
2991	Lukki Operating Token	LOT	https://coinmarketcap.com//currencies/lukki-operating-token/	0
2992	LCG	LCG	https://coinmarketcap.com//currencies/lcg/	0.02
2993	Basis Dollar	BSD	https://coinmarketcap.com//currencies/basis-dollar/	0.07057
2994	SIMBA Storage Token	SST	https://coinmarketcap.com//currencies/simba-storage-token/	0.4969
2995	Tcoin	TCO	https://coinmarketcap.com//currencies/tcoin-token/	0.0014
2996	ROCKI	ROCKS	https://coinmarketcap.com//currencies/rocki/	0.211
2997	Bitcashpay	BCP	https://coinmarketcap.com//currencies/bitcashpay/	0.01636
2998	Satopay Network	STOP	https://coinmarketcap.com//currencies/satopay-network/	0.002692
2999	Buxcoin	BUX	https://coinmarketcap.com//currencies/buxcoin/	0.09464
3000	Global Digital Content	GDC	https://coinmarketcap.com//currencies/global-digital-content/	0.004648
3001	GYEN	GYEN	https://coinmarketcap.com//currencies/gyen/	0.009173
3002	3X Long Midcap Index Token	MIDBULL	https://coinmarketcap.com//currencies/3x-long-midcap-index-token/	3275
3003	Shabu Shabu Finance	KOBE	https://coinmarketcap.com//currencies/shabu-shabu-finance/	4.88
3004	bZx Vesting Token	VBZRX	https://coinmarketcap.com//currencies/vbzrx/	0.16
3005	BitoPro Exchange Token	BITO	https://coinmarketcap.com//currencies/bitopro-exchange-token/	0.05
3006	Debase	DEBASE	https://coinmarketcap.com//currencies/debase/	0.5
3007	NNB Token	NNB	https://coinmarketcap.com//currencies/nnb-token/	0
3008	Soju Finance	SOJU	https://coinmarketcap.com//currencies/soju-finance/	0.45
3009	Gstcoin	GST	https://coinmarketcap.com//currencies/gstcoin/	0.35
3010	Golden Token	GOLD	https://coinmarketcap.com//currencies/golden-token/	0.09
3011	LONG COIN	LONG	https://coinmarketcap.com//currencies/long-coin/	0
3012	DegenVC	DGVC	https://coinmarketcap.com//currencies/degenvc/	1.26
3013	Freecash	FCH	https://coinmarketcap.com//currencies/freecash/	0.09
3014	CRYPTO20	C20	https://coinmarketcap.com//currencies/c20/	2.82
3015	USDFreeLiquidity	USDFL	https://coinmarketcap.com//currencies/usdfreeliquidity/	0.9
3016	Billibilli Inc tokenized stock FTX	BILI	https://coinmarketcap.com//currencies/billibilli-inc-tokenized-stock-ftx/	115.83
3017	APOyield	SOUL	https://coinmarketcap.com//currencies/apoyield/	0
3018	Ziktalk	ZIK	https://coinmarketcap.com//currencies/ziktalk/	0
3019	Cenfura Token	XCF	https://coinmarketcap.com//currencies/cenfura-token/	0.13
3020	Arcona	ARCONA	https://coinmarketcap.com//currencies/arcona/	0.29
3021	Libonomy	LBY	https://coinmarketcap.com//currencies/libonomy/	4.58
3023	BAEPAY	BAEPAY	https://coinmarketcap.com//currencies/baepay/	0.4
3024	Space Cow Boy	SCB	https://coinmarketcap.com//currencies/space-cow-boy/	17.09
3025	Spore Finance	SPORE	https://coinmarketcap.com//currencies/spore/	14.87
3026	GYSR	GYSR	https://coinmarketcap.com//currencies/gysr/	0.67
3027	Narwhale.finance	NAWA	https://coinmarketcap.com//currencies/narwhale-finance/	0.09
3028	VidyX	VIDYX	https://coinmarketcap.com//currencies/vidyx/	0.17
3029	ZUSD	ZUSD	https://coinmarketcap.com//currencies/zusd/	1
3030	Bincentive	BCNT	https://coinmarketcap.com//currencies/bincentive/	0.01
3031	Web Coin Pay	WEC	https://coinmarketcap.com//currencies/web-coin-pay/	2.03
3032	Experty Wisdom Token	WIS	https://coinmarketcap.com//currencies/experty-wisdom-token/	0.04
3033	Swaprol	SWPRL	https://coinmarketcap.com//currencies/swaprol/	0.01
3034	Xion Finance	XGT	https://coinmarketcap.com//currencies/xion-finance/	0.24
3035	Elevate	ELE	https://coinmarketcap.com//currencies/elevate/	0.65
3036	QUSD	QUSD	https://coinmarketcap.com//currencies/qian-qusd/	0.97
3037	PHILLIPS PAY COIN	PPC	https://coinmarketcap.com//currencies/philips-pay-coin/	0
3038	One Share	ONS	https://coinmarketcap.com//currencies/one-share/	6.85
3039	CSPC	CSPC	https://coinmarketcap.com//currencies/cspc/	0
3040	Spockchain Network	SPOK	https://coinmarketcap.com//currencies/spockchain-network/	0.01
3041	Meter Stable	MTR	https://coinmarketcap.com//currencies/meter-stable/	0.84
3042	Anime Token	ANI	https://coinmarketcap.com//currencies/anime-token/	0.1
3043	DefHold	DEFO	https://coinmarketcap.com//currencies/defhold/	119.8
3044	NEXT	NET	https://coinmarketcap.com//currencies/next/	0.14
3045	Tutor's Diary	TUDA	https://coinmarketcap.com//currencies/tutors-diary/	0.01
3046	JSB FOUNDATION	JSB	https://coinmarketcap.com//currencies/jsb-foundation/	1.77
3047	BitDNS	DNS	https://coinmarketcap.com//currencies/bitdns/	0.41
3048	Rozeus	ROZ	https://coinmarketcap.com//currencies/rozeus/	0.01
3049	3X Short Chainlink Token	LINKBEAR	https://coinmarketcap.com//currencies/3x-short-chainlink-token/	0
3050	MiniSwap	MINI	https://coinmarketcap.com//currencies/miniswap/	0.05
3051	Coinbase Pre-IPO tokenized stock FTX	CBSE	https://coinmarketcap.com//currencies/coinbase-pre-ipo-tokenized-stock-ftx/	444.26
3052	Galaxy Wallet	GC	https://coinmarketcap.com//currencies/galaxy-wallet/	0.01
3053	Liquidity Dividends Protocol	LID	https://coinmarketcap.com//currencies/liquidity-dividends-protocol/	0.04
3054	Portal	PORTAL	https://coinmarketcap.com//currencies/portal/	0
3055	sETH	SETH	https://coinmarketcap.com//currencies/seth/	1762.27
3056	Wagerr	WGR	https://coinmarketcap.com//currencies/wagerr/	0.05
3057	VinDax Coin	VD	https://coinmarketcap.com//currencies/vindax-coin/	0.01
3058	LinkToken	LTK	https://coinmarketcap.com//currencies/linktoken/	0.02
3059	PayRue (Propel)	PROPEL	https://coinmarketcap.com//currencies/payrue-propel/	0
3060	Uberstate RIT 2.0	RIT20	https://coinmarketcap.com//currencies/uberstate-inc/	0.85
3061	BITCOINHEDGE	BTCHG	https://coinmarketcap.com//currencies/bitcoinhedge/	7.63
3062	CAPITAL X CELL	CXC	https://coinmarketcap.com//currencies/capital-x-cell/	0.05
3063	BUILD Finance	BUILD	https://coinmarketcap.com//currencies/build-finance/	33.45
3064	COVIR.IO	CVR	https://coinmarketcap.com//currencies/covir-io/	40.54
3065	3x Long Tezos Token	XTZBULL	https://coinmarketcap.com//currencies/3x-long-tezos-token/	35.66
3066	CAD Coin	CADC	https://coinmarketcap.com//currencies/cad-coin/	0.78
3067	Strudel Finance	$TRDL	https://coinmarketcap.com//currencies/strudel-finance/	2.62
3068	REALPAY	RAP	https://coinmarketcap.com//currencies/realpay/	0.56
3069	DSYS	DSYS	https://coinmarketcap.com//currencies/dsys/	0
3070	DoDreamChain	DRM	https://coinmarketcap.com//currencies/dodreamchain/	0.05
3071	JD Coin	JDC	https://coinmarketcap.com//currencies/jd-coin/	0.14
3072	Absorber Protocol	ABS	https://coinmarketcap.com//currencies/absorber-protocol/	5.47
3073	Interop	TROP	https://coinmarketcap.com//currencies/interop/	37.38
3074	Bankcoin Reserve	BCR	https://coinmarketcap.com//currencies/bankcoin-reserve/	12170.24
3075	CommunityToken	CT	https://coinmarketcap.com//currencies/communitytoken/	0
3076	BioNTech tokenized stock FTX	BNTX	https://coinmarketcap.com//currencies/biontech-tokenized-stock-ftx/	108.16
3077	ARMTOKEN	TARM	https://coinmarketcap.com//currencies/armtoken/	0.02
3078	CORN	CORN	https://coinmarketcap.com//currencies/corn/	10.27
3079	UNIFI DeFi	UNIFI	https://coinmarketcap.com//currencies/unifi-defi/	0.01
3080	HappinessToken	HPS	https://coinmarketcap.com//currencies/happinesstoken/	4.48
3081	N3RD Finance	N3RDz	https://coinmarketcap.com//currencies/n3rd-finance/	159.46
3082	CryptoKek	KEK	https://coinmarketcap.com//currencies/cryptokek/	0.08
3083	3X Long Huobi Token Token	HTBULL	https://coinmarketcap.com//currencies/3x-long-huobi-token-token/	446.9
3084	Crypto Heroes	HEROES	https://coinmarketcap.com//currencies/crypto-heroes/	1.02
3085	Blockchain Exchange Alliance	BXA	https://coinmarketcap.com//currencies/blockchain-exchange-alliance/	0
3086	3x Short Bitcoin Cash Token	BCHBEAR	https://coinmarketcap.com//currencies/3x-short-bitcoin-cash-token/	0.05
3087	MP3	MP3	https://coinmarketcap.com//currencies/mp3/	0.03
3088	R34P	R34P	https://coinmarketcap.com//currencies/r34p/	15.79
3089	CheeseSwap	CHS	https://coinmarketcap.com//currencies/cheeseswap/	0.59
3090	Punk	PUNK	https://coinmarketcap.com//currencies/punk/	156276.48
3091	Bit Financial	BFC	https://coinmarketcap.com//currencies/bit-financial/	0.02
3092	DEJAVE	DJV	https://coinmarketcap.com//currencies/dejave/	2025.01
3093	United	UTED	https://coinmarketcap.com//currencies/united/	0.006137
3094	ECOC Financial Growth	EFG	https://coinmarketcap.com//currencies/ecoc-financial-growth/	18.76
3095	FOCV	FOCV	https://coinmarketcap.com//currencies/focv/	0.009672
3096	YAS	YAS	https://coinmarketcap.com//currencies/yas/	0.03239
3097	Viking Swap	VIKING	https://coinmarketcap.com//currencies/viking-swap/	0.3271
3098	PieDAO Balanced Crypto Pie	BCP	https://coinmarketcap.com//currencies/piedao-balanced-crypto-pie/	3.25
3099	YMAX	YMAX	https://coinmarketcap.com//currencies/ymax/	9.89
3100	Transmute Protocol	XPB	https://coinmarketcap.com//currencies/transmute-protocol/	0.8131
3101	Daikicoin	DIC	https://coinmarketcap.com//currencies/daikicoin/	0.147
3102	SHD CASH	SHDC	https://coinmarketcap.com//currencies/shd-cash/	0.03
3103	Asia Reserve Currency Coin	ARCC	https://coinmarketcap.com//currencies/asia-reserve-currency-coin/	0.11
3104	K-Tune	KTT	https://coinmarketcap.com//currencies/k-tune/	0.13
3105	Cometh	MUST	https://coinmarketcap.com//currencies/cometh/	188.93
3106	Kiwi Finance	KIWI	https://coinmarketcap.com//currencies/kiwi-finance/	8.03
3107	DUO Network Token	DUO	https://coinmarketcap.com//currencies/duo-network-token/	0
3108	BitDEFi	BFI	https://coinmarketcap.com//currencies/bitdefi/	12.81
3109	Upper Dollar	USDU	https://coinmarketcap.com//currencies/upper-dollar/	1
3110	Deipool	DIP	https://coinmarketcap.com//currencies/deipool/	0.14
3111	Wrapped Celo	WCELO	https://coinmarketcap.com//currencies/wrapped-celo/	3.75
3112	AICON	AICO	https://coinmarketcap.com//currencies/aicon/	0.08
3113	KP2R.Network	KP2R	https://coinmarketcap.com//currencies/kp2r-network/	0.74
3114	LavaSwap	LAVA	https://coinmarketcap.com//currencies/lavaswap/	0.63
3115	YFIX Finance	YFIX	https://coinmarketcap.com//currencies/yfix-finance/	2.5
3116	Jubi Token	JT	https://coinmarketcap.com//currencies/jubi-token/	0.14
3117	Meter Governance mapped by Meter.io	eMTRG	https://coinmarketcap.com//currencies/meter-governance-mapped-by-meter-io/	1.24
3118	Jackpot	777	https://coinmarketcap.com//currencies/jackpot/	0.16
3119	KeyFi	KEYFI	https://coinmarketcap.com//currencies/keyfi/	2.07
3120	Extend Finance	EXF	https://coinmarketcap.com//currencies/extend-finance/	0
3121	PUML Better Health	PUML	https://coinmarketcap.com//currencies/puml-better-health/	0.02
3122	Grain	GRAIN	https://coinmarketcap.com//currencies/grain-token/	0.04
3123	Ulgen Hash Power	UHP	https://coinmarketcap.com//currencies/ulgen-hash-power/	0.05
3124	Goldblock	GBK	https://coinmarketcap.com//currencies/goldblock/	0
3125	LYFE	LYFE	https://coinmarketcap.com//currencies/lyfe/	0.04
3126	Xriba	XRA	https://coinmarketcap.com//currencies/xriba/	0.04
3127	Crudeoil Finance	OIL	https://coinmarketcap.com//currencies/crudeoil-finance/	11.62
3128	Yearn Loans Finance	YLFI	https://coinmarketcap.com//currencies/yearn-loans-finance/	15.37
3129	Interfinex	IFEX	https://coinmarketcap.com//currencies/interfinex/	0
3130	Smol	SMOL	https://coinmarketcap.com//currencies/smol/	31.15
3131	Gbrick	GBX	https://coinmarketcap.com//currencies/gbrick/	0.01
3132	Fashion Coin	FSHN	https://coinmarketcap.com//currencies/fashion-coin/	0
3133	bXIOT	BXIOT	https://coinmarketcap.com//currencies/bxiot/	2.69
3134	BLOCKMAX	OCB	https://coinmarketcap.com//currencies/blockmax/	0.04
3135	3X Long Shitcoin Index Token	BULLSHIT	https://coinmarketcap.com//currencies/3x-long-shitcoin-index-token/	6490
3136	Krypton Galaxy Coin	KGC	https://coinmarketcap.com//currencies/krypton-galaxy-coin/	0
3137	Cryptochrome	CHM	https://coinmarketcap.com//currencies/cryptochrome/	0.02
3138	Dogz	DOGZ	https://coinmarketcap.com//currencies/dogz/	0
3139	Libfx	LIBFX	https://coinmarketcap.com//currencies/libfx/	0.31
3140	MEX	MEX	https://coinmarketcap.com//currencies/mex/	0
3141	Naruto BSC	NARUTO2	https://coinmarketcap.com//currencies/naruto-bsc/	0.13
3142	YFTether	YFTE	https://coinmarketcap.com//currencies/yftether/	73.86
3143	Dymmax	DMX	https://coinmarketcap.com//currencies/dymmax/	1.72
3144	Crypto Price Index	CPI	https://coinmarketcap.com//currencies/crypto-price-index/	0.2
3145	X-Block	IX	https://coinmarketcap.com//currencies/x-block/	0
3146	JFIN	JFC	https://coinmarketcap.com//currencies/jfin/	0.17
3147	Dragonbit	DRGB	https://coinmarketcap.com//currencies/dragonbit/	0
3148	TUNE TOKEN	TUNE	https://coinmarketcap.com//currencies/tune-token/	0
3149	NanTrade	NAN	https://coinmarketcap.com//currencies/nantrade/	0.36
3150	Reserve	RSV	https://coinmarketcap.com//currencies/reserve/	1.01
3151	MGC Token	MGC	https://coinmarketcap.com//currencies/mgc-token/	0
3152	Grace Period Token	GPT	https://coinmarketcap.com//currencies/grace-period-token/	1015.87
3153	ACoconut	AC	https://coinmarketcap.com//currencies/acoconut/	2.34
3154	EtherBone	ETHBN	https://coinmarketcap.com//currencies/etherbone/	0
3155	YFOX FINANCE	YFOX	https://coinmarketcap.com//currencies/yfox-finance/	12.9
3156	CAT.trade Protocol	CATX	https://coinmarketcap.com//currencies/cat-trade-protocol/	0.11
3157	Bundles Finance	BUND	https://coinmarketcap.com//currencies/bundles/	22.33
3158	Upper Pound	GBPU	https://coinmarketcap.com//currencies/upper-pound/	1.38
3159	Polkainsure Finance	PIS	https://coinmarketcap.com//currencies/polkainsure-finance/	25.8
3160	AMC Entertainment Holdings tokenized stock FTX	AMC	https://coinmarketcap.com//currencies/amc-entertainment-holdings-tokenized-stock-ftx/	12.94
3161	SUP8EME	SUP8EME	https://coinmarketcap.com//currencies/sup8eme/	4.51
3162	Grayscale Bitcoin Trust tokenized stock FTX	GBTC	https://coinmarketcap.com//currencies/grayscale-bitcoin-trust-tokenized-stock-ftx/	50.53
3163	Candy Protocol	CAD	https://coinmarketcap.com//currencies/candy-protocol/	0.8
3164	CENTERCOIN	CENT	https://coinmarketcap.com//currencies/centercoin/	0
3165	Hydro	HYDRO	https://coinmarketcap.com//currencies/hydro/	0
3166	LemoChain	LEMO	https://coinmarketcap.com//currencies/lemochain/	0
3167	hodlearn	HODL	https://coinmarketcap.com//currencies/hodlearn-net/	0
3168	Alchemy	ACOIN	https://coinmarketcap.com//currencies/alchemy/	0.8
3169	WingShop	WING	https://coinmarketcap.com//currencies/wingshop/	0
3170	BeeSwap	BEE	https://coinmarketcap.com//currencies/beeswap/	5.85
3171	Ztranzit Coin	ZTNZ	https://coinmarketcap.com//currencies/ztranzit-coin/	7.81
3172	Bullswap Exchange	BVL	https://coinmarketcap.com//currencies/bullswap-exchange/	0
3173	DeFiSocial Gaming	DFSOCIAL	https://coinmarketcap.com//currencies/defisocial-gaming/	107.52
3174	3X Short BNB Token	BNBBEAR	https://coinmarketcap.com//currencies/3x-short-bnb-token/	0
3175	One Cash	ONC	https://coinmarketcap.com//currencies/one-cash/	0.44
3176	IterationSyndicate	ITS	https://coinmarketcap.com//currencies/iterationsyndicate/	45.4
3177	Bytus	BYTS	https://coinmarketcap.com//currencies/bytus/	1.23
3178	FIRST INTERCHANGEABLE TOKEN	FIT	https://coinmarketcap.com//currencies/first-interchangeable-token/	18.65
3179	Sphere	SPHR	https://coinmarketcap.com//currencies/sphere/	0.34
3180	Goldcoin	GLC	https://coinmarketcap.com//currencies/goldcoin/	0.18
3181	Camp	CAMP	https://coinmarketcap.com//currencies/camp/	0
3182	growth Root Token	GROOT	https://coinmarketcap.com//currencies/growth-root-token/	539.62
3183	3X Short Sushi Token	SUSHIBEAR	https://coinmarketcap.com//currencies/3x-short-sushi-token/	0
3184	FME	FME	https://coinmarketcap.com//currencies/fme/	0
3185	Carrot	CRT	https://coinmarketcap.com//currencies/carrot/	0.18
3186	UltrAlpha	UAT	https://coinmarketcap.com//currencies/ultralpha/	0.06
3187	ZYX	ZYX	https://coinmarketcap.com//currencies/zyx/	0.01
3188	PeerEx	PERX	https://coinmarketcap.com//currencies/peerex/	0
3189	intexcoin	INTX	https://coinmarketcap.com//currencies/intexcoin/	0
3190	Justdobet	JDB	https://coinmarketcap.com//currencies/justdobet/	1.23
3191	bitCEO	BCEO	https://coinmarketcap.com//currencies/bitceo/	0.36
3192	DOOS TOKEN	DOOS	https://coinmarketcap.com//currencies/doos-token/	0.07111
3193	Metric Exchange	METRIC	https://coinmarketcap.com//currencies/metric-exchange/	3.29
3194	Chintai	CHEX	https://coinmarketcap.com//currencies/chex-token/	0.004593
3195	Pokeball	POKE	https://coinmarketcap.com//currencies/pokeball/	0.1901
3196	Spice	SPICE	https://coinmarketcap.com//currencies/spice/	0.003735
3197	Groovy Finance	GVY	https://coinmarketcap.com//currencies/groovy-finance/	4.65
3198	KONJUNGATE	KONJ	https://coinmarketcap.com//currencies/konjungate/	0.007032
3199	1x Short Bitcoin Token	HEDGE	https://coinmarketcap.com//currencies/1x-short-bitcoin-token/	117.65
3200	Potentiam	PTM	https://coinmarketcap.com//currencies/potentiam/	0.05209
3201	RYI Platinum	RYIP	https://coinmarketcap.com//currencies/ryi-platinum/	3.67
3202	Spore Engineering	SPORE	https://coinmarketcap.com//currencies/spore-engineering/	5.48
3203	Lottonation	LNT	https://coinmarketcap.com//currencies/lottonation/	0.05
3204	Civitas Protocol	CVT	https://coinmarketcap.com//currencies/civitas-protocol/	217.48
3205	4ART Coin	4ART	https://coinmarketcap.com//currencies/4artechnologies/	0.18
3206	Moonday Finance	MOONDAY	https://coinmarketcap.com//currencies/moonday-finance/	52.12
3207	LUXOCHAIN	LUXO	https://coinmarketcap.com//currencies/luxochain/	0.19
3208	STPAY	STP	https://coinmarketcap.com//currencies/stpay/	5.89
3209	Release Project	REL	https://coinmarketcap.com//currencies/release-project/	0
3210	Nydronia	NIA	https://coinmarketcap.com//currencies/nydronia/	0
3211	Guapcoin	GUAP	https://coinmarketcap.com//currencies/guapcoin/	0.37
3212	MDsquare	TMED	https://coinmarketcap.com//currencies/mdsquare/	0
3213	Future1coin	F1C	https://coinmarketcap.com//currencies/future1coin/	0
3214	yRise Finance	YRISE	https://coinmarketcap.com//currencies/yrise-finance/	1.36
3215	HLand Token	HLAND	https://coinmarketcap.com//currencies/hland-token/	29.24
3216	Definex	DSWAP	https://coinmarketcap.com//currencies/definex/	1.79
3217	Digex	DIGEX	https://coinmarketcap.com//currencies/digex/	0
3218	BigBang Core	BBC	https://coinmarketcap.com//currencies/bigbang-core/	0.14
3219	XDAG	XDAG	https://coinmarketcap.com//currencies/xdag/	0.01
3220	Vera Cruz Coin	VCCO	https://coinmarketcap.com//currencies/vera-cruz-coin/	2.14
3221	IOEX	IOEX	https://coinmarketcap.com//currencies/ioex/	0.03
3222	Crypto Cricket Club	3Cs	https://coinmarketcap.com//currencies/crypto-cricket-club/	0.07
3223	My Crypto Play	MCP	https://coinmarketcap.com//currencies/my-crypto-play/	0.13
3224	Globalvillage Ecosystem	GVE	https://coinmarketcap.com//currencies/globalvillage-ecosystem/	0
3225	3X Long Altcoin Index Token	ALTBULL	https://coinmarketcap.com//currencies/3x-long-altcoin-index-token/	429.62
3226	Punk Basic	PUNK-BASIC	https://coinmarketcap.com//currencies/punk-basic/	36112.39
3227	OTCBTC Token	OTB	https://coinmarketcap.com//currencies/otcbtc-token/	0.01
3228	Grom	GR	https://coinmarketcap.com//currencies/grom/	3.73
3229	VKF Platform	VKF	https://coinmarketcap.com//currencies/vkf-platform/	0
3230	IZE	IZE	https://coinmarketcap.com//currencies/ize/	0.06
3231	1irstGold	1GOLD	https://coinmarketcap.com//currencies/1irstgold/	60.08
3232	Diamond	DMD	https://coinmarketcap.com//currencies/diamond/	2.53
3233	Social Rocket	ROCKS	https://coinmarketcap.com//currencies/social-rocket/	1.6
3234	Sav3Token	SAV3	https://coinmarketcap.com//currencies/sav3token/	0.05
3235	Tesla tokenized stock Bittrex	TSLA	https://coinmarketcap.com//currencies/tesla-tokenized-stock-bittrex/	704.72
3236	VersoView	VVT	https://coinmarketcap.com//currencies/versoview/	0.16
3237	TEAL	TEAT	https://coinmarketcap.com//currencies/teal/	0.14
3238	swiss.finance	SWISS	https://coinmarketcap.com//currencies/swiss-finance/	62.63
3239	Utile Network	UTL	https://coinmarketcap.com//currencies/utile-network/	0.05
3240	SERGS	SERGS	https://coinmarketcap.com//currencies/sergs/	0.48
3241	UNCL	UNCL	https://coinmarketcap.com//currencies/uncl/	12.22
3242	Ethanol	ENOL	https://coinmarketcap.com//currencies/ethanol/	162.35
3243	SWAG Finance	SWAG	https://coinmarketcap.com//currencies/swag-finance/	0.07
3244	Freedom Reserve	FR	https://coinmarketcap.com//currencies/freedom-reserve/	0
3245	YFFS Finance	YFFS	https://coinmarketcap.com//currencies/yffs/	1.56
3246	Zer-Dex	ZDX	https://coinmarketcap.com//currencies/zer-dex/	0
3247	dForce USDx	USDX	https://coinmarketcap.com//currencies/dforce-usdx/	1.02
3248	GSENetwork	GSE	https://coinmarketcap.com//currencies/gsenetwork/	0
3249	Joys Digital	JOYS	https://coinmarketcap.com//currencies/joys-digital/	0.01
3250	Dionpay	DION	https://coinmarketcap.com//currencies/dionpay/	0
3251	Crypto Rewards Studio	CRS	https://coinmarketcap.com//currencies/crypto-rewards-studio/	0.01
3252	KING OF DEFI	KODX	https://coinmarketcap.com//currencies/king-of-defi/	60.52
3253	Utopia Genesis Foundation	UOP	https://coinmarketcap.com//currencies/utopia-genesis-foundation/	0.41
3254	01coin	ZOC	https://coinmarketcap.com//currencies/01coin/	0
3255	Stronghold Token	SHX	https://coinmarketcap.com//currencies/stronghold-token/	0
3256	DoYourTip	DYT	https://coinmarketcap.com//currencies/doyourtip/	0.57
3257	Morpher	MPH	https://coinmarketcap.com//currencies/morpher/	0.04
3258	UniDexGas	UNDG	https://coinmarketcap.com//currencies/unidexgas/	190.72
3259	Mazzuma	MAZ	https://coinmarketcap.com//currencies/mazzuma/	0.73
3260	Yeld Finance	YELD	https://coinmarketcap.com//currencies/yeld-finance/	23.42
3261	Advanced Internet Blocks	AIB	https://coinmarketcap.com//currencies/advanced-internet-blocks/	0.02
3262	Thirm Protocol	THIRM	https://coinmarketcap.com//currencies/thirm-protocol/	10.9
3263	Bali Coin	BALI	https://coinmarketcap.com//currencies/bali-coin/	0.02
3264	Holyheld	HH	https://coinmarketcap.com//currencies/holyheld/	0.93
3265	Birthday Cake	BDAY	https://coinmarketcap.com//currencies/birthday-cake/	0.11
3266	BitTube	TUBE	https://coinmarketcap.com//currencies/bit-tube/	0.01
3267	Bancor Governance Token	VBNT	https://coinmarketcap.com//currencies/bancor-governance-token/	4.16
3268	Basix	BASX	https://coinmarketcap.com//currencies/basix/	0.69
3269	GM Holding	GM	https://coinmarketcap.com//currencies/gm-holding/	0.01
3270	Freight Trust & Clearing Network	EDI	https://coinmarketcap.com//currencies/freight-trust-clearing-network/	0
3271	Vanywhere	VANY	https://coinmarketcap.com//currencies/vanywhere/	0
3272	FANBI TOKEN	FBT	https://coinmarketcap.com//currencies/fanbi-token/	0
3273	Equilibria	XEQ	https://coinmarketcap.com//currencies/equilibria/	0.08
3274	AGAr	AGAR	https://coinmarketcap.com//currencies/agar/	390.82
3275	Dinastycoin	DCY	https://coinmarketcap.com//currencies/dinastycoin/	0
3276	Capital.Finance	CAP	https://coinmarketcap.com//currencies/capital-finance/	8.08
3277	X Infinity	XIF	https://coinmarketcap.com//currencies/x-infinity/	0.01
3278	SURF Finance	SURF	https://coinmarketcap.com//currencies/surf/	0.43
3279	Degens	DEGENS	https://coinmarketcap.com//currencies/degens/	3.44
3280	NewsToken	NEWOS	https://coinmarketcap.com//currencies/newstoken/	0
3281	SPDR S&P 500 ETF tokenized stock FTX	SPY	https://coinmarketcap.com//currencies/spdr-sp-500-etf-tokenized-stock-ftx/	397.26
3282	r3fi.finance	R3FI	https://coinmarketcap.com//currencies/r3fi-finance/	0.08
3283	XVIX	XVIX	https://coinmarketcap.com//currencies/xvix/	21.88
3284	Trade Butler Bot	TBB	https://coinmarketcap.com//currencies/trade-butler-bot/	495.21
3285	CashBackPro	CBP	https://coinmarketcap.com//currencies/cashbackpro/	0.32
3286	Revelation Finance	ADAO	https://coinmarketcap.com//currencies/revelation-finance/	4.91
3287	governance ZIL	GZIL	https://coinmarketcap.com//currencies/governance-zil/	307.75
3288	1X Short Ethereum Token	ETHHEDGE	https://coinmarketcap.com//currencies/1x-short-ethereum-token/	70.74
3289	BlockCDN	BCDN	https://coinmarketcap.com//currencies/blockcdn/	0
3290	Coral Swap	CORAL	https://coinmarketcap.com//currencies/coral-swap/	0.92
3291	BlockCDN	BCDN	https://coinmarketcap.com//currencies/blockcdn/	0.001829
3292	Coral Swap	CORAL	https://coinmarketcap.com//currencies/coral-swap/	0.9246
3293	gAsp	GASP	https://coinmarketcap.com//currencies/gasp/	0.4936
3294	SPDR S&P 500 ETF tokenized stock FTX	SPY	https://coinmarketcap.com//currencies/spdr-sp-500-etf-tokenized-stock-ftx/	397.26
3295	Bitsonic	BSC	https://coinmarketcap.com//currencies/bitsonic/	0.01529
3296	Degov	DEGOV	https://coinmarketcap.com//currencies/degov/	30.95
3297	Based Money	$BASED	https://coinmarketcap.com//currencies/based-money/	1.55
3298	DeFi100	D100	https://coinmarketcap.com//currencies/defi100/	0.1806
3299	MERCI	MERCI	https://coinmarketcap.com//currencies/merci/	0.000412
3300	Marblecoin	MBC	https://coinmarketcap.com//currencies/marblecoin/	0.8526
3301	Equilibria	XEQ	https://coinmarketcap.com//currencies/equilibria/	0.08
3302	Cofinex Coin	CNX	https://coinmarketcap.com//currencies/cofinex-coin/	0.03
3303	srnArt Gallery	SACT	https://coinmarketcap.com//currencies/srnartgallery/	0.07
3304	BuySell	BULL	https://coinmarketcap.com//currencies/buysell/	1.12
3305	Khipu Token	KIP	https://coinmarketcap.com//currencies/khipu-token/	0
3306	Midas	MIDAS	https://coinmarketcap.com//currencies/midas/	3.06
3307	HRDCOIN	HRD	https://coinmarketcap.com//currencies/hrdcoin/	0.01
3308	Taco Finance	TACO	https://coinmarketcap.com//currencies/taco-finance/	0.08
3309	Uber tokenized stock FTX	UBER	https://coinmarketcap.com//currencies/uber-tokenized-stock-ftx/	59.91
3310	TAMA EGG NiftyGotchi	TME	https://coinmarketcap.com//currencies/tama-egg-niftygotchi/	3907.24
3311	Xeonbit Token	XNS	https://coinmarketcap.com//currencies/xeonbit-token/	0.01
3312	Pasta Finance	PASTA	https://coinmarketcap.com//currencies/pasta-finance/	0.16
3313	Gadoshi	GADOSHI	https://coinmarketcap.com//currencies/gadoshi/	0
3314	City Coin	CITY	https://coinmarketcap.com//currencies/city-coin/	0.1
3315	Curio Governance	CGT	https://coinmarketcap.com//currencies/curio-governance/	0.43
3316	En-Tan-Mo	ETM	https://coinmarketcap.com//currencies/en-tan-mo/	0
3317	Bitpower	BPP	https://coinmarketcap.com//currencies/bitpower/	0.02
3318	Rug Proof	RPT	https://coinmarketcap.com//currencies/rug-proof/	0.39
3319	VeriBlock	VBK	https://coinmarketcap.com//currencies/veriblock/	0.01
3320	Tacos	TACO	https://coinmarketcap.com//currencies/tacos/	0.07
3321	EsportsPro	ESPRO	https://coinmarketcap.com//currencies/esportspro/	0.07
3322	Wrapped BIND	WBIND	https://coinmarketcap.com//currencies/wrapped-bind/	0.06
3323	Prime Whiterock Company	PWC	https://coinmarketcap.com//currencies/prime-whiterock-company/	0.02
3324	Receive Access Ecosystem	RAE	https://coinmarketcap.com//currencies/receive-access-ecosystem/	2.42
3325	Agoras Tokens	AGRS	https://coinmarketcap.com//currencies/agoras-tokens/	0.66
3326	A2A	A2A	https://coinmarketcap.com//currencies/a2a-50x-com/	0.02
3327	DEVA TOKEN	DEVA	https://coinmarketcap.com//currencies/deva-token/	1.03
3328	Midas Dollar	MDO	https://coinmarketcap.com//currencies/midas-dollar/	3.77
3329	3X Long Ethereum Classic Token	ETCBULL	https://coinmarketcap.com//currencies/3x-long-ethereum-classic-token/	62.8
3330	Securypto	SCU	https://coinmarketcap.com//currencies/securypto/	0.11
3331	Leading Coin 4 Entrepreneurs	LC4	https://coinmarketcap.com//currencies/leading-coin-4-entrepreneurs/	0.11
3332	AK12	AK12	https://coinmarketcap.com//currencies/ak12/	0.14
3333	Advanced Micro Devices tokenized stock FTX	AMD	https://coinmarketcap.com//currencies/advanced-micro-devices-tokenized-stock-ftx/	84.49
3334	SPACE-iZ	SPIZ	https://coinmarketcap.com//currencies/space-iz/	0
3335	SpankChain	SPANK	https://coinmarketcap.com//currencies/spankchain/	0.01
3336	AC Index	ACX	https://coinmarketcap.com//currencies/ac-index/	0.85
3337	PieDAO DEFI Small Cap	DEFI+S	https://coinmarketcap.com//currencies/piedao-defi-small-cap/	6.02
3338	XPToken.io	XPT	https://coinmarketcap.com//currencies/xptoken-io/	1.09
3339	Trendering	TRND	https://coinmarketcap.com//currencies/trendering/	6.96
3340	Ape Tools	NANA	https://coinmarketcap.com//currencies/ape-tools/	0.01
3341	APY Vision	VISION	https://coinmarketcap.com//currencies/apy-vision/	2.98
3342	Emanate	EMT	https://coinmarketcap.com//currencies/emanate/	0.01
3343	Healing Plus	HP	https://coinmarketcap.com//currencies/healing-plus/	0.05
3344	AI Network	AIN	https://coinmarketcap.com//currencies/ai-network/	0.07
3345	Unicap.finance	UCAP	https://coinmarketcap.com//currencies/unicap-finance/	1.01
3346	Excavo Finance	CAVO	https://coinmarketcap.com//currencies/excavo-finance/	273.41
3347	Bitsten Token	BST	https://coinmarketcap.com//currencies/bitsten-token/	0.03
3348	TTCRYPTO	TTC	https://coinmarketcap.com//currencies/ttcrypto/	1.57
3349	FRMx Token	FRMX	https://coinmarketcap.com//currencies/frmx-token/	9042.17
3350	Flits	FLS	https://coinmarketcap.com//currencies/flits/	0.99
3351	dXIOT	DXIOT	https://coinmarketcap.com//currencies/dxiot/	0.56
3352	snglsDAO	SGT	https://coinmarketcap.com//currencies/snglsdao/	0
3353	BNSD Finance	BNSD	https://coinmarketcap.com//currencies/bnsd-finance/	0.01
3354	ProperSix	PSIX	https://coinmarketcap.com//currencies/propersix/	0
3355	LifetionCoin	LTP	https://coinmarketcap.com//currencies/lifetioncoin/	0.01
3356	Filenet	FN	https://coinmarketcap.com//currencies/filenet/	0.01
3357	Tokemon	TKMN	https://coinmarketcap.com//currencies/tokemon/	146.57
3358	BitcoinSoV	BSOV	https://coinmarketcap.com//currencies/bitcoinsov/	0.22
3359	Wisdom Chain	WDC	https://coinmarketcap.com//currencies/wisdom-chain/	0.08
3360	Coinwaycoin	CAN	https://coinmarketcap.com//currencies/coinwaycoin/	0.68
3361	KEY	KEY	https://coinmarketcap.com//currencies/key/	0
3362	TheOvOrideA	OVOA	https://coinmarketcap.com//currencies/theovoridea/	1282.34
3363	Love Coin	LOVE	https://coinmarketcap.com//currencies/love-coin/	0
3364	Crypto Accept	ACPT	https://coinmarketcap.com//currencies/crypto-accept/	0
3365	Hybrid Bank Cash	HBC	https://coinmarketcap.com//currencies/hybrid-bank-cash/	0.01
3366	Everus	EVR	https://coinmarketcap.com//currencies/everus/	0.04
3367	Etherpay	ETHPY	https://coinmarketcap.com//currencies/etherpay/	0.02
3368	Counterparty	XCP	https://coinmarketcap.com//currencies/counterparty/	2.89
3369	Decentralized Nations	DENA	https://coinmarketcap.com//currencies/decentralized-nations/	6.51
3370	xscoin	XSC	https://coinmarketcap.com//currencies/xscoin/	0.49
3371	Bonkey dAPP	BNKY	https://coinmarketcap.com//currencies/bonkey-dapp/	0.01
3372	CORD.Finance	CORD	https://coinmarketcap.com//currencies/cord-finance/	16.75
3373	Yearn Finance DOT	YFDOT	https://coinmarketcap.com//currencies/yearn-finance-dot/	15.3
3374	Ubiner	UBIN	https://coinmarketcap.com//currencies/ubiner/	0
3375	Chicken	KFC	https://coinmarketcap.com//currencies/chicken/	5.22
3376	Talent Token	TTX	https://coinmarketcap.com//currencies/talent-token/	0.54
3377	LEVELG	LEVELG	https://coinmarketcap.com//currencies/levelg/	0.18
3378	VeraOne	VRO	https://coinmarketcap.com//currencies/veraone/	55.64
3379	BIOKKOIN	BKKG	https://coinmarketcap.com//currencies/biokkoin/	18.65
3380	Metacoin	MTC	https://coinmarketcap.com//currencies/metacoin/	0.09
3381	Asian-African Capital Chain	ACC	https://coinmarketcap.com//currencies/asian-african-capital-chain/	0.07
3382	TILWIKI	TLW	https://coinmarketcap.com//currencies/tilwiki/	4.53
3383	Alias	ALIAS	https://coinmarketcap.com//currencies/alias/	0.14
3384	Axioms	AXI	https://coinmarketcap.com//currencies/axioms/	0.05
3385	Giftedhands	GHD	https://coinmarketcap.com//currencies/giftedhands/	0
3386	Limestone Network	LIMEX	https://coinmarketcap.com//currencies/limestone-network/	0.01
3387	Mandi Token	MANDI	https://coinmarketcap.com//currencies/mandi-token/	0.02
3388	DeadSoul	DSL	https://coinmarketcap.com//currencies/deadsoul/	0.11
3389	COST COIN+	AKM	https://coinmarketcap.com//currencies/cost-coin/	0
3390	Ternio	TERN	https://coinmarketcap.com//currencies/ternio/	0.02
3391	Unobtanium	UNO	https://coinmarketcap.com//currencies/unobtanium/	53.71
3392	Aspire	ASP	https://coinmarketcap.com//currencies/aspire/	0.1541
3393	Fridn	EFAR	https://coinmarketcap.com//currencies/fridn/	0.2318
3394	Modex	MODEX	https://coinmarketcap.com//currencies/modex/	0.02179
3395	QChi Chain	QHC	https://coinmarketcap.com//currencies/qchi-chain/	0.007087
3396	Davion	DAVP	https://coinmarketcap.com//currencies/davion/	0.05303
3397	Davincij15 Token	DJ15	https://coinmarketcap.com//currencies/davincij15-token/	169.18
3398	WaterDrop	WDP	https://coinmarketcap.com//currencies/waterdrop/	33.65
3399	PalletOne	PTN	https://coinmarketcap.com//currencies/palletone/	0.001461
3400	YieldNyan	NYAN	https://coinmarketcap.com//currencies/yieldnyan/	0.0561
3401	Global Game Coin	GGC	https://coinmarketcap.com//currencies/global-game-coin/	1.22
3402	ThoreNext	THX	https://coinmarketcap.com//currencies/thorenext/	0.11
3403	Exchange Payment Coin	EXP	https://coinmarketcap.com//currencies/exchange-payment-coin/	0
3404	Pub Finance	PINT	https://coinmarketcap.com//currencies/pub-finance/	0.6
3405	Uranus	URAC	https://coinmarketcap.com//currencies/uranus/	0
3406	Kauri	KAU	https://coinmarketcap.com//currencies/kauri-crypto/	0.71
3407	Gomics	GOM	https://coinmarketcap.com//currencies/gomics/	0
3408	eosBLACK	BLACK	https://coinmarketcap.com//currencies/eosblack/	0
3409	Amazon tokenized stock FTX	AMZN	https://coinmarketcap.com//currencies/amazon-tokenized-stock-ftx/	3108.4
3410	MicroStrategy tokenized stock FTX	MSTR	https://coinmarketcap.com//currencies/microstrategy-tokenized-stock-ftx/	753.5
3411	Hyper Credit Network	HPAY	https://coinmarketcap.com//currencies/hyper-credit-network/	0.67
3412	LINA	LINA	https://coinmarketcap.com//currencies/lina/	0.01
3413	SaluS	SLS	https://coinmarketcap.com//currencies/salus/	14.44
3414	DWS	DWS	https://coinmarketcap.com//currencies/dws/	0
3415	Komet	KOMET	https://coinmarketcap.com//currencies/komet/	57.53
3416	BenePit Protocol	BNP	https://coinmarketcap.com//currencies/benepit-protocol/	0
3417	Dark.Build	DARK	https://coinmarketcap.com//currencies/darkbuild/	91.28
3418	FLAMA	FMA	https://coinmarketcap.com//currencies/flama/	0.01
3419	Stacy	STACY	https://coinmarketcap.com//currencies/stacy/	0
3420	Xdef Finance	XDEF2	https://coinmarketcap.com//currencies/xdef-finance/	0.21
3421	Dice	DICE	https://coinmarketcap.com//currencies/dice/	0
3422	Keysians Network	KEN	https://coinmarketcap.com//currencies/keysians-network/	1.17
3423	Rootkit Finance	ROOT	https://coinmarketcap.com//currencies/rootkit-finance/	5304.98
3424	Cipher Core Token	CIPHC	https://coinmarketcap.com//currencies/cipher-core-token/	6.67
3425	Derivex	DVX	https://coinmarketcap.com//currencies/derivex/	0
3426	HOLD	HOLD	https://coinmarketcap.com//currencies/hold/	0.01
3427	happy birthday coin	HBDC	https://coinmarketcap.com//currencies/happy-birthday-coin/	0.03
3428	Coupon Chain	CCT	https://coinmarketcap.com//currencies/coupon-chain/	1.64
3429	TideBit Token	TBT	https://coinmarketcap.com//currencies/tidebit-token/	0.06
3430	DragonFarm Finance	DRAGON	https://coinmarketcap.com//currencies/dragonfarm-finance/	0
3431	Trade.win	TWI	https://coinmarketcap.com//currencies/trade-win/	0.07
3432	Global Reserve System	GLOB	https://coinmarketcap.com//currencies/global-reserve-system/	23.92
3433	Bone	BONE	https://coinmarketcap.com//currencies/bone/	0.53
3434	LOA Protocol	LOA	https://coinmarketcap.com//currencies/loa-protocol/	0.01
3435	Psychic	PSY	https://coinmarketcap.com//currencies/psychic/	0.14
3436	EOX	EOX	https://coinmarketcap.com//currencies/eox/	0.03
3437	PREDIQT	PQT	https://coinmarketcap.com//currencies/prediqt/	0
3438	McBase Finance	MCBASE	https://coinmarketcap.com//currencies/mcbase-finance/	2.54
3439	CARAT	CARAT	https://coinmarketcap.com//currencies/carat/	0.27
3440	CloudCoin	CCE	https://coinmarketcap.com//currencies/cloudcoin/	0
3441	StakeHound Staked Ether	STETH	https://coinmarketcap.com//currencies/stakehound-staked-ether/	1636.55
3442	Ecoreal Estate	ECOREAL	https://coinmarketcap.com//currencies/ecoreal-estate/	0.17
3443	MEME CASH	MCH	https://coinmarketcap.com//currencies/meme-cash/	11.33
3444	VANCI FINANCE	VANCII	https://coinmarketcap.com//currencies/vanci-finance/	10.52
3445	Wrapped LEO	WLEO	https://coinmarketcap.com//currencies/wrapped-leo/	1.09
3446	ISALCOIN	ISAL	https://coinmarketcap.com//currencies/isalcoin/	0.16
3447	Vinci	VINCI	https://coinmarketcap.com//currencies/vinci/	8.21
3448	Bitalgo	ALG	https://coinmarketcap.com//currencies/bitalgo/	0.15
3449	LIQUID	LIQUID	https://coinmarketcap.com//currencies/liquidefi/	27.04
3450	Taiwan Semiconductor Mfg tokenized stock FTX	TSM	https://coinmarketcap.com//currencies/taiwan-semiconductor-mfg-tokenized-stock-ftx/	120.8
3451	Universa	UTNP	https://coinmarketcap.com//currencies/universa/	0
3452	DeFi Omega	DFIO	https://coinmarketcap.com//currencies/defi-omega/	6.07
3453	Electra Protocol	XEP	https://coinmarketcap.com//currencies/electra-protocol/	0
3454	Uniform Fiscal Object	UFO	https://coinmarketcap.com//currencies/uniform-fiscal-object/	0
3455	Combine.finance	COMB	https://coinmarketcap.com//currencies/combine-finance/	94.23
3456	BitherCash	BICAS	https://coinmarketcap.com//currencies/bithercash/	0.26
3457	Aureus Nummus Gold	ANG	https://coinmarketcap.com//currencies/aureus-nummus-gold/	0
3458	Value Set Dollar	VSD	https://coinmarketcap.com//currencies/value-set-dollar/	1.06
3459	SimpleChain	SIMPLE	https://coinmarketcap.com//currencies/simplechain/	0.18
3460	VELO Token	VLO	https://coinmarketcap.com//currencies/velo-token/	0
3461	LegalBlock	LBK	https://coinmarketcap.com//currencies/legalblock/	0
3462	3X Short Dogecoin Token	DOGEBEAR	https://coinmarketcap.com//currencies/3x-short-dogecoin-token/	0
3463	Astosch	ATC	https://coinmarketcap.com//currencies/astosch/	0.03
3464	YAM v1	YAM	https://coinmarketcap.com//currencies/yamv1/	3.33
3465	Skillchain	SKI	https://coinmarketcap.com//currencies/skillchain/	0.04
3466	Nyan V2	NYAN-2	https://coinmarketcap.com//currencies/nyan-v2/	42.15
3467	Pollux Coin	POX	https://coinmarketcap.com//currencies/pollux-coin/	0.61
3468	Medicalveda	MVEDA	https://coinmarketcap.com//currencies/medicalveda/	0.08
3469	DeFi Gold	DFGL	https://coinmarketcap.com//currencies/defi-gold/	2.32
3470	StakeCubeCoin	SCC	https://coinmarketcap.com//currencies/stakecubecoin/	1.35
3471	Contribute	TRIB	https://coinmarketcap.com//currencies/contribute/	1.71
3472	Dfinance	XFI	https://coinmarketcap.com//currencies/dfinance/	0.06
3473	BiTToken	BITT	https://coinmarketcap.com//currencies/bittoken/	0.19
3474	Global X Change Token	GXT	https://coinmarketcap.com//currencies/global-x-change-token/	0.16
3475	BioNTech tokenized stock Bittrex	BNTX	https://coinmarketcap.com//currencies/biontech-tokenized-stock-bittrex/	108.65
3476	Opennity	OPNN	https://coinmarketcap.com//currencies/opennity/	0
3477	Lykke	LKK	https://coinmarketcap.com//currencies/lykke/	0.01
3478	WHEN Token	WHEN	https://coinmarketcap.com//currencies/when-token/	0
3479	HebeBlock	HEBE	https://coinmarketcap.com//currencies/hebeblock/	0
3480	3X Short TRX Token	TRXBEAR	https://coinmarketcap.com//currencies/3x-short-trx-token/	0
3481	PrimeFinance	PFI	https://coinmarketcap.com//currencies/primefinance/	3.64
3482	Boltt Coin 	BOLTT	https://coinmarketcap.com//currencies/boltt-coin/	0.16
3483	Typhoon Cash	PHOON	https://coinmarketcap.com//currencies/typhoon-cash/	19.92
3484	Zoom Protocol	ZOM	https://coinmarketcap.com//currencies/zoom-protocol/	14.11
3485	SwapShip	SWSH	https://coinmarketcap.com//currencies/swapship/	4.17
3486	ModiHost	AIM	https://coinmarketcap.com//currencies/modihost/	0.16
3487	Robotina	ROX	https://coinmarketcap.com//currencies/robotina/	0.01
3488	Adelphoi	ADL	https://coinmarketcap.com//currencies/adelphoi/	0.08
3489	Pledge Coin	PLG	https://coinmarketcap.com//currencies/pledge-coin/	0.0009816
3490	Nio tokenized stock FTX	NIO	https://coinmarketcap.com//currencies/nio-tokenized-stock-ftx/	45.65
3491	T.OS	TOSC	https://coinmarketcap.com//currencies/t-os/	0.01056
3492	Emrals	EMRALS	https://coinmarketcap.com//currencies/emrals/	0.009652
3493	CURE Farm	CURE	https://coinmarketcap.com//currencies/cure-farm/	2.79
3494	ZUM TOKEN	ZUM	https://coinmarketcap.com//currencies/zum-token/	0.0001979
3495	Xank	XANK	https://coinmarketcap.com//currencies/xank/	0.04384
3496	Predictz	PRDZ	https://coinmarketcap.com//currencies/predictz/	31.44
3497	NAGA	NGC	https://coinmarketcap.com//currencies/naga/	0.02792
3498	DeCash	DESH	https://coinmarketcap.com//currencies/decash/	0.0007993
3499	Terra SDT	SDT	https://coinmarketcap.com//currencies/terra-sdt/	1.53
3500	YieldPanda Finance	yPANDA	https://coinmarketcap.com//currencies/yieldpanda-finance/	0
3501	IYF.finance	IYF	https://coinmarketcap.com//currencies/iyf-finance/	3.22
3502	Apple Finance	APLP	https://coinmarketcap.com//currencies/apple-finance/	0.09
3503	Neumark	NEU	https://coinmarketcap.com//currencies/neumark/	0.15
3504	Nectar	NEC	https://coinmarketcap.com//currencies/nectar/	0.16
3505	Super CoinView Token	SCV	https://coinmarketcap.com//currencies/super-coinview/	0
3506	YFWorld	YFW	https://coinmarketcap.com//currencies/yfworld/	26.66
3507	Defi	DEFI	https://coinmarketcap.com//currencies/defi/	0.02
3508	YFMoonBeam	YFMB	https://coinmarketcap.com//currencies/yfmoonbeam/	11.05
3509	Apple tokenized stock FTX	AAPL	https://coinmarketcap.com//currencies/apple-tokenized-stock-ftx/	126.67
3510	SoPay	SOP	https://coinmarketcap.com//currencies/sopay/	0
3511	Ubique Chain Of Things	UCT	https://coinmarketcap.com//currencies/ubique-chain-of-things/	0
3512	KVANT	KVNT	https://coinmarketcap.com//currencies/kvant/	0.12
3513	Semitoken	SEMI	https://coinmarketcap.com//currencies/semitoken/	0
3514	iOWN Token	iOWN	https://coinmarketcap.com//currencies/iown-token/	0.01
3515	Tratok	TRAT	https://coinmarketcap.com//currencies/tratok/	0.04
3516	BlackPearl Token	BPLC	https://coinmarketcap.com//currencies/blackpearl-chain/	0
3517	PieDAO Yearn Ecosystem Pie	YPIE	https://coinmarketcap.com//currencies/piedao-yearn-ecosystem-pie/	3.25
3518	zHEGIC	ZHEGIC	https://coinmarketcap.com//currencies/zhegic/	0.25
3519	Gasgains	GASG	https://coinmarketcap.com//currencies/gasgains/	0.45
3520	Decurian	ECU	https://coinmarketcap.com//currencies/decurian/	888.23
3521	Toshify.finance	YFT	https://coinmarketcap.com//currencies/toshify-finance/	4.63
3522	BitCherry	BCHC	https://coinmarketcap.com//currencies/bitcherry/	0.01
3523	ENTONE	ENTONE	https://coinmarketcap.com//currencies/entone/	0.03
3524	Koloop Basic	KPC	https://coinmarketcap.com//currencies/koloop-basic/	0.06
3525	ECOChain	ECOC	https://coinmarketcap.com//currencies/ecochain/	0.04
3526	Square tokenized stock FTX	SQ	https://coinmarketcap.com//currencies/square-tokenized-stock-ftx/	249.3
3527	The LoveChain	LOV	https://coinmarketcap.com//currencies/the-lovechain/	0.03
3528	Upper Euro	EURU	https://coinmarketcap.com//currencies/upper-euro/	1.2
3529	YFISCURITY	YFIS	https://coinmarketcap.com//currencies/yfiscurity/	0.98
3530	NewYorkCoin	NYC	https://coinmarketcap.com//currencies/newyorkcoin/	0
3531	United Traders Token	UTT	https://coinmarketcap.com//currencies/uttoken/	0.43
3532	The global index chain	TGIC	https://coinmarketcap.com//currencies/the-global-index-chain/	0
3533	Ecoin	ECOIN	https://coinmarketcap.com//currencies/ecoin-2/	0
3534	NDN Link	NDN	https://coinmarketcap.com//currencies/ndn-link/	0.01
3535	XUSD Stable	XUSD	https://coinmarketcap.com//currencies/xusd-stable/	0.96
3536	Marscoin	MARS	https://coinmarketcap.com//currencies/marscoin/	0.18
3537	Kimochi Finance	KIMOCHI	https://coinmarketcap.com//currencies/kimochi-finance/	0.04
3538	LinkCoin Token	LKN	https://coinmarketcap.com//currencies/linkcoin-token/	0
3539	RoboCalls	RC20	https://coinmarketcap.com//currencies/robocalls/	0
3540	Dune Network	DUN	https://coinmarketcap.com//currencies/dune-network/	0
3541	Infinity Economics	XIN	https://coinmarketcap.com//currencies/infinity-economics/	0
3542	VNDC	VNDC	https://coinmarketcap.com//currencies/vndc/	0
3543	FirstBlood	1ST	https://coinmarketcap.com//currencies/firstblood/	0.24
3544	Diamond Platform Token	DPT	https://coinmarketcap.com//currencies/diamond-platform-token/	4.21
3545	PlayChip	PLA	https://coinmarketcap.com//currencies/playchip/	0
3546	ODEM	ODE	https://coinmarketcap.com//currencies/odem/	0.03
3547	MINDOL	MIN	https://coinmarketcap.com//currencies/mindol/	0.25
3548	Hithotx	HITX	https://coinmarketcap.com//currencies/hithotx/	0
3549	BPOP	BPOP	https://coinmarketcap.com//currencies/bpop/	0
3550	GorillaYield	YAPE	https://coinmarketcap.com//currencies/gorillayield/	0
3551	Quasarcoin	QAC	https://coinmarketcap.com//currencies/quasarcoin/	0.01
3552	MetaWhale BTC	MWBTC	https://coinmarketcap.com//currencies/metawhale-btc/	0.23
3553	Narwhalswap	NAR	https://coinmarketcap.com//currencies/narwhalswap/	0.33
3554	WEBN token	WEBN	https://coinmarketcap.com//currencies/webn-token/	0
3555	Consentium	CSM	https://coinmarketcap.com//currencies/consentium/	0
3556	HeartBout Pay	HP	https://coinmarketcap.com//currencies/heartbout-pay/	0
3557	Simbcoin Swap	SMBSWAP	https://coinmarketcap.com//currencies/simbcoin-swap/	0.96
3558	CanYaCoin	CAN	https://coinmarketcap.com//currencies/canyacoin/	0.05
3559	1x Long Bitcoin Implied Volatility Token	BVOL	https://coinmarketcap.com//currencies/1x-long-bitcoin-implied-volatility-token/	1220.14
3560	MITH Cash	MIC	https://coinmarketcap.com//currencies/mith-cash/	0.13
3561	WallStreet.Finance	WSF	https://coinmarketcap.com//currencies/wallstreet-finance/	96.12
3562	AurusGOLD	AWG	https://coinmarketcap.com//currencies/aurusgold/	55.53
3563	Golden Ratio Coin	GOLDR	https://coinmarketcap.com//currencies/golden-ratio-coin/	12.97
3564	Twitter tokenized stock FTX	TWTR	https://coinmarketcap.com//currencies/twitter-tokenized-stock-ftx/	70.68
3565	NoLimitCoin	NLC2	https://coinmarketcap.com//currencies/nolimitcoin/	0.01
3566	DeVault	DVT	https://coinmarketcap.com//currencies/devault/	0
3567	XNODE	XNODE	https://coinmarketcap.com//currencies/xnode/	1.04
3568	3X Long OKB Token	OKBBULL	https://coinmarketcap.com//currencies/3x-long-okb-token/	1483.5
3569	Metawhale Gold	MWG	https://coinmarketcap.com//currencies/metawhale-gold/	0.57
3570	Tepleton	TEP	https://coinmarketcap.com//currencies/tepleton/	0.01
3571	DogDeFiCoin	DOGDEFI	https://coinmarketcap.com//currencies/dogdeficoin/	1.21
3572	YFIDapp	YFID	https://coinmarketcap.com//currencies/yfidapp/	0.68
3573	LimitSwap	LIMIT	https://coinmarketcap.com//currencies/limitswap/	8.82
3574	ASYAGRO	ASY	https://coinmarketcap.com//currencies/asyagro/	0.04
3575	POP Network Token	POP	https://coinmarketcap.com//currencies/pop-network-token/	0
3576	Yearn Finance Network	YFN	https://coinmarketcap.com//currencies/yearn-finance-network/	0.33
3577	Imsmart	IMT	https://coinmarketcap.com//currencies/imsmart/	0.01
3578	DeepCloud AI	DEEP	https://coinmarketcap.com//currencies/deepcloud-ai/	0.01
3579	Empow	EM	https://coinmarketcap.com//currencies/empow/	0.01
3580	AmazonasCoin	AMZ	https://coinmarketcap.com//currencies/amazonascoin/	0
3581	BitCore	BTX	https://coinmarketcap.com//currencies/bitcore/	0.49
3582	Electronero Pulse	ETNXP	https://coinmarketcap.com//currencies/electronero-pulse/	0
3583	ESR Coin	ESRC	https://coinmarketcap.com//currencies/esr-coin/	0.44
3584	Timers	IPM	https://coinmarketcap.com//currencies/timers/	0.04
3585	YFIEXCHANGE.FINANCE	YFIE	https://coinmarketcap.com//currencies/yfiexchange-finance/	2.38
3586	Honey	HNY	https://coinmarketcap.com//currencies/honey-token/	1251.98
3587	ALQO	XLQ	https://coinmarketcap.com//currencies/alqo/	0.09
3588	Kompass	KOMP	https://coinmarketcap.com//currencies/kompass/	0.01
3589	UCX FOUNDATION	UCX	https://coinmarketcap.com//currencies/ucx-foundation/	0.2354
3590	NVIDIA tokenized stock FTX	NVDA	https://coinmarketcap.com//currencies/nvidia-tokenized-stock-ftx/	537.7
3591	Lyra	LYR	https://coinmarketcap.com//currencies/lyra/	0.0004379
3592	AEN Smart Token	AENS	https://coinmarketcap.com//currencies/aen-smart-token/	0.01168
3593	Ixinium	XXA	https://coinmarketcap.com//currencies/ixinium/	0.1836
3594	EcoG9coin	EGC	https://coinmarketcap.com//currencies/ecog9coin/	0.0006894
3595	Mythic Finance	MYTHIC	https://coinmarketcap.com//currencies/mythic-finance/	26.93
3596	Asgard finance	THOR	https://coinmarketcap.com//currencies/asgard-finance/	0.002852
3597	YFBitcoin	YFBTC	https://coinmarketcap.com//currencies/yfbitcoin/	399.05
3598	Passive Income	PSI	https://coinmarketcap.com//currencies/passive-income/	122.56
3599	3X Long Dragon Index Token	DRGNBULL	https://coinmarketcap.com//currencies/3x-long-dragon-index-token/	1252
3600	Big Bang Game Coin	BBGC	https://coinmarketcap.com//currencies/big-bang-game-coin/	0
3601	Neeva Defi	NVA	https://coinmarketcap.com//currencies/neeva-defi/	25.84
3602	Algory Project	ALG	https://coinmarketcap.com//currencies/algory-project/	0.15
3603	Braziliex Token	BRZX	https://coinmarketcap.com//currencies/braziliexs-token/	0.03
3604	Facebook tokenized stock Bittrex	FB	https://coinmarketcap.com//currencies/facebook-tokenized-stock-bittrex/	270.02
3605	KingXChain	KXC	https://coinmarketcap.com//currencies/kingxchain/	0
3606	MyTVchain	MYTV	https://coinmarketcap.com//currencies/mytvchain/	0.01
3607	Torex	TOR	https://coinmarketcap.com//currencies/torex/	0.11
3608	YFIA	YFIA	https://coinmarketcap.com//currencies/yfia/	3.58
3609	NTON	NTON	https://coinmarketcap.com//currencies/nton/	0.01
3610	CryptoBharatCoin	CBC	https://coinmarketcap.com//currencies/cryptobharatcoin/	0.15
3611	Vodi X	VDX	https://coinmarketcap.com//currencies/vodi-x/	0
3612	Ultra Clear	UCR	https://coinmarketcap.com//currencies/ultra-clear/	0.05
3613	Amazon tokenized stock Bittrex	AMZN	https://coinmarketcap.com//currencies/amazon-tokenized-stock-bittrex/	3115.08
3614	Chainpay	CPAY	https://coinmarketcap.com//currencies/chainpay/	0.03
3615	LUCY	LUCY	https://coinmarketcap.com//currencies/lucy/	0.02
3616	Umbria Network	UMBR	https://coinmarketcap.com//currencies/umbria-network/	1.74
3617	LOLTOKEN	LOL	https://coinmarketcap.com//currencies/loltoken/	0.01
3618	Yearn Shark Finance	YSKF	https://coinmarketcap.com//currencies/yearn-shark-finance/	1.69
3619	Daiquilibrium	DAIQ	https://coinmarketcap.com//currencies/daiquilibrium/	0.12
3620	Luxurious Pro Network Token	LPNT	https://coinmarketcap.com//currencies/luxurious-pro-network-token/	1.75
3621	3X Short TomoChain Token	TOMOBEAR	https://coinmarketcap.com//currencies/3x-short-tomochain-token/	0
3622	Historia	HTA	https://coinmarketcap.com//currencies/historia/	0
3623	Bonorum	BONO	https://coinmarketcap.com//currencies/bonorum/	27.3
3624	Prophet	PROPHET	https://coinmarketcap.com//currencies/prophet/	0.1
3625	Fractal	FCL	https://coinmarketcap.com//currencies/fractal/	0.97
3626	Inverse Bitcoin Volatility Token	IBVOL	https://coinmarketcap.com//currencies/inverse-bitcoin-volatility-token/	5152.49
3627	BitcoinV	BTCV	https://coinmarketcap.com//currencies/bitcoinv/	0.02
3628	UCROWDME	UCM	https://coinmarketcap.com//currencies/ucrowdme/	0
3629	3x Short Tezos Token	XTZBEAR	https://coinmarketcap.com//currencies/3x-short-tezos-token/	0.01
3630	Blood	BLOOD	https://coinmarketcap.com//currencies/blood/	0
3631	IDall	IDALL	https://coinmarketcap.com//currencies/idall/	0.01
3632	Polybius	PLBT	https://coinmarketcap.com//currencies/polybius/	2.23
3633	Compound Augur	CREP	https://coinmarketcap.com//currencies/compound-augur/	0.6
3634	Wrapped Conceal	WCCX	https://coinmarketcap.com//currencies/wrapped-conceal/	0.2
3635	The Universal Settlement Coin	TUSC	https://coinmarketcap.com//currencies/the-universal-settlement-coin/	0
3636	Sparkster	SPRK	https://coinmarketcap.com//currencies/sparkster/	0
3637	DEONEX COIN	DON	https://coinmarketcap.com//currencies/deonex-coin/	0.85
3638	Universal Protocol Token	UPT	https://coinmarketcap.com//currencies/universal-protocol-token/	0
3639	BuckHathCoin	BHIG	https://coinmarketcap.com//currencies/buck-hath-coin/	0.06
3640	Profile Utility Token	PUT	https://coinmarketcap.com//currencies/profile-utility-token/	0
3641	Cherry Token	YT	https://coinmarketcap.com//currencies/cherry-token/	0.79
3642	Rich Maker	RICH	https://coinmarketcap.com//currencies/rich-maker/	2.74
3643	XFUEL	XFUEL	https://coinmarketcap.com//currencies/xfuel/	0
3644	Electra	ECA	https://coinmarketcap.com//currencies/electra/	0
3645	Digital Rand	DZAR	https://coinmarketcap.com//currencies/digital-rand/	0.06
3646	XCredit	XFYI	https://coinmarketcap.com//currencies/xcredit/	0.71
3647	indaHash	IDH	https://coinmarketcap.com//currencies/indahash/	0.01
3648	Blockmason Link	BLINK	https://coinmarketcap.com//currencies/blockmason-link/	0
3649	Google tokenized stock Bittrex	GOOGL	https://coinmarketcap.com//currencies/google-tokenized-stock-bittrex/	2073.81
3650	Bestay	BSY	https://coinmarketcap.com//currencies/bestay/	0.14
3651	FLEX	FLEX	https://coinmarketcap.com//currencies/flex/	0.26
3652	DogeYield	DOGY	https://coinmarketcap.com//currencies/dogeyield/	0
3653	Crex Token	CREX	https://coinmarketcap.com//currencies/crex-token/	3.45
3654	RIFT Token	RIFT	https://coinmarketcap.com//currencies/rift-token/	23.33
3655	Mega Lottery Services Global	MLR	https://coinmarketcap.com//currencies/mega-lottery-services-global/	0.02
3656	Reflector.Finance	RFCTR	https://coinmarketcap.com//currencies/reflector-finance/	0.01
3657	IDL Token	IDL	https://coinmarketcap.com//currencies/idl-token/	2.39
3658	Defi Shopping Stake	DSS	https://coinmarketcap.com//currencies/defi-shopping-stake/	0.01
3659	Gric Coin	GC	https://coinmarketcap.com//currencies/gric-coin/	0.16
3660	TheToken.Network	TTN	https://coinmarketcap.com//currencies/thetoken-network/	3579.21
3661	Auroracoin	AUR	https://coinmarketcap.com//currencies/auroracoin/	0.21
3662	BitCanna	BCNA	https://coinmarketcap.com//currencies/bitcanna/	0.08
3663	Native Utility Token	NUT	https://coinmarketcap.com//currencies/native-utility-token/	23.15
3664	EasySwap	ESWA	https://coinmarketcap.com//currencies/easyswap/	0.06
3665	ACE	ACE	https://coinmarketcap.com//currencies/ace-entertainment/	0
3666	Airbnb tokenized stock FTX	ABNB	https://coinmarketcap.com//currencies/airbnb-tokenized-stock-ftx/	208.15
3667	Sergey Save Link	SSL	https://coinmarketcap.com//currencies/sergey-save-link/	179.97
3668	Dequant	DEQ	https://coinmarketcap.com//currencies/dequant/	0.25
3669	DigiDinar	DDR	https://coinmarketcap.com//currencies/digidinar/	1.06
3670	DMme	DMME	https://coinmarketcap.com//currencies/dmme/	0
3671	Omega Protocol Money	OPM	https://coinmarketcap.com//currencies/omega-protocol-money/	0
3672	Cyclops Treasure	CYTR	https://coinmarketcap.com//currencies/cyclops-treasure/	373.85
3673	Facebook tokenized stock FTX	FB	https://coinmarketcap.com//currencies/facebook-tokenized-stock-ftx/	281.02
3674	Google tokenized stock FTX	GOOGL	https://coinmarketcap.com//currencies/google-tokenized-stock-ftx/	2099.7
3675	Sense	SENSE	https://coinmarketcap.com//currencies/sense/	0.01
3676	yplutus	YPLT	https://coinmarketcap.com//currencies/yplutus/	0
3677	SatoExchange Token	SATX	https://coinmarketcap.com//currencies/satoexchange-token/	0
3678	Bitbook Gambling	BXK	https://coinmarketcap.com//currencies/bitbook-gambling/	0.01
3679	MACH Project	MACH	https://coinmarketcap.com//currencies/mach-project/	0.02
3680	MONNOS	MNS	https://coinmarketcap.com//currencies/monnos/	0
3681	ITEN	ITEN	https://coinmarketcap.com//currencies/iten/	0
3682	Eska	ESK	https://coinmarketcap.com//currencies/eska/	0.01
3683	Volt	ACDC	https://coinmarketcap.com//currencies/volt/	0
3684	SPDR S&P 500 ETF tokenized stock Bittrex	SPY	https://coinmarketcap.com//currencies/spdr-sp-500-etf-tokenized-stock-bittrex/	395.05
3685	GazeCoin	GZE	https://coinmarketcap.com//currencies/gazecoin/	0.05
3686	Flash	FLASH	https://coinmarketcap.com//currencies/flash/	0
3687	STONK	STONK	https://coinmarketcap.com//currencies/stonk/	0
3688	Cyber Network Token	CNT	https://coinmarketcap.com//currencies/cyber-network-token/	0
3689	ZenSports	SPORTS	https://coinmarketcap.com//currencies/zensports/	4.975e-05
3690	Elxis	LEX	https://coinmarketcap.com//currencies/elxis/	6.5
3691	Samurai	SAM	https://coinmarketcap.com//currencies/samurai/	7.74
3692	Tronipay	TRP	https://coinmarketcap.com//currencies/tronipay/	0.001422
3693	BLOCKIDCOIN	BID	https://coinmarketcap.com//currencies/blockidcoin/	0.00278
3694	IOV BlockChain	IOV	https://coinmarketcap.com//currencies/iov-blockchain/	8.453e-05
3695	ZCore Token	ZCRT	https://coinmarketcap.com//currencies/zcore-token/	0.2817
3696	THECASH	TCH	https://coinmarketcap.com//currencies/thecash/	0.008839
3697	ORS Group	ORS	https://coinmarketcap.com//currencies/ors-group/	0.009392
3698	Taxi	TAXI	https://coinmarketcap.com//currencies/taxi/	0.4998
3699	Zippie	ZIPT	https://coinmarketcap.com//currencies/zippie/	0
3700	Delphi Chain Link	DCL	https://coinmarketcap.com//currencies/delphi-chain-link/	0.01
3701	EarnBet	BET	https://coinmarketcap.com//currencies/earnbet/	0.12
3702	XSwap	XSP	https://coinmarketcap.com//currencies/xsp/	0
3703	Endorsit	EDS	https://coinmarketcap.com//currencies/endorsit/	0
3704	Wrapped Shift	WSHIFT	https://coinmarketcap.com//currencies/wshift/	0.08
3705	PocketNode	NODE	https://coinmarketcap.com//currencies/pocketnode/	0
3706	Orient	OFT	https://coinmarketcap.com//currencies/orient/	0.7
3707	Wallet Plus X	WPX	https://coinmarketcap.com//currencies/wallet-plus-x/	0
3708	Belt	BELT	https://coinmarketcap.com//currencies/belt/	63.53
3709	XTRM COIN	XTRM	https://coinmarketcap.com//currencies/xtrm-coin/	0
3710	Zero Collateral Dai	ZAI	https://coinmarketcap.com//currencies/zero-collateral-dai/	0
3711	Experience Token	EXT	https://coinmarketcap.com//currencies/experience-token/	0
3712	zzz.finance v2	ZZZV2	https://coinmarketcap.com//currencies/zzz-finance-v2/	2.98
3713	EncrypGen	DNA	https://coinmarketcap.com//currencies/encrypgen/	0.08
3714	Netflix tokenized stock FTX	NFLX	https://coinmarketcap.com//currencies/netflix-tokenized-stock-ftx/	532.22
3715	sBTC	sBTC	https://coinmarketcap.com//currencies/softbtc/	0.01
3716	Five Star Coin	FSC	https://coinmarketcap.com//currencies/five-star-coin/	0.07
3717	BitOnyx	BTNYX	https://coinmarketcap.com//currencies/bitonyx/	0.02
3718	Xenon	XEN	https://coinmarketcap.com//currencies/xenon-xen/	0
3719	PayPal tokenized stock FTX	PYPL	https://coinmarketcap.com//currencies/paypal-tokenized-stock-ftx/	253.03
3720	Novo	NOVO	https://coinmarketcap.com//currencies/novo/	3.39
3721	SKINCHAIN	SKC	https://coinmarketcap.com//currencies/skinchain/	0
3722	DEFI Top 5 Tokens Index	DEFI5	https://coinmarketcap.com//currencies/defi-top-5-tokens-index/	102.71
3723	Vybe	VYBE	https://coinmarketcap.com//currencies/vybe/	0.09
3724	YFED.Finance	YFED	https://coinmarketcap.com//currencies/yfedfinance/	0.58
3725	Foresight	FORS	https://coinmarketcap.com//currencies/foresight/	0.02
3726	PlusOneCoin	PLUS1	https://coinmarketcap.com//currencies/plusonecoin/	0.01
3727	Apple tokenized stock Bittrex	AAPL	https://coinmarketcap.com//currencies/apple-tokenized-stock-bittrex/	124.75
3728	Newsolution	NST	https://coinmarketcap.com//currencies/newsolution/	0
3729	THENODE	THE	https://coinmarketcap.com//currencies/thenode/	0.01
3730	Pfizer tokenized stock FTX	PFE	https://coinmarketcap.com//currencies/pfizer-tokenized-stock-ftx/	35.63
3731	DeFi Insurance Protocol	DFIP	https://coinmarketcap.com//currencies/defi-insurance-protocol/	0
3732	Sogur	SGR	https://coinmarketcap.com//currencies/sogur/	2.32
3733	Documentchain	DMS	https://coinmarketcap.com//currencies/documentchain/	0.02
3734	qiibee	QBX	https://coinmarketcap.com//currencies/qiibee/	0.01
3735	Hub - Human Trust Protocol	HUB	https://coinmarketcap.com//currencies/hub-human-trust-protocol/	0.01
3736	Thorium	TORM	https://coinmarketcap.com//currencies/thorium/	0.01
3737	ColossusXT	COLX	https://coinmarketcap.com//currencies/colossusxt/	0
3738	Vision Network	VSN	https://coinmarketcap.com//currencies/vision-network/	0
3739	Golden Ratio Per Liquidity	GRPL	https://coinmarketcap.com//currencies/goldenratioperliquidity/	2.57
3740	Moon Juice	JUICE	https://coinmarketcap.com//currencies/moon-juice/	0.04
3741	Beyond Meat Inc tokenized stock Bittrex	BYND	https://coinmarketcap.com//currencies/beyond-meat-inc-tokenized-stock-bittrex/	147.42
3742	SymVerse	SYM	https://coinmarketcap.com//currencies/symverse/	0.16
3743	Hauteclere Shards	HAUT	https://coinmarketcap.com//currencies/hauteclere-shards/	7.59
3744	WOWswap	WOW	https://coinmarketcap.com//currencies/wowswap/	20.48
3745	Diagon	DGN	https://coinmarketcap.com//currencies/diagon/	0
3746	Ymen.Finance	YMEN	https://coinmarketcap.com//currencies/ymen-finance/	1.82
3747	Yearn Finance Bit2	YFB2	https://coinmarketcap.com//currencies/yearn-finance-bit2/	11.7
3748	TokenBacon	BAK	https://coinmarketcap.com//currencies/tokenbacon/	1.27
3749	BEAR Coin	BEAR	https://coinmarketcap.com//currencies/voytek-bear-coin/	0.01
3750	Aunite	AUNIT	https://coinmarketcap.com//currencies/aunite/	0.02
3751	ShareAt	XAT	https://coinmarketcap.com//currencies/shareat/	1.01
3752	Beyond Meat tokenized stock FTX	BYND	https://coinmarketcap.com//currencies/beyond-meat-tokenized-stock-ftx/	142.98
3753	Jointer	JNTR	https://coinmarketcap.com//currencies/jointer/	0.16
3754	Robonomics Web Services	RWS	https://coinmarketcap.com//currencies/robonomics-web-services/	59927.87
3755	Litecash	CASH	https://coinmarketcap.com//currencies/litecash/	0
3756	Fission Cash	FCX	https://coinmarketcap.com//currencies/fission-cash/	2.38
3757	MoonDayPlus	MD+	https://coinmarketcap.com//currencies/moondayplus/	41.8
3758	TranslateMe Network Token	TMN	https://coinmarketcap.com//currencies/translateme-network-token/	0
3759	BlackBerry tokenized stock FTX	BB	https://coinmarketcap.com//currencies/blackberry-tokenized-stock-ftx/	12.21
3760	VENJOCOIN	VJC	https://coinmarketcap.com//currencies/venjocoin/	0.54
3761	EzyStayz	EZY	https://coinmarketcap.com//currencies/ezystayz/	0
3762	Kepler Network	KMW	https://coinmarketcap.com//currencies/kepler-network/	0
3763	Yearn Finance Red Moon	YFRM	https://coinmarketcap.com//currencies/yearn-finance-red-moon/	5.54
3764	ROMToken	ROM	https://coinmarketcap.com//currencies/romtoken/	0
3765	YFE Money	YFE	https://coinmarketcap.com//currencies/yfe-money/	4.96
3766	Richlab Token	RLE	https://coinmarketcap.com//currencies/richlab-token/	0
3767	ABCC Token	AT	https://coinmarketcap.com//currencies/abcc-token/	0.01
3768	LivenPay	LVN	https://coinmarketcap.com//currencies/livenpay/	0
3769	Billibilli tokenized stock Bittrex	BILI	https://coinmarketcap.com//currencies/billibilli-tokenized-stock-bittrex/	119.44
3770	Codeo Token	CODEO	https://coinmarketcap.com//currencies/codeo-token/	0
3771	Alibaba tokenized stock Bittrex	BABA	https://coinmarketcap.com//currencies/alibaba-tokenized-stock-bittrex/	229.71
3772	EXOR	EXOR	https://coinmarketcap.com//currencies/exor/	0
3773	PIRANHAS	$PIR	https://coinmarketcap.com//currencies/piranhas/	2.31
3774	PeepCoin	PCN	https://coinmarketcap.com//currencies/peepcoin/	0
3775	DeFiato	DFO	https://coinmarketcap.com//currencies/defiato/	0.01
3776	3X Short Ethereum Classic Token	ETCBEAR	https://coinmarketcap.com//currencies/3x-short-ethereum-classic-token/	0.04
3777	CPUcoin	CPU	https://coinmarketcap.com//currencies/cpucoin/	0.07
3778	ChallengeDac	CHL	https://coinmarketcap.com//currencies/challengedac/	0
3779	KUN	KUN	https://coinmarketcap.com//currencies/qian-kun/	0.9
3780	MTI Finance	MTI	https://coinmarketcap.com//currencies/mti-finance/	0.01
3781	Demeter Chain	DMTC	https://coinmarketcap.com//currencies/demeter-chain/	0.11
3782	Rivermount	RM	https://coinmarketcap.com//currencies/rivermount/	0.01
3783	yffi finance	YFFI	https://coinmarketcap.com//currencies/yffi-finance/	5.75
3784	RUSH	RUC	https://coinmarketcap.com//currencies/rush/	0.39
3785	AurusDeFi	AWX	https://coinmarketcap.com//currencies/aurusdefi/	3.22
3786	weBloc	WOK	https://coinmarketcap.com//currencies/webloc/	0
3787	YieldWars	WAR	https://coinmarketcap.com//currencies/yieldwars-com/	0.03
3788	KingMoney	KIM	https://coinmarketcap.com//currencies/kingmoney/	1639.73
3789	DigiDinar Token	DDRT	https://coinmarketcap.com//currencies/digidinar-token/	2.41
3790	TKN Token	TKNT	https://coinmarketcap.com//currencies/tkn-token/	0.0085
3791	Netflix tokenized stock Bittrex	NFLX	https://coinmarketcap.com//currencies/netflix-tokenized-stock-bittrex/	531.62
3792	ECC	ECC	https://coinmarketcap.com//currencies/eccoin/	0.000556
3793	Everyonescrypto	EOC	https://coinmarketcap.com//currencies/everyonescrypto/	0.01566
3794	Usechain Token	USE	https://coinmarketcap.com//currencies/usechain-token/	0.0003951
3795	Rank Token	RANK	https://coinmarketcap.com//currencies/rank-token/	0.0006751
3796	Blurt	BLURT	https://coinmarketcap.com//currencies/blurt/	0.01056
3797	WeSing Coin	WSC	https://coinmarketcap.com//currencies/wesing-coin/	0.0009723
3798	0cash	ZCH	https://coinmarketcap.com//currencies/0cash/	0.0005555
3799	Netkoin	NTK	https://coinmarketcap.com//currencies/netkoin/	0
3800	Free Tool Box Coin	FTB	https://coinmarketcap.com//currencies/free-tool-box-coin/	0
3801	eToro Euro	EURX	https://coinmarketcap.com//currencies/etoro-euro/	1.19
3802	SHENG	SHENG	https://coinmarketcap.com//currencies/sheng/	0
3803	BigdataCash	BDCASH	https://coinmarketcap.com//currencies/bigdatacash/	0.01
3804	Fiscus.fyi	FFYI	https://coinmarketcap.com//currencies/fiscus-fyi/	3.89
3805	CitiOs	R2R	https://coinmarketcap.com//currencies/citios/	0
3806	AlphaLink	ANK	https://coinmarketcap.com//currencies/alphalink/	0.01
3807	Dragon Ball	DRAGON	https://coinmarketcap.com//currencies/dragon-ball/	0.09
3808	Secure Cash	SCSX	https://coinmarketcap.com//currencies/secure-cash/	0.12
3809	YFI CREDITS GROUP	YFICG	https://coinmarketcap.com//currencies/yfi-credits-group/	0.08
3810	GermanCoin	GCX	https://coinmarketcap.com//currencies/germancoin/	0
3811	Codex	CDEX	https://coinmarketcap.com//currencies/codex/	0
3812	Hanacoin	HANA	https://coinmarketcap.com//currencies/hanacoin/	0
3813	Harcomia	HCA	https://coinmarketcap.com//currencies/harcomia/	0.03
3814	Enzo	NZO	https://coinmarketcap.com//currencies/enzo/	0.14
3815	SaveToken	SAVE	https://coinmarketcap.com//currencies/savetoken/	0.01
3816	Pfizer tokenized stock Bittrex	PFE	https://coinmarketcap.com//currencies/pfizer-tokenized-stock-bittrex/	34.98
3817	BitKAM	KAM	https://coinmarketcap.com//currencies/bitkam/	0
3818	Zuplo	ZLP	https://coinmarketcap.com//currencies/zuplo/	0
3819	Voltz	VOLTZ	https://coinmarketcap.com//currencies/voltz/	0.01
3820	Markaccy	MKCY	https://coinmarketcap.com//currencies/markaccy/	0.01
3821	USDA	USDA	https://coinmarketcap.com//currencies/usda/	0.08
3822	Schilling-Coin	SCH	https://coinmarketcap.com//currencies/schilling-coin/	0
3823	Maggie	MAG	https://coinmarketcap.com//currencies/maggie/	0
3824	Bit Trust System	BIUT	https://coinmarketcap.com//currencies/bit-trust-system/	0
3825	HOMT	HOMT	https://coinmarketcap.com//currencies/homt/	0.01
3826	Dragon Token	DT	https://coinmarketcap.com//currencies/dragon-token/	2.76
3827	StockChain	SCC	https://coinmarketcap.com//currencies/stockchain/	0
3828	OceanChain	OC	https://coinmarketcap.com//currencies/oceanchain/	0
3829	FuturoCoin	FTO	https://coinmarketcap.com//currencies/futurocoin/	0.01
3830	NOIZ	NOIZ	https://coinmarketcap.com//currencies/noizchain/	0.07
3831	Resistance	RES	https://coinmarketcap.com//currencies/resistance/	0
3832	Petrachor	PTA	https://coinmarketcap.com//currencies/petrachor/	0.01
3833	Aerotoken	AET	https://coinmarketcap.com//currencies/aerotoken/	0.05
3834	SorachanCoin	SORA	https://coinmarketcap.com//currencies/sorachancoin/	0.04
3835	The Hustle App	HUSL	https://coinmarketcap.com//currencies/the-hustle-app/	0
3836	Ladder Network Token	LAD	https://coinmarketcap.com//currencies/ladder-network-token/	0
3837	Our Pay	OUR	https://coinmarketcap.com//currencies/our-pay/	0.01
3838	One DEX	ODEX	https://coinmarketcap.com//currencies/one-dex/	0
3839	TokenSwap	TOP	https://coinmarketcap.com//currencies/tokenswap/	0.08
3840	Super Running Coin	SRC	https://coinmarketcap.com//currencies/super-running-coin/	0
3841	7Eleven	7E	https://coinmarketcap.com//currencies/7eleven/	0
3842	VN Token	VN	https://coinmarketcap.com//currencies/vn-token/	0.07
3843	P2P	P2P	https://coinmarketcap.com//currencies/p2p/	0
3844	AgaveCoin	AGVC	https://coinmarketcap.com//currencies/agavecoin/	0.05
3845	Epanus	EPS	https://coinmarketcap.com//currencies/epanus/	0.2
3846	Infinite Ricks	RICK	https://coinmarketcap.com//currencies/infinite-ricks/	0
3847	Sudan Gold Coin	SGC	https://coinmarketcap.com//currencies/sudan-gold-coin/	0
3848	Altbet	ABET	https://coinmarketcap.com//currencies/altbet/	0.01
3849	Xtock	XTX	https://coinmarketcap.com//currencies/xtock/	0
3850	3X Long Tether Gold Token	XAUTBULL	https://coinmarketcap.com//currencies/3x-long-tether-gold-token/	6357.5
3851	Fanaticos Cash	FCH	https://coinmarketcap.com//currencies/fanaticos-cash/	0
3852	YFI3.money	YFI3	https://coinmarketcap.com//currencies/yfi3-money/	2.94
3853	New Year Bull	NYB	https://coinmarketcap.com//currencies/new-year-bull/	0
3854	Azzure	AZZR	https://coinmarketcap.com//currencies/azzure/	0
3855	CelCoin	CELC	https://coinmarketcap.com//currencies/celcoin/	0
3856	Mavro	MAVRO	https://coinmarketcap.com//currencies/mavro/	0.17
3857	Colibri Protocol	CLBR	https://coinmarketcap.com//currencies/colibri/	0
3858	Sombe	SBE	https://coinmarketcap.com//currencies/sombe/	0
3859	Bitcurate	BTCR	https://coinmarketcap.com//currencies/bitcurate/	0.01
3860	Davies	DVS	https://coinmarketcap.com//currencies/davies/	0.02
3861	Amoveo	VEO	https://coinmarketcap.com//currencies/amoveo/	79.87
3862	Bitpayer Token	BPT	https://coinmarketcap.com//currencies/bitpayer-token/	0.3
3863	Junsonmingchncoin	JMC	https://coinmarketcap.com//currencies/junsonmingchncoin/	0
3864	BlockStamp	BST	https://coinmarketcap.com//currencies/blockstamp/	0.1
3865	Wallabee	WLB	https://coinmarketcap.com//currencies/wallabee/	0.57
3866	SaveNode	SNO	https://coinmarketcap.com//currencies/savenode/	0
3867	HGH Token	HGH	https://coinmarketcap.com//currencies/hgh-token/	0
3868	QOOBER	QOOB	https://coinmarketcap.com//currencies/qoober/	0.03
3869	Hatch DAO	HATCH	https://coinmarketcap.com//currencies/hatch-dao/	0.01
3870	Colletrix	CIPX	https://coinmarketcap.com//currencies/colletrix/	0
3871	Tronx Coin	TRONX	https://coinmarketcap.com//currencies/tronx-coin/	0.04
3872	Decentralized Data Assets Management	DDAM	https://coinmarketcap.com//currencies/decentralized-data-assets-management/	0
3873	HondaisCoin	HNDC	https://coinmarketcap.com//currencies/hondaiscoin/	0
3874	MYCE	YCE	https://coinmarketcap.com//currencies/myce/	0.01
3875	CMITCOIN	CMIT	https://coinmarketcap.com//currencies/cmitcoin/	0
3876	Krosscoin	KSS	https://coinmarketcap.com//currencies/krosscoin/	0
3877	AladiEx	ALA	https://coinmarketcap.com//currencies/aladiex/	0.37
3878	ORBYT Token	ORBYT	https://coinmarketcap.com//currencies/orbyt-token/	0
3879	Ystar	YSR	https://coinmarketcap.com//currencies/ystar/	0.01
3880	Etheroll	DICE	https://coinmarketcap.com//currencies/etheroll/	0.65
3881	B ONE PAYMENT	B1P	https://coinmarketcap.com//currencies/b-one-payment/	0.01
3882	808TA	808TA	https://coinmarketcap.com//currencies/808ta/	0
3883	Lapis Chain	LPS	https://coinmarketcap.com//currencies/lapis-chain/	0
3884	EcoDollar	ECOS	https://coinmarketcap.com//currencies/ecodollar/	0.02
3885	Shift	SHIFT	https://coinmarketcap.com//currencies/shift/	1.45
3886	LibreFreelencer	LIBREF	https://coinmarketcap.com//currencies/librefreelencer/	0
3887	Master Coin Point	MACPO	https://coinmarketcap.com//currencies/macpo/	0
3888	FXPay	FXP	https://coinmarketcap.com//currencies/fxpay/	0
3889	GoalTime N	GTX	https://coinmarketcap.com//currencies/goaltime-n/	0.000278
3890	ROS Coin	ROS	https://coinmarketcap.com//currencies/ros-coin/	0.005144
3891	BLOCKCLOUT	CLOUT	https://coinmarketcap.com//currencies/blockclout/	7.189e-05
3892	VERA	VERA	https://coinmarketcap.com//currencies/vera/	0.000556
3893	French Digital Reserve	FDR	https://coinmarketcap.com//currencies/french-digital-reserve/	0.3086
3894	Valuto	VLU	https://coinmarketcap.com//currencies/valuto/	0.001112
3895	i9 Coin	I9C	https://coinmarketcap.com//currencies/i9-coin/	0.000556
3896	SpokLottery	SPKL	https://coinmarketcap.com//currencies/spoklottery/	6.53e-05
3897	LOOPREX	LOOP	https://coinmarketcap.com//currencies/looprex/	0.0003764
3898	Oracolxor	XOR	https://coinmarketcap.com//currencies/oracolxor/	0.000278
3899	BitcoinRegular	BTRL	https://coinmarketcap.com//currencies/bitcoinregular/	0.12
3900	MintCoin	MINT	https://coinmarketcap.com//currencies/mintcoin/	0
3901	Libera	LIB	https://coinmarketcap.com//currencies/libera/	0.56
3902	Nilu	NILU	https://coinmarketcap.com//currencies/nilu/	0
3903	OBIC	OBIC	https://coinmarketcap.com//currencies/obic/	0.31
3904	Centaure	CEN	https://coinmarketcap.com//currencies/centaure/	0
3905	Sumcoin	SUM	https://coinmarketcap.com//currencies/sumcoin/	773.73
3906	Touch Social	TST	https://coinmarketcap.com//currencies/touch-social/	0
3907	RabbitCoin	RBBT	https://coinmarketcap.com//currencies/rabbitcoin/	0
3908	HoboNickels	HBN	https://coinmarketcap.com//currencies/hobonickels/	0.01
3909	Karatgold Coin	KBC	https://coinmarketcap.com//currencies/karatgold-coin/	0
3910	Axiom	AXIOM	https://coinmarketcap.com//currencies/axiom/	0
3911	Social Send	SEND	https://coinmarketcap.com//currencies/social-send/	0.02
3912	Rubycoin	RBY	https://coinmarketcap.com//currencies/rubycoin/	0.14
3913	Clams	CLAM	https://coinmarketcap.com//currencies/clams/	1.59
3914	Quotient	XQN	https://coinmarketcap.com//currencies/quotient/	0
3915	Bubble	BUB	https://coinmarketcap.com//currencies/bubble/	0.01
3916	ClubCoin	CLUB	https://coinmarketcap.com//currencies/clubcoin/	0.02
3917	AvatarCoin	AV	https://coinmarketcap.com//currencies/avatarcoin/	0.05
3918	Francs	FRN	https://coinmarketcap.com//currencies/francs/	0.01
3919	Aces	ACES	https://coinmarketcap.com//currencies/aces/	0
3920	PROUD Money	PROUD	https://coinmarketcap.com//currencies/proud-money/	0
3921	InflationCoin	IFLT	https://coinmarketcap.com//currencies/inflationcoin/	0
3922	BlazerCoin	BLAZR	https://coinmarketcap.com//currencies/blazercoin/	0
3923	Moving Cloud Coin	MCC	https://coinmarketcap.com//currencies/moving-cloud-coin/	0
3924	XPA	XPA	https://coinmarketcap.com//currencies/xpa/	0
3925	MSD	MSD	https://coinmarketcap.com//currencies/msd/	0.02
3926	Hubii Network	HBT	https://coinmarketcap.com//currencies/hubii-network/	0.35
3927	MOAC	MOAC	https://coinmarketcap.com//currencies/moac/	0.39
3928	IDEX Membership	IDXM	https://coinmarketcap.com//currencies/idex-membership/	0
3929	ValueChain	VLC	https://coinmarketcap.com//currencies/valuechain/	0
3930	JET8	J8T	https://coinmarketcap.com//currencies/jet8/	0
3931	Pixie Coin	PXC	https://coinmarketcap.com//currencies/pixie-coin/	0
3932	Coni	CONI	https://coinmarketcap.com//currencies/coni/	0.04
3933	Wiki Token	WIKI	https://coinmarketcap.com//currencies/wiki-token/	2.29
3934	Insight Chain	INB	https://coinmarketcap.com//currencies/insight-chain/	1.63
3935	Obitan Chain	OBTC	https://coinmarketcap.com//currencies/obitan-chain/	0
3936	Twinkle	TKT	https://coinmarketcap.com//currencies/twinkle/	0
3937	YUKI	YUKI	https://coinmarketcap.com//currencies/yuki/	0
3938	DOWCOIN	DOW	https://coinmarketcap.com//currencies/dowcoin/	0
3939	Traceability Chain	TAC	https://coinmarketcap.com//currencies/traceability-chain/	0
3940	MobilinkToken	MOLK	https://coinmarketcap.com//currencies/mobilinktoken/	0
3941	TRONCLASSIC	TRXC	https://coinmarketcap.com//currencies/tronclassic/	0
3942	Digital Asset Guarantee Token	DAGT	https://coinmarketcap.com//currencies/digital-asset-guarantee-token/	0.14
3943	Wixlar	WIX	https://coinmarketcap.com//currencies/wixlar/	0.01
3944	Ondori	RSTR	https://coinmarketcap.com//currencies/ondori/	0
3945	AirWire	WIRE	https://coinmarketcap.com//currencies/airwire/	0.06
3946	Commerce Data Connection	CDC	https://coinmarketcap.com//currencies/commerce-data-connection/	0
3947	DEEX	DEEX	https://coinmarketcap.com//currencies/deex/	0
3948	Dark	D4RK	https://coinmarketcap.com//currencies/dark/	0
3949	MediBit	MEDIBIT	https://coinmarketcap.com//currencies/medibit/	0
3950	Bgogo Token	BGG	https://coinmarketcap.com//currencies/bgogo-token/	0
3951	Baer Chain	BRC	https://coinmarketcap.com//currencies/baer-chain/	0.08
3952	MoX	MOX	https://coinmarketcap.com//currencies/mox/	0
3953	EurocoinToken	ECTE	https://coinmarketcap.com//currencies/eurocoin-token/	0.08
3954	QUSD	QUSD	https://coinmarketcap.com//currencies/qusd/	0.01
3955	1SG	1SG	https://coinmarketcap.com//currencies/1sg/	3.94
3956	Jewel	JWL	https://coinmarketcap.com//currencies/jewel/	0.2
3957	Bitex Global XBX Coin	XBX	https://coinmarketcap.com//currencies/bitex-global-xbx-coin/	0.01
3958	Esportbits	HLT	https://coinmarketcap.com//currencies/esportbits/	0.16
3959	OOOBTC TOKEN	OBX	https://coinmarketcap.com//currencies/ooobtc-token/	0.01
3960	BIZKEY	BZKY	https://coinmarketcap.com//currencies/bizkey/	0
3961	Stellar Gold	XLMG	https://coinmarketcap.com//currencies/stellar-gold/	0
3962	Atlas Token	ATLS	https://coinmarketcap.com//currencies/atlas-token/	0
3963	Coineal Token	NEAL	https://coinmarketcap.com//currencies/coineal-token/	0.02
3964	Tratin	TRAT	https://coinmarketcap.com//currencies/tratin/	0
3965	B91	B91	https://coinmarketcap.com//currencies/b91/	0
3966	SpectreSecurityCoin	XSPC	https://coinmarketcap.com//currencies/spectre-security-coin/	0
3967	USDQ	USDQ	https://coinmarketcap.com//currencies/usdq/	0.61
3968	SPIN Protocol	SPIN	https://coinmarketcap.com//currencies/spin-protocol/	0
3969	Maya Preferred 223	MAPR	https://coinmarketcap.com//currencies/maya-preferred-223/	120.61
3970	Eco Value Coin	EVC	https://coinmarketcap.com//currencies/eco-value-coin/	0
3971	YottaChain	YTA	https://coinmarketcap.com//currencies/yottachain/	0.02
3972	GLOBEX	GEX	https://coinmarketcap.com//currencies/globex/	0
3973	Volume Network	VOL	https://coinmarketcap.com//currencies/volume-network/	0
3974	Xenoverse	XENO	https://coinmarketcap.com//currencies/xenoverse/	0
3975	Custody Token	CUST	https://coinmarketcap.com//currencies/custody-token/	0
3976	CITEX Token	CTT	https://coinmarketcap.com//currencies/citex-token/	0.01
3977	Aitheon	ACU	https://coinmarketcap.com//currencies/aitheon/	0
3978	Ritocoin	RITO	https://coinmarketcap.com//currencies/ritocoin/	0
3979	Bitscoin	BTCX	https://coinmarketcap.com//currencies/bitscoin/	0
3980	Aeryus	AER	https://coinmarketcap.com//currencies/aeryus/	0
3981	Korbot	KBOT	https://coinmarketcap.com//currencies/korbot/	0.07
3982	Electronero	ETNX	https://coinmarketcap.com//currencies/electronero/	0
3983	VENA	VENA	https://coinmarketcap.com//currencies/vena/	0
3984	LINK	LN	https://coinmarketcap.com//currencies/link/	85.57
3985	Tchain	TCH	https://coinmarketcap.com//currencies/tchain/	0
3986	STK Coin	STK	https://coinmarketcap.com//currencies/stk-coin/	0.08
3987	BOSCore	BOS	https://coinmarketcap.com//currencies/boscore/	0
3988	Sierracoin	SIERRA	https://coinmarketcap.com//currencies/sierracoin/	0
3989	Tokenize Xchange	TKX	https://coinmarketcap.com//currencies/tokenize-xchange/	4.37
3990	Versess Coin	VERS	https://coinmarketcap.com//currencies/versess-coin/	21.69
3991	INLOCK	ILK	https://coinmarketcap.com//currencies/inlock/	0.005049
3992	QPay	QPY	https://coinmarketcap.com//currencies/qpay/	1.795e-05
3993	Bancacy	BNY	https://coinmarketcap.com//currencies/bancacy/	0
3994	NOVA	NOVA	https://coinmarketcap.com//currencies/nova/	7.756e-05
3995	Dash Cash	DSC	https://coinmarketcap.com//currencies/dash-cash/	10
3996	BITFXT COIN	BXT	https://coinmarketcap.com//currencies/bitfxt-coin/	0
3997	Marshal Lion Group Coin	MLGC	https://coinmarketcap.com//currencies/marshal-lion-group-coin/	0.5555
3998	SOLBIT	SBT	https://coinmarketcap.com//currencies/solbit/	0.00156
3999	Bitcoin & Company Network	BITN	https://coinmarketcap.com//currencies/bitcoin-and-company-network/	0
4000	Bankroll Network	BNKR	https://coinmarketcap.com//currencies/bankroll-network/	0.18
4001	Bitgrin	XBG	https://coinmarketcap.com//currencies/bitgrin/	0
4002	Moozicore	MZG	https://coinmarketcap.com//currencies/moozicore/	0
4003	NairaX	NIRX	https://coinmarketcap.com//currencies/nairax/	0
4004	Sesameseed	SEED	https://coinmarketcap.com//currencies/sesameseed/	0.08
4005	Tradeplus	TDPS	https://coinmarketcap.com//currencies/tradeplus/	0.06
4006	Apple Network	ANK	https://coinmarketcap.com//currencies/apple-network/	0
4007	Building Cities Beyond Blockchain	BCB	https://coinmarketcap.com//currencies/building-cities-beyond-blockchain/	0.38
4008	CRYPTOBUCKS	CBUCKS	https://coinmarketcap.com//currencies/cryptobucks/	0.03
4009	VOMER	VMR	https://coinmarketcap.com//currencies/vomer/	0.59
4010	1AI Token	1AI	https://coinmarketcap.com//currencies/1ai-token/	0
4011	FRED Energy	FRED	https://coinmarketcap.com//currencies/fred-energy/	0
4012	AfroDex	AfroX	https://coinmarketcap.com//currencies/afrodex/	0
4013	Kryptofranc	KYF	https://coinmarketcap.com//currencies/kryptofranc/	0
4014	Bitcoffeen	BFF	https://coinmarketcap.com//currencies/bitcoffeen/	0.03
4015	ExtStock Token	XT	https://coinmarketcap.com//currencies/extstock-token/	0.07
4016	Kratscoin	KTC	https://coinmarketcap.com//currencies/kratscoin/	0.32
4017	Compound SAI	CSAI	https://coinmarketcap.com//currencies/compound-sai/	0.03
4018	Asac Coin	ASAC	https://coinmarketcap.com//currencies/asac-coin/	0.01
4019	The Tokenized Bitcoin	imBTC	https://coinmarketcap.com//currencies/the-tokenized-bitcoin/	55422.71
4020	SuperSkynet	SSN	https://coinmarketcap.com//currencies/superskynet/	0
4021	Ethereum eRush	EER	https://coinmarketcap.com//currencies/ethereum-erush/	0.01
4022	StellarPayGlobal	XLPG	https://coinmarketcap.com//currencies/stellarpayglobal/	0.14
4023	ODE	ODE	https://coinmarketcap.com//currencies/ode/	242.29
4024	SappChain	SAPP	https://coinmarketcap.com//currencies/sappchain/	0
4025	Charg Coin	CHG	https://coinmarketcap.com//currencies/charg-coin/	0.49
4026	Unknown Fair Object	UFO	https://coinmarketcap.com//currencies/unknown-fair-object/	0.11
4027	Cicoin	CICX	https://coinmarketcap.com//currencies/cicoin/	0.04
4028	Swapzilla	SWZL	https://coinmarketcap.com//currencies/swapzilla/	0
4029	pTokens BTC	PBTC	https://coinmarketcap.com//currencies/ptokens-btc/	56220.92
4030	Entherfound	ETF	https://coinmarketcap.com//currencies/entherfound/	0.04
4031	Metaprediction	METP	https://coinmarketcap.com//currencies/metaprediction/	0
4032	Binance KRW	BKRW	https://coinmarketcap.com//currencies/binance-krw/	0
4033	eToro Pound Sterling	GBPX	https://coinmarketcap.com//currencies/etoro-pound-sterling/	0
4034	eToro New Zealand Dollar	NZDX	https://coinmarketcap.com//currencies/etoro-new-zealand-dollar/	0
4035	Becaz	BCZ	https://coinmarketcap.com//currencies/becaz/	0.62
4036	BeeEx	BEE	https://coinmarketcap.com//currencies/beeex/	0.01
4037	Litecoin SV	LSV	https://coinmarketcap.com//currencies/litecoin-sv/	1.57
4038	VEHICLE DATA ARTIFICIAL INTELLIGENCE PLATFORM	VAIP	https://coinmarketcap.com//currencies/vehicle-data-artificial-intelligence-platform/	0.42
4039	Compound Ether	CETH	https://coinmarketcap.com//currencies/compound-ether/	34.9
4040	CryptoBet	CBET	https://coinmarketcap.com//currencies/cryptobet/	0
4041	EYES Protocol	EYES	https://coinmarketcap.com//currencies/eyes-protocol/	0.01
4042	UniDollar	UNIUSD	https://coinmarketcap.com//currencies/unidollar/	0
4043	Quish Coin	QTV	https://coinmarketcap.com//currencies/quish-coin/	0.05
4044	Golden Ratio Token	GRT	https://coinmarketcap.com//currencies/golden-ratio-token/	0.86
4045	PieDAO BTC++	BTC++	https://coinmarketcap.com//currencies/piedao-btc/	54569.17
4046	ChainZ Arena	SOUL	https://coinmarketcap.com//currencies/chainz-arena/	0.03
4047	IOOX System	IOOX	https://coinmarketcap.com//currencies/ioox-system/	0.02
4048	Aave TUSD	ATUSD	https://coinmarketcap.com//currencies/aave-tusd/	1
4049	Aave KNC	AKNC	https://coinmarketcap.com//currencies/aave-knc/	2.18
4050	Aave LINK	ALINK	https://coinmarketcap.com//currencies/aave-link/	27.78
4051	Aave SNX	ASNX	https://coinmarketcap.com//currencies/aave-snx/	19.02
4052	Aave MKR	AMKR	https://coinmarketcap.com//currencies/aave-mkr/	2036.23
4053	Aave BAT	ABAT	https://coinmarketcap.com//currencies/aave-bat/	0.82
4054	Aave BUSD	ABUSD	https://coinmarketcap.com//currencies/aave-busd/	1
4055	Aave DAI	ADAI	https://coinmarketcap.com//currencies/aave-dai/	1
4056	sBTC	SBTC	https://coinmarketcap.com//currencies/sbtc/	54698.99
4057	sADA	SADA	https://coinmarketcap.com//currencies/sada/	1.06
4058	Oikos	OKS	https://coinmarketcap.com//currencies/oikos/	0.05
4059	HelixNetwork	MHLX	https://coinmarketcap.com//currencies/helixnetwork/	0
4060	Kublaicoin	KUB	https://coinmarketcap.com//currencies/kublaicoin/	0.03
4061	Nokencoin	NOKN	https://coinmarketcap.com//currencies/nokencoin/	0.01
4062	AntiscamToken	AST	https://coinmarketcap.com//currencies/antiscamtoken/	0.05
4063	Bankroll Vault	VLT	https://coinmarketcap.com//currencies/bankroll-vault/	0.21
4064	sDEFI	SDEFI	https://coinmarketcap.com//currencies/sdefi/	13889.81
4065	sXAG	SXAG	https://coinmarketcap.com//currencies/sxag/	24.27
4066	SafePost	STAMP	https://coinmarketcap.com//currencies/safepost/	0
4067	Originate Coin	ORC	https://coinmarketcap.com//currencies/originate-coin/	0
4068	NEOBITCOIN	NBTC	https://coinmarketcap.com//currencies/neobitcoin/	5.5
4069	Unit Protocol	COL	https://coinmarketcap.com//currencies/unit-protocol/	0
4070	Union Fair Coin	UFC	https://coinmarketcap.com//currencies/union-fair-coin/	0.07
4071	Bitcoin True	BTCT	https://coinmarketcap.com//currencies/bitcoin-true/	0
4072	Cybercoin	CBR	https://coinmarketcap.com//currencies/cybercoin/	0
4073	Crypto User Base	CUB	https://coinmarketcap.com//currencies/crypto-user-base/	0.18
4074	CoinZoom	ZOOM	https://coinmarketcap.com//currencies/coinzoom/	0.07
4075	ZOM	ZOM	https://coinmarketcap.com//currencies/zom/	0.14
4076	DEMOS	DOS	https://coinmarketcap.com//currencies/demos/	0.04
4077	Unicrypt	UNC	https://coinmarketcap.com//currencies/unicrypt/	0.06
4078	DeFi Nation Signals DAO	DSD	https://coinmarketcap.com//currencies/defi-nation-signals-dao/	2.49
4079	PayFrequent USD	PUSD	https://coinmarketcap.com//currencies/payfrequent-usd-2/	1.19
4080	PowerBalt	PWRB	https://coinmarketcap.com//currencies/powerbalt/	0.15
4081	Kids Cash	KASH	https://coinmarketcap.com//currencies/kids-cash/	0.09
4082	CNY Tether	CNYT	https://coinmarketcap.com//currencies/cny-tether/	0.15
4083	Bit World Token	BWB	https://coinmarketcap.com//currencies/bw-token/	0.01
4084	FEX Token	FEX	https://coinmarketcap.com//currencies/fex-token/	0.33
4085	DigiMax DGMT	DGMT	https://coinmarketcap.com//currencies/digimax-dgmt/	0
4086	TOPBTC Token	TOPB	https://coinmarketcap.com//currencies/topb/	0.01
4087	LP-yCurve	YDAI+YUSDC+YUSDT+YTUSD	https://coinmarketcap.com//currencies/curve-fi-ydai-yusdc-yusdt-ytusd/	1.13
4088	888tron	888	https://coinmarketcap.com//currencies/888tron/	0.28
4089	888tron	888	https://coinmarketcap.com//currencies/888tron/	0.2797
4090	3X Short Matic Token	MATICBEAR	https://coinmarketcap.com//currencies/3x-short-matic-token/	1e-08
4091	Essek Tov	ETO	https://coinmarketcap.com//currencies/essek-tov/	0.003889
4092	KYSC Token	KYSC	https://coinmarketcap.com//currencies/kysc-token/	0.0001021
4093	Chess Coin	CHESS	https://coinmarketcap.com//currencies/chess-coin/	0.001945
4094	BTC Network Demand Set II	BYTE	https://coinmarketcap.com//currencies/btc-network-demand-set-ii/	710.43
4095	Chad Link Set	CHADLINK	https://coinmarketcap.com//currencies/chad-link-set/	529.11
4096	ETH 20 Day MA Crossover Set	ETH20SMACO	https://coinmarketcap.com//currencies/eth-20-day-ma-crossover-set/	1698.53
4097	ETH 20 Day MA Crossover Yield Set	ETHMACOAPY	https://coinmarketcap.com//currencies/eth-20-day-ma-crossover-yield-set/	1564.85
4098	ETH 26 EMA Crossover Yield II	ETHEMAAPY	https://coinmarketcap.com//currencies/eth-26-ma-crossover-yield-ii/	1212.84
4099	ETH/BTC RSI Ratio Trading Set	ETHBTCRSI	https://coinmarketcap.com//currencies/eth-btc-rsi-ratio-trading-set/	897.78
4100	ETH/LINK Price Action Candlestick Set	LINKETHPA	https://coinmarketcap.com//currencies/eth-link-price-action-candlestick-set/	1775.09
4101	ETH Price Action Candlestick Set	ETHPA	https://coinmarketcap.com//currencies/eth-price-action-candlestick-set/	1321.24
4102	ETH RSI 60/40 Crossover Set	ETHRSI6040	https://coinmarketcap.com//currencies/eth-rsi-60-40-crossover-set/	929.44
4103	ETH RSI 60/40 Yield Set	ETHRSIAPY	https://coinmarketcap.com//currencies/eth-rsi-60-40-yield-set/	922.82
4104	FlexETH/BTC Set	FLEXETHBTC	https://coinmarketcap.com//currencies/flexeth-btc-set/	931.64
4105	Intelligent Ratio Set	INTRATIO	https://coinmarketcap.com//currencies/intelligent-ratio-set/	1650.16
4106	LINK/ETH RSI Ratio Trading Set	LINKETHRSI	https://coinmarketcap.com//currencies/link-eth-rsi-ratio-trading-set/	514.17
4107	LINK Profit Taker Set	LINKPT	https://coinmarketcap.com//currencies/link-profit-taker-set/	712.5
4108	WhiteRockCasino	WRC	https://coinmarketcap.com//currencies/whiterockcasino/	0
4109	iETH	IETH	https://coinmarketcap.com//currencies/ieth/	1346.57
4110	sLINK	sLINK	https://coinmarketcap.com//currencies/slink/	27
4111	sXAU	SXAU	https://coinmarketcap.com//currencies/sxau/	1755.79
4112	sXTZ	SXTZ	https://coinmarketcap.com//currencies/sxtz/	4.06
4113	iBTC (Synthetix)	IBTC	https://coinmarketcap.com//currencies/ibtc-synthetix/	20247.41
4114	sBNB	SBNB	https://coinmarketcap.com//currencies/sbnb/	276.43
4115	Bityuan	BTY	https://coinmarketcap.com//currencies/bityuan/	0.5
4116	Diamond Voucher	DVS	https://coinmarketcap.com//currencies/diamond-voucher/	13.11
4117	3X Short Tether Gold Token	XAUTBEAR	https://coinmarketcap.com//currencies/3x-short-tether-gold-token/	3080
4118	Yield Farming Token	YFT	https://coinmarketcap.com//currencies/yield-farming-token/	5
4119	HL Chain	HL	https://coinmarketcap.com//currencies/hl-chain/	0
4120	DApp Evolution	EVO	https://coinmarketcap.com//currencies/dapp-evolution/	0.14
4121	Dark Energy Crystals	DEC	https://coinmarketcap.com//currencies/dark-energy-crystals/	0
4122	Play Royal	PLAY	https://coinmarketcap.com//currencies/play-royal/	0
4123	SCC DIGforIT	SCC	https://coinmarketcap.com//currencies/scc-digforit/	0.01
4124	AVCCOIN	AVC	https://coinmarketcap.com//currencies/avccoin/	0.05
4125	BTS Coin	BTSC	https://coinmarketcap.com//currencies/bts-coin/	0
4126	DTOP Token	DTOP	https://coinmarketcap.com//currencies/data-trade-on-demand-platform/	0
4127	Distributed Energy Coin	DEC	https://coinmarketcap.com//currencies/distributed-energy-coin/	0
4128	FunKeyPay	FNK	https://coinmarketcap.com//currencies/funkeypay/	0
4129	GLOBALTRUSTFUND TOKEN	GTF	https://coinmarketcap.com//currencies/globaltrustfund-token/	0
4130	Hemelios	HEM	https://coinmarketcap.com//currencies/hemelios/	0
4131	IDCM Token	IT	https://coinmarketcap.com//currencies/idcm-token/	0
4132	Galaxy Pool Coin	GPO	https://coinmarketcap.com//currencies/galaxy-pool-coin/	0.04
4133	Intelligence Quickly Chain	IQC	https://coinmarketcap.com//currencies/intelligence-quickly-chain/	0
4134	LoveHearts	LVH	https://coinmarketcap.com//currencies/lovehearts/	0
4135	TRONbetDice	DICE	https://coinmarketcap.com//currencies/tronbetdice/	0.03
4136	Aludra Network	ALD	https://coinmarketcap.com//currencies/aludra-network/	0
4137	UniversalEnergyChain	UENC	https://coinmarketcap.com//currencies/universalenergychain/	1
4138	Ripple Alpha	XLA	https://coinmarketcap.com//currencies/ripple-alpha/	0
4139	Frens Community	FRENS	https://coinmarketcap.com//currencies/frens-community/	1.08
4140	MONEY PARTY	PARTY	https://coinmarketcap.com//currencies/money-party/	0
4141	OWL	OWL	https://coinmarketcap.com//currencies/owl/	1.04
4142	Equus Mining Token	EQMT	https://coinmarketcap.com//currencies/equus-mining-token/	0
4143	Trump Wins Token	TRUMPWIN	https://coinmarketcap.com//currencies/trump-wins-token/	0.02
4144	Sendvibe	SVB	https://coinmarketcap.com//currencies/sendvibe/	0
4145	MYX Network	MYX	https://coinmarketcap.com//currencies/myx-network/	0
4146	cherry	CHERRY	https://coinmarketcap.com//currencies/cherry/	0.06
4147	Tokens of Babel	TOB	https://coinmarketcap.com//currencies/tokens-of-babel/	0.12
4148	Schain Wallet	SCHA	https://coinmarketcap.com//currencies/schain-wallet/	0
4149	Community Token	COM	https://coinmarketcap.com//currencies/community-token/	0.17
4150	Ethereum Vault	ETHV	https://coinmarketcap.com//currencies/ethereum-vault/	0.03
4151	Porkchop	CHOP	https://coinmarketcap.com//currencies/porkchop/	0.01
4152	Camouflage.eth	CAMO	https://coinmarketcap.com//currencies/camouflage-eth/	0
4153	COIL	COIL	https://coinmarketcap.com//currencies/coil/	0.78
4154	DACC2	DACC2	https://coinmarketcap.com//currencies/dacc2/	0
4155	MIKS COIN	MIKS	https://coinmarketcap.com//currencies/miks-coin/	0
4156	Orbicular	ORBI	https://coinmarketcap.com//currencies/orbicular/	0
4157	Newtonium	NEWTON	https://coinmarketcap.com//currencies/newtonium/	0.04
4158	OC Protocol	OCP	https://coinmarketcap.com//currencies/oc-protocol/	0.01
4159	JBOX	JBX	https://coinmarketcap.com//currencies/jboxcoin/	0.02
4160	Dextrust	DETS	https://coinmarketcap.com//currencies/dextrust/	0.04
4161	Blaze Network	BLZN	https://coinmarketcap.com//currencies/blaze-network/	0.5
4162	Wenburn	WENB	https://coinmarketcap.com//currencies/wenburn/	0.03
4163	Vision	VSN	https://coinmarketcap.com//currencies/vision/	0.39
4164	Port of DeFi Network	PDF	https://coinmarketcap.com//currencies/port-of-defi-network/	0.11
4165	Hands of Steel	STEEL	https://coinmarketcap.com//currencies/hands-of-steel/	0.01
4166	CEDARS	CEDS	https://coinmarketcap.com//currencies/cedars/	0.52
4167	YEP COIN	YEP	https://coinmarketcap.com//currencies/yep-coin/	29.5
4168	Fee Active Collateral Token	FACT	https://coinmarketcap.com//currencies/fee-active-collateral-token/	0.02
4169	GOT Token	GOT	https://coinmarketcap.com//currencies/got-token/	1.66
4170	Popcorn	CORN	https://coinmarketcap.com//currencies/popcorn/	0.08
4171	Bast	BAST	https://coinmarketcap.com//currencies/bast/	5.14
4172	Social Finance	SOFI	https://coinmarketcap.com//currencies/social-finance/	4.4
4173	Walnut.finance	WTF	https://coinmarketcap.com//currencies/walnut-finance/	0.6
4174	TAI	TAI	https://coinmarketcap.com//currencies/tai/	0.19
4175	SalmonSwap	SAL	https://coinmarketcap.com//currencies/salmonswap/	6.29
4176	YFIBusiness	YFIB	https://coinmarketcap.com//currencies/yfibusiness/	2.24
4177	yfBeta	YFBETA	https://coinmarketcap.com//currencies/yfbeta/	7.96
4178	TRONbetLive	LIVE	https://coinmarketcap.com//currencies/tronbetlive/	0.04
4179	SSS Finance	SSS	https://coinmarketcap.com//currencies/sss-finance/	2.76
4180	YFA Finance	YFA	https://coinmarketcap.com//currencies/yfa-finance/	2.5
4181	YFUEL	YFUEL	https://coinmarketcap.com//currencies/yfuel/	2.57
4182	junca Cash	JCC	https://coinmarketcap.com//currencies/junca-cash/	0.11
4183	Yearn Finance Protocol	YFP	https://coinmarketcap.com//currencies/yearn-finance-protocol/	0.28
4184	The Forms	FRMS	https://coinmarketcap.com//currencies/the-forms/	0.02
4185	Definitex	DFX	https://coinmarketcap.com//currencies/definitex/	0.46
4186	FlashSwap	FSP	https://coinmarketcap.com//currencies/flashswap/	0.02
4187	Token CashPay	TCP	https://coinmarketcap.com//currencies/token-cashpay/	0.03
4188	GOD KIMCHI	gKIMCHI	https://coinmarketcap.com//currencies/god-kimchi/	0
4189	YFIKING,FINANCE	YFIKING	https://coinmarketcap.com//currencies/yfiking-finance/	0.2211
4190	HBTC Captain Token	HBC	https://coinmarketcap.com//currencies/hbtc-token/	13.76
4191	Mafia.Network	MAFI	https://coinmarketcap.com//currencies/mafia-network/	0.004663
4192	Maki Finance	MAKI	https://coinmarketcap.com//currencies/maki-finance/	0.7891
4193	Predix Network	PRDX	https://coinmarketcap.com//currencies/predix-network/	0.3896
4194	YEARN2.FINANCE	YFI2	https://coinmarketcap.com//currencies/yearn2-finance/	4.66
4195	Candela Coin	CLA	https://coinmarketcap.com//currencies/candelacoin/	0.000296
4196	Farm House Finance	FHSE	https://coinmarketcap.com//currencies/farm-house-finance/	8.7
4197	SherLOCK Security	LOCK	https://coinmarketcap.com//currencies/sherlock-security/	0.4916
4198	OFIN Token	ON	https://coinmarketcap.com//currencies/ofin-token/	0.004957
4199	Momentum	XMM	https://coinmarketcap.com//currencies/momentum/	0
4200	Yield Farming Known as Ash	YFKA	https://coinmarketcap.com//currencies/yield-farming-known-as-ash/	15.35
4201	Tribute	TRBT	https://coinmarketcap.com//currencies/tribute/	0.29
4202	Atlantis Token	ATIS	https://coinmarketcap.com//currencies/atlantis-token/	0.05
4203	YFarmLand Token	YFARMER	https://coinmarketcap.com//currencies/yfarmland-token/	0.53
4204	Fame	FAME	https://coinmarketcap.com//currencies/fame/	0
4205	Va Na Su	VNS	https://coinmarketcap.com//currencies/va-na-su/	0.81
4206	Moon YFI	MYFI	https://coinmarketcap.com//currencies/moon-yfi/	7.08
4207	YFMoonshot	YFMS	https://coinmarketcap.com//currencies/yfmoonshot/	7.38
4208	ETF Dao	TFD	https://coinmarketcap.com//currencies/etf-dao/	0.01
4209	Resoper	RSP	https://coinmarketcap.com//currencies/resoper/	0
4210	Shill & Win	PoSH	https://coinmarketcap.com//currencies/shill-win/	0
4211	Livenodes Token	LNOT	https://coinmarketcap.com//currencies/livenodes-token/	64.03
4212	RI Token	RI	https://coinmarketcap.com//currencies/ri-token/	0.18
4213	Finswap	FNSP	https://coinmarketcap.com//currencies/finswap/	0.06
4214	Digital Currency Daily	DCD	https://coinmarketcap.com//currencies/digital-currency-daily/	0
4215	Gire Token	GET	https://coinmarketcap.com//currencies/gire-token/	0.06
4216	iCherry Finance	ICH	https://coinmarketcap.com//currencies/icherry-finance/	0.3
4217	UnityDAO	UTY	https://coinmarketcap.com//currencies/unitydao/	9.19
4218	Acuity Token	ACU	https://coinmarketcap.com//currencies/acuity-token/	0
4219	DeFi Firefly	DFF	https://coinmarketcap.com//currencies/defi-firefly/	0.25
4220	Safari	SFR	https://coinmarketcap.com//currencies/safari/	0
4221	UNII Finance	UNII	https://coinmarketcap.com//currencies/unii-finance/	0
4222	Buccaneer V2	BUCC	https://coinmarketcap.com//currencies/buccaneer-v2/	6.28
4223	CryptoWater	C2O	https://coinmarketcap.com//currencies/cryptowater/	1.18
4224	CXN Network	CXN	https://coinmarketcap.com//currencies/cxn-network/	0
4225	Yearn Land	YLAND	https://coinmarketcap.com//currencies/yearn-land/	14.01
4226	AmericanHorror.Finance	AHF	https://coinmarketcap.com//currencies/americanhorror-finance/	317.95
4227	Yoink	YNK	https://coinmarketcap.com//currencies/yoink/	0
4228	APEcoin	APE	https://coinmarketcap.com//currencies/apecoin/	0.03
4229	Pajama.Finance	PJM	https://coinmarketcap.com//currencies/pajama-finance/	4.3
4230	Yearn Finance Ecosystem	YFIEC	https://coinmarketcap.com//currencies/yearn-finance-ecosystem/	3.43
4231	YFET	YFET	https://coinmarketcap.com//currencies/yfet/	3
4232	Yearn4 Finance	YF4	https://coinmarketcap.com//currencies/yearn4-finance/	1.47
4233	ErcauX	RAUX	https://coinmarketcap.com//currencies/ercaux/	0.12
4234	ZAC Finance	ZAC	https://coinmarketcap.com//currencies/zac-finance/	0.29
4235	Bellevue Network	BLV	https://coinmarketcap.com//currencies/bellevue-network/	0.01
4236	YeaFinance	YEA	https://coinmarketcap.com//currencies/yeafinance/	8.09
4237	YFOS.finance	YFOS	https://coinmarketcap.com//currencies/yfos-finance/	4.06
4238	OWL Token (StealthSwap)	OWL	https://coinmarketcap.com//currencies/owl-token-stealthswap/	0.22
4239	YFPRO Finance	YFPRO	https://coinmarketcap.com//currencies/yfpro-finance/	2.98
4240	PieDAO DEFI Large Cap	DEFI+L	https://coinmarketcap.com//currencies/piedao-defi-large-cap/	4.64
4241	Crypxie	CPX	https://coinmarketcap.com//currencies/crypxie/	5.23
4242	Jiviz	JVZ	https://coinmarketcap.com//currencies/jiviz/	0
4243	BFis.Finance	BFI	https://coinmarketcap.com//currencies/bfis-finance/	1.16
4244	deCraft Finance	CRAFT	https://coinmarketcap.com//currencies/decraft-finance/	21.37
4245	Tavittcoin	TAVITT	https://coinmarketcap.com//currencies/tavittcoin/	0.08
4246	Load Network	LOAD	https://coinmarketcap.com//currencies/load-network/	0.03
4247	Payship	PSHP	https://coinmarketcap.com//currencies/payship/	33.69
4248	BonezYard	BNZ	https://coinmarketcap.com//currencies/bonezyard/	0
4249	Bitfinex Bitcoin Dominance Perps	BTCDOM	https://coinmarketcap.com//currencies/bitfinex-bitcoin-dominance-perps/	251.74
4250	PayUSD	PUSD	https://coinmarketcap.com//currencies/payusd/	1.42
4251	Payou Finance	PAYOU	https://coinmarketcap.com//currencies/payou-finance/	1.07
4252	McNetworkDefi	MCN	https://coinmarketcap.com//currencies/mcnetwork/	0.01
4253	Friends With Benefits	FWB	https://coinmarketcap.com//currencies/friends-with-benefits/	0.45
4254	BananoDOS	yBAN	https://coinmarketcap.com//currencies/bananodos/	119.23
4255	Naz Coin	NAZ	https://coinmarketcap.com//currencies/naz-coin/	0
4256	BSCswap	BSWAP	https://coinmarketcap.com//currencies/bscswap/	64.95
4257	Lumos	LMS	https://coinmarketcap.com//currencies/lumos/	0.02
4258	pxUSD Synthetic USD Expiring 1 April 2021	PXUSD_MAR2021	https://coinmarketcap.com//currencies/pxusd-synthetic-usd-expiring-1-april-2021/	2.14
4259	Tatcoin	TAT	https://coinmarketcap.com//currencies/tatcoin/	0.04
4260	HubDao	HD	https://coinmarketcap.com//currencies/hubdao/	0.11
4261	Bloody Token	BLOODY	https://coinmarketcap.com//currencies/bloody-token/	0
4262	Hoo Token	HOO	https://coinmarketcap.com//currencies/hoo-token/	0.3
4263	YYFI.Protocol	YYFI	https://coinmarketcap.com//currencies/yyfi-protocol/	0.38
4264	Hiz Finance	HIZ	https://coinmarketcap.com//currencies/hiz-finance/	0.41
4265	Gold BCR	GBCR	https://coinmarketcap.com//currencies/gold-bcr/	1918.85
4266	DotBased	xDOT	https://coinmarketcap.com//currencies/dotbased/	9.17
4267	$YEET	YEET	https://coinmarketcap.com//currencies/yeet/	3.18
4268	stakedFIRO	stFIRO	https://coinmarketcap.com//currencies/stakedfiro/	8.28
4269	Smpl foundation	SMPL	https://coinmarketcap.com//currencies/smplfoundation/	24.39
4270	zLOT Finance	ZLOT	https://coinmarketcap.com//currencies/zlot-finance/	1160.8
4271	Fiola	FLA	https://coinmarketcap.com//currencies/fiola/	0
4272	wormhole.finance	WHOLE	https://coinmarketcap.com//currencies/wormhole-finance/	1.15
4273	Future-Cash Digital	FCD	https://coinmarketcap.com//currencies/future-cash-digital/	0.02
4274	XFII	XFII	https://coinmarketcap.com//currencies/xfii/	4.36
4275	7Finance	SVN	https://coinmarketcap.com//currencies/7finance/	0.18
4276	Blueshare Token	BST1	https://coinmarketcap.com//currencies/blueshare-token/	0
4277	Volts.Finance	VOLTS	https://coinmarketcap.com//currencies/volts-finance/	11.71
4278	Kanva	KNV	https://coinmarketcap.com//currencies/kanva/	1.96
4279	PBS Chain	PBS	https://coinmarketcap.com//currencies/pbs-chain/	0
4280	Light Coin Exchange Token	LCT	https://coinmarketcap.com//currencies/light-coin-exchange-token/	0.01
4281	UnoSwap	UNOS	https://coinmarketcap.com//currencies/unoswap/	0.87
4282	Lock3r Network	LK3R	https://coinmarketcap.com//currencies/lk3r/	2.82
4283	Wrapped ZEC	WZEC	https://coinmarketcap.com//currencies/wrapped-zec/	132.27
4284	HoldToWin	7ADD	https://coinmarketcap.com//currencies/holdtowin/	1.02
4285	Tradebitpay	TBP	https://coinmarketcap.com//currencies/tradebitpay/	0.02
4286	LeLeFoodChain	LELE	https://coinmarketcap.com//currencies/lelefoodchain/	0.06
4287	Bitpumps Token	BPT	https://coinmarketcap.com//currencies/bitpumps-token/	0
4288	STVKE	STV	https://coinmarketcap.com//currencies/stvke-network/	0.01
4289	Money of Tomorrow, Today	MTTCOIN	https://coinmarketcap.com//currencies/money-of-tomorrow-today/	87.31
4290	PoolStake	PSK	https://coinmarketcap.com//currencies/poolstake/	0.1991
4291	Greenheart Punt	PUNT	https://coinmarketcap.com//currencies/greenheart-punt/	0.1119
4292	DGPayment	DGP	https://coinmarketcap.com//currencies/dgpayment/	0.1276
4293	WAV3	WAV3	https://coinmarketcap.com//currencies/wav3/	6.91
4294	Elynet	ELYX	https://coinmarketcap.com//currencies/elynet/	0.01984
4295	Sugarchain	SUGAR	https://coinmarketcap.com//currencies/sugarchain/	0.002821
4296	Yearn Ecosystem Token Index	YETI	https://coinmarketcap.com//currencies/yearn-ecosystem-token-index/	1.89
4297	ARVO	ARVO	https://coinmarketcap.com//currencies/arvo/	3.32
4298	502 Bad Gateway Token	Z502	https://coinmarketcap.com//currencies/z502-bad-gateway-token/	1.767e-05
4299	Venus XRP	vXRP	https://coinmarketcap.com//currencies/venus-xrp/	0.01
4300	Deflect	DEFLCT	https://coinmarketcap.com//currencies/deflect/	2.52
4301	Blaze DeFi	BNFI	https://coinmarketcap.com//currencies/blaze-defi/	0.36
4302	Trinity Protocol	TRI	https://coinmarketcap.com//currencies/trinity-protocol/	0.01
4303	MediconnectUk	MEDI	https://coinmarketcap.com//currencies/mediconnectuk/	0.02
4304	Refract	RFR	https://coinmarketcap.com//currencies/refract/	432.08
4305	DAPPCENTS	DPC	https://coinmarketcap.com//currencies/dappcents/	0
4306	3XT TOKEN	3XT	https://coinmarketcap.com//currencies/3xt-token/	243.94
4307	Diffract Finance	DFR	https://coinmarketcap.com//currencies/diffract-finance/	0.09
4308	Supreme Finance	HYPE	https://coinmarketcap.com//currencies/hype/	5.43
4309	LinkBased	LBD	https://coinmarketcap.com//currencies/linkbased/	3.17
4310	Zipmex	ZMT	https://coinmarketcap.com//currencies/zipmex/	2.41
4311	Basiscoin Cash	BCC	https://coinmarketcap.com//currencies/basiscoin/	0.03
4312	Basiscoin Share	BCS	https://coinmarketcap.com//currencies/basiscoin-share/	0.15
4313	Space Dollar	SPAD	https://coinmarketcap.com//currencies/space-dollar/	0
4314	FOX Token	FOX	https://coinmarketcap.com//currencies/fox-token/	0.41
4315	Venus Filecoin	vFIL	https://coinmarketcap.com//currencies/venus-filecoin/	1.18
4316	Vaultz	VAULTZ	https://coinmarketcap.com//currencies/vaultz/	6.32
4317	YFST.Protocol	YFST	https://coinmarketcap.com//currencies/yfst-protocol/	0
4318	Wrapped Monero	WXMR	https://coinmarketcap.com//currencies/wrapped-monero/	243.71
4319	Dynamic Supply Tracker	DSTR	https://coinmarketcap.com//currencies/dynamic-supply-tracker/	6.36
4320	Dynamic Supply	DST	https://coinmarketcap.com//currencies/dynamic-supply/	0.68
4321	PegsUSD	PUSD	https://coinmarketcap.com//currencies/pegs-usd/	0.19
4322	PegsShares	PEGS	https://coinmarketcap.com//currencies/pegsshares/	0.05
4323	Bitbot Protocol	BBP	https://coinmarketcap.com//currencies/bitbot-protocol/	23.55
4324	Xstable.Protocol	XST	https://coinmarketcap.com//currencies/xstable-protocol/	0.6
4325	Natus Vincere Fan Token	NAVI	https://coinmarketcap.com//currencies/natus-vincere-fan-token/	14.96
4326	Young Boys Fan Token	YBO	https://coinmarketcap.com//currencies/young-boys-fan-token/	5.12
4327	USDEX	USDEX	https://coinmarketcap.com//currencies/usdex/	1
4328	Universal Dollar	U8D	https://coinmarketcap.com//currencies/universal-dollar/	0.13
4329	Gladius Finance	GLAD	https://coinmarketcap.com//currencies/gladius-finance/	5.58
4330	Venus BETH	VBETH	https://coinmarketcap.com//currencies/venus-beth/	34.48
4331	YFLink Synthetic	sYFL	https://coinmarketcap.com//currencies/yflink-synthetic/	0.19
4332	Option Token	OT	https://coinmarketcap.com//currencies/option-token/	2.34
4333	Defla	DEFLA	https://coinmarketcap.com//currencies/defla/	0.57
4334	DeFiStarter	DFST	https://coinmarketcap.com//currencies/defistarter/	3.02
4335	SIMBA Storage	SIMBA	https://coinmarketcap.com//currencies/simba/	0
4336	mStable BTC	MBTC	https://coinmarketcap.com//currencies/mstable-btc/	57167.87
4337	Parsiq Boost	PRQBOOST	https://coinmarketcap.com//currencies/parsiq-boost/	0.03
4338	Quantfury Token	QTF	https://coinmarketcap.com//currencies/quantfury-token/	9.83
4339	ShuttleOne	wSZO	https://coinmarketcap.com//currencies/shuttleone/	0.26
4340	Aave Enjin	aENJ	https://coinmarketcap.com//currencies/aave-enjin/	2.62
4341	Weather Finance	WEATHER	https://coinmarketcap.com//currencies/weather-finance/	0.68
4342	wanSUSHI	WANSUSHI	https://coinmarketcap.com//currencies/wansushi/	8.17
4343	Lua Token	LUA	https://coinmarketcap.com//currencies/lua-token/	0.13
4344	Bitcoin	BTC	https://coinmarketcap.com//currencies/bitcoin/	55713.15
4345	Ethereum	ETH	https://coinmarketcap.com//currencies/ethereum/	1800.57
4346	Binance Coin	BNB	https://coinmarketcap.com//currencies/binance-coin/	257.16
4347	Tether	USDT	https://coinmarketcap.com//currencies/tether/	1
4348	Cardano	ADA	https://coinmarketcap.com//currencies/cardano/	1.1
4349	Polkadot	DOT	https://coinmarketcap.com//currencies/polkadot-new/	34.9
4350	XRP	XRP	https://coinmarketcap.com//currencies/xrp/	0.4753
4351	Uniswap	UNI	https://coinmarketcap.com//currencies/uniswap/	29.33
4352	Litecoin	LTC	https://coinmarketcap.com//currencies/litecoin/	202.47
4353	Chainlink	LINK	https://coinmarketcap.com//currencies/chainlink/	27.75
4354	Bitcoin Cash	BCH	https://coinmarketcap.com//currencies/bitcoin-cash/	526.66
4355	USD Coin	USDC	https://coinmarketcap.com//currencies/usd-coin/	1
4356	Stellar	XLM	https://coinmarketcap.com//currencies/stellar/	0.4
4357	Terra	LUNA	https://coinmarketcap.com//currencies/terra-luna/	20.53
4358	Dogecoin	DOGE	https://coinmarketcap.com//currencies/dogecoin/	0.06
4359	Wrapped Bitcoin	WBTC	https://coinmarketcap.com//currencies/wrapped-bitcoin/	55540.83
4360	THETA	THETA	https://coinmarketcap.com//currencies/theta/	7.35
4361	VeChain	VET	https://coinmarketcap.com//currencies/vechain/	0.08
4362	Crypto.com Coin	CRO	https://coinmarketcap.com//currencies/crypto-com-coin/	0.19
4363	Aave	AAVE	https://coinmarketcap.com//currencies/aave/	372.28
4364	Cosmos	ATOM	https://coinmarketcap.com//currencies/cosmos/	21.07
4365	Monero	XMR	https://coinmarketcap.com//currencies/monero/	225.2
4366	Avalanche	AVAX	https://coinmarketcap.com//currencies/avalanche/	30.75
4367	EOS	EOS	https://coinmarketcap.com//currencies/eos/	4.08
4368	FTX Token	FTT	https://coinmarketcap.com//currencies/ftx-token/	40.21
4369	IOTA	MIOTA	https://coinmarketcap.com//currencies/iota/	1.35
4370	TRON	TRX	https://coinmarketcap.com//currencies/tron/	0.05
4371	Solana	SOL	https://coinmarketcap.com//currencies/solana/	13.73
4372	Bitcoin SV	BSV	https://coinmarketcap.com//currencies/bitcoin-sv/	194.44
4373	NEM	XEM	https://coinmarketcap.com//currencies/nem/	0.4
4374	Filecoin	FIL	https://coinmarketcap.com//currencies/filecoin/	59.13
4375	Algorand	ALGO	https://coinmarketcap.com//currencies/algorand/	1.22
4376	Binance USD	BUSD	https://coinmarketcap.com//currencies/binance-usd/	1
4377	Tezos	XTZ	https://coinmarketcap.com//currencies/tezos/	4.03
4378	Chiliz	CHZ	https://coinmarketcap.com//currencies/chiliz/	0.54
4379	Neo	NEO	https://coinmarketcap.com//currencies/neo/	40.44
4380	Dai	DAI	https://coinmarketcap.com//currencies/multi-collateral-dai/	1
4381	Kusama	KSM	https://coinmarketcap.com//currencies/kusama/	326.86
4382	Huobi Token	HT	https://coinmarketcap.com//currencies/huobi-token/	14.82
4383	Hedera Hashgraph	HBAR	https://coinmarketcap.com//currencies/hedera-hashgraph/	0.35
4384	SushiSwap	SUSHI	https://coinmarketcap.com//currencies/sushiswap/	20.19
4385	Elrond	EGLD	https://coinmarketcap.com//currencies/elrond-egld/	132.46
4386	Dash	DASH	https://coinmarketcap.com//currencies/dash/	222.63
4387	Synthetix	SNX	https://coinmarketcap.com//currencies/synthetix-network-token/	19.03
4388	Enjin Coin	ENJ	https://coinmarketcap.com//currencies/enjin-coin/	2.6
4389	Ravencoin	RVN	https://coinmarketcap.com//currencies/ravencoin/	0.26
4390	The Graph	GRT	https://coinmarketcap.com//currencies/the-graph/	1.75
4391	NEAR Protocol	NEAR	https://coinmarketcap.com//currencies/near-protocol/	6.69
4392	Decred	DCR	https://coinmarketcap.com//currencies/decred/	166.2
4393	UNUS SED LEO	LEO	https://coinmarketcap.com//currencies/unus-sed-leo/	2.06
4394	Compound	COMP	https://coinmarketcap.com//currencies/compound/	438.2
4395	Maker	MKR	https://coinmarketcap.com//currencies/maker/	2035.33
4396	Polygon	MATIC	https://coinmarketcap.com//currencies/polygon/	0.38
4397	Theta Fuel	TFUEL	https://coinmarketcap.com//currencies/theta-fuel/	0.36
4398	Zilliqa	ZIL	https://coinmarketcap.com//currencies/zilliqa/	0.16
4399	BitTorrent	BTT	https://coinmarketcap.com//currencies/bittorrent/	0
4400	Zcash	ZEC	https://coinmarketcap.com//currencies/zcash/	137.56
4401	Decentraland	MANA	https://coinmarketcap.com//currencies/decentraland/	0.97
4402	Nexo	NEXO	https://coinmarketcap.com//currencies/nexo/	2.69
4403	Ethereum Classic	ETC	https://coinmarketcap.com//currencies/ethereum-classic/	12.37
4404	Bitcoin BEP2	BTCB	https://coinmarketcap.com//currencies/bitcoin-bep2/	54661.86
4405	UMA	UMA	https://coinmarketcap.com//currencies/uma/	23.9
4406	PancakeSwap	CAKE	https://coinmarketcap.com//currencies/pancakeswap/	10.17
4407	THORChain	RUNE	https://coinmarketcap.com//currencies/thorchain/	5.62
4408	Voyager Token	VGX	https://coinmarketcap.com//currencies/voyager-token/	5.77
4409	yearn.finance	YFI	https://coinmarketcap.com//currencies/yearn-finance/	34379.32
4410	SwissBorg	CHSB	https://coinmarketcap.com//currencies/swissborg/	1.33
4411	Bancor	BNT	https://coinmarketcap.com//currencies/bancor/	7.56
4412	Basic Attention Token	BAT	https://coinmarketcap.com//currencies/basic-attention-token/	0.82
4413	Stacks	STX	https://coinmarketcap.com//currencies/stacks/	1.14
4414	Celsius	CEL	https://coinmarketcap.com//currencies/celsius/	4.83
4415	ICON	ICX	https://coinmarketcap.com//currencies/icon/	1.91
4416	Holo	HOT	https://coinmarketcap.com//currencies/holo/	0.01
4417	Ren	REN	https://coinmarketcap.com//currencies/ren/	1.11
4418	TerraUSD	UST	https://coinmarketcap.com//currencies/terrausd/	1
4419	Revain	REV	https://coinmarketcap.com//currencies/revain/	0.01
4420	0x	ZRX	https://coinmarketcap.com//currencies/0x/	1.37
4421	Reserve Rights	RSR	https://coinmarketcap.com//currencies/reserve-rights/	0.08
4422	Waves	WAVES	https://coinmarketcap.com//currencies/waves/	9.9
4423	Flow	FLOW	https://coinmarketcap.com//currencies/flow/	33.07
4424	Fantom	FTM	https://coinmarketcap.com//currencies/fantom/	0.39
4425	Ontology	ONT	https://coinmarketcap.com//currencies/ontology/	1.19
4426	DigiByte	DGB	https://coinmarketcap.com//currencies/digibyte/	0.07
4427	Siacoin	SC	https://coinmarketcap.com//currencies/siacoin/	0.02
4428	OKB	OKB	https://coinmarketcap.com//currencies/okb/	14.69
4429	Harmony	ONE	https://coinmarketcap.com//currencies/harmony/	0.09
4430	Pundi X	NPXS	https://coinmarketcap.com//currencies/pundi-x/	0
4431	Paxos Standard	PAX	https://coinmarketcap.com//currencies/paxos-standard/	1
4432	renBTC	RENBTC	https://coinmarketcap.com//currencies/renbtc/	55812.76
4433	OMG Network	OMG	https://coinmarketcap.com//currencies/omg/	5.12
4434	Celo	CELO	https://coinmarketcap.com//currencies/celo/	3.69
4435	IOST	IOST	https://coinmarketcap.com//currencies/iostoken/	0.04
4436	Loopring	LRC	https://coinmarketcap.com//currencies/loopring/	0.54
4437	Nano	NANO	https://coinmarketcap.com//currencies/nano/	4.93
4438	Orbs	ORBS	https://coinmarketcap.com//currencies/orbs/	0.28
4439	Curve DAO Token	CRV	https://coinmarketcap.com//currencies/curve-dao-token/	2.47
4440	Ocean Protocol	OCEAN	https://coinmarketcap.com//currencies/ocean-protocol/	1.45
4441	Qtum	QTUM	https://coinmarketcap.com//currencies/qtum/	6.12
4442	1inch	1INCH	https://coinmarketcap.com//currencies/1inch/	4.11
4443	Helium	HNT	https://coinmarketcap.com//currencies/helium/	7.72
4444	Ankr	ANKR	https://coinmarketcap.com//currencies/ankr/	0.0805
4445	HUSD	HUSD	https://coinmarketcap.com//currencies/husd/	0.9997
4446	Horizen	ZEN	https://coinmarketcap.com//currencies/horizen/	51.6
4447	ZKSwap	ZKS	https://coinmarketcap.com//currencies/zkswap/	2.67
4448	Bitcoin Gold	BTG	https://coinmarketcap.com//currencies/bitcoin-gold/	30.04
4449	Verge	XVG	https://coinmarketcap.com//currencies/verge/	0.03101
4450	SKALE Network	SKL	https://coinmarketcap.com//currencies/skale-network/	0.7635
4451	Reef	REEF	https://coinmarketcap.com//currencies/reef/	0.04061
4452	Quant	QNT	https://coinmarketcap.com//currencies/quant/	37.6
4453	Kyber Network	KNC	https://coinmarketcap.com//currencies/kyber-network/	2.18
4454	Fetch.ai	FET	https://coinmarketcap.com//currencies/fetch/	0.6
4455	DODO	DODO	https://coinmarketcap.com//currencies/dodo/	4.42
4456	Golem	GLM	https://coinmarketcap.com//currencies/golem-network-tokens/	0.43
4457	Energy Web Token	EWT	https://coinmarketcap.com//currencies/energy-web-token/	14.14
4458	Arweave	AR	https://coinmarketcap.com//currencies/arweave/	12.65
4459	Lisk	LSK	https://coinmarketcap.com//currencies/lisk/	3.28
4460	IoTeX	IOTX	https://coinmarketcap.com//currencies/iotex/	0.04
4461	Alpha Finance Lab	ALPHA	https://coinmarketcap.com//currencies/alpha-finance-lab/	1.64
4462	FunFair	FUN	https://coinmarketcap.com//currencies/funfair/	0.04
4463	WAX	WAXP	https://coinmarketcap.com//currencies/wax/	0.26
4464	HedgeTrade	HEDG	https://coinmarketcap.com//currencies/hedgetrade/	1.14
4465	Nervos Network	CKB	https://coinmarketcap.com//currencies/nervos-network/	0.02
4466	Venus	XVS	https://coinmarketcap.com//currencies/venus/	43.52
4467	VeThor Token	VTHO	https://coinmarketcap.com//currencies/vethor-token/	0.01
4468	Balancer	BAL	https://coinmarketcap.com//currencies/balancer/	54.48
4469	KuCoin Token	KCS	https://coinmarketcap.com//currencies/kucoin-token/	4.66
4470	Kava.io	KAVA	https://coinmarketcap.com//currencies/kava/	6.23
4471	Telcoin	TEL	https://coinmarketcap.com//currencies/telcoin/	0.01
4472	MVL	MVL	https://coinmarketcap.com//currencies/mvl/	0.03
4473	Polymath	POLY	https://coinmarketcap.com//currencies/polymath-network/	0.55
4474	Augur	REP	https://coinmarketcap.com//currencies/augur/	29.76
4475	Status	SNT	https://coinmarketcap.com//currencies/status/	0.09
4476	Badger DAO	BADGER	https://coinmarketcap.com//currencies/badger-dao/	42.89
4477	OriginTrail	TRAC	https://coinmarketcap.com//currencies/origintrail/	0.86
4478	Celer Network	CELR	https://coinmarketcap.com//currencies/celer-network/	0.06
4479	Oasis Network	ROSE	https://coinmarketcap.com//currencies/oasis-network/	0.2
4480	TrueUSD	TUSD	https://coinmarketcap.com//currencies/trueusd/	1
4481	Utrust	UTK	https://coinmarketcap.com//currencies/utrust/	0.66
4482	Dent	DENT	https://coinmarketcap.com//currencies/dent/	0
4483	BORA	BORA	https://coinmarketcap.com//currencies/bora/	0.34
4484	Polkastarter	POLS	https://coinmarketcap.com//currencies/polkastarter/	4.87
4485	MyNeighborAlice	ALICE	https://coinmarketcap.com//currencies/myneighboralice/	16.28
4486	QuarkChain	QKC	https://coinmarketcap.com//currencies/quarkchain/	0.04
4487	Civic	CVC	https://coinmarketcap.com//currencies/civic/	0.42
4488	Swipe	SXP	https://coinmarketcap.com//currencies/swipe/	3.19
4489	Band Protocol	BAND	https://coinmarketcap.com//currencies/band-protocol/	13.42
4490	Aragon	ANT	https://coinmarketcap.com//currencies/aragon/	6.93
4491	MaidSafeCoin	MAID	https://coinmarketcap.com//currencies/maidsafecoin/	0.61
4492	Origin Protocol	OGN	https://coinmarketcap.com//currencies/origin-protocol/	1.03
4493	Serum	SRM	https://coinmarketcap.com//currencies/serum/	5.39
4494	COTI	COTI	https://coinmarketcap.com//currencies/coti/	0.39
4495	Wanchain	WAN	https://coinmarketcap.com//currencies/wanchain/	1.53
4496	Ampleforth	AMPL	https://coinmarketcap.com//currencies/ampleforth/	0.89
4497	Everipedia	IQ	https://coinmarketcap.com//currencies/everipedia/	0.02
4498	NuCypher	NU	https://coinmarketcap.com//currencies/nucypher/	0.59
4499	SingularityNET	AGI	https://coinmarketcap.com//currencies/singularitynet/	0.28
4500	Storj	STORJ	https://coinmarketcap.com//currencies/storj/	0.94
4501	Ardor	ARDR	https://coinmarketcap.com//currencies/ardor/	0.23
4502	BitShares	BTS	https://coinmarketcap.com//currencies/bitshares/	0.08
4503	Gnosis	GNO	https://coinmarketcap.com//currencies/gnosis-gno/	150.24
4504	RSK Infrastructure Framework	RIF	https://coinmarketcap.com//currencies/rsk-infrastructure-framework/	0.31
4505	Numeraire	NMR	https://coinmarketcap.com//currencies/numeraire/	46.42
4506	Orchid	OXT	https://coinmarketcap.com//currencies/orchid/	0.6
4507	Bitcoin Standard Hashrate Token	BTCST	https://coinmarketcap.com//currencies/bitcoin-standard-hashrate-token/	370.39
4508	Travala.com	AVA	https://coinmarketcap.com//currencies/travala/	4.15
4509	Secret	SCRT	https://coinmarketcap.com//currencies/secret/	3.03
4510	TrustSwap	SWAP	https://coinmarketcap.com//currencies/trustswap/	2.6
4511	IRISnet	IRIS	https://coinmarketcap.com//currencies/irisnet/	0.22
4512	Litentry	LIT	https://coinmarketcap.com//currencies/litentry/	11.13
4513	Bitcoin Diamond	BCD	https://coinmarketcap.com//currencies/bitcoin-diamond/	1.08
4514	iExec RLC	RLC	https://coinmarketcap.com//currencies/rlc/	2.5
4515	Stratis	STRAX	https://coinmarketcap.com//currencies/stratis/	1.53
4516	Metadium	META	https://coinmarketcap.com//currencies/metadium/	0.16
4517	ZB Token	ZB	https://coinmarketcap.com//currencies/zb-token/	0.42
4518	TomoChain	TOMO	https://coinmarketcap.com//currencies/tomochain/	2.34
4519	Hifi Finance	MFT	https://coinmarketcap.com//currencies/mainframe/	0.02
4520	Ultra	UOS	https://coinmarketcap.com//currencies/ultra/	0.66
4521	Unibright	UBT	https://coinmarketcap.com//currencies/unibright/	1.22
4522	Chromia	CHR	https://coinmarketcap.com//currencies/chromia/	0.43
4523	Velas	VLX	https://coinmarketcap.com//currencies/velas/	0.09
4524	Injective Protocol	INJ	https://coinmarketcap.com//currencies/injective-protocol/	11.56
4525	Haven Protocol	XHV	https://coinmarketcap.com//currencies/haven-protocol/	12.81
4526	Komodo	KMD	https://coinmarketcap.com//currencies/komodo/	1.4
4527	Syscoin	SYS	https://coinmarketcap.com//currencies/syscoin/	0.28
4528	Steem	STEEM	https://coinmarketcap.com//currencies/steem/	0.46
4529	Sora	XOR	https://coinmarketcap.com//currencies/sora/	489.66
4530	Ark	ARK	https://coinmarketcap.com//currencies/ark/	1.32
4531	aelf	ELF	https://coinmarketcap.com//currencies/aelf/	0.31
4532	Phala Network	PHA	https://coinmarketcap.com//currencies/phala-network/	0.94
4533	StormX	STMX	https://coinmarketcap.com//currencies/stormx/	0.02
4534	district0x	DNT	https://coinmarketcap.com//currencies/district0x/	0.28
4535	Divi	DIVI	https://coinmarketcap.com//currencies/divi/	0.07
4536	Kin	KIN	https://coinmarketcap.com//currencies/kin/	0
4537	Syntropy	NOIA	https://coinmarketcap.com//currencies/syntropy/	0.48
4538	Thunder Token	TT	https://coinmarketcap.com//currencies/thunder-token/	0.02
4539	LTO Network	LTO	https://coinmarketcap.com//currencies/lto-network/	0.57
4540	JUST	JST	https://coinmarketcap.com//currencies/just/	0.07
4541	CyberVein	CVT	https://coinmarketcap.com//currencies/cybervein/	0.15
4542	ABBC Coin	ABBC	https://coinmarketcap.com//currencies/abbc-coin/	0.19
4543	Loom Network	LOOM	https://coinmarketcap.com//currencies/loom-network/	0.16
4544	Creditcoin	CTC	https://coinmarketcap.com//currencies/creditcoin/	6.95
4545	Bridge Oracle	BRG	https://coinmarketcap.com//currencies/bridge-oracle/	0.2808
4546	HEX	HEX	https://coinmarketcap.com//currencies/hex/	0.01198
4547	Counos X	CCXX	https://coinmarketcap.com//currencies/counos-x/	92.61
4548	Huobi BTC	HBTC	https://coinmarketcap.com//currencies/huobi-btc/	55682.56
4549	WhiteCoin	XWC	https://coinmarketcap.com//currencies/whitecoin/	2.03
4550	DeFiChain	DFI	https://coinmarketcap.com//currencies/defichain/	3.49
4551	Wrapped BNB	WBNB	https://coinmarketcap.com//currencies/wbnb/	253.77
4552	INO COIN	INO	https://coinmarketcap.com//currencies/ino-coin/	6.87
4553	ThoreCoin	THR	https://coinmarketcap.com//currencies/thorecoin/	10833.06
4554	BitMax Token	BTMX	https://coinmarketcap.com//currencies/bitmax-token/	1.34
4555	The Transfer Token	TTT	https://coinmarketcap.com//currencies/the-transfer-token/	6.95
4556	Orbit Chain	ORC	https://coinmarketcap.com//currencies/orbit-chain/	1.01
4557	XinFin Network	XDC	https://coinmarketcap.com//currencies/xinfin-network/	0.04
4558	Bitpanda Ecosystem Token	BEST	https://coinmarketcap.com//currencies/bitpanda-ecosystem-token/	1.08
4559	Mixin	XIN	https://coinmarketcap.com//currencies/mixin/	840.24
4560	NXM	NXM	https://coinmarketcap.com//currencies/nxm/	68.7
4561	Venus BNB	vBNB	https://coinmarketcap.com//currencies/venus-bnb/	5.17
4562	The Sandbox	SAND	https://coinmarketcap.com//currencies/the-sandbox/	0.62
4563	Conflux Network	CFX	https://coinmarketcap.com//currencies/conflux-network/	0.59
4564	Orion Protocol	ORN	https://coinmarketcap.com//currencies/orion-protocol/	18.93
4565	Klever	KLV	https://coinmarketcap.com//currencies/klever/	0.1
4566	Zelwin	ZLW	https://coinmarketcap.com//currencies/zelwin/	4.86
4567	stETH (Lido)	STETH	https://coinmarketcap.com//currencies/steth/	1748.1
4568	GNY	GNY	https://coinmarketcap.com//currencies/gny/	1.69
4569	Mirror Protocol	MIR	https://coinmarketcap.com//currencies/mirror-protocol/	7.32
4570	Electroneum	ETN	https://coinmarketcap.com//currencies/electroneum/	0.02
4571	Hathor	HTR	https://coinmarketcap.com//currencies/hathor/	1.67
4572	Bitcoin Cash ABC	BCHA	https://coinmarketcap.com//currencies/bitcoin-cash-abc-2/	16.14
4573	Rewardiqa	REW	https://coinmarketcap.com//currencies/rewardiqa/	29.87
4574	Mdex	MDX	https://coinmarketcap.com//currencies/mdex/	3.9
4575	Keep Network	KEEP	https://coinmarketcap.com//currencies/keep-network/	0.59
4576	SafePal	SFP	https://coinmarketcap.com//currencies/safepal/	2.7
4577	dKargo	DKA	https://coinmarketcap.com//currencies/dkargo/	0.36
4578	Axie Infinity	AXS	https://coinmarketcap.com//currencies/axie-infinity/	5.93
4579	Sologenic	SOLO	https://coinmarketcap.com//currencies/sologenic/	1.26
4580	MATH	MATH	https://coinmarketcap.com//currencies/math/	2.18
4581	Idea Chain Coin	ICH	https://coinmarketcap.com//currencies/idea-chain-coin/	4.7
4582	botXcoin	BOTX	https://coinmarketcap.com//currencies/botxcoin/	0.15
4583	Linear	LINA	https://coinmarketcap.com//currencies/linear/	0.09
4584	Amp	AMP	https://coinmarketcap.com//currencies/amp/	0.02
4585	Venus BTC	vBTC	https://coinmarketcap.com//currencies/venus-btc/	1103.91
4586	Eauric	EAURIC	https://coinmarketcap.com//currencies/eauric/	8.33
4587	RedFOX Labs	RFOX	https://coinmarketcap.com//currencies/redfox-labs/	0.16
4588	Audius	AUDIO	https://coinmarketcap.com//currencies/audius/	1.75
4589	Gala	GALA	https://coinmarketcap.com//currencies/gala/	0.03
4590	Akash Network	AKT	https://coinmarketcap.com//currencies/akash-network/	5.11
4591	WHALE	WHALE	https://coinmarketcap.com//currencies/whale/	37.75
4592	Venus XVS	vXVS	https://coinmarketcap.com//currencies/venus-xvs/	0.87
4593	Dego Finance	DEGO	https://coinmarketcap.com//currencies/dego-finance/	22.46
4594	TROY	TROY	https://coinmarketcap.com//currencies/troy/	0.02
4595	SuperFarm	SUPER	https://coinmarketcap.com//currencies/superfarm/	1.83
4596	BakeryToken	BAKE	https://coinmarketcap.com//currencies/bakerytoken/	1.27
4597	MimbleWimbleCoin	MWC	https://coinmarketcap.com//currencies/mimblewimblecoin/	16.63
4598	DerivaDAO	DDX	https://coinmarketcap.com//currencies/derivadao/	6.68
4599	Elitium	EUM	https://coinmarketcap.com//currencies/elitium/	5.79
4600	Neutrino USD	USDN	https://coinmarketcap.com//currencies/neutrino-usd/	1
4601	Doctors Coin	DRS	https://coinmarketcap.com//currencies/doctors-coin/	0.74
4602	SparkPoint	SRK	https://coinmarketcap.com//currencies/sparkpoint/	0.02
4603	Beefy.Finance	BIFI	https://coinmarketcap.com//currencies/beefy-finance/	2307.31
4604	Rocket Pool	RPL	https://coinmarketcap.com//currencies/rocket-pool/	16.03
4605	Rari Governance Token	RGT	https://coinmarketcap.com//currencies/rari-governance-token/	18.73
4606	Trust Wallet Token	TWT	https://coinmarketcap.com//currencies/trust-wallet-token/	0.63
4607	KardiaChain	KAI	https://coinmarketcap.com//currencies/kardiachain/	0.08
4608	MANTRA DAO	OM	https://coinmarketcap.com//currencies/mantra-dao/	0.57
4609	Akropolis	AKRO	https://coinmarketcap.com//currencies/akropolis/	0.06
4610	Livepeer	LPT	https://coinmarketcap.com//currencies/livepeer/	7.12
4611	Tokenlon Network Token	LON	https://coinmarketcap.com//currencies/tokenlon-network-token/	6.87
4612	Populous	PPT	https://coinmarketcap.com//currencies/populous/	2.8
4613	NewYork Exchange	NYE	https://coinmarketcap.com//currencies/newyork-exchange/	20.93
4614	Power Ledger	POWR	https://coinmarketcap.com//currencies/power-ledger/	0.34
4615	Vai	VAI	https://coinmarketcap.com//currencies/vai/	0.9
4616	Hive	HIVE	https://coinmarketcap.com//currencies/hive-blockchain/	0.37
4617	CoinMetro Token	XCM	https://coinmarketcap.com//currencies/coinmetro-token/	0.48
4618	Streamr	DATA	https://coinmarketcap.com//currencies/streamr/	0.17
4619	sUSD	SUSD	https://coinmarketcap.com//currencies/susd/	1.01
4620	Perpetual Protocol	PERP	https://coinmarketcap.com//currencies/perpetual-protocol/	6.54
4621	PAX Gold	PAXG	https://coinmarketcap.com//currencies/pax-gold/	1741.32
4622	EFFORCE	WOZX	https://coinmarketcap.com//currencies/efforce/	2.18
4623	PIBBLE	PIB	https://coinmarketcap.com//currencies/pibble/	0.01
4624	Uquid Coin	UQC	https://coinmarketcap.com//currencies/uquid-coin/	14
4625	Aion	AION	https://coinmarketcap.com//currencies/aion/	0.28
4626	Hellenic Coin	HNC	https://coinmarketcap.com//currencies/helleniccoin/	1.99
4627	Bloomzed Loyalty Club Ticket	BLCT	https://coinmarketcap.com//currencies/bloomzed-token/	3.64
4628	PARSIQ	PRQ	https://coinmarketcap.com//currencies/parsiq/	1.26
4629	saffron.finance	SFI	https://coinmarketcap.com//currencies/saffron-finance/	1767.59
4630	Mask Network	MASK	https://coinmarketcap.com//currencies/mask-network/	15.19
4631	Carry	CRE	https://coinmarketcap.com//currencies/carry/	0.02
4632	Bytom	BTM	https://coinmarketcap.com//currencies/bytom/	0.09
4633	Edgeware	EDG	https://coinmarketcap.com//currencies/edgeware/	0.02
4634	Spendcoin	SPND	https://coinmarketcap.com//currencies/spendcoin/	0.05
4635	Centrality	CENNZ	https://coinmarketcap.com//currencies/centrality/	0.11
4636	Attila	ATT	https://coinmarketcap.com//currencies/attila/	0.28
4637	GreenPower	GRN	https://coinmarketcap.com//currencies/greenpower/	0.04
4638	12Ships	TSHP	https://coinmarketcap.com//currencies/12ships/	0.03
4639	REVV	REVV	https://coinmarketcap.com//currencies/revv/	0.45
4640	MediBloc	MED	https://coinmarketcap.com//currencies/medibloc/	0.05
4641	Big Data Protocol	BDP	https://coinmarketcap.com//currencies/big-data-protocol/	3.89
4642	Dusk Network	DUSK	https://coinmarketcap.com//currencies/dusk-network/	0.34
4643	Wootrade	WOO	https://coinmarketcap.com//currencies/wootrade/	0.32
4644	Request	REQ	https://coinmarketcap.com//currencies/request/	0.1222
4645	MonaCoin	MONA	https://coinmarketcap.com//currencies/monacoin/	1.85
4646	Sapphire	SAPP	https://coinmarketcap.com//currencies/sapphire/	0.2361
4647	HARD Protocol	HARD	https://coinmarketcap.com//currencies/hard-protocol/	2.37
4648	GateToken	GT	https://coinmarketcap.com//currencies/gatetoken/	1.55
4649	WazirX	WRX	https://coinmarketcap.com//currencies/wazirx/	0.4939
4650	Gemini Dollar	GUSD	https://coinmarketcap.com//currencies/gemini-dollar/	0.9961
4651	Chimpion	BNANA	https://coinmarketcap.com//currencies/chimpion/	3.66
4652	Gas	GAS	https://coinmarketcap.com//currencies/gas/	11.48
4653	LBRY Credits	LBC	https://coinmarketcap.com//currencies/library-credit/	0.2228
4654	BarnBridge	BOND	https://coinmarketcap.com//currencies/barnbridge/	66.8
4655	NFTX	NFTX	https://coinmarketcap.com//currencies/nftx/	259.11
4656	Radix	EXRD	https://coinmarketcap.com//currencies/radix/	0.15
4657	Bella Protocol	BEL	https://coinmarketcap.com//currencies/bella-protocol/	4.03
4658	Frax	FRAX	https://coinmarketcap.com//currencies/frax/	1
4659	AdEx Network	ADX	https://coinmarketcap.com//currencies/adx-net/	0.96
4660	yOUcash	YOUC	https://coinmarketcap.com//currencies/youcash/	0.06
4661	Raydium	RAY	https://coinmarketcap.com//currencies/raydium/	9.75
4662	Handshake	HNS	https://coinmarketcap.com//currencies/handshake/	0.3
4663	PIXEL	PXL	https://coinmarketcap.com//currencies/pixel/	0.19
4664	ShareToken	SHR	https://coinmarketcap.com//currencies/sharetoken/	0.05
4665	AllianceBlock	ALBT	https://coinmarketcap.com//currencies/allianceblock/	0.77
4666	Atari Token	ATRI	https://coinmarketcap.com//currencies/atari-token/	0.27
4667	Unifi Protocol DAO	UNFI	https://coinmarketcap.com//currencies/unifi-protocol-dao/	31.3
4668	KLAYswap Protocol	KSP	https://coinmarketcap.com//currencies/klayswap-protocol/	18.06
4669	Darwinia Network	RING	https://coinmarketcap.com//currencies/darwinia-network/	0.23
4670	ReddCoin	RDD	https://coinmarketcap.com//currencies/redd/	0
4671	SOLVE	SOLVE	https://coinmarketcap.com//currencies/solve/	0.28
4672	Bluzelle	BLZ	https://coinmarketcap.com//currencies/bluzelle/	0.38
4673	Waltonchain	WTC	https://coinmarketcap.com//currencies/waltonchain/	1.43
4674	DIA	DIA	https://coinmarketcap.com//currencies/dia/	3.63
4675	Mobilian Coin	MBN	https://coinmarketcap.com//currencies/mobilian-coin/	0.72
4676	xDai	STAKE	https://coinmarketcap.com//currencies/xdai/	24.04
4677	XeniosCoin	XNC	https://coinmarketcap.com//currencies/xenioscoin/	1.32
4678	Zenon	ZNN	https://coinmarketcap.com//currencies/zenon/	18.9
4679	Cartesi	CTSI	https://coinmarketcap.com//currencies/cartesi/	0.33
4680	SUN	SUN	https://coinmarketcap.com//currencies/sun/	20.48
4681	Ethernity Chain	ERN	https://coinmarketcap.com//currencies/ethernity-chain/	22.67
4682	bZx Protocol	BZRX	https://coinmarketcap.com//currencies/bzx-protocol/	0.57
4683	Massnet	MASS	https://coinmarketcap.com//currencies/massnet/	1.04
4684	Lambda	LAMB	https://coinmarketcap.com//currencies/lambda/	0.07
4685	Firo	FIRO	https://coinmarketcap.com//currencies/firo/	8.34
4686	Venus USDC	vUSDC	https://coinmarketcap.com//currencies/venus-usdc/	0.02
4687	Zap	ZAP	https://coinmarketcap.com//currencies/zap/	0.41
4688	Frax Share	FXS	https://coinmarketcap.com//currencies/frax-share/	12.61
4689	Kylin	KYL	https://coinmarketcap.com//currencies/kylin/	1.92
4690	Enzyme	MLN	https://coinmarketcap.com//currencies/enzyme/	53.14
4691	Hegic	HEGIC	https://coinmarketcap.com//currencies/hegic/	0.24
4692	Contentos	COS	https://coinmarketcap.com//currencies/contentos/	0.03
4693	Ignis	IGNIS	https://coinmarketcap.com//currencies/ignis/	0.12
4694	Bridge Mutual	BMI	https://coinmarketcap.com//currencies/bridge-mutual/	3.83
4695	RAMP	RAMP	https://coinmarketcap.com//currencies/ramp/	0.47
4696	Morpheus.Network	MRPH	https://coinmarketcap.com//currencies/morpheus-network/	1.94
4697	Frontier	FRONT	https://coinmarketcap.com//currencies/frontier/	2.49
4698	PAC Global	PAC	https://coinmarketcap.com//currencies/pac-global/	0.01
4699	CertiK	CTK	https://coinmarketcap.com//currencies/certik/	2.5
4700	Dragonchain	DRGN	https://coinmarketcap.com//currencies/dragonchain/	0.24
4701	NKN	NKN	https://coinmarketcap.com//currencies/nkn/	0.15
4702	MovieBloc	MBL	https://coinmarketcap.com//currencies/moviebloc/	0.01
4703	Metal	MTL	https://coinmarketcap.com//currencies/metal/	1.35
4704	DuckDaoDime	DDIM	https://coinmarketcap.com//currencies/duckdaodime/	85.29
4705	Terra Virtua Kolect	TVK	https://coinmarketcap.com//currencies/terra-virtua-kolect/	0.99
4706	Energi	NRG	https://coinmarketcap.com//currencies/energi/	2.25
4707	Constellation	DAG	https://coinmarketcap.com//currencies/constellation/	0.07
4708	Perlin	PERL	https://coinmarketcap.com//currencies/perlin/	0.18
4709	ARPA Chain	ARPA	https://coinmarketcap.com//currencies/arpa-chain/	0.08
4710	Ferrum Network	FRM	https://coinmarketcap.com//currencies/ferrum-network/	0.58
4711	ChainX	PCX	https://coinmarketcap.com//currencies/chainx/	11.2
4712	Harvest Finance	FARM	https://coinmarketcap.com//currencies/harvest-finance/	170.5
4713	Verasity	VRA	https://coinmarketcap.com//currencies/verasity/	0.02
4714	NULS	NULS	https://coinmarketcap.com//currencies/nuls/	0.86
4715	WaykiChain	WICC	https://coinmarketcap.com//currencies/waykichain/	0.41
4716	DFI.Money	YFII	https://coinmarketcap.com//currencies/yearn-finance-ii/	2203.41
4717	Bytecoin	BCN	https://coinmarketcap.com//currencies/bytecoin-bcn/	0
4718	WINk	WIN	https://coinmarketcap.com//currencies/wink/	0
4719	Nexus	NXS	https://coinmarketcap.com//currencies/nexus/	1.19
4720	JustLiquidity	JUL	https://coinmarketcap.com//currencies/justliquidity/	185.68
4721	Beam	BEAM	https://coinmarketcap.com//currencies/beam/	0.98
4722	BioPassport Token	BIOT	https://coinmarketcap.com//currencies/biopassport-token/	0.04
4723	Beldex	BDX	https://coinmarketcap.com//currencies/beldex/	0.08
4724	Meme	MEME	https://coinmarketcap.com//currencies/degenerator-meme/	2902.24
4725	Huobi Pool Token	HPT	https://coinmarketcap.com//currencies/huobi-pool-token/	0.01
4726	Aavegotchi	GHST	https://coinmarketcap.com//currencies/aavegotchi/	1.6
4727	Aergo	AERGO	https://coinmarketcap.com//currencies/aergo/	0.3
4728	FC Barcelona Fan Token	BAR	https://coinmarketcap.com//currencies/fc-barcelona-fan-token/	49.61
4729	Cashaa	CAS	https://coinmarketcap.com//currencies/cashaa/	0.12
4730	Wrapped NXM	WNXM	https://coinmarketcap.com//currencies/wrapped-nxm/	47.23
4731	Decentral Games	DG	https://coinmarketcap.com//currencies/decentral-games/	442.97
4732	DxChain Token	DX	https://coinmarketcap.com//currencies/dxchain-token/	0
4733	YIELD App	YLD	https://coinmarketcap.com//currencies/yield-app/	0.86
4734	Radicle	RAD	https://coinmarketcap.com//currencies/radicle/	15.87
4735	Nash	NEX	https://coinmarketcap.com//currencies/nash-exchange/	2.66
4736	Marlin	POND	https://coinmarketcap.com//currencies/marlin/	0.17
4737	SUKU	SUKU	https://coinmarketcap.com//currencies/suku/	0.67
4738	Switcheo	SWTH	https://coinmarketcap.com//currencies/switcheo/	0.06
4739	Hxro	HXRO	https://coinmarketcap.com//currencies/hxro/	0.34
4740	DeXe	DEXE	https://coinmarketcap.com//currencies/dexe/	26.73
4741	NEST Protocol	NEST	https://coinmarketcap.com//currencies/nest-protocol/	0.04
4742	MX Token	MX	https://coinmarketcap.com//currencies/mx-token/	0.63
4743	LUKSO	LYXe	https://coinmarketcap.com//currencies/lukso/	19.84
4744	LUKSO	LYXe	https://coinmarketcap.com//currencies/lukso/	19.84
4745	inSure DeFi	SURE	https://coinmarketcap.com//currencies/insure/	0.004852
4746	DEXTools	DEXT	https://coinmarketcap.com//currencies/dextools/	0.7769
4747	Swingby	SWINGBY	https://coinmarketcap.com//currencies/swingby/	0.632
4748	RChain	REV	https://coinmarketcap.com//currencies/rchain/	0.1545
4749	API3	API3	https://coinmarketcap.com//currencies/api3/	5.39
4750	AXEL	AXEL	https://coinmarketcap.com//currencies/axel/	0.2801
4751	Vitae	VITAE	https://coinmarketcap.com//currencies/vitae/	3.81
4752	Venus ETH	vETH	https://coinmarketcap.com//currencies/venus-eth/	36.05
4753	Standard Tokenization Protocol	STPT	https://coinmarketcap.com//currencies/standard-tokenization-protocol/	0.0716
4754	v.systems	VSYS	https://coinmarketcap.com//currencies/v-systems/	0.03
4755	PIVX	PIVX	https://coinmarketcap.com//currencies/pivx/	1.1
4756	BOSAGORA	BOA	https://coinmarketcap.com//currencies/bosagora/	0.24
4757	KARMA	KARMA	https://coinmarketcap.com//currencies/karma-eos/	0.01
4758	MAPS	MAPS	https://coinmarketcap.com//currencies/maps/	1.12
4759	LCX	LCX	https://coinmarketcap.com//currencies/lcx/	0.11
4760	Selfkey	KEY	https://coinmarketcap.com//currencies/selfkey/	0.02
4761	Qcash	QC	https://coinmarketcap.com//currencies/qcash/	0.15
4762	Prometeus	PROM	https://coinmarketcap.com//currencies/prometeus/	11.47
4763	Flamingo	FLM	https://coinmarketcap.com//currencies/flamingo/	0.47
4764	DigitalBits	XDB	https://coinmarketcap.com//currencies/digitalbits/	0.09
4765	Oxen	OXEN	https://coinmarketcap.com//currencies/oxen/	1.29
4766	Render Token	RNDR	https://coinmarketcap.com//currencies/render-token/	0.48
4767	Quantstamp	QSP	https://coinmarketcap.com//currencies/quantstamp/	0.1
4768	Elastos	ELA	https://coinmarketcap.com//currencies/elastos/	3.96
4769	VideoCoin	VID	https://coinmarketcap.com//currencies/videocoin/	0.46
4770	DREP	DREP	https://coinmarketcap.com//currencies/drep/	0.02
4771	BEPRO Network	BEPRO	https://coinmarketcap.com//currencies/bepro-network/	0.04
4772	Sport and Leisure	SNL	https://coinmarketcap.com//currencies/sport-and-leisure/	0.1
4773	Bondly	BONDLY	https://coinmarketcap.com//currencies/bondly/	0.64
4774	Spartan Protocol	SPARTA	https://coinmarketcap.com//currencies/spartan-protocol/	1.04
4775	Dock	DOCK	https://coinmarketcap.com//currencies/dock/	0.1
4776	Cindicator	CND	https://coinmarketcap.com//currencies/cindicator/	0.03
4777	Vesper	VSP	https://coinmarketcap.com//currencies/vesper/	35.55
4778	Everest	ID	https://coinmarketcap.com//currencies/everest/	0.53
4779	Nimiq	NIM	https://coinmarketcap.com//currencies/nimiq/	0.01
4780	VIDT Datalink	VIDT	https://coinmarketcap.com//currencies/vidt-datalink/	1.3
4781	Einsteinium	EMC2	https://coinmarketcap.com//currencies/einsteinium/	0.29
4782	Fusion	FSN	https://coinmarketcap.com//currencies/fusion/	1.29
4783	Lympo	LYM	https://coinmarketcap.com//currencies/lympo/	0.08
4784	MiL.k	MLK	https://coinmarketcap.com//currencies/milk-alliance/	0.83
4785	TrueFi	TRU	https://coinmarketcap.com//currencies/truefi-token/	0.46
4786	Cream Finance	CREAM	https://coinmarketcap.com//currencies/cream-finance/	103.16
4787	Maro	MARO	https://coinmarketcap.com//currencies/maro/	0.13
4788	Ripio Credit Network	RCN	https://coinmarketcap.com//currencies/ripio-credit-network/	0.12
4789	Stakenet	XSN	https://coinmarketcap.com//currencies/stakenet/	0.54
4790	Mithril	MITH	https://coinmarketcap.com//currencies/mithril/	0.06
4791	Tellor	TRB	https://coinmarketcap.com//currencies/tellor/	51.33
4792	Kleros	PNK	https://coinmarketcap.com//currencies/kleros/	0.1
4793	Multiplier	MXX	https://coinmarketcap.com//currencies/multiplier/	0.33
4794	Endor Protocol	EDR	https://coinmarketcap.com//currencies/endor-protocol/	0.04
4795	MXC	MXC	https://coinmarketcap.com//currencies/mxc/	0.02
4796	Duck DAO (DLP Duck Token)	DUCK	https://coinmarketcap.com//currencies/duck-dao/	1.93
4797	Aeternity	AE	https://coinmarketcap.com//currencies/aeternity/	0.18
4798	Pirate Chain	ARRR	https://coinmarketcap.com//currencies/pirate-chain/	0.34
4799	Ambrosus	AMB	https://coinmarketcap.com//currencies/amber/	0.15
4800	Cocos-BCX	COCOS	https://coinmarketcap.com//currencies/cocos-bcx/	1.42
4801	TEMCO	TEMCO	https://coinmarketcap.com//currencies/temco/	0.02
4802	SIRIN LABS Token	SRN	https://coinmarketcap.com//currencies/sirin-labs-token/	0.12
4803	Phantasma	SOUL	https://coinmarketcap.com//currencies/phantasma/	0.6
4804	YUSRA	YUSRA	https://coinmarketcap.com//currencies/yusra/	1.56
4805	Tornado Cash	TORN	https://coinmarketcap.com//currencies/torn/	179.31
4806	Empty Set Dollar	ESD	https://coinmarketcap.com//currencies/empty-set-dollar/	0.13
4807	FIO Protocol	FIO	https://coinmarketcap.com//currencies/fio-protocol/	0.26
4808	Cortex	CTXC	https://coinmarketcap.com//currencies/cortex/	0.23
4809	dForce	DF	https://coinmarketcap.com//currencies/dforce/	0.51
4810	Sentinel	SENT	https://coinmarketcap.com//currencies/sentinel/	0.03
4811	Sentinel Protocol	UPP	https://coinmarketcap.com//currencies/sentinel-protocol/	0.12
4812	IDEX	IDEX	https://coinmarketcap.com//currencies/idex/	0.1
4813	STASIS EURO	EURS	https://coinmarketcap.com//currencies/stasis-euro/	1.2
4814	BSCPAD	BSCPAD	https://coinmarketcap.com//currencies/bscpad/	3.84
4815	PowerPool	CVP	https://coinmarketcap.com//currencies/powerpool/	2.76
4816	Nexalt	XLT	https://coinmarketcap.com//currencies/nexalt/	2.72
4817	USDX [Kava]	USDX	https://coinmarketcap.com//currencies/usdx-kava/	0.92
4818	GoChain	GO	https://coinmarketcap.com//currencies/gochain/	0.05
4819	Crust	CRU	https://coinmarketcap.com//currencies/crust/	33.86
4820	Venus BUSD	vBUSD	https://coinmarketcap.com//currencies/venus-busd/	0.02
4821	Phoenix Global	PHB	https://coinmarketcap.com//currencies/phoenix-global/	0.02
4822	Dovu	DOV	https://coinmarketcap.com//currencies/dovu/	0.07
4823	Basid Coin	BASID	https://coinmarketcap.com//currencies/basid-coin/	0.09
4824	Groestlcoin	GRS	https://coinmarketcap.com//currencies/groestlcoin/	0.69
4825	Anyswap	ANY	https://coinmarketcap.com//currencies/anyswap/	2.84
4826	DIGG	DIGG	https://coinmarketcap.com//currencies/digg/	34190.31
4827	Measurable Data Token	MDT	https://coinmarketcap.com//currencies/measurable-data-token/	0.08
4828	suterusu	SUTER	https://coinmarketcap.com//currencies/suterusu/	0.01
4829	Ergo	ERG	https://coinmarketcap.com//currencies/ergo/	1.72
4830	Poseidon Network	QQQ	https://coinmarketcap.com//currencies/poseidon-network/	0.02
4831	MCO	MCO	https://coinmarketcap.com//currencies/crypto-com/	3.29
4832	LockTrip	LOC	https://coinmarketcap.com//currencies/lockchain/	3.52
4833	TitanSwap	TITAN	https://coinmarketcap.com//currencies/titanswap/	0.97
4834	GXChain	GXC	https://coinmarketcap.com//currencies/gxchain/	0.74
4835	BTU Protocol	BTU	https://coinmarketcap.com//currencies/btu-protocol/	0.57
4836	VITE	VITE	https://coinmarketcap.com//currencies/vite/	0.11
4837	pNetwork	PNT	https://coinmarketcap.com//currencies/pnetwork/	1.89
4838	Galatasaray Fan Token	GAL	https://coinmarketcap.com//currencies/galatasaray-fan-token/	14.68
4839	Goose Finance	EGG	https://coinmarketcap.com//currencies/goose-finance/	53.49
4840	ankrETH	aEth	https://coinmarketcap.com//currencies/ankreth/	1714.31
4841	cVault.finance	CORE	https://coinmarketcap.com//currencies/cvault-finance/	4982.87
4842	DSLA Protocol	DSLA	https://coinmarketcap.com//currencies/dsla-protocol/	0.01
4843	Nebulas	NAS	https://coinmarketcap.com//currencies/nebulas-token/	0.84
4844	DSLA Protocol	DSLA	https://coinmarketcap.com//currencies/dsla-protocol/	0.007714
4845	Keep3rV1	KP3R	https://coinmarketcap.com//currencies/keep3rv1/	246.71
4846	Nebulas	NAS	https://coinmarketcap.com//currencies/nebulas-token/	0.8433
4847	Bitrue Coin	BTR	https://coinmarketcap.com//currencies/bitrue-coin/	0.3738
4848	BitZ Token	BZ	https://coinmarketcap.com//currencies/bit-z-token/	0.3952
4849	TNC Coin	TNC	https://coinmarketcap.com//currencies/tnc-coin/	0.02101
4850	Bounce Token	AUCTION	https://coinmarketcap.com//currencies/bounce-token/	23.28
4851	The Midas Touch Gold	TMTG	https://coinmarketcap.com//currencies/the-midas-touch-gold/	0.006196
4852	HyperCash	HC	https://coinmarketcap.com//currencies/hypercash/	1.07
4853	VerusCoin	VRSC	https://coinmarketcap.com//currencies/veruscoin/	0.7851
4854	Shopping	SPI	https://coinmarketcap.com//currencies/shopping/	50.14
4855	GET Protocol	GET	https://coinmarketcap.com//currencies/get-protocol/	4.2
4856	Observer	OBSR	https://coinmarketcap.com//currencies/observer/	0.02
4857	UniLend	UFT	https://coinmarketcap.com//currencies/unilend/	3.32
4858	Burger Swap	BURGER	https://coinmarketcap.com//currencies/burger-swap/	3.96
4859	Muse	MUSE	https://coinmarketcap.com//currencies/muse/	91.92
4860	Quiztok	QTCON	https://coinmarketcap.com//currencies/quiztok/	0.07
4861	Wirex Token	WXT	https://coinmarketcap.com//currencies/wirex-token/	0.01
4862	Shroom.Finance	SHROOM	https://coinmarketcap.com//currencies/shroom-finance/	0.9
4863	Skycoin	SKY	https://coinmarketcap.com//currencies/skycoin/	2.3
4864	Wing	WING	https://coinmarketcap.com//currencies/wing/	43.97
4865	Bonfida	FIDA	https://coinmarketcap.com//currencies/bonfida/	1.75
4866	Function X	FX	https://coinmarketcap.com//currencies/function-x/	0.2
4867	Etherisc DIP Token	DIP	https://coinmarketcap.com//currencies/etherisc/	0.25
4868	Darma Cash	DMCH	https://coinmarketcap.com//currencies/darma-cash/	0.21
4869	Crowns	CWS	https://coinmarketcap.com//currencies/crowns/	56.39
4870	TerraKRW	KRT	https://coinmarketcap.com//currencies/terra-krw/	0
4871	Humanscape	HUM	https://coinmarketcap.com//currencies/humanscape/	0.09
4872	Auto	AUTO	https://coinmarketcap.com//currencies/auto/	3034.48
4873	Hacken Token	HAI	https://coinmarketcap.com//currencies/hackenai/	0.18
4874	mStable USD	MUSD	https://coinmarketcap.com//currencies/mstable-usd/	1
4875	Veritaseum	VERI	https://coinmarketcap.com//currencies/veritaseum/	20.41
4876	RING X PLATFORM	RINGX	https://coinmarketcap.com//currencies/ring-x-platform/	0.2
4877	Rally	RLY	https://coinmarketcap.com//currencies/rally/	0.71
4878	AirSwap	AST	https://coinmarketcap.com//currencies/airswap/	0.29
4879	Sentivate	SNTVT	https://coinmarketcap.com//currencies/sentivate/	0.01
4880	EasyFi	EASY	https://coinmarketcap.com//currencies/easyfi/	22.78
4881	Steem Dollars	SBD	https://coinmarketcap.com//currencies/steem-dollars/	6.6
4882	Celo Dollar	CUSD	https://coinmarketcap.com//currencies/celo-dollar/	1
4883	Neblio	NEBL	https://coinmarketcap.com//currencies/neblio/	2.43
4884	Moss Coin	MOC	https://coinmarketcap.com//currencies/moss-coin/	0.11
4885	Nxt	NXT	https://coinmarketcap.com//currencies/nxt/	0.04
4886	Wabi	WABI	https://coinmarketcap.com//currencies/wabi/	0.42
4887	All Sports	SOC	https://coinmarketcap.com//currencies/all-sports/	0.03
4888	Apollo Currency	APL	https://coinmarketcap.com//currencies/apollo-currency/	0
4889	BitKan	KAN	https://coinmarketcap.com//currencies/bitkan/	0
4890	Stafi	FIS	https://coinmarketcap.com//currencies/stafi/	3.6
4891	RioDeFi	RFUEL	https://coinmarketcap.com//currencies/rio-defi/	0.16
4892	DMarket	DMT	https://coinmarketcap.com//currencies/dmarket/	0.71
4893	BoringDAO	BOR	https://coinmarketcap.com//currencies/boringdao/	493.63
4894	OptionRoom	ROOM	https://coinmarketcap.com//currencies/optionroom/	3.26
4895	APY.Finance	APY	https://coinmarketcap.com//currencies/apy-finance/	1.66
4896	Super Zero Protocol	SERO	https://coinmarketcap.com//currencies/super-zero-protocol/	0.13
4897	Alpha Quark Token	AQT	https://coinmarketcap.com//currencies/alpha-quark-token/	7.76
4898	Bitball Treasure	BTRS	https://coinmarketcap.com//currencies/bitball-treasure/	87.46
4899	Telos	TLOS	https://coinmarketcap.com//currencies/telos/	0.15
4900	OctoFi	OCTO	https://coinmarketcap.com//currencies/octofi/	68.23
4901	Refereum	RFR	https://coinmarketcap.com//currencies/refereum/	0.01
4902	Meta	MTA	https://coinmarketcap.com//currencies/meta/	2.25
4903	BigONE Token	ONE	https://coinmarketcap.com//currencies/bigone-token/	0
4904	LATOKEN	LA	https://coinmarketcap.com//currencies/latoken/	0.1
4905	Proton	XPR	https://coinmarketcap.com//currencies/proton/	0.01
4906	DAD	DAD	https://coinmarketcap.com//currencies/dad/	0.29
4907	0Chain	ZCN	https://coinmarketcap.com//currencies/0chain/	0.79
4908	Whiteheart	WHITE	https://coinmarketcap.com//currencies/whiteheart/	4286.6
4909	88mph	MPH	https://coinmarketcap.com//currencies/88mph/	126.16
4910	TE-FOOD	TONE	https://coinmarketcap.com//currencies/te-food/	0.07
4911	Grin	GRIN	https://coinmarketcap.com//currencies/grin/	0.55
4912	Kadena	KDA	https://coinmarketcap.com//currencies/kadena/	0.35
4913	Robonomics.network	XRT	https://coinmarketcap.com//currencies/robonomics-network/	39.07
4914	DigixDAO	DGD	https://coinmarketcap.com//currencies/digixdao/	346.74
4915	Bifrost	BFC	https://coinmarketcap.com//currencies/bifrost/	0.07
4916	Oraichain Token	ORAI	https://coinmarketcap.com//currencies/oraichain-token/	40.69
4917	Vertcoin	VTC	https://coinmarketcap.com//currencies/vertcoin/	0.61
4918	ZeroSwap	ZEE	https://coinmarketcap.com//currencies/zeroswap/	1.09
4919	Project Pai	PAI	https://coinmarketcap.com//currencies/project-pai/	0.02
4920	Navcoin	NAV	https://coinmarketcap.com//currencies/nav-coin/	0.5
4921	Propy	PRO	https://coinmarketcap.com//currencies/propy/	0.51
4922	Raiden Network Token	RDN	https://coinmarketcap.com//currencies/raiden-network-token/	0.7
4923	Effect.AI	EFX	https://coinmarketcap.com//currencies/effect-ai/	0.14
4924	dHedge DAO	DHT	https://coinmarketcap.com//currencies/dhedge-dao/	4.06
4925	TOP	TOP	https://coinmarketcap.com//currencies/top/	0.01
4926	Aurora	AOA	https://coinmarketcap.com//currencies/aurora/	0
4927	Bankera	BNK	https://coinmarketcap.com//currencies/bankera/	0
4928	Minter Network	BIP	https://coinmarketcap.com//currencies/minter-network/	0.01
4929	Molecular Future	MOF	https://coinmarketcap.com//currencies/molecular-future/	0.4
4930	Neutrino Token	NSBT	https://coinmarketcap.com//currencies/neutrino-system-base-token/	18.6
4931	Finxflo	FXF	https://coinmarketcap.com//currencies/finxflo/	0.56
4932	Presearch	PRE	https://coinmarketcap.com//currencies/presearch/	0.1
4933	COVER Protocol	COVER	https://coinmarketcap.com//currencies/cover-protocol-new/	555.46
4934	PumaPay	PMA	https://coinmarketcap.com//currencies/pumapay/	0
4935	Newscrypto	NWC	https://coinmarketcap.com//currencies/newscrypto/	0.35
4936	Bao Finance	BAO	https://coinmarketcap.com//currencies/bao-finance/	0
4937	Aidos Kuneen	ADK	https://coinmarketcap.com//currencies/aidos-kuneen/	1.32
4938	RSK Smart Bitcoin	RBTC	https://coinmarketcap.com//currencies/rsk-smart-bitcoin/	56085.8
4939	CasinoCoin	CSC	https://coinmarketcap.com//currencies/casinocoin/	0
4940	Bounce Token (OLD)	BOT	https://coinmarketcap.com//currencies/bounce-token-old/	2595.01
4941	Gifto	GTO	https://coinmarketcap.com//currencies/gifto/	0.04
4942	BASIC	BASIC	https://coinmarketcap.com//currencies/basic/	0.01
4943	Tixl	TXL	https://coinmarketcap.com//currencies/tixl-new/	0.59
4944	BASIC	BASIC	https://coinmarketcap.com//currencies/basic/	0.007237
4945	Tixl	TXL	https://coinmarketcap.com//currencies/tixl-new/	0.5897
4946	Genesis Vision	GVT	https://coinmarketcap.com//currencies/genesis-vision/	7.03
4947	LGO Token	LGO	https://coinmarketcap.com//currencies/lgo-token/	0.5361
4948	AGA Token	AGA	https://coinmarketcap.com//currencies/aga/	4.2
4949	BUX Token	BUX	https://coinmarketcap.com//currencies/bux-token/	0.5308
4950	Aleph.im	ALEPH	https://coinmarketcap.com//currencies/aleph-im/	0.3658
4951	Ultiledger	ULT	https://coinmarketcap.com//currencies/ultiledger/	0.01289
4952	Opium	OPIUM	https://coinmarketcap.com//currencies/opium/	7.34
4953	PEAKDEFI	PEAK	https://coinmarketcap.com//currencies/peakdefi/	0.2758
4954	Venus USDT	vUSDT	https://coinmarketcap.com//currencies/venus-usdt/	0.02
4955	Bitcoin 2	BTC2	https://coinmarketcap.com//currencies/bitcoin2/	1.72
4956	BnkToTheFuture	BFT	https://coinmarketcap.com//currencies/bnktothefuture/	0.04
4957	LGCY Network	LGCY	https://coinmarketcap.com//currencies/lgcy-network/	0
4958	Blocknet	BLOCK	https://coinmarketcap.com//currencies/blocknet/	3.96
4959	Decentralized Vulnerability Platform	DVP	https://coinmarketcap.com//currencies/decentralized-vulnerability-platform/	0.1
4960	Rubic	RBC	https://coinmarketcap.com//currencies/rubic/	0.3
4961	Metronome	MET	https://coinmarketcap.com//currencies/metronome/	2.55
4962	NFT	NFT	https://coinmarketcap.com//currencies/nft/	0.79
4963	Morpheus Labs	MITX	https://coinmarketcap.com//currencies/morpheus-labs/	0.07
4964	AntiMatter	MATTER	https://coinmarketcap.com//currencies/antimatter/	3.04
4965	Rarible	RARI	https://coinmarketcap.com//currencies/rarible/	33.74
4966	Invictus Hyperion Fund	IHF	https://coinmarketcap.com//currencies/invictus-hyperion-fund/	0.24
4967	OAX	OAX	https://coinmarketcap.com//currencies/oax/	0.38
4968	QASH	QASH	https://coinmarketcap.com//currencies/qash/	0.08
4969	USDK	USDK	https://coinmarketcap.com//currencies/usdk/	1
4970	AppCoins	APPC	https://coinmarketcap.com//currencies/appcoins/	0.11
4971	Dynamic	DYN	https://coinmarketcap.com//currencies/dynamic/	1.83
4972	YFDAI.FINANCE	YF-DAI	https://coinmarketcap.com//currencies/yfdai-finance/	7027.26
4973	Anchor Neural World	ANW	https://coinmarketcap.com//currencies/anchor-neural-world/	0.1
4974	Smartlands Network	SLT	https://coinmarketcap.com//currencies/smartlands-network/	5.47
4975	Freeway Token	FWT	https://coinmarketcap.com//currencies/freeway-token/	0.01
4976	ForTube	FOR	https://coinmarketcap.com//currencies/the-force-protocol/	0.06
4977	ASTA	ASTA	https://coinmarketcap.com//currencies/asta/	0.03
4978	Paris Saint-Germain Fan Token	PSG	https://coinmarketcap.com//currencies/paris-saint-germain-fan-token/	21.34
4979	HUNT	HUNT	https://coinmarketcap.com//currencies/hunt/	0.25
4980	TokenClub	TCT	https://coinmarketcap.com//currencies/tokenclub/	0.04
4981	FirmaChain	FCT	https://coinmarketcap.com//currencies/firmachain/	0.15
4982	POA	POA	https://coinmarketcap.com//currencies/poa/	0.1
4983	WOM Protocol	WOM	https://coinmarketcap.com//currencies/wom-protocol/	0.27
4984	WePower	WPR	https://coinmarketcap.com//currencies/wepower/	0.04
4985	TenX	PAY	https://coinmarketcap.com//currencies/tenx/	0.18
4986	Obyte	GBYTE	https://coinmarketcap.com//currencies/obyte/	35.83
4987	Unisocks	SOCKS	https://coinmarketcap.com//currencies/unisocks/	84959.63
4988	Davinci Coin	DAC	https://coinmarketcap.com//currencies/davinci-coin/	0.01
4989	StableXSwap	STAX	https://coinmarketcap.com//currencies/stablexswap/	1.93
4990	ExNetwork Token	EXNT	https://coinmarketcap.com//currencies/exnetwork-token/	0.42
4991	1irstcoin	FST	https://coinmarketcap.com//currencies/1irstcoin/	0.75
4992	Elamachain	ELAMA	https://coinmarketcap.com//currencies/elamachain/	0.04
4993	Decentr	DEC	https://coinmarketcap.com//currencies/decentr/	0.35
4994	Blox	CDT	https://coinmarketcap.com//currencies/blox/	0.04
4995	Hermez Network	HEZ	https://coinmarketcap.com//currencies/hermez-network/	5.52
4996	OST	OST	https://coinmarketcap.com//currencies/ost/	0.03
4997	Mirrored Tesla	mTSLA	https://coinmarketcap.com//currencies/mirrored-tesla/	708.92
4998	Bread	BRD	https://coinmarketcap.com//currencies/bread/	0.28
4999	Venus SXP	vSXP	https://coinmarketcap.com//currencies/vsxp/	0.06
5000	AC Milan Fan Token	ACM	https://coinmarketcap.com//currencies/ac-milan-fan-token/	12.67
5001	CyberMiles	CMT	https://coinmarketcap.com//currencies/cybermiles/	0.03
5002	PCHAIN	PI	https://coinmarketcap.com//currencies/pchain/	0.03
5003	CUTcoin	CUT	https://coinmarketcap.com//currencies/cutcoin/	0.19
5004	Safex Token	SFT	https://coinmarketcap.com//currencies/safex-token/	0.01
5005	Nerve Finance	NRV	https://coinmarketcap.com//currencies/nerve-finance/	0.54
5006	Helmet.insure	HELMET	https://coinmarketcap.com//currencies/helmet-insure/	1
5007	ICHI	ICHI	https://coinmarketcap.com//currencies/ichi/	9.64
5008	Safe Haven	SHA	https://coinmarketcap.com//currencies/safe-haven/	0.01
5009	CryptalDash	CRD	https://coinmarketcap.com//currencies/cryptaldash/	0.02
5010	Newton	NEW	https://coinmarketcap.com//currencies/newton/	0
5011	Umbrella Network	UMB	https://coinmarketcap.com//currencies/umbrella-network/	1.34
5012	Razor Network	RAZOR	https://coinmarketcap.com//currencies/razor-network/	0.5
5013	Bonded Finance	BOND	https://coinmarketcap.com//currencies/bonded-finance/	0.06
5014	Quantum Resistant Ledger	QRL	https://coinmarketcap.com//currencies/quantum-resistant-ledger/	0.33
5015	Moeda Loyalty Points	MDA	https://coinmarketcap.com//currencies/moeda-loyalty-points/	1.21
5016	Lattice Token	LTX	https://coinmarketcap.com//currencies/lattice-token/	0.83
5017	UnlimitedIP	UIP	https://coinmarketcap.com//currencies/unlimitedip/	0.02
5018	Crypterium	CRPT	https://coinmarketcap.com//currencies/crpt/	0.28
5019	MurAll	PAINT	https://coinmarketcap.com//currencies/murall/	0
5020	Rakon	RKN	https://coinmarketcap.com//currencies/rakon/	0.1
5021	Egretia	EGT	https://coinmarketcap.com//currencies/egretia/	0.01
5022	SALT	SALT	https://coinmarketcap.com//currencies/salt/	0.29
5023	PowerTrade Fuel	PTF	https://coinmarketcap.com//currencies/powertrade-fuel/	1.13
5024	Ducato Protocol Token	DUCATO	https://coinmarketcap.com//currencies/ducato-protocol-token/	17.98
5025	MediShares	MDS	https://coinmarketcap.com//currencies/medishares/	0.02
5026	Homeros	HMR	https://coinmarketcap.com//currencies/homeros/	0.05
5027	MixMarvel	MIX	https://coinmarketcap.com//currencies/mixmarvel/	0.01
5028	Mirrored Netflix	mNFLX	https://coinmarketcap.com//currencies/mirrored-netflix/	531.25
5029	Metaverse Dualchain Network Architecture	DNA	https://coinmarketcap.com//currencies/metaverse-dualchain-network-architecture/	0
5030	EUNO	EUNO	https://coinmarketcap.com//currencies/euno/	0
5031	Mirrored iShares Silver Trust	mSLV	https://coinmarketcap.com//currencies/mirrored-ishares-silver-trust/	24.62
5032	QLC Chain	QLC	https://coinmarketcap.com//currencies/qlink/	0.09
5033	Gameswap	GSWAP	https://coinmarketcap.com//currencies/gameswap/	2.64
5034	Tokamak Network	TON	https://coinmarketcap.com//currencies/tokamak-network/	8.19
5035	Shadows	DOWS	https://coinmarketcap.com//currencies/shadows/	2.95
5036	Gleec	GLEEC	https://coinmarketcap.com//currencies/gleec/	1.04
5037	Mirrored Amazon	mAMZN	https://coinmarketcap.com//currencies/mirrored-amazon/	3213.74
5038	Dero	DERO	https://coinmarketcap.com//currencies/dero/	2.07
5039	Mirrored Invesco QQQ Trust	mQQQ	https://coinmarketcap.com//currencies/mirrored-invesco-qqq-trust/	328.69
5040	ArdCoin	ARDX	https://coinmarketcap.com//currencies/ardcoin/	0.08
5041	Mirrored Apple	mAAPL	https://coinmarketcap.com//currencies/mirrored-apple/	129.33
5042	Benchmark Protocol	MARK	https://coinmarketcap.com//currencies/benchmark-protocol/	1.32
5043	Kryll	KRL	https://coinmarketcap.com//currencies/kryll/	0.67
5044	ZEON	ZEON	https://coinmarketcap.com//currencies/zeon/	0.0007055
5045	Namecoin	NMC	https://coinmarketcap.com//currencies/namecoin/	1.4
5046	Mirrored Twitter	mTWTR	https://coinmarketcap.com//currencies/mirrored-twitter/	70.9
5047	Hashgard	GARD	https://coinmarketcap.com//currencies/hashgard/	0.0002061
5048	Cobak Token	CBK	https://coinmarketcap.com//currencies/cobak-token/	7.26
5049	Factom	FCT	https://coinmarketcap.com//currencies/factom/	2.09
5050	Prosper	PROS	https://coinmarketcap.com//currencies/prosper/	4.48
5051	Non-Fungible Yearn	NFY	https://coinmarketcap.com//currencies/non-fungible-yearn/	316.9
5052	UniLayer	LAYER	https://coinmarketcap.com//currencies/unilayer/	1
5053	TrueChain	TRUE	https://coinmarketcap.com//currencies/truechain/	0.2505
5054	AnRKey X	$ANRX	https://coinmarketcap.com//currencies/anrkey-x/	0.21
5055	Aryacoin	AYA	https://coinmarketcap.com//currencies/aryacoin/	0.12
5056	Metaverse ETP	ETP	https://coinmarketcap.com//currencies/metaverse/	0.25
5057	BitcoinHD	BHD	https://coinmarketcap.com//currencies/bitcoinhd/	3.25
5058	Mirrored Microsoft	mMSFT	https://coinmarketcap.com//currencies/mirrored-microsoft/	240.24
5059	STEM CELL COIN	SCC	https://coinmarketcap.com//currencies/stem-cell-coin/	0.06
5060	Juventus Fan Token	JUV	https://coinmarketcap.com//currencies/juventus-fan-token/	14.82
5061	Agrello	DLT	https://coinmarketcap.com//currencies/agrello-delta/	0.19
5062	BitcoinPoS	BPS	https://coinmarketcap.com//currencies/bitcoinpos/	4.69
5063	BLOCKv	VEE	https://coinmarketcap.com//currencies/blockv/	0.01
5064	ELYSIA	EL	https://coinmarketcap.com//currencies/elysia/	0.01
5065	Autonio	NIOX	https://coinmarketcap.com//currencies/autonio/	0.27
5066	Bird.Money	BIRD	https://coinmarketcap.com//currencies/bird-money/	219.7
5067	YOYOW	YOYOW	https://coinmarketcap.com//currencies/yoyow/	0.04
5068	Everex	EVX	https://coinmarketcap.com//currencies/everex/	0.84
5069	CUDOS	CUDOS	https://coinmarketcap.com//currencies/cudos/	0.05
5070	Centaur	CNTR	https://coinmarketcap.com//currencies/centaur/	0.02
5071	#MetaHash	MHC	https://coinmarketcap.com//currencies/metahash/	0.01
5072	XSGD	XSGD	https://coinmarketcap.com//currencies/xsgd/	0.76
5073	Mirrored Alibaba	mBABA	https://coinmarketcap.com//currencies/mirrored-alibaba/	231.72
5151	Credits	CS	https://coinmarketcap.com//currencies/credits/	0.06682
5074	Mirrored iShares Gold Trust	mIAU	https://coinmarketcap.com//currencies/mirrored-ishares-gold-trust/	17.06
5075	FOAM	FOAM	https://coinmarketcap.com//currencies/foam/	0.06
5076	Basis Cash	BAC	https://coinmarketcap.com//currencies/basis-cash/	0.32
5077	Cardstack	CARD	https://coinmarketcap.com//currencies/cardstack/	0.01
5078	SakeToken	SAKE	https://coinmarketcap.com//currencies/sake-token/	0.24
5079	Yield Optimization Platform & Protocol	YOP	https://coinmarketcap.com//currencies/yop/	1.82
5080	X-CASH	XCASH	https://coinmarketcap.com//currencies/x-cash/	0
5081	Ruff	RUFF	https://coinmarketcap.com//currencies/ruff/	0.02
5082	Nucleus Vision	NCASH	https://coinmarketcap.com//currencies/nucleus-vision/	0
5083	GameCredits	GAME	https://coinmarketcap.com//currencies/gamecredits/	0.13
5084	Mirrored United States Oil Fund	mUSO	https://coinmarketcap.com//currencies/mirrored-united-states-oil-fund/	44.29
5085	Kira Network	KEX	https://coinmarketcap.com//currencies/kira-network/	1.44
5086	SYNC Network	SYNC	https://coinmarketcap.com//currencies/sync-network/	0.16
5087	Props Token	PROPS	https://coinmarketcap.com//currencies/props/	0.06
5088	Unistake	UNISTAKE	https://coinmarketcap.com//currencies/unistake/	0.13
5089	Polyient Games Governance Token	PGT	https://coinmarketcap.com//currencies/polyient-games-governance-token/	1007.55
5090	Dentacoin	DCN	https://coinmarketcap.com//currencies/dentacoin/	0
5091	CONUN	CON	https://coinmarketcap.com//currencies/conun/	0.01
5092	DOS Network	DOS	https://coinmarketcap.com//currencies/dos-network/	0.13
5093	PRIZM	PZM	https://coinmarketcap.com//currencies/prizm/	0.01
5094	Viacoin	VIA	https://coinmarketcap.com//currencies/viacoin/	0.75
5095	PolySwarm	NCT	https://coinmarketcap.com//currencies/polyswarm/	0.01
5096	carVertical	CV	https://coinmarketcap.com//currencies/carvertical/	0
5097	Trittium	TRTT	https://coinmarketcap.com//currencies/trittium/	0.13
5098	Venus LINK	vLINK	https://coinmarketcap.com//currencies/venus-link/	0.56
5099	Viberate	VIB	https://coinmarketcap.com//currencies/viberate/	0.09
5100	NerveNetwork	NVT	https://coinmarketcap.com//currencies/nervenetwork/	0.06
5101	Permission Coin	ASK	https://coinmarketcap.com//currencies/permission-coin/	0
5102	DeepBrain Chain	DBC	https://coinmarketcap.com//currencies/deepbrain-chain/	0.01
5103	Pickle Finance	PICKLE	https://coinmarketcap.com//currencies/pickle-finance/	12.85
5104	DATA	DTA	https://coinmarketcap.com//currencies/data/	0
5105	THEKEY	TKY	https://coinmarketcap.com//currencies/thekey/	0
5106	Venus LTC	vLTC	https://coinmarketcap.com//currencies/venus-ltc/	4.04
5107	eBoost	EBST	https://coinmarketcap.com//currencies/eboostcoin/	0.17
5108	Monetha	MTH	https://coinmarketcap.com//currencies/monetha/	0.04
5109	Zano	ZANO	https://coinmarketcap.com//currencies/zano/	1.58
5110	Emirex Token	EMRX	https://coinmarketcap.com//currencies/emirex-token/	0.61
5111	Arcblock	ABT	https://coinmarketcap.com//currencies/arcblock/	0.17
5112	BitForex Token	BF	https://coinmarketcap.com//currencies/bitforex-token/	0
5113	Leverj Gluon	L2	https://coinmarketcap.com//currencies/leverj-gluon/	0.06
5114	Public Mint	MINT	https://coinmarketcap.com//currencies/public-mint/	0.94
5115	King DAG	KDAG	https://coinmarketcap.com//currencies/king-dag/	0.3
5116	Folgory Coin	FLG	https://coinmarketcap.com//currencies/folgory-coin/	0.94
5117	SOMESING	SSX	https://coinmarketcap.com//currencies/somesing/	0.11
5118	Doki Doki Finance	DOKI	https://coinmarketcap.com//currencies/doki-doki-finance/	323.92
5119	SingularDTV	SNGLS	https://coinmarketcap.com//currencies/singulardtv/	0.03
5120	CoinEx Token	CET	https://coinmarketcap.com//currencies/coinex-token/	0.02
5121	Bithao	BHAO	https://coinmarketcap.com//currencies/bithao/	0.35
5122	SONM	SNM	https://coinmarketcap.com//currencies/sonm/	0.04
5123	SpaceChain	SPC	https://coinmarketcap.com//currencies/spacechain/	0.05
5124	Nestree	EGG	https://coinmarketcap.com//currencies/nestree/	0.01
5125	Ubiq	UBQ	https://coinmarketcap.com//currencies/ubiq/	0.36
5126	Mirrored ProShares VIX	mVIXY	https://coinmarketcap.com//currencies/mirrored-proshares-vix-short-term-futures-etf/	10.9
5127	GeoDB	GEO	https://coinmarketcap.com//currencies/geodb/	0.71
5128	VIDY	VIDY	https://coinmarketcap.com//currencies/vidy/	0
5129	VestChain	VEST	https://coinmarketcap.com//currencies/vestchain/	0
5130	Tachyon Protocol	IPX	https://coinmarketcap.com//currencies/tachyon-protocol/	0.06
5131	Achain	ACT	https://coinmarketcap.com//currencies/achain/	0.02
5132	USDJ	USDJ	https://coinmarketcap.com//currencies/usdj/	1.03
5133	Idle	IDLE	https://coinmarketcap.com//currencies/idle/	12.36
5134	Atomic Wallet Coin	AWC	https://coinmarketcap.com//currencies/atomic-wallet-coin/	1.41
5135	S4FE	S4F	https://coinmarketcap.com//currencies/s4fe/	0.02
5136	Gem Exchange And Trading	GXT	https://coinmarketcap.com//currencies/gem-exchange-and-trading/	0.34
5137	xRhodium	XRC	https://coinmarketcap.com//currencies/xrhodium/	11.96
5138	Credits	CS	https://coinmarketcap.com//currencies/credits/	0.06
5139	Juggernaut	JGN	https://coinmarketcap.com//currencies/juggernaut/	1.45
5140	SnowSwap	SNOW	https://coinmarketcap.com//currencies/snowswap/	48.75
5141	Hedget	HGET	https://coinmarketcap.com//currencies/hedget/	8.43
5142	Swerve	SWRV	https://coinmarketcap.com//currencies/swerve/	1.29
5143	CONTRACOIN	CTCN	https://coinmarketcap.com//currencies/contracoin/	0.46
5144	MoonSwap	MOON	https://coinmarketcap.com//currencies/moonswap/	0.8766
5145	SEEN	SEEN	https://coinmarketcap.com//currencies/seen/	19.56
5146	Dracula Token	DRC	https://coinmarketcap.com//currencies/dracula-token/	1.01
5147	ProximaX	XPX	https://coinmarketcap.com//currencies/proximax/	0.002312
5148	CargoX	CXO	https://coinmarketcap.com//currencies/cargox/	0.08511
5149	Genaro Network	GNX	https://coinmarketcap.com//currencies/genaro-network/	0.05157
5150	Falcon Project	FNT	https://coinmarketcap.com//currencies/falcon-project/	0.003498
5152	TrueFeedBack	TFB	https://coinmarketcap.com//currencies/truefeedback/	0.005918
5153	IQeon	IQN	https://coinmarketcap.com//currencies/iqeon/	2.54
5154	Restart Energy MWAT	MWAT	https://coinmarketcap.com//currencies/restart-energy-mwat/	0.03
5155	Hydro Protocol	HOT	https://coinmarketcap.com//currencies/hydro-protocol/	0.02
5156	Fuse Network	FUSE	https://coinmarketcap.com//currencies/fuse-network/	0.25
5157	Atletico De Madrid Fan Token	ATM	https://coinmarketcap.com//currencies/atletico-de-madrid-fan-token/	11.02
5158	BaaSid	BAAS	https://coinmarketcap.com//currencies/baasid/	0
5159	LiquidApps	DAPP	https://coinmarketcap.com//currencies/liquid-apps/	0.02
5160	Poolz Finance	POOLZ	https://coinmarketcap.com//currencies/poolz-finance/	19.79
5161	Jupiter	JUP	https://coinmarketcap.com//currencies/jupiter/	0.11
5162	Berry Data	BRY	https://coinmarketcap.com//currencies/berry-data/	6.7
5163	Unido EP	UDO	https://coinmarketcap.com//currencies/unido/	0.57
5164	Abyss	ABYSS	https://coinmarketcap.com//currencies/abyss/	0.06
5165	BHPCoin	BHP	https://coinmarketcap.com//currencies/bhp-coin/	0.66
5166	Xensor	XSR	https://coinmarketcap.com//currencies/xensor/	0
5167	Beowulf	BWF	https://coinmarketcap.com//currencies/beowulf/	0.01
5168	Blockzero Labs	XIO	https://coinmarketcap.com//currencies/blockzerolabs/	0.4
5169	Phore	PHR	https://coinmarketcap.com//currencies/phore/	0.56
5170	ARMOR	ARMOR	https://coinmarketcap.com//currencies/armor/	0.63
5171	TrustVerse	TRV	https://coinmarketcap.com//currencies/trustverse/	0.03
5172	Geeq	GEEQ	https://coinmarketcap.com//currencies/geeq/	1.54
5173	Peercoin	PPC	https://coinmarketcap.com//currencies/peercoin/	0.48
5174	Dawn Protocol	DAWN	https://coinmarketcap.com//currencies/dawn-protocol/	0.28
5175	AS Roma Fan Token	ASR	https://coinmarketcap.com//currencies/as-roma-fan-token/	10.45
5176	Circuits of Value	COVAL	https://coinmarketcap.com//currencies/circuits-of-value/	0.01
5177	BIKI	BIKI	https://coinmarketcap.com//currencies/biki/	0.04
5178	Aluna.Social	ALN	https://coinmarketcap.com//currencies/aluna-social/	1.16
5179	Casino Betting Coin	CBC	https://coinmarketcap.com//currencies/casino-betting-coin/	0.08
5180	Alchemy Pay	ACH	https://coinmarketcap.com//currencies/alchemy-pay/	0.01
5181	Emercoin	EMC	https://coinmarketcap.com//currencies/emercoin/	0.26
5182	reflect.finance	RFI	https://coinmarketcap.com//currencies/reflect-finance/	1.29
5183	KeeperDAO	ROOK	https://coinmarketcap.com//currencies/keeperdao/	335.31
5184	LinkEye	LET	https://coinmarketcap.com//currencies/linkeye/	0.01
5185	Waifu Token	WAIF	https://coinmarketcap.com//currencies/waifu-token/	0.01
5186	PolkaBridge	PBR	https://coinmarketcap.com//currencies/polkabridge/	0.38
5187	Parachute	PAR	https://coinmarketcap.com//currencies/parachute/	0.02
5188	High Performance Blockchain	HPB	https://coinmarketcap.com//currencies/high-performance-blockchain/	0.17
5189	Defis Network	DFS	https://coinmarketcap.com//currencies/defis-network/	32.72
5190	SENSO	SENSO	https://coinmarketcap.com//currencies/senso/	0.98
5191	Banano	BAN	https://coinmarketcap.com//currencies/banano/	0.01
5192	Santiment Network Token	SAN	https://coinmarketcap.com//currencies/santiment/	0.19
5193	Kcash	KCASH	https://coinmarketcap.com//currencies/kcash/	0.03
5194	OG Fan Token	OG	https://coinmarketcap.com//currencies/og-fan-token/	9.37
5195	OIN Finance	OIN	https://coinmarketcap.com//currencies/oin-finance/	0.47
5196	Nord Finance	NORD	https://coinmarketcap.com//currencies/nord-finance/	13.1
5197	Aragon Court	ANJ	https://coinmarketcap.com//currencies/aragon-court/	0.1
5198	AMEPAY	AME	https://coinmarketcap.com//currencies/amepay/	0.05
5199	Venus DOT	vDOT	https://coinmarketcap.com//currencies/venus-dot/	0.7
5200	XYO	XYO	https://coinmarketcap.com//currencies/xyo/	0
5201	Furucombo	COMBO	https://coinmarketcap.com//currencies/furucombo/	2.52
5202	Zynecoin	ZYN	https://coinmarketcap.com//currencies/zynecoin/	0.52
5203	FLETA	FLETA	https://coinmarketcap.com//currencies/fleta/	0.01
5204	NIX	NIX	https://coinmarketcap.com//currencies/nix/	0.23
5205	SwftCoin	SWFTC	https://coinmarketcap.com//currencies/swftcoin/	0
5206	PLATINCOIN	PLC	https://coinmarketcap.com//currencies/platincoin/	2.29
5207	SmartMesh	SMT	https://coinmarketcap.com//currencies/smartmesh/	0.01
5208	Mettalex	MTLX	https://coinmarketcap.com//currencies/mettalex/	9.96
5209	Darwinia Commitment Token	KTON	https://coinmarketcap.com//currencies/darwinia-commitment-token/	232.01
5210	Callisto Network	CLO	https://coinmarketcap.com//currencies/callisto-network/	0
5211	Monavale	MONA	https://coinmarketcap.com//currencies/monavale/	1845.6
5212	DePay	DEPAY	https://coinmarketcap.com//currencies/depay/	3.62
5213	PlatonCoin	PLTC	https://coinmarketcap.com//currencies/platoncoin/	0.43
5214	DigitalNote	XDN	https://coinmarketcap.com//currencies/digitalnote/	0
5215	EOS Force	EOSC	https://coinmarketcap.com//currencies/eos-force/	0.01
5216	Realio Network	RIO	https://coinmarketcap.com//currencies/realio-network/	1.65
5217	U Network	UUU	https://coinmarketcap.com//currencies/u-network/	0
5218	DeFi Yield Protocol	DYP	https://coinmarketcap.com//currencies/defi-yield-protocol/	3.27
5219	Monolith	TKN	https://coinmarketcap.com//currencies/monolith/	0.32
5220	APYSwap	APYS	https://coinmarketcap.com//currencies/apyswap/	1.9
5221	IoT Chain	ITC	https://coinmarketcap.com//currencies/iot-chain/	0.13
5222	APIX	APIX	https://coinmarketcap.com//currencies/apix/	0.09
5223	PressOne	PRS	https://coinmarketcap.com//currencies/pressone/	0.04
5224	DragonVein	DVC	https://coinmarketcap.com//currencies/dragonvein/	0.07
5225	Name Changing Token	NCT	https://coinmarketcap.com//currencies/name-changing-token/	0.28
5226	Offshift	XFT	https://coinmarketcap.com//currencies/offshift/	4.05
5227	Archer DAO Governance Token	ARCH	https://coinmarketcap.com//currencies/archer-dao-governance-token/	1.3
5228	SharedStake	SGT	https://coinmarketcap.com//currencies/sharedstake/	70.34
5229	Valor Token	VALOR	https://coinmarketcap.com//currencies/valor-token/	0.52
5230	Levolution	LEVL	https://coinmarketcap.com//currencies/levolution/	0.13
5231	UniMex Network	UMX	https://coinmarketcap.com//currencies/unimex-network/	1.86
5232	YF Link	YFL	https://coinmarketcap.com//currencies/yflink/	204.92
5233	TERA	TERA	https://coinmarketcap.com//currencies/tera/	0.01
5234	Hakka.Finance	HAKKA	https://coinmarketcap.com//currencies/hakka-finance/	0.07
5235	SIX	SIX	https://coinmarketcap.com//currencies/six/	0.04
5236	Dev Protocol	DEV	https://coinmarketcap.com//currencies/dev-protocol/	9.43
5237	ROOBEE	ROOBEE	https://coinmarketcap.com//currencies/roobee/	0
5238	Airbloc	ABL	https://coinmarketcap.com//currencies/airbloc/	0.04
5239	Anchor	ANCT	https://coinmarketcap.com//currencies/anchor/	0.8
5240	Tap	XTP	https://coinmarketcap.com//currencies/tap/	0.01
5241	BABB	BAX	https://coinmarketcap.com//currencies/babb/	0
5242	ZBG Token	ZT	https://coinmarketcap.com//currencies/zbg-token/	0.04
5243	FNB Protocol	FNB	https://coinmarketcap.com//currencies/fnb-protocol/	0
5244	yieldwatch	WATCH	https://coinmarketcap.com//currencies/yieldwatch/	0.9731
5245	Multi Channel Influencer Creater Cloud Funding Platform	MCI	https://coinmarketcap.com//currencies/multi-channel-influencer-creater-cloud-funding-platform/	0.01764
5246	Eminer	EM	https://coinmarketcap.com//currencies/eminer/	0.006899
5247	FIBOS	FO	https://coinmarketcap.com//currencies/fibos/	0.009319
5248	apM Coin	APM	https://coinmarketcap.com//currencies/apm-coin/	0.02744
5249	MahaDAO	MAHA	https://coinmarketcap.com//currencies/mahadao/	8.54
5250	ASKO	ASKO	https://coinmarketcap.com//currencies/askobar-network/	0.08405
5251	Validity	VAL	https://coinmarketcap.com//currencies/validity/	2.32
5252	DAOstack	GEN	https://coinmarketcap.com//currencies/daostack/	0.2019
5253	Strong	STRONG	https://coinmarketcap.com//currencies/strong/	70.81
5254	BeatzCoin	BTZC	https://coinmarketcap.com//currencies/beatzcoin/	0
5255	Crypto Village Accelerator	CVA	https://coinmarketcap.com//currencies/crypto-village-accelerator/	0.1
5256	FLO	FLO	https://coinmarketcap.com//currencies/flo/	0.06
5257	Odyssey	OCN	https://coinmarketcap.com//currencies/odyssey/	0
5258	FinNexus	FNX	https://coinmarketcap.com//currencies/finnexus/	0.37
5259	DMScript	DMST	https://coinmarketcap.com//currencies/dmscript/	0.17
5260	Royale Finance	ROYA	https://coinmarketcap.com//currencies/royale-finance/	0.95
5261	Unitrade	TRADE	https://coinmarketcap.com//currencies/unitrade/	0.35
5262	Time New Bank	TNB	https://coinmarketcap.com//currencies/time-new-bank/	0
5263	Seele-N	SEELE	https://coinmarketcap.com//currencies/seele/	0.01
5264	Aitra	AITRA	https://coinmarketcap.com//currencies/aitra/	6.33
5265	Atlas Protocol	ATP	https://coinmarketcap.com//currencies/atlas-protocol/	0
5266	Zel	ZEL	https://coinmarketcap.com//currencies/zel/	0.08
5267	Bibox Token	BIX	https://coinmarketcap.com//currencies/bibox-token/	0.11
5268	TriumphX	TRIX	https://coinmarketcap.com//currencies/triumphx/	0.75
5269	XMax	XMX	https://coinmarketcap.com//currencies/xmax/	0
5270	Insured Finance	INFI	https://coinmarketcap.com//currencies/insured-finance/	0.65
5271	bAlpha	BALPHA	https://coinmarketcap.com//currencies/balpha/	1591.62
5272	Fyooz	FYZ	https://coinmarketcap.com//currencies/fyooz/	1.53
5273	Node Runners	NDR	https://coinmarketcap.com//currencies/node-runners/	395.9
5274	Hyve	HYVE	https://coinmarketcap.com//currencies/hyve/	0.36
5275	VIBE	VIBE	https://coinmarketcap.com//currencies/vibe/	0.03
5276	Smart MFG	MFG	https://coinmarketcap.com//currencies/smart-mfg/	0.03
5277	Lead Wallet	LEAD	https://coinmarketcap.com//currencies/lead-wallet/	0.02
5278	smARTOFGIVING	AOG	https://coinmarketcap.com//currencies/smartofgiving/	0.12
5279	Tokenomy	TEN	https://coinmarketcap.com//currencies/tokenomy/	0.04
5280	FREE Coin	FREE	https://coinmarketcap.com//currencies/free-coin/	0
5281	Pluton	PLU	https://coinmarketcap.com//currencies/pluton/	10.41
5282	Insureum	ISR	https://coinmarketcap.com//currencies/insureum/	0.04
5283	Pundi X NEM	NPXSXEM	https://coinmarketcap.com//currencies/pundi-x-nem/	0
5284	Waves Enterprise	WEST	https://coinmarketcap.com//currencies/waves-enterprise/	0.21
5285	Nsure.Network	NSURE	https://coinmarketcap.com//currencies/nsure-network/	1.54
5286	SmartCash	SMART	https://coinmarketcap.com//currencies/smartcash/	0.01
5287	Falconswap	FSW	https://coinmarketcap.com//currencies/fsw-token/	0.23
5288	PlayFuel	PLF	https://coinmarketcap.com//currencies/playfuel/	0.02
5289	NaPoleonX	NPX	https://coinmarketcap.com//currencies/napoleonx/	0.34
5290	Primecoin	XPM	https://coinmarketcap.com//currencies/primecoin/	0.24
5291	Particl	PART	https://coinmarketcap.com//currencies/particl/	0.88
5292	Tokes	TKS	https://coinmarketcap.com//currencies/tokes/	0.04
5293	Open Platform	OPEN	https://coinmarketcap.com//currencies/open-platform/	0.01
5294	UCA Coin	UCA	https://coinmarketcap.com//currencies/uca-coin/	0.01
5295	Lightning Bitcoin	LBTC	https://coinmarketcap.com//currencies/lightning-bitcoin/	2.17
5296	Zenfuse	ZEFU	https://coinmarketcap.com//currencies/zenfuse/	0.15
5297	Chrono.tech	TIME	https://coinmarketcap.com//currencies/chrono-tech/	11.72
5298	Swapcoinz	SPAZ	https://coinmarketcap.com//currencies/swapcoinz/	0.78
5299	Burst	BURST	https://coinmarketcap.com//currencies/burst/	0
5300	Aeon	AEON	https://coinmarketcap.com//currencies/aeon/	0.52
5301	Azuki	AZUKI	https://coinmarketcap.com//currencies/azuki/	0.95
5302	Monero Classic	XMC	https://coinmarketcap.com//currencies/monero-classic/	0.42
5303	STATERA	STA	https://coinmarketcap.com//currencies/statera/	0.1
5304	Peculium	PCL	https://coinmarketcap.com//currencies/peculium/	0
5305	Caspian	CSP	https://coinmarketcap.com//currencies/caspian/	0.02
5306	WeShow Token	WET	https://coinmarketcap.com//currencies/weshow-token/	0.01
5307	Enecuum	ENQ	https://coinmarketcap.com//currencies/enecuum/	0.05
5308	Evedo	EVED	https://coinmarketcap.com//currencies/evedo/	0.55
5309	Precium	PCM	https://coinmarketcap.com//currencies/precium/	0.01
5310	HitChain	HIT	https://coinmarketcap.com//currencies/hitchain/	0
5311	MultiVAC	MTV	https://coinmarketcap.com//currencies/multivac/	0
5312	QuickX Protocol	QCX	https://coinmarketcap.com//currencies/quickx-protocol/	0.02
5313	Grid+	GRID	https://coinmarketcap.com//currencies/grid/	0.2
5314	Wings	WINGS	https://coinmarketcap.com//currencies/wings/	0.08
5315	DEAPcoin	DEP	https://coinmarketcap.com//currencies/deapcoin/	0.01
5316	PlotX	PLOT	https://coinmarketcap.com//currencies/plotx/	0.22
5317	PieDAO DOUGH v2	DOUGH	https://coinmarketcap.com//currencies/piedao-dough-v2/	1.14
5318	GoCrypto Token	GOC	https://coinmarketcap.com//currencies/gocrypto-token/	0.04
5319	Sylo	SYLO	https://coinmarketcap.com//currencies/sylo/	0
5320	QuadrantProtocol	EQUAD	https://coinmarketcap.com//currencies/quadrantprotocol/	0.02
5321	Team Heretics Fan Token	TH	https://coinmarketcap.com//currencies/team-heretics-fan-token/	17.89
5322	Project WITH	WIKEN	https://coinmarketcap.com//currencies/project-with/	0.02
5323	Growth DeFi	GRO	https://coinmarketcap.com//currencies/growthdefi/	21.19
5324	Mysterium	MYST	https://coinmarketcap.com//currencies/mysterium/	0.38
5325	Xaya	CHI	https://coinmarketcap.com//currencies/xaya/	0.16
5326	Auctus	AUC	https://coinmarketcap.com//currencies/auctus/	0.23
5327	Feathercoin	FTC	https://coinmarketcap.com//currencies/feathercoin/	0.03
5328	Covesting	COV	https://coinmarketcap.com//currencies/covesting/	0.41
5329	MicroBitcoin	MBC	https://coinmarketcap.com//currencies/microbitcoin/	0
5330	LikeCoin	LIKE	https://coinmarketcap.com//currencies/likecoin/	0.01
5331	Pillar	PLR	https://coinmarketcap.com//currencies/pillar/	0.03
5332	BTSE	BTSE	https://coinmarketcap.com//currencies/btse/	1.66
5333	Origin Dollar	OUSD	https://coinmarketcap.com//currencies/origin-dollar/	0.99
5334	Ethverse	ETHV	https://coinmarketcap.com//currencies/ethverse/	0.51
5335	Matrix AI Network	MAN	https://coinmarketcap.com//currencies/matrix-ai-network/	0.03
5336	HAPI	HAPI	https://coinmarketcap.com//currencies/hapi-one/	91.19
5337	Lamden	TAU	https://coinmarketcap.com//currencies/lamden/	0.05
5338	Gulden	NLG	https://coinmarketcap.com//currencies/gulden/	0.01
5339	e-Gulden	EFL	https://coinmarketcap.com//currencies/e-gulden/	0.42
5340	VNX Exchange	VNXLU	https://coinmarketcap.com//currencies/vnx-exchange/	0.25
5341	ScPrime	SCP	https://coinmarketcap.com//currencies/scprime/	0.21
5342	BitMart Token	BMX	https://coinmarketcap.com//currencies/bitmart-token/	0.04
5343	OneLedger	OLT	https://coinmarketcap.com//currencies/oneledger/	0.02
5344	Orient Walt	HTDF	https://coinmarketcap.com//currencies/orient-walt/	0.07446
5345	Jobchain	JOB	https://coinmarketcap.com//currencies/jobchain/	0.001097
5346	Myriad	XMY	https://coinmarketcap.com//currencies/myriad/	0.003892
5347	Cryptocean	CRON	https://coinmarketcap.com//currencies/cryptocean/	0.8401
5348	Kambria	KAT	https://coinmarketcap.com//currencies/kambria/	0.002953
5349	CWV Chain	CWV	https://coinmarketcap.com//currencies/cwv-chain/	0.001932
5350	Skrumble Network	SKM	https://coinmarketcap.com//currencies/skrumble-network/	0.006688
5351	Idena	IDNA	https://coinmarketcap.com//currencies/idena/	0.1763
5352	TurtleCoin	TRTL	https://coinmarketcap.com//currencies/turtlecoin/	6.985e-05
5353	IG Gold	IGG	https://coinmarketcap.com//currencies/ig-gold/	0.0007588
5354	PotCoin	POT	https://coinmarketcap.com//currencies/potcoin/	0.03
5355	Litecoin Cash	LCC	https://coinmarketcap.com//currencies/litecoin-cash/	0.01
5356	EDUCare	EKT	https://coinmarketcap.com//currencies/educare/	0.01
5357	Fatcoin	FAT	https://coinmarketcap.com//currencies/fatcoin/	0.06
5358	SOAR.FI	SOAR	https://coinmarketcap.com//currencies/soar-fi/	0.75
5359	Blockchain Certified Data Token	BCDT	https://coinmarketcap.com//currencies/blockchain-certified-data-token/	0.17
5360	Safe	SAFE	https://coinmarketcap.com//currencies/safe/	0.31
5361	Opacity	OPCT	https://coinmarketcap.com//currencies/opacity/	0.08
5362	Penta	PNT	https://coinmarketcap.com//currencies/penta/	0
5363	Game.com	GTC	https://coinmarketcap.com//currencies/game/	0.01
5364	Router Protocol	ROUTE	https://coinmarketcap.com//currencies/router-protocol/	3.73
5365	SmartCredit Token	SMARTCREDIT	https://coinmarketcap.com//currencies/smartcredit-token/	4.73
5366	Stealth	XST	https://coinmarketcap.com//currencies/stealth/	0.16
5367	Chonk	CHONK	https://coinmarketcap.com//currencies/chonk/	159.01
5368	BIDR	BIDR	https://coinmarketcap.com//currencies/binance-idr/	0
5369	SunContract	SNC	https://coinmarketcap.com//currencies/suncontract/	0.05
5370	HyperDAO	HDAO	https://coinmarketcap.com//currencies/hyperdao/	0.02
5371	ZIMBOCASH	ZASH	https://coinmarketcap.com//currencies/zimbocash/	0.01
5372	Apollon Limassol	APL	https://coinmarketcap.com//currencies/apollon-limassol/	24.03
5373	Urus	URUS	https://coinmarketcap.com//currencies/urus/	12.65
5374	DEX	DEX	https://coinmarketcap.com//currencies/dex/	0.03
5375	Tripio	TRIO	https://coinmarketcap.com//currencies/tripio/	0
5376	ChatCoin	CHAT	https://coinmarketcap.com//currencies/chatcoin/	0.01
5377	YOU COIN	YOU	https://coinmarketcap.com//currencies/you-coin/	0.01
5378	Iconic Token	ICNQ	https://coinmarketcap.com//currencies/iconic-token/	0.71
5379	MAP Protocol	MAP	https://coinmarketcap.com//currencies/map-protocol/	0.02
5380	HTMLCOIN	HTML	https://coinmarketcap.com//currencies/html-coin/	0
5381	HollyGold	HGOLD	https://coinmarketcap.com//currencies/hollygold/	2.23
5382	Add.xyz	PLT	https://coinmarketcap.com//currencies/add-xyz/	1
5383	Pepemon Pepeballs	PPBLZ	https://coinmarketcap.com//currencies/pepemon-pepeballs/	402.01
5384	Litex	LXT	https://coinmarketcap.com//currencies/litex/	0
5385	Lido DAO Token	LDO	https://coinmarketcap.com//currencies/lido-dao/	0.63
5386	Venus DAI	vDAI	https://coinmarketcap.com//currencies/venus-dai/	0.02
5387	Mobius	MOBI	https://coinmarketcap.com//currencies/mobius/	0.01
5388	Filecash	FIC	https://coinmarketcap.com//currencies/filecash/	0.21
5389	Blocery	BLY	https://coinmarketcap.com//currencies/blocery/	0.1
5390	Cred	LBA	https://coinmarketcap.com//currencies/libra-credit/	0.01
5391	CPChain	CPC	https://coinmarketcap.com//currencies/cpchain/	0.01
5392	Origo	OGO	https://coinmarketcap.com//currencies/origo/	0.01
5393	Gourmet Galaxy	GUM	https://coinmarketcap.com//currencies/gourmet-galaxy/	1.6
5394	Valobit	VBIT	https://coinmarketcap.com//currencies/valobit/	0.01
5395	Raven Protocol	RAVEN	https://coinmarketcap.com//currencies/raven-protocol/	0
5396	Insights Network	INSTAR	https://coinmarketcap.com//currencies/insights-network/	0.03
5397	Global Social Chain	GSC	https://coinmarketcap.com//currencies/global-social-chain/	0.01
5398	BOLT	BOLT	https://coinmarketcap.com//currencies/bolt/	0.01
5399	XMON	XMON	https://coinmarketcap.com//currencies/xmon/	3494.86
5400	ILCOIN	ILC	https://coinmarketcap.com//currencies/ilcoin/	0.01
5401	42-coin	42	https://coinmarketcap.com//currencies/42-coin/	122325.69
5402	Vidya	VIDYA	https://coinmarketcap.com//currencies/vidya/	0.14
5403	Jarvis Network	JRT	https://coinmarketcap.com//currencies/jarvis-network/	0.18
5404	Lunes	LUNES	https://coinmarketcap.com//currencies/lunes/	0.03
5405	Open Governance Token	OPEN	https://coinmarketcap.com//currencies/open-governance-token/	1.4
5406	VeriDocGlobal	VDG	https://coinmarketcap.com//currencies/veridocglobal/	0
5407	Remme	REM	https://coinmarketcap.com//currencies/remme/	0.01
5408	SolarCoin	SLR	https://coinmarketcap.com//currencies/solarcoin/	0.08
5409	OTOCASH	OTO	https://coinmarketcap.com//currencies/otocash/	0.14
5410	Swarm	SWM	https://coinmarketcap.com//currencies/swarm-network/	0.06
5411	PANTHEON X	XPN	https://coinmarketcap.com//currencies/pantheon-x/	0.01
5412	Indexed Finance	NDX	https://coinmarketcap.com//currencies/indexed-finance/	5.77
5413	Scala	XLA	https://coinmarketcap.com//currencies/scala/	0
5414	INT	INT	https://coinmarketcap.com//currencies/int-chain/	0.01
5415	Earneo	RNO	https://coinmarketcap.com//currencies/earneo/	0.02
5416	Stake DAO	SDT	https://coinmarketcap.com//currencies/stake-dao/	6.85
5417	DECOIN	DTEP	https://coinmarketcap.com//currencies/decoin/	0.09
5418	MIR COIN	MIR	https://coinmarketcap.com//currencies/mir-coin/	0.01
5419	Internxt	INXT	https://coinmarketcap.com//currencies/internxt/	7.63
5420	Footballcoin	XFC	https://coinmarketcap.com//currencies/footballcoin/	0.01
5421	Content Neutrality Network	CNN	https://coinmarketcap.com//currencies/content-neutrality-network/	0
5422	Digitex	DGTX	https://coinmarketcap.com//currencies/digitex/	0.01
5423	Nyzo	NYZO	https://coinmarketcap.com//currencies/nyzo/	0.4
5424	Panda Yield	BBOO	https://coinmarketcap.com//currencies/panda-yield/	5.07
5425	DeFiner	FIN	https://coinmarketcap.com//currencies/definer/	0.42
5426	SaTT	SATT	https://coinmarketcap.com//currencies/satt/	0
5427	GridCoin	GRC	https://coinmarketcap.com//currencies/gridcoin/	0.01
5428	Cubiex	CBIX	https://coinmarketcap.com//currencies/cubiex/	0.07
5429	FairCoin	FAIR	https://coinmarketcap.com//currencies/faircoin/	0.09
5430	FedoraCoin	TIPS	https://coinmarketcap.com//currencies/fedoracoin/	0
5431	BIZZCOIN	BIZZ	https://coinmarketcap.com//currencies/bizzcoin/	0.63
5432	SignatureChain	SIGN	https://coinmarketcap.com//currencies/signature-chain/	0.01
5433	Vexanium	VEX	https://coinmarketcap.com//currencies/vexanium/	0.01
5434	Polis	POLIS	https://coinmarketcap.com//currencies/polis/	0.46
5435	WeOwn	CHX	https://coinmarketcap.com//currencies/we-own/	0.05
5436	TENT	TENT	https://coinmarketcap.com//currencies/tent/	0.14
5437	Beaxy	BXY	https://coinmarketcap.com//currencies/beaxy/	0.02
5438	YGGDRASH	YEED	https://coinmarketcap.com//currencies/yeed/	0
5439	NextDAO	NAX	https://coinmarketcap.com//currencies/nextdao/	0
5440	Swace	SWACE	https://coinmarketcap.com//currencies/swace/	0.01
5441	Acute Angle Cloud	AAC	https://coinmarketcap.com//currencies/acute-angle-cloud/	0.02
5442	BitGreen	BITG	https://coinmarketcap.com//currencies/bitgreen/	0.37
5443	Cryptopay	CPAY	https://coinmarketcap.com//currencies/cryptopay/	0.06
5444	1World	1WO	https://coinmarketcap.com//currencies/1world/	0.1184
5445	Linker Coin	LNC	https://coinmarketcap.com//currencies/linker-coin/	0.03895
5446	Pantos	PAN	https://coinmarketcap.com//currencies/pantos/	0.1542
5447	Chi Gastoken	CHI	https://coinmarketcap.com//currencies/chi-gastoken/	5.08
5448	OceanEx Token	OCE	https://coinmarketcap.com//currencies/oceanex-token/	0.002424
5449	bitCNY	BITCNY	https://coinmarketcap.com//currencies/bitcny/	0.1533
5450	Alpaca Finance	ALPACA	https://coinmarketcap.com//currencies/alpaca-finance/	0.6516
5451	fyeth.finance	YETH	https://coinmarketcap.com//currencies/fyeth-finance/	7.72
5452	ERC20	ERC20	https://coinmarketcap.com//currencies/erc20/	0.00378
5453	Digix Gold Token	DGX	https://coinmarketcap.com//currencies/digix-gold-token/	55.53
5454	Pizza	PIZZA	https://coinmarketcap.com//currencies/pizza/	0.14
5455	Kebab Token	KEBAB	https://coinmarketcap.com//currencies/kebab-token/	3.91
5456	GAMB	GMB	https://coinmarketcap.com//currencies/gamb/	0
5457	ContentBox	BOX	https://coinmarketcap.com//currencies/contentbox/	0
5458	Crown	CRW	https://coinmarketcap.com//currencies/crown/	0.16
5459	Thisoption	TONS	https://coinmarketcap.com//currencies/thisoption/	0.75
5460	Bittwatt	BWT	https://coinmarketcap.com//currencies/bittwatt/	0.01
5461	UGAS	UGAS	https://coinmarketcap.com//currencies/ugas/	0.02
5462	Netbox Coin	NBX	https://coinmarketcap.com//currencies/netbox-coin/	0.05
5463	Xiotri	XIOT	https://coinmarketcap.com//currencies/xiotri/	914.47
5464	adbank	ADB	https://coinmarketcap.com//currencies/adbank/	0
5465	Carbon	CRBN	https://coinmarketcap.com//currencies/carbon/	0.16
5466	Machi X	MCX	https://coinmarketcap.com//currencies/machix/	0.01
5467	CHADS VC	CHADS	https://coinmarketcap.com//currencies/chads-vc/	0.1
5468	Small Love Potion	SLP	https://coinmarketcap.com//currencies/small-love-potion/	0.09
5469	Ideaology	IDEA	https://coinmarketcap.com//currencies/ideaology/	0.32
5470	MASQ	MASQ	https://coinmarketcap.com//currencies/masq/	0.26
5471	BlackCoin	BLK	https://coinmarketcap.com//currencies/blackcoin/	0.06
5472	Cajutel	CAJ	https://coinmarketcap.com//currencies/cajutel/	2.87
5473	Essentia	ESS	https://coinmarketcap.com//currencies/essentia/	0
5474	Club Atletico Independiente	CAI	https://coinmarketcap.com//currencies/club-atletico-independiente/	7.01
5475	TronEuropeRewardCoin	TERC	https://coinmarketcap.com//currencies/troneuroperewardcoin/	0.02
5476	AIDUS TOKEN	AIDUS	https://coinmarketcap.com//currencies/aidus-token/	0.01
5477	ALLY	ALY	https://coinmarketcap.com//currencies/ally/	0
5478	QunQun	QUN	https://coinmarketcap.com//currencies/qunqun/	0.01
5479	YEE	YEE	https://coinmarketcap.com//currencies/yee/	0
5480	Datum	DAT	https://coinmarketcap.com//currencies/datum/	0
5481	Peerplays	PPY	https://coinmarketcap.com//currencies/peerplays-ppy/	0.84
5482	REPO	REPO	https://coinmarketcap.com//currencies/repo/	0.17
5483	HalalChain	HLC	https://coinmarketcap.com//currencies/halalchain/	0.01
5484	Rupiah Token	IDRT	https://coinmarketcap.com//currencies/rupiah-token/	0
5485	AstroTools	ASTRO	https://coinmarketcap.com//currencies/astrotools/	1.25
5486	EXRNchain	EXRN	https://coinmarketcap.com//currencies/exrnchain/	0
5487	GHOST	GHOST	https://coinmarketcap.com//currencies/ghost/	0.25
5488	DMM: Governance	DMG	https://coinmarketcap.com//currencies/dmm-governance/	0.07
5489	BiFi	BIFI	https://coinmarketcap.com//currencies/bifi/	0.04
5490	OKCash	OK	https://coinmarketcap.com//currencies/okcash/	0.05
5491	Coin Artist	COIN	https://coinmarketcap.com//currencies/coin-artist/	3.44
5492	Pigeoncoin	PGN	https://coinmarketcap.com//currencies/pigeoncoin/	0
5493	PhoenixDAO	PHNX	https://coinmarketcap.com//currencies/phoenixdao/	0.06
5494	Qitmeer	PMEER	https://coinmarketcap.com//currencies/qitmeer/	0.02
5495	Hyperion	HYN	https://coinmarketcap.com//currencies/hyperion/	0.02
5496	Blackmoon	BMC	https://coinmarketcap.com//currencies/blackmoon/	0.07
5497	Playcent	PCNT	https://coinmarketcap.com//currencies/playcent/	0.51
5498	DAEX	DAX	https://coinmarketcap.com//currencies/daex/	0.01
5499	Omni	OMNI	https://coinmarketcap.com//currencies/omni/	6.45
5500	Pivot Token	PVT	https://coinmarketcap.com//currencies/pivot-token/	0
5501	BackPacker Coin	BPC	https://coinmarketcap.com//currencies/backpacker-coin/	0.57
5502	Amon	AMN	https://coinmarketcap.com//currencies/amon/	0.01
5503	Ravencoin Classic	RVC	https://coinmarketcap.com//currencies/ravencoin-classic/	0
5504	Thugs Finance	THUGS	https://coinmarketcap.com//currencies/thugs-fi/	5.33
5505	GAPS	GAP	https://coinmarketcap.com//currencies/gaps/	0.35
5506	Seigniorage Shares	SHARE	https://coinmarketcap.com//currencies/seigniorage-shares/	0.17
5507	Catex Token	CATT	https://coinmarketcap.com//currencies/catex-token/	0
5508	MoonTools	MOONS	https://coinmarketcap.com//currencies/moontools/	127.74
5509	CoTrader	COT	https://coinmarketcap.com//currencies/cotrader/	0
5510	HOMIHELP	HOMI	https://coinmarketcap.com//currencies/homihelp/	2.97
5511	Noku	NOKU	https://coinmarketcap.com//currencies/noku/	0.11
5512	BLink	BLINK	https://coinmarketcap.com//currencies/blink/	0.02
5513	Tierion	TNT	https://coinmarketcap.com//currencies/tierion/	0.01
5514	KuboCoin	KUBO	https://coinmarketcap.com//currencies/kubocoin/	0
5515	Pinkcoin	PINK	https://coinmarketcap.com//currencies/pinkcoin/	0.01
5516	xBTC	xBTC	https://coinmarketcap.com//currencies/xbtc/	0.58
5517	Sumokoin	SUMO	https://coinmarketcap.com//currencies/sumokoin/	0.12
5518	AXPR	AXPR	https://coinmarketcap.com//currencies/axpr-token/	0.01
5519	BarterTrade	BART	https://coinmarketcap.com//currencies/bartertrade/	0.05
5520	CEEK VR	CEEK	https://coinmarketcap.com//currencies/ceek-vr/	0
5521	Minereum	MNE	https://coinmarketcap.com//currencies/minereum/	0.28
5522	Folder Protocol	FOL	https://coinmarketcap.com//currencies/folder-protocol/	0.91
5523	xSigma	SIG	https://coinmarketcap.com//currencies/xsigma/	3.47
5524	Phantasma Energy	KCAL	https://coinmarketcap.com//currencies/phantasma-energy/	0.08
5525	Vid	VI	https://coinmarketcap.com//currencies/vid/	0.13
5526	MenaPay	MPAY	https://coinmarketcap.com//currencies/menapay/	0.02
5527	Graviocoin	GIO	https://coinmarketcap.com//currencies/graviocoin/	0.04
5528	VNT Chain	VNT	https://coinmarketcap.com//currencies/vnt-chain/	0
5529	Bitswift	BITS	https://coinmarketcap.com//currencies/bitswift/	0.8
5530	NFTLootBox	LOOT	https://coinmarketcap.com//currencies/nftlootbox/	136.12
5531	Enigma	ENG	https://coinmarketcap.com//currencies/enigma/	0.22
5532	OneRoot Network	RNT	https://coinmarketcap.com//currencies/oneroot-network/	0.01
5533	WPP TOKEN	WPP	https://coinmarketcap.com//currencies/wpp-token/	0
5534	KanadeCoin	KNDC	https://coinmarketcap.com//currencies/kanadecoin/	0
5535	AMLT	AMLT	https://coinmarketcap.com//currencies/amlt/	0.01
5536	Pascal	PASC	https://coinmarketcap.com//currencies/pascal/	0.09
5537	CryptoPing	PING	https://coinmarketcap.com//currencies/cryptoping/	0.41
5538	4THPILLAR TECHNOLOGIES	FOUR	https://coinmarketcap.com//currencies/4thpillar-technologies/	0.02
5539	Breezecoin	BRZE	https://coinmarketcap.com//currencies/breezecoin/	0.03
5540	Heart Number	HTN	https://coinmarketcap.com//currencies/heartnumber/	0
5698	Pakcoin	PAK	https://coinmarketcap.com//currencies/pakcoin/	0.03
5541	NativeCoin	N8V	https://coinmarketcap.com//currencies/native-coin/	0.12
5542	HashNet BitEco	HNB	https://coinmarketcap.com//currencies/hashnet-biteco/	0.1
5543	VINchain	VIN	https://coinmarketcap.com//currencies/vinchain/	0
5544	hybrix	HY	https://coinmarketcap.com//currencies/hybrix/	1.25
5545	Golden Goose	GOLD	https://coinmarketcap.com//currencies/golden-goose/	0.01948
5546	Meter Governance	MTRG	https://coinmarketcap.com//currencies/meter-governance/	1.24
5547	Cube	AUTO	https://coinmarketcap.com//currencies/cube/	0.0004228
5548	Hyprr (Howdoo)	UDOO	https://coinmarketcap.com//currencies/hyprr/	0.02913
5549	Nexty	NTY	https://coinmarketcap.com//currencies/nexty/	1.797e-05
5550	Nuco.cloud	NCDT	https://coinmarketcap.com//currencies/nuco-cloud/	0.4333
5551	ThreeFold	TFT	https://coinmarketcap.com//currencies/threefold/	0.03455
5552	BitcoinZ	BTCZ	https://coinmarketcap.com//currencies/bitcoinz/	0.0003324
5553	2key.network	2KEY	https://coinmarketcap.com//currencies/2key-network/	0.05321
5554	OWNDATA	OWN	https://coinmarketcap.com//currencies/owndata/	0
5555	MyWish	WISH	https://coinmarketcap.com//currencies/mywish/	0.29
5556	BitCapitalVendor	BCV	https://coinmarketcap.com//currencies/bitcapitalvendor/	0
5557	BonFi	BNF	https://coinmarketcap.com//currencies/bonfi/	0.01
5558	Denarius	D	https://coinmarketcap.com//currencies/denarius-d/	0.36
5559	CACHE Gold	CGT	https://coinmarketcap.com//currencies/cache-gold/	55.53
5560	Eden	EDN	https://coinmarketcap.com//currencies/eden/	0
5561	Mushroom	MUSH	https://coinmarketcap.com//currencies/mushroom/	13.55
5562	FortKnoxster	FKX	https://coinmarketcap.com//currencies/fortknoxster/	0.02
5563	EOSDT	EOSDT	https://coinmarketcap.com//currencies/eosdt/	1
5564	ZClassic	ZCL	https://coinmarketcap.com//currencies/zclassic/	0.29
5565	BOX Token	BOX	https://coinmarketcap.com//currencies/box-token/	0.03
5566	MobileGo	MGO	https://coinmarketcap.com//currencies/mobilego/	0.03
5567	Zero Utility Token	ZUT	https://coinmarketcap.com//currencies/zero-utility-token/	2698.39
5568	VeriCoin	VRC	https://coinmarketcap.com//currencies/vericoin/	0.08
5569	Rotharium	RTH	https://coinmarketcap.com//currencies/rotharium/	0.78
5570	Expanse	EXP	https://coinmarketcap.com//currencies/expanse/	0.14
5571	Merculet	MVP	https://coinmarketcap.com//currencies/merculet/	0
5572	Wownero	WOW	https://coinmarketcap.com//currencies/wownero/	0.07
5573	DiFy.Finance	YFIII	https://coinmarketcap.com//currencies/dify-finance/	321.41
5574	FYDcoin	FYD	https://coinmarketcap.com//currencies/fydcoin/	0
5575	Ether-1	ETHO	https://coinmarketcap.com//currencies/ether-1/	0.05
5576	Aventus	AVT	https://coinmarketcap.com//currencies/aventus/	0.43
5577	Databroker	DTX	https://coinmarketcap.com//currencies/databroker/	0.03
5578	Epic Cash	EPIC	https://coinmarketcap.com//currencies/epic-cash/	0.23
5579	WinStars.live	WNL	https://coinmarketcap.com//currencies/winstars-live/	0.13
5580	DACSEE	DACS	https://coinmarketcap.com//currencies/dacsee/	0
5581	Base Protocol	BASE	https://coinmarketcap.com//currencies/base-protocol/	1.66
5582	ODUWA	OWC	https://coinmarketcap.com//currencies/oduwa/	0.6
5583	Themis	GET	https://coinmarketcap.com//currencies/themis/	0
5584	DOC.COM	MTC	https://coinmarketcap.com//currencies/doc-com/	0
5585	PayPie	PPP	https://coinmarketcap.com//currencies/paypie/	0.03
5586	DeFinition	DZI	https://coinmarketcap.com//currencies/definition/	1.07
5587	CVCoin	CVN	https://coinmarketcap.com//currencies/cvcoin/	0.2
5588	HempCoin	THC	https://coinmarketcap.com//currencies/hempcoin/	0.01
5589	HiCoin	XHI	https://coinmarketcap.com//currencies/hicoin/	0
5590	Bonk	BONK	https://coinmarketcap.com//currencies/bonk/	0.82
5591	Xfinance	XFI	https://coinmarketcap.com//currencies/xfinance/	52.15
5592	NOW Token	NOW	https://coinmarketcap.com//currencies/now-token/	0.03
5593	Snetwork	SNET	https://coinmarketcap.com//currencies/snetwork/	0.01
5594	Curecoin	CURE	https://coinmarketcap.com//currencies/curecoin/	0.1
5595	BillionHappiness	BHC	https://coinmarketcap.com//currencies/billionhappiness/	88.53
5596	Rapidz	RPZX	https://coinmarketcap.com//currencies/rapidz/	0
5597	Open Predict Token	OPT	https://coinmarketcap.com//currencies/open-predict-token/	2.39
5598	Education Ecosystem	LEDU	https://coinmarketcap.com//currencies/education-ecosystem/	0.01
5599	Ditto	DITTO	https://coinmarketcap.com//currencies/ditto/	0.93
5600	Cappasity	CAPP	https://coinmarketcap.com//currencies/cappasity/	0
5601	Kuende	KUE	https://coinmarketcap.com//currencies/kuende/	0
5602	AICHAIN	AIT	https://coinmarketcap.com//currencies/aichain/	0
5603	JUIICE	JUI	https://coinmarketcap.com//currencies/juiice/	0
5604	ChartEx	CHART	https://coinmarketcap.com//currencies/chartex/	0.12
5605	Bismuth	BIS	https://coinmarketcap.com//currencies/bismuth/	0.12
5606	Honest	HNST	https://coinmarketcap.com//currencies/honest/	0.03
5607	HEROcoin	PLAY	https://coinmarketcap.com//currencies/herocoin/	0.01
5608	Dimecoin	DIME	https://coinmarketcap.com//currencies/dimecoin/	0
5609	Naka Bodhi Token	NBOT	https://coinmarketcap.com//currencies/naka-bodhi-token/	0.03
5610	Uptrennd	1UP	https://coinmarketcap.com//currencies/uptrennd/	0.01
5611	TransferCoin	TX	https://coinmarketcap.com//currencies/transfercoin/	0.28
5612	GHOSTPRISM	GHOST	https://coinmarketcap.com//currencies/ghostprism/	0.61
5613	SafeCoin	SAFE	https://coinmarketcap.com//currencies/safecoin/	0.08
5614	Rapids	RPD	https://coinmarketcap.com//currencies/rapids/	0
5615	Boosted Finance	BOOST	https://coinmarketcap.com//currencies/boosted-finance/	38.84
5616	Maecenas	ART	https://coinmarketcap.com//currencies/maecenas/	0.03
5617	Experty	EXY	https://coinmarketcap.com//currencies/experty/	0.09
5618	Tadpole Finance	TAD	https://coinmarketcap.com//currencies/tadpole-finance/	10.02
5619	PirateCash	PIRATE	https://coinmarketcap.com//currencies/piratecash/	0.09
5620	IHT Real Estate Protocol	IHT	https://coinmarketcap.com//currencies/iht-real-estate-protocol/	0
5621	HYCON	HYC	https://coinmarketcap.com//currencies/hycon/	0
5622	Bean Cash	BITB	https://coinmarketcap.com//currencies/bean-cash/	0
5623	KickToken	KICK	https://coinmarketcap.com//currencies/kick-token/	0
5624	Zoracles	ZORA	https://coinmarketcap.com//currencies/zoracles/	408.75
5625	Gene Source Code Chain	GENE	https://coinmarketcap.com//currencies/gene-source-code-chain/	0.01
5626	Bitcoin Confidential	BC	https://coinmarketcap.com//currencies/bitcoin-confidential/	0
5627	IntelliShare	INE	https://coinmarketcap.com//currencies/intellishare/	0.01
5628	Kingdom Game 4.0	KDG	https://coinmarketcap.com//currencies/kingdom-game-4/	0.07
5629	NuBits	USNBT	https://coinmarketcap.com//currencies/nubits/	0.2
5630	Medicalchain	MTN	https://coinmarketcap.com//currencies/medical-chain/	0.01
5631	CryptoFranc	XCHF	https://coinmarketcap.com//currencies/cryptofranc/	1.08
5632	EOS TRUST	EOST	https://coinmarketcap.com//currencies/eos-trust/	0
5633	ECOSC	ECU	https://coinmarketcap.com//currencies/ecosc/	2.48
5634	Coinlancer	CL	https://coinmarketcap.com//currencies/coinlancer/	0.03
5635	Plair	PLA	https://coinmarketcap.com//currencies/plair/	0
5636	Lobstex	LOBS	https://coinmarketcap.com//currencies/lobstex/	0.1
5637	Blue Whale EXchange	BWX	https://coinmarketcap.com//currencies/blue-whale-exchange/	0.05
5638	PWR Coin	PWR	https://coinmarketcap.com//currencies/powercoin/	0
5639	Membrana	MBN	https://coinmarketcap.com//currencies/membrana/	0.01
5640	SonoCoin	SONO	https://coinmarketcap.com//currencies/sonocoin/	0.07
5641	Karbo	KRB	https://coinmarketcap.com//currencies/karbo/	0.23
5642	YVS.Finance	YVS	https://coinmarketcap.com//currencies/yvs-finance/	5.82
5643	Vites	VITES	https://coinmarketcap.com//currencies/vites/	0
5644	MonetaryUnit	MUE	https://coinmarketcap.com//currencies/monetaryunit/	0.009452
5645	I/O Coin	IOC	https://coinmarketcap.com//currencies/iocoin/	0.109
5646	Mochimo	MCM	https://coinmarketcap.com//currencies/mochimo/	0.2428
5647	LINKA	LINKA	https://coinmarketcap.com//currencies/linka/	0.002915
5648	Vanilla Network	VNLA	https://coinmarketcap.com//currencies/vanilla-network/	2.77
5649	Infinitus Token	INF	https://coinmarketcap.com//currencies/infinitus-token/	0.03477
5650	Ulord	UT	https://coinmarketcap.com//currencies/ulord/	0.02597
5651	GMB	GMB	https://coinmarketcap.com//currencies/gmb/	0.003875
5652	Donut	DONUT	https://coinmarketcap.com//currencies/donut/	0.01664
5653	Patientory	PTOY	https://coinmarketcap.com//currencies/patientory/	0.02854
5654	Conceal	CCX	https://coinmarketcap.com//currencies/conceal/	0.2
5655	Humaniq	HMQ	https://coinmarketcap.com//currencies/humaniq/	0.01
5656	Ormeus Cash	OMC	https://coinmarketcap.com//currencies/ormeus-cash/	0.01
5657	Lunyr	LUN	https://coinmarketcap.com//currencies/lunyr/	0.85
5658	DAOBet	BET	https://coinmarketcap.com//currencies/daobet/	0.01
5659	BetterBetting	BETR	https://coinmarketcap.com//currencies/betterbetting/	0.01
5660	Cryptaur	CPT	https://coinmarketcap.com//currencies/cryptaur/	0
5661	Stobox Token	STBU	https://coinmarketcap.com//currencies/stobox-token/	0.33
5662	XcelToken Plus	XLAB	https://coinmarketcap.com//currencies/xceltoken-plus/	0
5663	CannabisCoin	CANN	https://coinmarketcap.com//currencies/cannabiscoin/	0.03
5664	XTRABYTES	XBY	https://coinmarketcap.com//currencies/xtrabytes/	0
5665	Alphacat	ACAT	https://coinmarketcap.com//currencies/alphacat/	0
5666	TrueFlip	TFL	https://coinmarketcap.com//currencies/trueflip/	0.31
5667	SeChain	SNN	https://coinmarketcap.com//currencies/sechain/	0
5668	Primas	PST	https://coinmarketcap.com//currencies/primas/	0.04
5669	Vipstar Coin	VIPS	https://coinmarketcap.com//currencies/vipstar-coin/	0
5670	Libertas Token	LIBERTAS	https://coinmarketcap.com//currencies/libertas-token/	0.02
5671	NeoWorld Cash	NASH	https://coinmarketcap.com//currencies/neoworld-cash/	0
5672	Scry.info	DDD	https://coinmarketcap.com//currencies/scryinfo/	0
5673	SoMee.Social	ONG	https://coinmarketcap.com//currencies/ongsocial/	0.01
5674	BOOM	BOOM	https://coinmarketcap.com//currencies/boom/	0
5675	The ChampCoin	TCC	https://coinmarketcap.com//currencies/the-champcoin/	0.01
5676	Tolar	TOL	https://coinmarketcap.com//currencies/tolar/	0.01
5677	0xBitcoin	0xBTC	https://coinmarketcap.com//currencies/0xbtc/	0.32
5678	FujiCoin	FJC	https://coinmarketcap.com//currencies/fujicoin/	0
5679	Dragon Coins	DRG	https://coinmarketcap.com//currencies/dragon-coins/	0.01
5680	Rope	$ROPE	https://coinmarketcap.com//currencies/rope/	65.23
5681	BOScoin	BOS	https://coinmarketcap.com//currencies/boscoin/	0
5682	Eureka Coin	ERK	https://coinmarketcap.com//currencies/eureka-coin/	0.03
5683	SpectrumCash	XSM	https://coinmarketcap.com//currencies/spectrumcash/	0
5684	PLANET	PLA	https://coinmarketcap.com//currencies/planet/	0.01
5685	Asch	XAS	https://coinmarketcap.com//currencies/asch/	0.02
5686	Qwertycoin	QWC	https://coinmarketcap.com//currencies/qwertycoin/	0
5687	Quark	QRK	https://coinmarketcap.com//currencies/quark/	0.01
5688	CryptoFlow	CFL	https://coinmarketcap.com//currencies/cryptoflow/	0.02
5689	Ormeus Ecosystem	ECO	https://coinmarketcap.com//currencies/ormeus-ecosystem/	0
5690	MetaMorph	METM	https://coinmarketcap.com//currencies/metamorph/	0.01
5691	PieDAO DEFI++	DEFI++	https://coinmarketcap.com//currencies/piedao-defi/	5.27
5692	Megacoin	MEC	https://coinmarketcap.com//currencies/megacoin/	0.04
5693	Rise	RISE	https://coinmarketcap.com//currencies/rise/	0.01
5694	Infinitecoin	IFC	https://coinmarketcap.com//currencies/infinitecoin/	0
5695	Upfiring	UFR	https://coinmarketcap.com//currencies/upfiring/	0.07
5696	DeepOnion	ONION	https://coinmarketcap.com//currencies/deeponion/	0.08
5697	Matryx	MTX	https://coinmarketcap.com//currencies/matryx/	0.07
5699	Kuai Token	KT	https://coinmarketcap.com//currencies/kuai-token/	0.15
5700	Tokoin	TOKO	https://coinmarketcap.com//currencies/tokoin/	0.01
5701	Vetri	VLD	https://coinmarketcap.com//currencies/vetri/	0
5702	Nuggets	NUG	https://coinmarketcap.com//currencies/nuggets/	0
5703	StrongHands	SHND	https://coinmarketcap.com//currencies/stronghands/	0
5704	Centric Cash	CNS	https://coinmarketcap.com//currencies/centric-cash/	0
5705	StaysBASE	SBS	https://coinmarketcap.com//currencies/staysbase/	0.51
5706	Zero	ZER	https://coinmarketcap.com//currencies/zero/	0.17
5707	VIG	VIG	https://coinmarketcap.com//currencies/vig/	0
5708	AI Doctor	AIDOC	https://coinmarketcap.com//currencies/aidoc/	0
5709	Xaurum	XAUR	https://coinmarketcap.com//currencies/xaurum/	0.02
5710	eosDAC	EOSDAC	https://coinmarketcap.com//currencies/eosdac/	0
5711	Shield Protocol	SHIELD	https://coinmarketcap.com//currencies/shield-protocol/	7.68
5712	EDC Blockchain	EDC	https://coinmarketcap.com//currencies/edc-blockchain/	0.04
5713	Modern Investment Coin	MODIC	https://coinmarketcap.com//currencies/modern-investment-coin/	0.56
5714	Bottos	BTO	https://coinmarketcap.com//currencies/bottos/	0
5715	OLXA	OLXA	https://coinmarketcap.com//currencies/olxa/	0
5716	Unification	FUND	https://coinmarketcap.com//currencies/unification/	0.05
5717	Venus BCH	vBCH	https://coinmarketcap.com//currencies/venus-bch/	10.53
5718	Energo	TSL	https://coinmarketcap.com//currencies/energo/	0
5719	Ycash	YEC	https://coinmarketcap.com//currencies/ycash/	0.15
5720	Japan Content Token	JCT	https://coinmarketcap.com//currencies/japan-content-token/	0.01
5721	ZumCoin	ZUM	https://coinmarketcap.com//currencies/zumcoin/	0
5722	SmileyCoin	SMLY	https://coinmarketcap.com//currencies/smileycoin/	0
5723	Tidex Token	TDX	https://coinmarketcap.com//currencies/tidex-token/	0.17
5724	Safex Cash	SFX	https://coinmarketcap.com//currencies/safex-cash/	0.02
5725	Sharder	SS	https://coinmarketcap.com//currencies/sharder/	0.01
5726	Substratum	SUB	https://coinmarketcap.com//currencies/substratum/	0
5727	Connectome	CNTM	https://coinmarketcap.com//currencies/connectome/	1.3
5728	Stabilize	STBZ	https://coinmarketcap.com//currencies/stabilize/	11.84
5729	SINOVATE	SIN	https://coinmarketcap.com//currencies/sinovate/	0
5730	Playgroundz	IOG	https://coinmarketcap.com//currencies/playgroundz/	0.05
5731	COVA	COVA	https://coinmarketcap.com//currencies/cova/	0
5732	GlobalBoost-Y	BSTY	https://coinmarketcap.com//currencies/globalboost-y/	0.09
5733	Renewable Electronic Energy Coin	REEC	https://coinmarketcap.com//currencies/electronic-energy-coin/	0
5734	Perth Mint Gold Token	PMGT	https://coinmarketcap.com//currencies/perth-mint-gold-token/	1770.49
5735	DexKit	KIT	https://coinmarketcap.com//currencies/dexkit/	1.91
5736	Insight Protocol	INX	https://coinmarketcap.com//currencies/insight-protocol/	0.01
5737	Banca	BANCA	https://coinmarketcap.com//currencies/banca/	0
5738	Adshares	ADS	https://coinmarketcap.com//currencies/adshares/	0.04
5739	Shard	SHARD	https://coinmarketcap.com//currencies/shard/	0.08
5740	CoinPoker	CHP	https://coinmarketcap.com//currencies/coinpoker/	0.01
5741	Axis DeFi	AXIS	https://coinmarketcap.com//currencies/axis-defi/	0.72
5742	TrezarCoin	TZC	https://coinmarketcap.com//currencies/trezarcoin/	0.01
5743	ADAMANT Messenger	ADM	https://coinmarketcap.com//currencies/adamant-messenger/	0.02
5744	Teloscoin	TELOS	https://coinmarketcap.com//currencies/teloscoin/	0.008908
5745	EXMO Coin	EXM	https://coinmarketcap.com//currencies/exmo-coin/	0.01336
5746	CloakCoin	CLOAK	https://coinmarketcap.com//currencies/cloakcoin/	0.2655
5747	0xcert	ZXC	https://coinmarketcap.com//currencies/0xcert/	0.004274
5748	Titan Coin	TTN	https://coinmarketcap.com//currencies/titan-coin/	0.001624
5749	Zen Protocol	ZP	https://coinmarketcap.com//currencies/zen-protocol/	0.06529
5750	BitBall	BTB	https://coinmarketcap.com//currencies/bitball/	0.002301
5751	LUXCoin	LUX	https://coinmarketcap.com//currencies/luxcoin/	0.1353
5752	Max Property Group	MPG	https://coinmarketcap.com//currencies/max-property-group/	0.01037
5753	FansTime	FTI	https://coinmarketcap.com//currencies/fanstime/	0.0005155
5754	Isiklar Coin	ISIKC	https://coinmarketcap.com//currencies/isiklar-coin/	0.62
5755	CryptoTask	CTASK	https://coinmarketcap.com//currencies/cryptotask/	1
5756	DeltaChain	DELTA	https://coinmarketcap.com//currencies/delta-chain/	0
5757	Havy	HAVY	https://coinmarketcap.com//currencies/havy/	0
5758	pEOS	PEOS	https://coinmarketcap.com//currencies/peos/	0
5759	8X8 PROTOCOL	EXE	https://coinmarketcap.com//currencies/8x8-protocol/	0
5760	Mercury	MER	https://coinmarketcap.com//currencies/mercury/	0.01
5761	Birake	BIR	https://coinmarketcap.com//currencies/birake/	0.02
5762	REAL	REAL	https://coinmarketcap.com//currencies/real/	0.14
5763	Memetic / PepeCoin	MEME	https://coinmarketcap.com//currencies/memetic/	0.05
5764	EchoLink	EKO	https://coinmarketcap.com//currencies/echolink/	0
5765	Ixcoin	IXC	https://coinmarketcap.com//currencies/ixcoin/	0.07
5766	BiblePay	BBP	https://coinmarketcap.com//currencies/biblepay/	0
5767	Phoenixcoin	PXC	https://coinmarketcap.com//currencies/phoenixcoin/	0.02
5768	Coinsbit Token	CNB	https://coinmarketcap.com//currencies/coinsbit-token/	0
5769	Global Cryptocurrency	GCC	https://coinmarketcap.com//currencies/global-cryptocurrency/	0
5770	CorionX	CORX	https://coinmarketcap.com//currencies/corionx/	0.01
5771	Bitcoin Plus	XBC	https://coinmarketcap.com//currencies/bitcoin-plus/	8.98
5772	WebDollar	WEBD	https://coinmarketcap.com//currencies/webdollar/	0
5773	Neurotoken	NTK	https://coinmarketcap.com//currencies/neurotoken/	0.02
5774	Fusible	FUSII	https://coinmarketcap.com//currencies/fusible/	3.34
5775	PetroDollar	XPD	https://coinmarketcap.com//currencies/petrodollar/	0.02
5776	Bezant	BZNT	https://coinmarketcap.com//currencies/bezant/	0
5777	Zebi Token	ZEBI	https://coinmarketcap.com//currencies/zebi-token/	0
5778	Origin Sport	ORS	https://coinmarketcap.com//currencies/origin-sport/	0
5779	DogeCash	DOGEC	https://coinmarketcap.com//currencies/dogecash/	0.1
5780	NFTX Hashmasks Index	MASK	https://coinmarketcap.com//currencies/nftx-hashmasks-index/	2254.7
5781	Q DAO Governance token v1.0	QDAO	https://coinmarketcap.com//currencies/q-dao-governance-token/	8.37
5782	CREA	CREA	https://coinmarketcap.com//currencies/crea/	0.07
5783	Azbit	AZ	https://coinmarketcap.com//currencies/azbit/	0
5784	Trollcoin	TROLL	https://coinmarketcap.com//currencies/trollcoin/	0
5785	Typerium	TYPE	https://coinmarketcap.com//currencies/typerium/	0
5786	SPINDLE	SPD	https://coinmarketcap.com//currencies/spindle/	0
5787	PUBLISH	NEWS	https://coinmarketcap.com//currencies/publish/	0.01
5788	CoinDeal Token	CDL	https://coinmarketcap.com//currencies/coindeal-token/	0.03
5789	SynchroBitcoin	SNB	https://coinmarketcap.com//currencies/synchrobitcoin/	0.02
5790	GeoCoin	GEO	https://coinmarketcap.com//currencies/geocoin/	0.41
5791	x42 Protocol	X42	https://coinmarketcap.com//currencies/x42-protocol/	0.06
5792	Bitcoin Private	BTCP	https://coinmarketcap.com//currencies/bitcoin-private/	0.27
5793	ebirah	EBRH	https://coinmarketcap.com//currencies/ebirah/	2.99
5794	NFT Index	NFTI	https://coinmarketcap.com//currencies/nft-index/	569.36
5795	Rivetz	RVT	https://coinmarketcap.com//currencies/rivetz/	0.05
5796	ZrCoin	ZRC	https://coinmarketcap.com//currencies/zrcoin/	0.25
5797	DeFi Bids	BID	https://coinmarketcap.com//currencies/defi-bids/	0.09
5798	LocalCoinSwap	LCS	https://coinmarketcap.com//currencies/local-coin-swap/	0.02
5799	Dether	DTH	https://coinmarketcap.com//currencies/dether/	0.01
5800	DAPS Coin	DAPS	https://coinmarketcap.com//currencies/daps-coin/	0
5801	Freicoin	FRC	https://coinmarketcap.com//currencies/freicoin/	0.02
5802	Eternity	ENT	https://coinmarketcap.com//currencies/eternity/	0.13
5803	Rise Protocol	RISE	https://coinmarketcap.com//currencies/rise-protocol/	17.74
5804	AAX Token	AAB	https://coinmarketcap.com//currencies/aax-token/	0.16
5805	DDKoin	DDK	https://coinmarketcap.com//currencies/ddkoin/	0.71
5806	Stox	STX	https://coinmarketcap.com//currencies/stox/	0.02
5807	CrowdWiz	WIZ	https://coinmarketcap.com//currencies/crowdwiz/	0.41
5808	ParkinGo	GOT	https://coinmarketcap.com//currencies/parkingo/	0.06
5809	DECENT	DCT	https://coinmarketcap.com//currencies/decent/	0.02
5810	Incent	INCNT	https://coinmarketcap.com//currencies/incent/	0.03
5811	AmonD	AMON	https://coinmarketcap.com//currencies/amond/	0
5812	SAFE DEAL	SFD	https://coinmarketcap.com//currencies/safe-deal/	3.11
5813	Garlicoin	GRLC	https://coinmarketcap.com//currencies/garlicoin/	0.02
5814	Blockpass	PASS	https://coinmarketcap.com//currencies/blockpass/	0.01
5815	IXT	IXT	https://coinmarketcap.com//currencies/ixledger/	0.03
5816	ATLANT	ATL	https://coinmarketcap.com//currencies/atlant/	0.02
5817	Color Platform	CLR	https://coinmarketcap.com//currencies/color-platform/	0.01
5818	TokenPay	TPAY	https://coinmarketcap.com//currencies/tokenpay/	0.05
5819	Nexxo	NEXXO	https://coinmarketcap.com//currencies/nexxo/	0.01
5820	BlockMesh	BMH	https://coinmarketcap.com//currencies/blockmesh/	0
5821	Cashbery Coin	CBC	https://coinmarketcap.com//currencies/cashbery-coin/	0.02
5822	On.Live	ONL	https://coinmarketcap.com//currencies/on-live/	0.05
5823	ZPER	ZPR	https://coinmarketcap.com//currencies/zper/	0
5824	Bitcoin Atom	BCA	https://coinmarketcap.com//currencies/bitcoin-atom/	0.06
5825	SHAKE	SHAKE	https://coinmarketcap.com//currencies/shake/	1979
5826	LanaCoin	LANA	https://coinmarketcap.com//currencies/lanacoin/	0
5827	Mcashchain	MCASH	https://coinmarketcap.com//currencies/mcashchain/	0
5828	HEAT	HEAT	https://coinmarketcap.com//currencies/heat-ledger/	0.02
5829	Decentrahub Coin	DCNTR	https://coinmarketcap.com//currencies/decentrahub-coin/	0.85
5830	Cosmo Coin	COSM	https://coinmarketcap.com//currencies/cosmo-coin/	0
5831	HashBX 	HBX	https://coinmarketcap.com//currencies/hashsbx/	0
5832	ZENZO	ZNZ	https://coinmarketcap.com//currencies/zenzo/	0.08
5833	Fesschain	FESS	https://coinmarketcap.com//currencies/fesschain/	0.01
5834	Meridian Network	LOCK	https://coinmarketcap.com//currencies/meridian-network/	0.1
5835	BUZZCoin	BUZZ	https://coinmarketcap.com//currencies/buzzcoin/	0
5836	Maincoin	MNC	https://coinmarketcap.com//currencies/maincoin/	0
5837	FUD.finance	FUD	https://coinmarketcap.com//currencies/fudfinance/	46.98
5838	Etherparty	FUEL	https://coinmarketcap.com//currencies/etherparty/	0
5839	SOTA Finance	SOTA	https://coinmarketcap.com//currencies/sota-finance/	0.44
5840	STK	STK	https://coinmarketcap.com//currencies/stk/	0
5841	Wrapped Origin Axie	WOA	https://coinmarketcap.com//currencies/wrapped-origin-axie/	4780.27
5842	Patron	PAT	https://coinmarketcap.com//currencies/patron/	0
5843	Smartshare	SSP	https://coinmarketcap.com//currencies/smartshare/	0
5844	DubaiCoin	DBIX	https://coinmarketcap.com//currencies/dubaicoin-dbix/	0.2519
5845	USDX [Lighthouse]	USDX	https://coinmarketcap.com//currencies/usdx-lighthouse/	0.2838
5846	Fera	FERA	https://coinmarketcap.com//currencies/fera/	0.005757
5847	Rate3	RTE	https://coinmarketcap.com//currencies/rate3/	0.001193
5848	Tendies	TEND	https://coinmarketcap.com//currencies/tendies/	0.1408
5849	BlitzPredict	XBP	https://coinmarketcap.com//currencies/blitzpredict/	0.002765
5850	Waves Community Token	WCT	https://coinmarketcap.com//currencies/waves-community-token/	0.1045
5851	Peony	PNY	https://coinmarketcap.com//currencies/peony/	0.1474
5852	Liquidity Network	LQD	https://coinmarketcap.com//currencies/liquidity-network/	0.01942
5853	BonusCloud	BXC	https://coinmarketcap.com//currencies/bonuscloud/	0.0003305
5854	Clash Token	SCT	https://coinmarketcap.com//currencies/clash-token/	0.69
5855	SIBCoin	SIB	https://coinmarketcap.com//currencies/sibcoin/	0.05
5856	NeuroChain	NCC	https://coinmarketcap.com//currencies/neurochain/	0
5857	PegNet	PEG	https://coinmarketcap.com//currencies/pegnet/	0
5858	EBCoin	EBC	https://coinmarketcap.com//currencies/ebcoin/	0
5859	ShipChain	SHIP	https://coinmarketcap.com//currencies/shipchain/	0
5860	NuShares	NSR	https://coinmarketcap.com//currencies/nushares/	0
5861	Squirrel Finance	NUTS	https://coinmarketcap.com//currencies/squirrel-finance/	2.05
5862	NEXT.coin	NEXT	https://coinmarketcap.com//currencies/next-coin/	0.16
5863	Cornichon	CORN	https://coinmarketcap.com//currencies/cornichon/	0.05
5864	Omnitude	ECOM	https://coinmarketcap.com//currencies/omnitude/	0.02
5865	Earnbase	ENB	https://coinmarketcap.com//currencies/earnbase/	8.05
5866	Ubex	UBEX	https://coinmarketcap.com//currencies/ubex/	0
5867	Fountain	FTN	https://coinmarketcap.com//currencies/fountain/	0.01
5868	Winco	WCO	https://coinmarketcap.com//currencies/winco/	0
5869	StrongHands Masternode	SHMN	https://coinmarketcap.com//currencies/stronghands-masternode/	0.34
5870	WeTrust	TRST	https://coinmarketcap.com//currencies/trust/	0.01
5871	Moneynet	MNC	https://coinmarketcap.com//currencies/moneynet/	0
5872	QANplatform	QARK	https://coinmarketcap.com//currencies/qanplatform/	0.05
5873	Flixxo	FLIXX	https://coinmarketcap.com//currencies/flixxo/	0.01
5874	Olyseum	OLY	https://coinmarketcap.com//currencies/olyseum/	0.47
5875	BitScreener Token	BITX	https://coinmarketcap.com//currencies/bitscreener-token/	0.01
5876	QChi	QCH	https://coinmarketcap.com//currencies/qchi/	0.03
5877	Veil	VEIL	https://coinmarketcap.com//currencies/veil/	0.01
5878	RigoBlock	GRG	https://coinmarketcap.com//currencies/rigoblock/	0.69
5879	Bitgear	GEAR	https://coinmarketcap.com//currencies/bitgear/	0.01
5880	Doge Token	DOGET	https://coinmarketcap.com//currencies/doge-token/	0
5881	Krios	GIG	https://coinmarketcap.com//currencies/krios/	0
5882	Cat Token	CAT	https://coinmarketcap.com//currencies/cat-token/	0.14
5883	Atmos	ATMOS	https://coinmarketcap.com//currencies/atmos/	0.01
5884	Artfinity	AT	https://coinmarketcap.com//currencies/artfinity/	0.01
5885	Ethereum Stake	ETHYS	https://coinmarketcap.com//currencies/ethereum-stake/	4.54
5886	Spaceswap	MILK2	https://coinmarketcap.com//currencies/spaceswap/	0.23
5887	HyperExchange	HX	https://coinmarketcap.com//currencies/hyperexchange/	0.02
5888	Hiveterminal Token	HVN	https://coinmarketcap.com//currencies/hiveterminal-token/	0
5889	Curio	CUR	https://coinmarketcap.com//currencies/curio/	0.52
5890	MARK.SPACE	MRK	https://coinmarketcap.com//currencies/mark-space/	0
5891	Beacon	BECN	https://coinmarketcap.com//currencies/beacon/	1.71
5892	BOMB	BOMB	https://coinmarketcap.com//currencies/bomb/	0.98
5893	Carboncoin	CARBON	https://coinmarketcap.com//currencies/carboncoin/	0
5894	Antiample	XAMP	https://coinmarketcap.com//currencies/antiample/	0
5895	Arbidex	ABX	https://coinmarketcap.com//currencies/arbidex/	0.04
5896	ONOToken	ONOT	https://coinmarketcap.com//currencies/onotoken/	0
5897	Jarvis+	JAR	https://coinmarketcap.com//currencies/jarvis/	0.01
5898	Friendz	FDZ	https://coinmarketcap.com//currencies/friends/	0
5899	Maxcoin	MAX	https://coinmarketcap.com//currencies/maxcoin/	0.01
5900	Bounty0x	BNTY	https://coinmarketcap.com//currencies/bounty0x/	0
5901	DEXA COIN	DEXA	https://coinmarketcap.com//currencies/dexa-coin/	0
5902	PolypuX	PUX	https://coinmarketcap.com//currencies/polypux/	0.02
5903	EventChain	EVC	https://coinmarketcap.com//currencies/eventchain/	0.03
5904	Orbitcoin	ORB	https://coinmarketcap.com//currencies/orbitcoin/	0.27
5905	Cryptobuyer	XPT	https://coinmarketcap.com//currencies/cryptobuyer/	0.01
5906	FuzeX	FXT	https://coinmarketcap.com//currencies/fuzex/	0
5907	Sapien	SPN	https://coinmarketcap.com//currencies/sapien/	0
5908	PRIA	PRIA	https://coinmarketcap.com//currencies/pria/	12.18
5909	Ink Protocol	XNK	https://coinmarketcap.com//currencies/ink-protocol/	0
5910	Rublix	RBLX	https://coinmarketcap.com//currencies/rublix/	0.04
5911	EveryCoin 	EVY	https://coinmarketcap.com//currencies/everycoin/	0
5912	TraDove B2BCoin	BBC	https://coinmarketcap.com//currencies/b2bcoin/	0
5913	Webflix Token	WFX	https://coinmarketcap.com//currencies/webflix-token/	0
5914	Spectre.ai Utility Token	SXUT	https://coinmarketcap.com//currencies/spectre-utility/	0.03
5915	Wrapped Gen-0 CryptoKitties	WG0	https://coinmarketcap.com//currencies/wrapped-gen-0-cryptokitties/	426.43
5916	Rotten	ROT	https://coinmarketcap.com//currencies/rotten/	0.01
5917	DATx	DATX	https://coinmarketcap.com//currencies/datx/	0
5918	iDealCash	DEAL	https://coinmarketcap.com//currencies/idealcash/	0
5919	TurtleNetwork	TN	https://coinmarketcap.com//currencies/turtlenetwork/	0.01
5920	Pawtocol	UPI	https://coinmarketcap.com//currencies/pawtocol/	0.01
5921	ATN	ATN	https://coinmarketcap.com//currencies/atn/	0.01
5922	ICE ROCK MINING	ROCK2	https://coinmarketcap.com//currencies/ice-rock-mining/	0.05
5923	FintruX Network	FTX	https://coinmarketcap.com//currencies/fintrux-network/	0.01
5924	POPCHAIN	PCH	https://coinmarketcap.com//currencies/popchain/	0
5925	Manna	MANNA	https://coinmarketcap.com//currencies/manna/	0
5926	Power Index Pool Token	PIPT	https://coinmarketcap.com//currencies/power-index-pool-token/	3.91
5927	ExclusiveCoin	EXCL	https://coinmarketcap.com//currencies/exclusivecoin/	0.13
5928	AidCoin	AID	https://coinmarketcap.com//currencies/aidcoin/	0.02
5929	Wolves of Wall Street	WOWS	https://coinmarketcap.com//currencies/wolves-of-wall-street/	175.75
5930	Ruler Protocol	RULER	https://coinmarketcap.com//currencies/ruler-protocol/	206.79
5931	Alpha Token	A	https://coinmarketcap.com//currencies/alpha-token/	0.02
5932	Gravity	GZRO	https://coinmarketcap.com//currencies/gravity/	0
5933	TrustDAO	TRUST	https://coinmarketcap.com//currencies/trustdao/	0.04
5934	Niobium Coin	NBC	https://coinmarketcap.com//currencies/niobium-coin/	0.01
5935	CyberFM	CYFM	https://coinmarketcap.com//currencies/cyberfm/	0
5936	NestEGG Coin	EGG	https://coinmarketcap.com//currencies/nestegg-coin/	0.02
5937	Vox.Finance	VOX	https://coinmarketcap.com//currencies/vox-finance/	142.03
5938	BITTO	BITTO	https://coinmarketcap.com//currencies/bitto/	0.25
5939	Pylon Network	PYLNT	https://coinmarketcap.com//currencies/pylon-network/	1.23
5940	Yearn Secure	YSEC	https://coinmarketcap.com//currencies/yearn-secure/	1.41
5941	Pesetacoin	PTC	https://coinmarketcap.com//currencies/pesetacoin/	0.01
5942	Noir	NOR	https://coinmarketcap.com//currencies/noir/	0.04
5943	MidasProtocol	MAS	https://coinmarketcap.com//currencies/midasprotocol/	0
5944	eXPerience Chain	XPC	https://coinmarketcap.com//currencies/experience-chain/	8.094e-06
5945	Digital Gold	GOLD	https://coinmarketcap.com//currencies/digital-gold/	54.15
5946	Woodcoin	LOG	https://coinmarketcap.com//currencies/woodcoin/	0.09102
5947	Indorse Token	IND	https://coinmarketcap.com//currencies/indorse-token/	0.01945
5948	VikkyToken	VIKKY	https://coinmarketcap.com//currencies/vikkytoken/	0.000193
5949	GoWithMi	GMAT	https://coinmarketcap.com//currencies/gowithmi/	0.0002636
5950	Debitum	DEB	https://coinmarketcap.com//currencies/debitum-network/	0.003588
5951	Ink	INK	https://coinmarketcap.com//currencies/ink/	0.001541
5952	Ormeus Coin	ORMEUS	https://coinmarketcap.com//currencies/ormeus-coin/	0.01988
5953	Almace Shards	ALMX	https://coinmarketcap.com//currencies/almace-shards/	71.07
5954	RED	RED	https://coinmarketcap.com//currencies/red/	0.01
5955	MIB Coin	MIB	https://coinmarketcap.com//currencies/mib-coin/	0.01
5956	GokuMarket Credit	GMC	https://coinmarketcap.com//currencies/gokumarket-credit/	0.23
5957	BZEdge	BZE	https://coinmarketcap.com//currencies/bzedge/	0
5958	Limitless VIP	VIP	https://coinmarketcap.com//currencies/limitless-vip/	0
5959	CryptoSoul	SOUL	https://coinmarketcap.com//currencies/cryptosoul/	0
5960	ESBC	ESBC	https://coinmarketcap.com//currencies/esbc/	0.03
5961	Trinity Network Credit	TNC	https://coinmarketcap.com//currencies/trinity-network-credit/	0
5962	Okschain	OKS	https://coinmarketcap.com//currencies/okschain/	0
5963	CaluraCoin	CLC	https://coinmarketcap.com//currencies/caluracoin/	0.05
5964	TigerCash	TCH	https://coinmarketcap.com//currencies/tigercash/	0.01
5965	MyBit	MYB	https://coinmarketcap.com//currencies/mybit/	0
5966	Deutsche eMark	DEM	https://coinmarketcap.com//currencies/deutsche-emark/	0.01
5967	FlypMe	FYP	https://coinmarketcap.com//currencies/flypme/	0.04
5968	LYNC Network	LYNC	https://coinmarketcap.com//currencies/lync-network/	0.71
5969	Electrify.Asia	ELEC	https://coinmarketcap.com//currencies/electrifyasia/	0
5970	RMPL	RMPL	https://coinmarketcap.com//currencies/rmpl/	0.98
5971	Lisk Machine Learning	LML	https://coinmarketcap.com//currencies/lisk-machine-learning/	0.01
5972	Yap Stone	YAP	https://coinmarketcap.com//currencies/yap-stone/	0
5973	Infinity Esaham	INFS	https://coinmarketcap.com//currencies/infinity-esaham/	1.02
5974	MEET.ONE	MEETONE	https://coinmarketcap.com//currencies/meetone/	0
5975	Delphy	DPY	https://coinmarketcap.com//currencies/delphy/	0.01
5976	CoinFi	COFI	https://coinmarketcap.com//currencies/coinfi/	0
5977	Stably USD	USDS	https://coinmarketcap.com//currencies/stableusd/	0.99
5978	Primalbase Token	PBT	https://coinmarketcap.com//currencies/primalbase/	494.54
5979	Blue Protocol	BLUE	https://coinmarketcap.com//currencies/ethereum-blue/	0.02
5980	Spiking	SPIKE	https://coinmarketcap.com//currencies/spiking/	0
5981	Ethereum Yield	ETHY	https://coinmarketcap.com//currencies/ethereum-yield/	6.11
5982	Trias	TRY	https://coinmarketcap.com//currencies/trias/	0
5983	CryptoBonusMiles	CBM	https://coinmarketcap.com//currencies/cryptobonusmiles/	0
5984	ZCore	ZCR	https://coinmarketcap.com//currencies/zcore/	0.07
5985	Internet of People	IOP	https://coinmarketcap.com//currencies/internet-of-people/	0.04
5986	Rentberry	BERRY	https://coinmarketcap.com//currencies/rentberry/	0
5987	OracleChain	OCT	https://coinmarketcap.com//currencies/oraclechain/	0.02
5988	Zeusshield	ZSC	https://coinmarketcap.com//currencies/zeusshield/	0
5989	Bitblocks	BBK	https://coinmarketcap.com//currencies/bitblocks/	0
5990	Sakura Bloom	SKB	https://coinmarketcap.com//currencies/sakura-bloom/	0
5991	Shadow Token	SHDW	https://coinmarketcap.com//currencies/shadow-token/	0.08
5992	Opus	OPT	https://coinmarketcap.com//currencies/opus/	0
5993	Transcodium	TNS	https://coinmarketcap.com//currencies/transcodium/	0.01
5994	Elementeum	ELET	https://coinmarketcap.com//currencies/elementeum/	0.11
5995	Sociall	SCL	https://coinmarketcap.com//currencies/sociall/	0.03
5996	TON Token	TON	https://coinmarketcap.com//currencies/tontoken/	0.01
5997	Crypton	CRP	https://coinmarketcap.com//currencies/utopia/	0.17
5998	PiplCoin	PIPL	https://coinmarketcap.com//currencies/piplcoin/	0
5999	Edgeless	EDG	https://coinmarketcap.com//currencies/edgeless/	0
6000	ZeuxCoin	ZUC	https://coinmarketcap.com//currencies/zeuxcoin/	0
6001	Insula	ISLA	https://coinmarketcap.com//currencies/insula/	0.71
6002	Merebel	MERI	https://coinmarketcap.com//currencies/merebel/	0.09
6003	HelloGold	HGT	https://coinmarketcap.com//currencies/hellogold/	0
6004	Sessia	KICKS	https://coinmarketcap.com//currencies/sessia/	0.06
6005	NFX Coin	NFXC	https://coinmarketcap.com//currencies/nfx-coin/	0
6006	Golos Blockchain	GLS	https://coinmarketcap.com//currencies/golos-blockchain/	0
6007	Digital Insurance Token	DIT	https://coinmarketcap.com//currencies/digital-insurance-token/	0
6008	Birdchain	BIRD	https://coinmarketcap.com//currencies/birdchain/	0
6009	X8X Token	X8X	https://coinmarketcap.com//currencies/x8x-token/	0.01
6010	Zennies	ZENI	https://coinmarketcap.com//currencies/zennies/	0
6011	Neutron	NTRN	https://coinmarketcap.com//currencies/neutron/	0.01
6012	Nerva	XNV	https://coinmarketcap.com//currencies/nerva/	0.03
6013	AudioCoin	ADC	https://coinmarketcap.com//currencies/audiocoin/	0
6014	Bob's Repair	BOB	https://coinmarketcap.com//currencies/bobs-repair/	0
6015	FantasyGold	FGC	https://coinmarketcap.com//currencies/fantasygold/	0
6016	Posscoin	POSS	https://coinmarketcap.com//currencies/posscoin/	0
6017	Master Contract Token	MCT	https://coinmarketcap.com//currencies/master-contract-token/	0
6018	PTON	PTON	https://coinmarketcap.com//currencies/pton/	0
6019	MAX Exchange Token	MAX	https://coinmarketcap.com//currencies/max-exchange-token/	0.3
6020	Bridge Protocol	BRDG	https://coinmarketcap.com//currencies/bridge-protocol/	0
6021	UpToken	UP	https://coinmarketcap.com//currencies/uptoken/	0
6022	SBank	STS	https://coinmarketcap.com//currencies/sbank/	0.09
6023	Block-Logic	BLTG	https://coinmarketcap.com//currencies/block-logic/	0.02
6024	Devery	EVE	https://coinmarketcap.com//currencies/devery/	0.01
6025	Tornado	TCORE	https://coinmarketcap.com//currencies/tornado/	87.94
6026	Lendingblock	LND	https://coinmarketcap.com//currencies/lendingblock/	0
6027	Lightstreams	PHT	https://coinmarketcap.com//currencies/lightstreams/	0.01
6028	MintMe.com Coin	MINTME	https://coinmarketcap.com//currencies/mintme-com-coin/	0
6029	RealTract	RET	https://coinmarketcap.com//currencies/realtract/	0
6030	ChessCoin	CHESS	https://coinmarketcap.com//currencies/chesscoin/	0.01
6031	Moneytoken	IMT	https://coinmarketcap.com//currencies/moneytoken/	0
6032	Bidesk	BDK	https://coinmarketcap.com//currencies/bidesk/	0.06
6033	ROAD	ROAD	https://coinmarketcap.com//currencies/road/	0.01
6034	Escroco Emerald	ESCE	https://coinmarketcap.com//currencies/escroco-emerald/	0
6035	DAV Coin	DAV	https://coinmarketcap.com//currencies/dav-coin/	0
6036	Actinium	ACM	https://coinmarketcap.com//currencies/actinium/	0.02
6037	DPRating	RATING	https://coinmarketcap.com//currencies/dprating/	0
6038	extraDNA	XDNA	https://coinmarketcap.com//currencies/extradna/	0
6039	Crypto Sports	CSPN	https://coinmarketcap.com//currencies/crypto-sports/	0.18
6040	Blocktix	TIX	https://coinmarketcap.com//currencies/blocktix/	0.01
6041	Nework	NKC	https://coinmarketcap.com//currencies/nework/	0
6042	Ethereum Meta	ETHM	https://coinmarketcap.com//currencies/ethereum-meta/	0
6043	Bitcloud	BTDX	https://coinmarketcap.com//currencies/bitcloud/	0.01
6044	CustomContractNetwork	CCN	https://coinmarketcap.com//currencies/customcontractnetwork/	0.000296
6045	VouchForMe	IPL	https://coinmarketcap.com//currencies/insurepal/	0.001956
6046	GoldenPyrex	GPYX	https://coinmarketcap.com//currencies/goldenpyrex/	0.04777
6047	GoldMint	MNTP	https://coinmarketcap.com//currencies/goldmint/	0.247
6048	Playkey	PKT	https://coinmarketcap.com//currencies/playkey/	0.02854
6049	VegaWallet Token	VGW	https://coinmarketcap.com//currencies/vegawallet-token/	0.02889
6050	BitDegree	BDG	https://coinmarketcap.com//currencies/bitdegree/	0.001205
6051	Alt.Estate token	ALT	https://coinmarketcap.com//currencies/alt-estate-token/	0.0006394
6052	KnoxFS (new)	KFX	https://coinmarketcap.com//currencies/knoxfs-new/	1.22
6053	SophiaTX	SPHTX	https://coinmarketcap.com//currencies/sophiatx/	0.001396
6054	Swap	XWP	https://coinmarketcap.com//currencies/swap/	0.04
6055	AMATEN	AMA	https://coinmarketcap.com//currencies/amaten/	0.06
6056	Wrapped Basic CryptoKitties	WCK	https://coinmarketcap.com//currencies/wrapped-cryptokitties/	6.98
6057	Equal	EQL	https://coinmarketcap.com//currencies/equal/	0
6058	Blockmason Credit Protocol	BCPT	https://coinmarketcap.com//currencies/blockmason/	0
6059	Solaris	XLR	https://coinmarketcap.com//currencies/solaris/	0.31
6060	Startcoin	START	https://coinmarketcap.com//currencies/startcoin/	0.01
6061	JustBet	WINR	https://coinmarketcap.com//currencies/justbet/	0
6062	PUBLYTO Token	PUB	https://coinmarketcap.com//currencies/publyto-token/	0
6063	CryptoVerificationCoin	CVCC	https://coinmarketcap.com//currencies/cryptoverificationcoin/	0.77
6064	Keep4r	KP4R	https://coinmarketcap.com//currencies/keep4r/	5.68
6065	Lition	LIT	https://coinmarketcap.com//currencies/lition/	0.01
6066	Lethean	LTHN	https://coinmarketcap.com//currencies/lethean/	0
6067	Auxilium	AUX	https://coinmarketcap.com//currencies/auxilium/	0
6068	Switch	ESH	https://coinmarketcap.com//currencies/switch/	0.04
6069	PENG	PENG	https://coinmarketcap.com//currencies/peng/	0
6070	Stipend	SPD	https://coinmarketcap.com//currencies/stipend/	0.04
6071	Ethereum Gold	ETG	https://coinmarketcap.com//currencies/ethereum-gold/	0.02
6072	Synergy	SNRG	https://coinmarketcap.com//currencies/synergy/	0.09
6073	uPlexa	UPX	https://coinmarketcap.com//currencies/uplexa/	0
6074	Rigel Finance	RIGEL	https://coinmarketcap.com//currencies/rigel-finance/	101.67
6075	Fundamenta	FMTA	https://coinmarketcap.com//currencies/fundamenta/	0.57
6076	CryptoAds Marketplace	CRAD	https://coinmarketcap.com//currencies/cryptoads-marketplace/	0.03
6077	Po.et	POE	https://coinmarketcap.com//currencies/poet/	0
6078	TrueDeck	TDP	https://coinmarketcap.com//currencies/truedeck/	0.01
6079	OptiToken	OPTI	https://coinmarketcap.com//currencies/optitoken/	0.01
6080	AtromG8	AG8	https://coinmarketcap.com//currencies/atromg8/	0.03
6081	ARAW	ARAW	https://coinmarketcap.com//currencies/araw/	0
6082	KIMCHI.finance	KIMCHI	https://coinmarketcap.com//currencies/kimchi-finance/	0
6083	SovranoCoin	SVR	https://coinmarketcap.com//currencies/sovranocoin/	0.29
6084	ImageCoin	IMG	https://coinmarketcap.com//currencies/imagecoin/	0.04
6085	NPCoin	NPC	https://coinmarketcap.com//currencies/npcoin/	0.01
6086	Masari	MSR	https://coinmarketcap.com//currencies/masari/	0.03
6087	AiLink Token	ALI	https://coinmarketcap.com//currencies/ailink-token/	0
6088	BidiPass	BDP	https://coinmarketcap.com//currencies/bidipass/	0
6089	Cryptonite	XCN	https://coinmarketcap.com//currencies/cryptonite/	0
6090	HashCoin	HSC	https://coinmarketcap.com//currencies/hashcoin/	0
6091	LunchMoney	LMY	https://coinmarketcap.com//currencies/lunchmoney/	0
6092	Qbao	QBT	https://coinmarketcap.com//currencies/qbao/	0.01
6093	Datamine	DAM	https://coinmarketcap.com//currencies/datamine/	0.09
6094	TerraCredit	CREDIT	https://coinmarketcap.com//currencies/credit/	0
6095	Faceter	FACE	https://coinmarketcap.com//currencies/faceter/	0
6096	Streamity	STM	https://coinmarketcap.com//currencies/streamity/	0.01
6097	TENA	TENA	https://coinmarketcap.com//currencies/tena/	0.13
6098	Starbase	STAR	https://coinmarketcap.com//currencies/starbase/	0
6099	Vidulum	VDL	https://coinmarketcap.com//currencies/vidulum/	0.05
6100	EtherGem	EGEM	https://coinmarketcap.com//currencies/ethergem/	0.02
6101	SwiftCash	SWIFT	https://coinmarketcap.com//currencies/swiftcash/	0
6102	SafeInsure	SINS	https://coinmarketcap.com//currencies/safeinsure/	0.02
6103	SHIELD	XSH	https://coinmarketcap.com//currencies/shield-xsh/	0
6104	nYFI	N0031	https://coinmarketcap.com//currencies/nyfi/	0.14
6105	Alchemint Standards	SDS	https://coinmarketcap.com//currencies/alchemint-standards/	0
6106	Scorum Coins	SCR	https://coinmarketcap.com//currencies/scorum-coins/	0.01
6107	Hashshare	HSS	https://coinmarketcap.com//currencies/hashshare/	0
6108	Zeepin	ZPT	https://coinmarketcap.com//currencies/zeepin/	0
6109	Swing	SWING	https://coinmarketcap.com//currencies/swing/	0.07
6110	Gems 	GEM	https://coinmarketcap.com//currencies/gems-protocol/	0
6111	Datawallet	DXT	https://coinmarketcap.com//currencies/datawallet/	0
6112	ION	ION	https://coinmarketcap.com//currencies/ion/	0.03
6113	iEthereum	IETH	https://coinmarketcap.com//currencies/iethereum/	0.02
6114	QUINADS	QUIN	https://coinmarketcap.com//currencies/quinads/	0
6115	NANJCOIN	NANJ	https://coinmarketcap.com//currencies/nanjcoin/	0
6116	Ether Zero	ETZ	https://coinmarketcap.com//currencies/ether-zero/	0
6117	Kora Network Token	KNT	https://coinmarketcap.com//currencies/kora-network-token/	0
6118	Coinsuper Ecosystem Network	CEN	https://coinmarketcap.com//currencies/coinsuper-ecosystem-network/	0
6119	Axe	AXE	https://coinmarketcap.com//currencies/axe/	0.07
6120	True Seigniorage Dollar	TSD	https://coinmarketcap.com//currencies/true-seigniorage-dollar/	0.01
6121	AllSafe	ASAFE	https://coinmarketcap.com//currencies/allsafe/	0.04
6122	FLIP	FLP	https://coinmarketcap.com//currencies/flip/	0.01
6123	Energycoin	ENRG	https://coinmarketcap.com//currencies/energycoin/	0
6124	Bitsdaq	BQQQ	https://coinmarketcap.com//currencies/bitsdaq/	0
6125	Sentinel Chain	SENC	https://coinmarketcap.com//currencies/sentinel-chain/	0
6126	Tokenbox	TBX	https://coinmarketcap.com//currencies/tokenbox/	0.03
6127	Felixo Coin	FLX	https://coinmarketcap.com//currencies/felixo-coin/	0
6128	ZelaaPayAE	ZPAE	https://coinmarketcap.com//currencies/zelaapayae/	0
6129	Bitstar	BITS	https://coinmarketcap.com//currencies/bitstar/	0.02
6130	Graft	GRFT	https://coinmarketcap.com//currencies/graft/	0
6131	WandX	WAND	https://coinmarketcap.com//currencies/wandx/	0.03
6132	DopeCoin	DOPE	https://coinmarketcap.com//currencies/dopecoin/	0
6133	Xeonbit	XNB	https://coinmarketcap.com//currencies/xeonbit/	0
6134	InvestDigital	IDT	https://coinmarketcap.com//currencies/investdigital/	0
6135	BlueCoin	BLU	https://coinmarketcap.com//currencies/bluecoin/	0
6136	InvestFeed	IFT	https://coinmarketcap.com//currencies/investfeed/	0
6137	Bigbom	BBO	https://coinmarketcap.com//currencies/bigbom/	0
6138	HOQU	HQX	https://coinmarketcap.com//currencies/hoqu/	0
6139	Parkgene	GENE	https://coinmarketcap.com//currencies/parkgene/	0
6140	Innovative Bioresearch Coin	INNBC	https://coinmarketcap.com//currencies/innovative-bioresearch-coin/	0
6141	Bitether	BTR	https://coinmarketcap.com//currencies/bitether/	0.01
6142	SynLev	SYN	https://coinmarketcap.com//currencies/synlev/	0.03
6143	adToken	ADT	https://coinmarketcap.com//currencies/adtoken/	0
6144	Aeron	ARNX	https://coinmarketcap.com//currencies/aeron/	0.009069
6145	MEXC Token	MEXC	https://coinmarketcap.com//currencies/mexc-token/	0.0003
6146	Scrypta	LYRA	https://coinmarketcap.com//currencies/scrypta/	0.01863
6147	Asura Coin	ASA	https://coinmarketcap.com//currencies/asura-coin/	0.001366
6148	Maverick Chain	MVC	https://coinmarketcap.com//currencies/maverick-chain/	0.00278
6149	GoNetwork	GOT	https://coinmarketcap.com//currencies/gonetwork/	0.004111
6150	2GIVE	2GIVE	https://coinmarketcap.com//currencies/2give/	0.000556
6151	pulltherug.finance	RUGZ	https://coinmarketcap.com//currencies/rugz/	30.86
6152	IQ.cash	IQ	https://coinmarketcap.com//currencies/iqcash/	0.02396
6153	GoByte	GBX	https://coinmarketcap.com//currencies/gobyte/	0.03003
6154	Ellaism	ELLA	https://coinmarketcap.com//currencies/ellaism/	0.01
6155	Decentralized Crypto Token	DCTO	https://coinmarketcap.com//currencies/decentralized-crypto-token/	0
6156	Beetle Coin	BEET	https://coinmarketcap.com//currencies/beetle-coin/	0
6157	Matrexcoin	MAC	https://coinmarketcap.com//currencies/matrexcoin/	0.01
6158	Bezop	BEZ	https://coinmarketcap.com//currencies/bezop/	0.01
6159	Italian Lira	ITL	https://coinmarketcap.com//currencies/italian-lira/	0
6160	HeroNode	HER	https://coinmarketcap.com//currencies/heronode/	0
6161	BitCash	BITC	https://coinmarketcap.com//currencies/bitcash/	0.01
6162	IONChain	IONC	https://coinmarketcap.com//currencies/ionchain/	0
6163	Phoneum	PHT	https://coinmarketcap.com//currencies/phoneum/	0
6164	AC3	AC3	https://coinmarketcap.com//currencies/ac3/	0
6165	Aigang	AIX	https://coinmarketcap.com//currencies/aigang/	0.01
6166	Castweet	CTT	https://coinmarketcap.com//currencies/castweet/	0.15
6167	MODEL-X-coin	MODX	https://coinmarketcap.com//currencies/model-x-coin/	0.02
6168	PDATA	PDATA	https://coinmarketcap.com//currencies/pdata/	0
6169	Zilla	ZLA	https://coinmarketcap.com//currencies/zilla/	0
6170	Jetcoin	JET	https://coinmarketcap.com//currencies/jetcoin/	0.03
6171	Ouroboros	OURO	https://coinmarketcap.com//currencies/ouroboros/	0.01
6172	CryptoCarbon	CCRB	https://coinmarketcap.com//currencies/cryptocarbon/	0.01
6173	PluraCoin	PLURA	https://coinmarketcap.com//currencies/pluracoin/	0
6174	Kuverit	KUV	https://coinmarketcap.com//currencies/kuverit/	0
6175	Beer Money	BEER	https://coinmarketcap.com//currencies/beer-money/	0.01
6176	ParallelCoin	DUO	https://coinmarketcap.com//currencies/parallelcoin/	0.8
6177	Dogeswap	DOGES	https://coinmarketcap.com//currencies/dogeswap/	12.58
6178	Yfscience	YFSI	https://coinmarketcap.com//currencies/yfscience/	18.2
6179	Fortuna	FOTA	https://coinmarketcap.com//currencies/fortuna/	0
6180	HollyWoodCoin	HWC	https://coinmarketcap.com//currencies/hollywoodcoin/	0.01
6181	Sharpay	S	https://coinmarketcap.com//currencies/sharpay/	0
6182	PoSW Coin	POSW	https://coinmarketcap.com//currencies/posw-coin/	0.01
6183	MicroMoney	AMM	https://coinmarketcap.com//currencies/micromoney/	0.02
6184	Advanced Technology Coin	ARC	https://coinmarketcap.com//currencies/arcticcoin/	0.01
6185	CROAT	CROAT	https://coinmarketcap.com//currencies/croat/	0
6186	HeartBout	HB	https://coinmarketcap.com//currencies/heartbout/	0
6187	DOGEFI	DOGEFI	https://coinmarketcap.com//currencies/dogefi/	0.24
6188	Wrapped Virgin Gen-0 CryptoKitties	WVG0	https://coinmarketcap.com//currencies/wrapped-virgin-gen-0-cryptokitties/	690.14
6189	CommunityGeneration	CGEN	https://coinmarketcap.com//currencies/communitygeneration/	0
6190	Kobocoin	KOBO	https://coinmarketcap.com//currencies/kobocoin/	0.01
6191	Baz Token	BAZT	https://coinmarketcap.com//currencies/baz-token/	0.21
6192	Terracoin	TRC	https://coinmarketcap.com//currencies/terracoin/	0.01
6193	AceD	ACED	https://coinmarketcap.com//currencies/aced/	0
6194	Cobinhood	COB	https://coinmarketcap.com//currencies/cobinhood/	0
6195	DecentBet	DBET	https://coinmarketcap.com//currencies/decent-bet/	0
6196	ATC Coin	ATCC	https://coinmarketcap.com//currencies/atc-coin/	0
6197	MMOCoin	MMO	https://coinmarketcap.com//currencies/mmocoin/	0
6198	Cryptojacks	CJ	https://coinmarketcap.com//currencies/cryptojacks/	0
6199	Dollars	USDX	https://coinmarketcap.com//currencies/dollars/	0.12
6200	PAYCENT	PYN	https://coinmarketcap.com//currencies/paycent/	0
6201	Innova	INN	https://coinmarketcap.com//currencies/innova/	0.03
6202	CryptoEnergy	CNRG	https://coinmarketcap.com//currencies/cryptoenergy/	8
6203	Utrum	OOT	https://coinmarketcap.com//currencies/utrum/	0
6204	Deri Protocol	DERI	https://coinmarketcap.com//currencies/deri-protocol/	0.2
6205	Bonpay	BON	https://coinmarketcap.com//currencies/bonpay/	0.02
6206	Fiii	FIII	https://coinmarketcap.com//currencies/fiii/	0
6207	1Million Token	1MT	https://coinmarketcap.com//currencies/1million-token/	0.26
6208	Ryo Currency	RYO	https://coinmarketcap.com//currencies/ryo-currency/	0.01
6209	EveriToken	EVT	https://coinmarketcap.com//currencies/everitoken/	0.01
6210	Helix	HLIX	https://coinmarketcap.com//currencies/helix/	0.01
6211	FLUX	FLUX	https://coinmarketcap.com//currencies/flux/	0.61
6212	Simple Software Solutions	SSS	https://coinmarketcap.com//currencies/simple-software-solutions/	0.08
6213	Opal	OPAL	https://coinmarketcap.com//currencies/opal/	0.01
6214	Ubricoin	UBN	https://coinmarketcap.com//currencies/ubricoin/	0
6215	Netko	NETKO	https://coinmarketcap.com//currencies/netko/	0.02
6216	MorCrypto Coin	MOR	https://coinmarketcap.com//currencies/morcrypto-coin/	0.01
6217	Commercium	CMM	https://coinmarketcap.com//currencies/commercium/	0
6218	Hush	HUSH	https://coinmarketcap.com//currencies/hush/	0.02
6219	SuperCoin	SUPER	https://coinmarketcap.com//currencies/supercoin/	0
6220	TenUp	TUP	https://coinmarketcap.com//currencies/tenup/	0.01
6221	Digitalcoin	DGC	https://coinmarketcap.com//currencies/digitalcoin/	0.01
6222	Martkist	MARTK	https://coinmarketcap.com//currencies/martkist/	0.01
6223	Leadcoin	LDC	https://coinmarketcap.com//currencies/leadcoin/	0
6224	BBSCoin	BBS	https://coinmarketcap.com//currencies/bbscoin/	0
6225	DraftCoin	DFT	https://coinmarketcap.com//currencies/draftcoin/	0.02
6226	SHPING	SHPING	https://coinmarketcap.com//currencies/shping/	0
6227	Storiqa	STQ	https://coinmarketcap.com//currencies/storiqa/	0
6228	Inex Project	INEX	https://coinmarketcap.com//currencies/inex-project/	0
6229	TagCoin	TAG	https://coinmarketcap.com//currencies/tagcoin/	0.03
6230	Baguette Token	BGTT	https://coinmarketcap.com//currencies/baguette-token/	0.02
6231	YMPL	YMPL	https://coinmarketcap.com//currencies/ympl/	4.4
6232	TCASH	TCASH	https://coinmarketcap.com//currencies/tcash/	0.01
6233	Proton Token	PTT	https://coinmarketcap.com//currencies/proton-token/	0
6234	Agora	VOTE	https://coinmarketcap.com//currencies/agora/	0.01
6235	LHT	LHT	https://coinmarketcap.com//currencies/lighthouse-token/	0
6236	MotaCoin	MOTA	https://coinmarketcap.com//currencies/motacoin/	0.01
6237	MesChain	MES	https://coinmarketcap.com//currencies/meschain/	0
6238	Bela	BELA	https://coinmarketcap.com//currencies/belacoin/	0
6239	Citadel	CTL	https://coinmarketcap.com//currencies/citadel/	0.02
6240	Pyrk	PYRK	https://coinmarketcap.com//currencies/pyrk/	0.01
6241	Pirl	PIRL	https://coinmarketcap.com//currencies/pirl/	0
6242	Decentralized Machine Learning	DML	https://coinmarketcap.com//currencies/decentralized-machine-learning/	0
6243	Bitgesell	BGL	https://coinmarketcap.com//currencies/bitgesell/	0.02
6322	Rimbit	RBT	https://coinmarketcap.com//currencies/rimbit/	0
6244	Bolivarcoin	BOLI	https://coinmarketcap.com//currencies/bolivarcoin/	0.01212
6245	Motocoin	MOTO	https://coinmarketcap.com//currencies/motocoin/	0.009452
6246	OSA Token	OSA	https://coinmarketcap.com//currencies/osa-token/	0.0001334
6247	TOKPIE	TKP	https://coinmarketcap.com//currencies/tokpie/	0.006454
6248	AquariusCoin	ARCO	https://coinmarketcap.com//currencies/aquariuscoin/	0.05343
6249	renDOGE	RENDOGE	https://coinmarketcap.com//currencies/rendoge/	0.05802
6250	Gentarium	GTM	https://coinmarketcap.com//currencies/gentarium/	0.03447
6251	Bitcoin Token	BTCT	https://coinmarketcap.com//currencies/bitcoin-token/	0.0139
6252	imbrex	REX	https://coinmarketcap.com//currencies/imbrex/	0.02051
6253	Arionum	ARO	https://coinmarketcap.com//currencies/arionum/	0.001112
6254	WinCash	WCC	https://coinmarketcap.com//currencies/wincash/	0.12
6255	Fire Lotto	FLOT	https://coinmarketcap.com//currencies/fire-lotto/	0.01
6256	Semux	SEM	https://coinmarketcap.com//currencies/semux/	0.01
6257	Chronologic	DAY	https://coinmarketcap.com//currencies/chronologic/	0.15
6258	PosEx	PEX	https://coinmarketcap.com//currencies/posex/	0.07
6259	GINcoin	GIN	https://coinmarketcap.com//currencies/gincoin/	0.02
6260	Taklimakan Network	TAN	https://coinmarketcap.com//currencies/taklimakan-network/	0
6261	SkinCoin	SKIN	https://coinmarketcap.com//currencies/skincoin/	0
6262	Thrive Token	THRT	https://coinmarketcap.com//currencies/thrive-token/	0
6263	More Coin	MORE	https://coinmarketcap.com//currencies/more-coin/	0.08
6264	Artis Turba	ARTIS	https://coinmarketcap.com//currencies/artis-turba/	0.01
6265	Adzcoin	ADZ	https://coinmarketcap.com//currencies/adzcoin/	0
6266	Fantasy Sports	DFS	https://coinmarketcap.com//currencies/fantasy-sports/	0.03
6267	EnterCoin	ENTRC	https://coinmarketcap.com//currencies/entercoin/	0.03
6268	wave edu coin	WEC	https://coinmarketcap.com//currencies/wave-edu-coin/	0
6269	Defis	XGM	https://coinmarketcap.com//currencies/defis/	0
6270	Photon	PHO	https://coinmarketcap.com//currencies/photon/	0
6271	Cashhand	CHND	https://coinmarketcap.com//currencies/cashhand/	0
6272	ITO Utility Token	IUT	https://coinmarketcap.com//currencies/ito-utility-token/	0
6273	CRDT	CRDT	https://coinmarketcap.com//currencies/crdt/	0.03
6274	Evil Coin	EVIL	https://coinmarketcap.com//currencies/evil-coin/	0.01
6275	TouchCon	TOC	https://coinmarketcap.com//currencies/touchcon/	0
6276	PopularCoin	POP	https://coinmarketcap.com//currencies/popularcoin/	0
6277	vSlice	VSL	https://coinmarketcap.com//currencies/vslice/	0
6278	GravityCoin	GXX	https://coinmarketcap.com//currencies/gravitycoin/	0.03
6279	Anoncoin	ANC	https://coinmarketcap.com//currencies/anoncoin/	0.07
6280	Donu	DONU	https://coinmarketcap.com//currencies/donu/	0.03
6281	PEPS Coin	PEPS	https://coinmarketcap.com//currencies/peps-coin/	0
6282	Bata	BTA	https://coinmarketcap.com//currencies/bata/	0.03
6283	Nasdacoin	NSD	https://coinmarketcap.com//currencies/nasdacoin/	0.01
6284	Litecoin Plus	LCP	https://coinmarketcap.com//currencies/litecoin-plus/	0.05
6285	TEAM (TokenStars)	TEAM	https://coinmarketcap.com//currencies/tokenstars/	0.01
6286	BitSend	BSD	https://coinmarketcap.com//currencies/bitsend/	0
6287	Emerald Crypto	EMD	https://coinmarketcap.com//currencies/emerald/	0.01
6288	Rebased	REB2	https://coinmarketcap.com//currencies/rebased/	0.95
6289	Monkey Project	MONK	https://coinmarketcap.com//currencies/monkey-project/	0.01
6290	Worldcore	WRC	https://coinmarketcap.com//currencies/worldcore/	0
6291	Mainstream For The Underground	MFTU	https://coinmarketcap.com//currencies/mainstream-for-the-underground/	0
6292	INRToken	INRT	https://coinmarketcap.com//currencies/inrtoken/	0
6293	Crowd Machine	CMCT	https://coinmarketcap.com//currencies/crowd-machine/	0
6294	HorusPay	HORUS	https://coinmarketcap.com//currencies/horuspay/	0
6295	SYB Coin	SYBC	https://coinmarketcap.com//currencies/syb-coin/	0.01
6296	VULCANO	VULC	https://coinmarketcap.com//currencies/vulcano/	0
6297	ZINC	ZINC	https://coinmarketcap.com//currencies/zinc/	0.02
6298	TRAXIA	TM2	https://coinmarketcap.com//currencies/traxia/	0
6299	Yocoin	YOC	https://coinmarketcap.com//currencies/yocoin/	0
6300	GCN Coin	GCN	https://coinmarketcap.com//currencies/gcn-coin/	0
6301	BitWhite	BTW	https://coinmarketcap.com//currencies/bitwhite/	0
6302	Boolberry	BBR	https://coinmarketcap.com//currencies/boolberry/	0.01
6303	MojoCoin	MOJO	https://coinmarketcap.com//currencies/mojocoin/	0.01
6304	Bitcoin Interest	BCI	https://coinmarketcap.com//currencies/bitcoin-interest/	0.01
6305	Aditus	ADI	https://coinmarketcap.com//currencies/aditus/	0
6306	Raise	RAISE	https://coinmarketcap.com//currencies/raise/	0.01
6307	W Green Pay	WGP	https://coinmarketcap.com//currencies/w-green-pay/	0.01
6308	Datarius Credit	DTRC	https://coinmarketcap.com//currencies/datarius-credit/	0
6309	Narrative	NRVE	https://coinmarketcap.com//currencies/narrative/	0
6310	PengolinCoin	PGO	https://coinmarketcap.com//currencies/pengolincoin/	0.01
6311	Global Currency Reserve	GCR	https://coinmarketcap.com//currencies/global-currency-reserve/	0
6312	Rupee	RUP	https://coinmarketcap.com//currencies/rupee/	0
6313	EtherInc	ETI	https://coinmarketcap.com//currencies/etherinc/	0
6314	Cheesecoin	CHEESE	https://coinmarketcap.com//currencies/cheesecoin/	0
6315	Zetacoin	ZET	https://coinmarketcap.com//currencies/zetacoin/	0
6316	BTC Lite	BTCL	https://coinmarketcap.com//currencies/btc-lite/	0.01
6317	InterValue	INVE	https://coinmarketcap.com//currencies/intervalue/	0
6318	BitCoen	BEN	https://coinmarketcap.com//currencies/bitcoen/	0.02
6319	Bitcoin Zero	BZX	https://coinmarketcap.com//currencies/bitcoin-zero/	0
6320	Silverway	SLV	https://coinmarketcap.com//currencies/silverway/	0
6321	Cyber Movie Chain	CMCT	https://coinmarketcap.com//currencies/cyber-movie-chain/	0
6323	Ignition	IC	https://coinmarketcap.com//currencies/ignition/	0.08
6324	Dynamite	DYNMT	https://coinmarketcap.com//currencies/dynamite/	0.29
6325	Bitcoin Incognito	XBI	https://coinmarketcap.com//currencies/bitcoin-incognito/	0.01
6326	EUNOMIA	ENTS	https://coinmarketcap.com//currencies/eunomia/	0
6327	DABANKING	DAB	https://coinmarketcap.com//currencies/dabanking/	0.02
6328	MktCoin	MLM	https://coinmarketcap.com//currencies/mktcoin/	0
6329	Elysian	ELY	https://coinmarketcap.com//currencies/elysian/	0
6330	Xuez	XUEZ	https://coinmarketcap.com//currencies/xuez/	0.03
6331	Veros	VRS	https://coinmarketcap.com//currencies/veros/	0.09
6332	Rubies	RBIES	https://coinmarketcap.com//currencies/rubies/	0.01
6333	Micromines	MICRO	https://coinmarketcap.com//currencies/micromines/	0
6334	Eroscoin	ERO	https://coinmarketcap.com//currencies/eroscoin/	0
6335	Fivebalance	FBN	https://coinmarketcap.com//currencies/fivebalance/	0
6336	Kurrent	KURT	https://coinmarketcap.com//currencies/kurrent/	0
6337	Ratecoin	XRA	https://coinmarketcap.com//currencies/ratecoin/	0
6338	SteepCoin	STEEP	https://coinmarketcap.com//currencies/steepcoin/	0
6339	ProxyNode	PRX	https://coinmarketcap.com//currencies/proxynode/	0
6340	ShowHand	HAND	https://coinmarketcap.com//currencies/showhand/	0
6341	MFCoin	MFC	https://coinmarketcap.com//currencies/mfcoin/	0
6342	Bintex Futures	BNTX	https://coinmarketcap.com//currencies/bintex-futures/	0.98
6343	PlayGame	PXG	https://coinmarketcap.com//currencies/playgame/	0
6344	Era Swap	ES	https://coinmarketcap.com//currencies/era-swap/	0.005035
6345	Abitshadow Token	ABST	https://coinmarketcap.com//currencies/abitshadow-token/	0.0002086
6346	Helex	HLX	https://coinmarketcap.com//currencies/helex/	1.4
6347	YENTEN	YTN	https://coinmarketcap.com//currencies/yenten/	0.004025
6348	Civitas	CIV	https://coinmarketcap.com//currencies/civitas/	0.01223
6349	FirstCoin	FRST	https://coinmarketcap.com//currencies/firstcoin/	0.003122
6350	Plus-Coin	NPLC	https://coinmarketcap.com//currencies/plus-coin/	0.0002063
6351	Truegame	TGAME	https://coinmarketcap.com//currencies/tgame/	0.001168
6352	SiaCashCoin	SCC	https://coinmarketcap.com//currencies/siacashcoin/	6.29e-06
6353	STRAKS	STAK	https://coinmarketcap.com//currencies/straks/	0.004003
6354	Waletoken	WTN	https://coinmarketcap.com//currencies/waletoken/	0
6355	Neuromorphic.io	NMP	https://coinmarketcap.com//currencies/neuromorphic-io/	0.02
6356	WeBlock	WON	https://coinmarketcap.com//currencies/weblock/	0.02
6357	Bitcoin CZ	BCZ	https://coinmarketcap.com//currencies/bitcoin-cz/	0.03
6358	Unify	UNIFY	https://coinmarketcap.com//currencies/unify/	0
6359	CBDAO	BREE	https://coinmarketcap.com//currencies/cbdao/	0.09
6360	Zloadr	ZDR	https://coinmarketcap.com//currencies/zloadr/	0.02
6361	Provoco Token	VOCO	https://coinmarketcap.com//currencies/provoco-token/	0
6362	Paytomat	PTI	https://coinmarketcap.com//currencies/paytomat/	0
6363	Coin2.1	C2	https://coinmarketcap.com//currencies/coin2-1/	0
6364	Thingschain	TIC	https://coinmarketcap.com//currencies/thingschain/	0
6365	GoldFund	GFUN	https://coinmarketcap.com//currencies/goldfund/	0
6366	SafeCapital	SCAP	https://coinmarketcap.com//currencies/safecapital/	0.05
6367	SF Capital	SFCP	https://coinmarketcap.com//currencies/sf-capital/	0
6368	Veles	VLS	https://coinmarketcap.com//currencies/veles/	0.06
6369	FSBT API Token	FSBT	https://coinmarketcap.com//currencies/fsbt-api-token/	0.02
6370	YFFII Finance	YFFII	https://coinmarketcap.com//currencies/yffii-finance/	2.64
6371	StarDEX	XSTAR	https://coinmarketcap.com//currencies/stardex/	0.01
6372	BERNcash	BERN	https://coinmarketcap.com//currencies/berncash/	0
6373	GuccioneCoin	GCC	https://coinmarketcap.com//currencies/guccionecoin/	0
6374	Niobio Cash	NBR	https://coinmarketcap.com//currencies/niobio-cash/	0
6375	IGToken	IG	https://coinmarketcap.com//currencies/igtoken/	0
6376	MalwareChain	MALW	https://coinmarketcap.com//currencies/malwarechain/	0.01
6377	Bitcoin Free Cash	BFC	https://coinmarketcap.com//currencies/bitcoin-free-cash/	0.02
6378	yTSLA Finance	yTSLA	https://coinmarketcap.com//currencies/ytsla-finance/	1.13
6379	Kush Finance	KSEED	https://coinmarketcap.com//currencies/kush-finance/	0.27
6380	CPUchain	CPU	https://coinmarketcap.com//currencies/cpuchain/	0
6381	ebakus	EBK	https://coinmarketcap.com//currencies/ebakus/	0
6382	Biotron	BTRN	https://coinmarketcap.com//currencies/biotron/	0
6383	WABnetwork	WAB	https://coinmarketcap.com//currencies/wabnetwork/	0
6384	Acoin	ACOIN	https://coinmarketcap.com//currencies/acoin/	0.06
6385	BoutsPro	BOUTS	https://coinmarketcap.com//currencies/boutspro/	0
6386	Universal Currency	UNIT	https://coinmarketcap.com//currencies/universal-currency/	0
6387	Fox Trading	FOXT	https://coinmarketcap.com//currencies/fox-trading/	0.01
6388	Swarm City	SWT	https://coinmarketcap.com//currencies/swarm-city/	0.01
6389	Webcoin	WEB	https://coinmarketcap.com//currencies/webcoin/	0
6390	S.Finance	SFG	https://coinmarketcap.com//currencies/s-finance/	0.83
6391	ATBCoin	ATB	https://coinmarketcap.com//currencies/atbcoin/	0
6392	PureVidz	VIDZ	https://coinmarketcap.com//currencies/purevidz/	0
6393	Kind Ads Token	KIND	https://coinmarketcap.com//currencies/kind-ads-token/	0
6394	Giant	GIC	https://coinmarketcap.com//currencies/giant-coin/	0.01
6395	EvenCoin	EVN	https://coinmarketcap.com//currencies/evencoin/	0
6396	Arion	ARION	https://coinmarketcap.com//currencies/arion/	0.01
6397	VoteCoin	VOT	https://coinmarketcap.com//currencies/votecoin/	0
6398	Privatix	PRIX	https://coinmarketcap.com//currencies/privatix/	0.06
6399	GlobalToken	GLT	https://coinmarketcap.com//currencies/globaltoken/	0
6400	Ethereum Gold Project	ETGP	https://coinmarketcap.com//currencies/ethereum-gold-project/	0
6401	CFun	CFUN	https://coinmarketcap.com//currencies/cfun/	0
6402	Blocklancer	LNC	https://coinmarketcap.com//currencies/blocklancer/	0
6403	Yearn Finance Bit	YFBT	https://coinmarketcap.com//currencies/yearn-finance-bit/	60.36
6404	Freyrchain	FREC	https://coinmarketcap.com//currencies/freyrchain/	0
6405	Arqma	ARQ	https://coinmarketcap.com//currencies/arqma/	0.01
6406	PutinCoin	PUT	https://coinmarketcap.com//currencies/putincoin/	0
6407	Cipher	CPR	https://coinmarketcap.com//currencies/cipher/	0
6408	YFIVE FINANCE	YFIVE	https://coinmarketcap.com//currencies/yfive-finance/	3.57
6409	SmartCoin	SMC	https://coinmarketcap.com//currencies/smartcoin/	0
6410	Atheios	ATH	https://coinmarketcap.com//currencies/atheios/	0
6411	MedicCoin	MEDIC	https://coinmarketcap.com//currencies/mediccoin/	0
6412	INDINODE	XIND	https://coinmarketcap.com//currencies/indinode/	0
6413	Universe	UNI	https://coinmarketcap.com//currencies/universe/	0
6414	ELTCOIN	ELTCOIN	https://coinmarketcap.com//currencies/eltcoin/	0
6415	BitRewards	BIT	https://coinmarketcap.com//currencies/bitrewards/	0
6416	Scanetchain	SWC	https://coinmarketcap.com//currencies/scanetchain/	0
6417	GoldBlocks	GB	https://coinmarketcap.com//currencies/goldblocks/	0
6418	CoinUs	CNUS	https://coinmarketcap.com//currencies/coinus/	0
6419	IFX24	IFX24	https://coinmarketcap.com//currencies/ifx24/	0.02
6420	BitGuild PLAT	PLAT	https://coinmarketcap.com//currencies/bitguild-plat/	0
6421	Onix	ONX	https://coinmarketcap.com//currencies/onix/	0
6422	ICOBID	ICOB	https://coinmarketcap.com//currencies/icobid/	0
6423	UNICORN Token	UNI	https://coinmarketcap.com//currencies/unicorn-token/	0
6424	Jade Currency	JADE	https://coinmarketcap.com//currencies/jade-currency/	0
6425	StarterCoin	STAC	https://coinmarketcap.com//currencies/startercoin/	0
6426	Digital Money Bits	DMB	https://coinmarketcap.com//currencies/digital-money-bits/	0
6427	MESEFA	SEFA	https://coinmarketcap.com//currencies/mesefa/	0.14
6428	Tourist Token	TOTO	https://coinmarketcap.com//currencies/tourist-token/	0
6429	Bitcoiin	B2G	https://coinmarketcap.com//currencies/bitcoiin/	0
6430	Zuflo Coin	ZFL	https://coinmarketcap.com//currencies/zuflo-coin/	0
6431	ArbitrageCT	ARCT	https://coinmarketcap.com//currencies/arbitragect/	0
6432	Bitnation	XPAT	https://coinmarketcap.com//currencies/bitnation/	0
6433	Trexcoin	TREX	https://coinmarketcap.com//currencies/trexcoin/	0
6434	ALL BEST ICO	ALLBI	https://coinmarketcap.com//currencies/all-best-ico/	0
6435	Theresa May Coin	MAY	https://coinmarketcap.com//currencies/theresa-may-coin/	0
6436	Peet DeFi	PTE	https://coinmarketcap.com//currencies/peet-defi/	1.39
6437	Cream	CRM	https://coinmarketcap.com//currencies/cream/	0
6438	Blakecoin	BLC	https://coinmarketcap.com//currencies/blakecoin/	0
6439	Bitcoin Adult	BTAD	https://coinmarketcap.com//currencies/bitcoin-adult/	0
6440	Herbalist Token	HERB	https://coinmarketcap.com//currencies/herbalist-token/	0
6441	BLAST	BLAST	https://coinmarketcap.com//currencies/blast/	0
6442	PostCoin	POST	https://coinmarketcap.com//currencies/postcoin/	0
6443	Neural Protocol	NRP	https://coinmarketcap.com//currencies/neural-protocol/	0
6444	Digiwage	WAGE	https://coinmarketcap.com//currencies/digiwage/	0.001112
6445	Knekted	KNT	https://coinmarketcap.com//currencies/knekted/	5.185e-05
6446	EthereumX	ETX	https://coinmarketcap.com//currencies/ethereumx/	0.0005856
6447	INMAX	INX	https://coinmarketcap.com//currencies/inmax/	0.01972
6448	ETHplode	ETHPLO	https://coinmarketcap.com//currencies/ethplode/	0.001149
6449	Compound Coin	COMP	https://coinmarketcap.com//currencies/compound-coin/	9.284e-07
6450	iBTC	IBTC	https://coinmarketcap.com//currencies/ibtc/	0.0009875
6451	Bitradio	BRO	https://coinmarketcap.com//currencies/bitradio/	0.005
6452	Zurcoin	ZUR	https://coinmarketcap.com//currencies/zurcoin/	0.000556
6453	Kemacoin	KEMA	https://coinmarketcap.com//currencies/kemacoin/	0.002002
6454	Decentralized Asset Trading Platform	DATP	https://coinmarketcap.com//currencies/decentralized-asset-trading-platform/	0
6455	FidexToken	FEX	https://coinmarketcap.com//currencies/fidex-token/	0
6456	SnodeCoin	SND	https://coinmarketcap.com//currencies/snodecoin/	0
6457	MultiCoinCasino	MCC	https://coinmarketcap.com//currencies/multicoincasino/	0.01
6458	Cryptrust	CTRT	https://coinmarketcap.com//currencies/cryptrust/	0
6459	FUZE Token	FUZE	https://coinmarketcap.com//currencies/fuze-token/	60.93
6460	FuzzBalls	FUZZ	https://coinmarketcap.com//currencies/fuzzballs/	0.01
6461	Arepacoin	AREPA	https://coinmarketcap.com//currencies/arepacoin/	0
6462	ACE (TokenStars)	ACE	https://coinmarketcap.com//currencies/ace/	0
6463	DistX	DISTX	https://coinmarketcap.com//currencies/distx/	0
6464	Obee Network	OBEE	https://coinmarketcap.com//currencies/obee-network/	0
6465	Dreamcoin	DRM	https://coinmarketcap.com//currencies/dreamcoin/	0.02
6466	HyperQuant	HQT	https://coinmarketcap.com//currencies/hyperquant/	0
6467	PYRO Network	PYRO	https://coinmarketcap.com//currencies/pyro-network/	0
6468	The Currency Analytics	TCAT	https://coinmarketcap.com//currencies/the-currency-analytics/	0
6469	Fireball	FIRE	https://coinmarketcap.com//currencies/fireball/	2.16
6470	Connect Coin	XCON	https://coinmarketcap.com//currencies/connect-coin/	0
6471	Centurion	CNT	https://coinmarketcap.com//currencies/centurion/	0
6472	CyberMusic	CYMT	https://coinmarketcap.com//currencies/cybermusic/	0
6473	Save Environment Token	SET	https://coinmarketcap.com//currencies/save-environment-token/	0.04
6474	SparksPay	SPK	https://coinmarketcap.com//currencies/sparkspay/	0
6475	Bitzeny	ZNY	https://coinmarketcap.com//currencies/bitzeny/	0
6476	Italo	XTA	https://coinmarketcap.com//currencies/italo/	0
6477	Kalkulus	KLKS	https://coinmarketcap.com//currencies/kalkulus/	0
6555	Akroma	AKA	https://coinmarketcap.com//currencies/akroma/	0
6478	BLOC.MONEY	BLOC	https://coinmarketcap.com//currencies/bloc-money/	0
6479	EDRCoin	EDRC	https://coinmarketcap.com//currencies/edrcoin/	0.01
6480	ZeusNetwork	ZEUS	https://coinmarketcap.com//currencies/zeusnetwork/	0
6481	Joint Ventures	JOINT	https://coinmarketcap.com//currencies/joint-ventures/	0
6482	ANON	ANON	https://coinmarketcap.com//currencies/anon/	0.01
6483	Yield Stake Finance	YI12	https://coinmarketcap.com//currencies/yield-stake-finance/	3.41
6484	Blockburn	BURN	https://coinmarketcap.com//currencies/blockburn/	0
6485	Celeum	CLX	https://coinmarketcap.com//currencies/celeum/	0.01
6486	BowsCoin	BSC	https://coinmarketcap.com//currencies/bowscoin/	0.01
6487	PRiVCY	PRIV	https://coinmarketcap.com//currencies/privcy/	0
6488	Blur	BLUR	https://coinmarketcap.com//currencies/blur/	0.01
6489	DNotes	NOTE	https://coinmarketcap.com//currencies/dnotes/	0
6490	Suretly	SUR	https://coinmarketcap.com//currencies/suretly/	0.15
6491	Gossip Coin	GOSS	https://coinmarketcap.com//currencies/gossipcoin/	0
6492	UChain	UCN	https://coinmarketcap.com//currencies/uchain/	0
6493	Payfair	PFR	https://coinmarketcap.com//currencies/payfair/	0
6494	BitcoiNote	BTCN	https://coinmarketcap.com//currencies/bitcoinote/	0
6495	Save and Gain	SANDG	https://coinmarketcap.com//currencies/save-and-gain/	0.01
6496	Mchain	MAR	https://coinmarketcap.com//currencies/mchain/	0
6497	Iridium	IRD	https://coinmarketcap.com//currencies/iridium/	0
6498	GeyserCoin	GSR	https://coinmarketcap.com//currencies/geysercoin/	0.02
6499	Nekonium	NUKO	https://coinmarketcap.com//currencies/nekonium/	0
6500	PayCoin	XPY	https://coinmarketcap.com//currencies/paycoin2/	0
6501	TrumpCoin	TRUMP	https://coinmarketcap.com//currencies/trumpcoin/	0.01
6502	Apollon	XAP	https://coinmarketcap.com//currencies/apollon/	0
6503	EXMR FDN	EXMR	https://coinmarketcap.com//currencies/exmr-fdn/	0.01
6504	Intelligent Trading Foundation	ITT	https://coinmarketcap.com//currencies/intelligent-trading-foundation/	0
6505	BitCoal	COAL	https://coinmarketcap.com//currencies/bitcoal/	0.01
6506	KWHCoin	KWH	https://coinmarketcap.com//currencies/kwhcoin/	0
6507	Galilel	GALI	https://coinmarketcap.com//currencies/galilel/	0
6508	Zenswap Network Token	ZNT	https://coinmarketcap.com//currencies/zenswap-network-token/	0
6509	Halving Token	HALV	https://coinmarketcap.com//currencies/halving-coin/	0.02
6510	Netrum	NTR	https://coinmarketcap.com//currencies/netrum/	0.01
6511	SCRIV NETWORK	SCRIV	https://coinmarketcap.com//currencies/scriv-network/	0
6512	Global Crypto Alliance	CALL	https://coinmarketcap.com//currencies/global-crypto-alliance/	0
6513	Shivers	SHVR	https://coinmarketcap.com//currencies/shivers/	0
6514	NevaCoin	NEVA	https://coinmarketcap.com//currencies/nevacoin/	0.01
6515	Ultragate	ULG	https://coinmarketcap.com//currencies/ultragate/	0
6516	APR Coin	APR	https://coinmarketcap.com//currencies/apr-coin/	0
6517	SWYFT	SWYFTT	https://coinmarketcap.com//currencies/swyft/	0.04
6518	ParkByte	PKB	https://coinmarketcap.com//currencies/parkbyte/	0.01
6519	Newton Coin Project	NCP	https://coinmarketcap.com//currencies/newton-coin-project/	0
6520	Natmin Pure Escrow	NAT	https://coinmarketcap.com//currencies/natmin-pure-escrow/	0
6521	Thore Cash	TCH	https://coinmarketcap.com//currencies/thore-cash/	0
6522	MustangCoin	MST	https://coinmarketcap.com//currencies/mustangcoin/	0.05
6523	Exosis	EXO	https://coinmarketcap.com//currencies/exosis/	0.05
6524	ROIyal Coin	ROCO	https://coinmarketcap.com//currencies/roiyal-coin/	0.02
6525	AmsterdamCoin	AMS	https://coinmarketcap.com//currencies/amsterdamcoin/	0
6526	Bionic	BNC	https://coinmarketcap.com//currencies/bionic/	0
6527	X-Coin	XCO	https://coinmarketcap.com//currencies/x-coin/	0
6528	BitMoney	BIT	https://coinmarketcap.com//currencies/bitmoney/	0
6529	Spectrum	SPT	https://coinmarketcap.com//currencies/spectrum/	0
6530	Mobile Crypto Pay Coin	MCPC	https://coinmarketcap.com//currencies/mobile-crypto-pay-coin/	0
6531	Zealium	NZL	https://coinmarketcap.com//currencies/zealium/	0
6532	Carebit	CARE	https://coinmarketcap.com//currencies/carebit/	0
6533	Californium	CF	https://coinmarketcap.com//currencies/californium/	0.01
6534	Ether Kingdoms Token	IMP	https://coinmarketcap.com//currencies/ether-kingdoms-token/	0
6535	Vivid Coin	VIVID	https://coinmarketcap.com//currencies/vivid-coin/	0
6536	Guider	GDR	https://coinmarketcap.com//currencies/guider/	0
6537	Dollarcoin	DLC	https://coinmarketcap.com//currencies/dollarcoin/	0
6538	SpreadCoin	SPR	https://coinmarketcap.com//currencies/spreadcoin/	0
6539	ARbit	ARB	https://coinmarketcap.com//currencies/arbit/	0
6540	MarteXcoin	MXT	https://coinmarketcap.com//currencies/martexcoin/	0.01
6541	TravelNote	TVNT	https://coinmarketcap.com//currencies/travelnote/	0.01
6542	Castle	CSTL	https://coinmarketcap.com//currencies/castle/	0
6543	Mining Core Coin	MCC	https://coinmarketcap.com//currencies/mining-core/	0
6544	TajCoin	TAJ	https://coinmarketcap.com//currencies/tajcoin/	0.001112
6545	Desire	DSR	https://coinmarketcap.com//currencies/desire/	0.002224
6546	Squorum	SQR	https://coinmarketcap.com//currencies/squorum/	0.001668
6547	noob.finance	$NOOB	https://coinmarketcap.com//currencies/noob-finance/	1.02
6548	Quantis Network	QUAN	https://coinmarketcap.com//currencies/quantis-network/	0.000556
6549	Datacoin	DTC	https://coinmarketcap.com//currencies/datacoin/	0.000556
6550	Kabberry Coin	KKC	https://coinmarketcap.com//currencies/kabberry-coin/	0.001112
6551	Bitcoin 21	XBTC21	https://coinmarketcap.com//currencies/bitcoin-21/	0.0278
6552	BoatPilot Token	NAVY	https://coinmarketcap.com//currencies/boat-pilot-token/	0.000296
6553	Diligence	IRA	https://coinmarketcap.com//currencies/diligence/	0.00104
6554	Phantomx	PNX	https://coinmarketcap.com//currencies/phantomx/	0
6556	onLEXpa	onLEXpa	https://coinmarketcap.com//currencies/onlexpa/	0
6557	Zayedcoin	ZYD	https://coinmarketcap.com//currencies/zayedcoin/	0
6558	Eurocoin	EUC	https://coinmarketcap.com//currencies/eurocoin/	0
6559	Hyper Speed Network	HSN	https://coinmarketcap.com//currencies/hyper-speed-network/	0
6560	Impleum	IMPL	https://coinmarketcap.com//currencies/impleum/	0
6561	KUN	KUN	https://coinmarketcap.com//currencies/kun/	10.2
6562	PlatinumBAR	XPTX	https://coinmarketcap.com//currencies/platinumbar/	0.01
6563	CrevaCoin	CREVA	https://coinmarketcap.com//currencies/crevacoin/	0
6564	XDNA	XDNA	https://coinmarketcap.com//currencies/xdna/	0
6565	PLNcoin	PLNC	https://coinmarketcap.com//currencies/plncoin/	0
6566	Paypex	PAYX	https://coinmarketcap.com//currencies/paypex/	0
6567	EagleX	EGX	https://coinmarketcap.com//currencies/eaglex/	0
6568	Sparkle Loyalty	SPRKL	https://coinmarketcap.com//currencies/sparkle-loyalty/	0
6569	GoHelpFund	HELP	https://coinmarketcap.com//currencies/gohelpfund/	0
6570	FUTURAX	FTXT	https://coinmarketcap.com//currencies/futurax/	0
6571	4NEW	KWATT	https://coinmarketcap.com//currencies/4new/	0
6572	Flowchain	FLC	https://coinmarketcap.com//currencies/flowchain/	0.02
6573	SongCoin	SONG	https://coinmarketcap.com//currencies/songcoin/	0
6574	Nyerium	NYEX	https://coinmarketcap.com//currencies/nyerium/	0
6575	SkyHub Coin	SHB	https://coinmarketcap.com//currencies/skyhub-coin/	0.03
6576	XOVBank	XOV	https://coinmarketcap.com//currencies/xovbank/	0
6577	Bitcoinus	BITS	https://coinmarketcap.com//currencies/bitcoinus/	0
6578	Feellike	FLL	https://coinmarketcap.com//currencies/feellike/	0.01
6579	Galactrum	ORE	https://coinmarketcap.com//currencies/galactrum/	0
6580	ImageCash	IMGC	https://coinmarketcap.com//currencies/imagecash/	0
6581	1X2 COIN	1X2	https://coinmarketcap.com//currencies/1x2-coin/	0
6582	Litecred	LTCR	https://coinmarketcap.com//currencies/litecred/	0
6583	aWSB	aWSB	https://coinmarketcap.com//currencies/awsb/	18.76
6584	Business Credit Alliance Chain	BCAC	https://coinmarketcap.com//currencies/business-credit-alliance-chain/	0
6585	Mirai	MRI	https://coinmarketcap.com//currencies/mirai/	0
6586	Electrum Dark	ELD	https://coinmarketcap.com//currencies/electrum-dark-eld/	0
6587	Klimatas	KTS	https://coinmarketcap.com//currencies/klimatas/	0.02
6588	Quebecoin	QBC	https://coinmarketcap.com//currencies/quebecoin/	0
6589	Streamit Coin	STREAM	https://coinmarketcap.com//currencies/streamit-coin/	0
6590	SpeedCash	SCS	https://coinmarketcap.com//currencies/speedcash/	0.03
6591	Grimm	GRIMM	https://coinmarketcap.com//currencies/grimm/	0
6592	Paparazzi	PAZZI	https://coinmarketcap.com//currencies/paparazzi/	0
6593	LuckySevenToken	LST	https://coinmarketcap.com//currencies/luckyseventoken/	0
6594	GenesisX	XGS	https://coinmarketcap.com//currencies/genesisx/	0
6595	Bulleon	BUL	https://coinmarketcap.com//currencies/bulleon/	0.01
6596	Prime-XI	PXI	https://coinmarketcap.com//currencies/prime-xi/	0
6597	MiloCoin	MILO	https://coinmarketcap.com//currencies/milocoin/	0
6598	IBStoken	IBS	https://coinmarketcap.com//currencies/ibstoken/	0.01
6599	Cabbage	CAB	https://coinmarketcap.com//currencies/cabbage/	0
6600	YoloCash	YLC	https://coinmarketcap.com//currencies/yolocash/	0
6601	CCUniverse	UVU	https://coinmarketcap.com//currencies/ccuniverse/	0
6602	Veltor	VLT	https://coinmarketcap.com//currencies/veltor/	0.02
6603	JavaScript Token	JS	https://coinmarketcap.com//currencies/javascript-token/	0
6604	ETHPlus	ETHP	https://coinmarketcap.com//currencies/ethplus/	0.01
6605	Soverain	SOVE	https://coinmarketcap.com//currencies/soverain/	0
6606	XMCT	XMCT	https://coinmarketcap.com//currencies/xmct/	0
6607	VectorAI	VEC2	https://coinmarketcap.com//currencies/vector/	0
6608	High Voltage	HVCO	https://coinmarketcap.com//currencies/high-voltage/	0.01
6609	Alpha Coin	APC	https://coinmarketcap.com//currencies/alpha-coin/	0
6610	Bitvolt	VOLT	https://coinmarketcap.com//currencies/bitvolt/	0
6611	MDtoken	MDTK	https://coinmarketcap.com//currencies/mdtoken/	0
6612	Bitcoin Classic	BXC	https://coinmarketcap.com//currencies/bitcoin-classic/	0.02
6613	Ragnarok	RAGNA	https://coinmarketcap.com//currencies/ragnarok/	0
6614	Independent Money System	IMS	https://coinmarketcap.com//currencies/independent-money-system/	0
6615	Agrolot	AGLT	https://coinmarketcap.com//currencies/agrolot/	0
6616	Dash Green	DASHG	https://coinmarketcap.com//currencies/dash-green/	0
6617	Luna Coin	LUNA	https://coinmarketcap.com//currencies/luna-coin/	0.01
6618	Ccore	CCO	https://coinmarketcap.com//currencies/ccore/	0
6619	LitecoinToken	LTK	https://coinmarketcap.com//currencies/litecoin-token/	0
6620	MoneroV 	XMV	https://coinmarketcap.com//currencies/monero-v/	0
6621	BriaCoin	BRIA	https://coinmarketcap.com//currencies/briacoin/	0.01
6622	Wild Beast Block	WBB	https://coinmarketcap.com//currencies/wild-beast-block/	0.04
6623	Cannation	CNNC	https://coinmarketcap.com//currencies/cannation/	0
6624	BoostCoin	BOST	https://coinmarketcap.com//currencies/boostcoin/	0
6625	PAXEX	PAXEX	https://coinmarketcap.com//currencies/paxex/	0
6626	Comet	CMT	https://coinmarketcap.com//currencies/comet/	0.01
6627	KZ Cash	KZC	https://coinmarketcap.com//currencies/kz-cash/	0
6628	SONO	SONO	https://coinmarketcap.com//currencies/altcommunity-coin/	0
6629	LiteCoin Ultra	LTCU	https://coinmarketcap.com//currencies/litecoin-ultra/	0.01
6630	iBank	IBANK	https://coinmarketcap.com//currencies/ibank/	0
6631	Storeum	STO	https://coinmarketcap.com//currencies/storeum/	0
6632	Coinonat	CXT	https://coinmarketcap.com//currencies/coinonat/	0
6633	SocialCoin	SOCC	https://coinmarketcap.com//currencies/socialcoin-socc/	0
6634	Abulaba	AAA	https://coinmarketcap.com//currencies/abulaba/	0
6635	Pamp Network	PAMP	https://coinmarketcap.com//currencies/pamp-network/	0
6636	Magic Cube Coin	MCC	https://coinmarketcap.com//currencies/magic-cube-coin/	0
6637	MNPCoin	MNP	https://coinmarketcap.com//currencies/mnpcoin/	0
6638	Crystal Token	CYL	https://coinmarketcap.com//currencies/crystal-token/	0.01
6639	Staker	STR	https://coinmarketcap.com//currencies/staker/	0
6640	Iconic	ICON	https://coinmarketcap.com//currencies/iconic/	0
6641	Gold Poker	GPKR	https://coinmarketcap.com//currencies/gold-poker/	0
6642	Dinero	DIN	https://coinmarketcap.com//currencies/dinero/	0
6643	Stakinglab	LABX	https://coinmarketcap.com//currencies/stakinglab/	0.01
6644	Qbic	QBIC	https://coinmarketcap.com//currencies/qbic/	0.000528
6645	WXCOINS	WXC	https://coinmarketcap.com//currencies/wxcoins/	5.56e-05
6646	HEIDI	HDI	https://coinmarketcap.com//currencies/heidi/	0.001833
6647	Project-X	NANOX	https://coinmarketcap.com//currencies/project-x/	18849.92
6648	Benz	BENZ	https://coinmarketcap.com//currencies/benz/	0.0002442
6649	PonziCoin	PONZI	https://coinmarketcap.com//currencies/ponzicoin/	0.001668
6650	Argus	ARGUS	https://coinmarketcap.com//currencies/argus/	0.001112
6651	Concoin	CONX	https://coinmarketcap.com//currencies/concoin/	0.001111
6652	Flit Token	FLT	https://coinmarketcap.com//currencies/flit-token/	1.9e-07
6653	Money Plant Token	MPT	https://coinmarketcap.com//currencies/money-plant-token/	0.000395
6654	Innovative Bioresearch Classic	INNBCL	https://coinmarketcap.com//currencies/innovative-bioresearch-classic/	0
6655	Helpico	HELP	https://coinmarketcap.com//currencies/helpico/	0.01
6656	Nibble	NBXC	https://coinmarketcap.com//currencies/nibble/	0
6657	WETH	WETH	https://coinmarketcap.com//currencies/weth/	1791.76
6658	Wrapped Huobi Token	WHT	https://coinmarketcap.com//currencies/wrapped-huobi-token/	14.79
6659	Klaytn	KLAY	https://coinmarketcap.com//currencies/klaytn/	3.19
6660	Xeno Token	XNO	https://coinmarketcap.com//currencies/xeno-token/	0.63
6661	PayProtocol	PCI	https://coinmarketcap.com//currencies/payprotocol/	2.27
6662	Oxygen	OXY	https://coinmarketcap.com//currencies/oxygen/	2.97
6663	Global China Cash	CNC	https://coinmarketcap.com//currencies/global-china-cash/	0.15
6664	PlayDapp	PLA	https://coinmarketcap.com//currencies/playdapp/	0.62
6665	Hdac	HDAC	https://coinmarketcap.com//currencies/hdac/	0.06
6666	Amun Bitcoin 3x Daily Long	BTC3L	https://coinmarketcap.com//currencies/amun-bitcoin-3x-daily-long/	11.73
6667	STING	STN	https://coinmarketcap.com//currencies/sting/	37.32
6668	Compound Dai	CDAI	https://coinmarketcap.com//currencies/compound-dai/	0.02
6669	BTCUP	BTCUP	https://coinmarketcap.com//currencies/btcup/	169.28
6670	ECOMI	OMI	https://coinmarketcap.com//currencies/ecomi/	0.01
6671	Compound USD Coin	CUSDC	https://coinmarketcap.com//currencies/compound-usd-coin/	0.02
6672	Ontology Gas	ONG	https://coinmarketcap.com//currencies/ontology-gas/	0.72
6673	Amun Ether 3x Daily Short	ETH3S	https://coinmarketcap.com//currencies/amun-ether-3x-daily-short/	0.01
6674	Amun Ether 3x Daily Long	ETH3L	https://coinmarketcap.com//currencies/amun-ether-3x-daily-long/	2.2
6675	WEMIX	WEMIX	https://coinmarketcap.com//currencies/wemix/	0.46
6676	MOTIV Protocol	MOV	https://coinmarketcap.com//currencies/motiv-protocol/	0.02
6677	CELEBPLUS	CELEB	https://coinmarketcap.com//currencies/celebplus/	0.87
6678	BTCDOWN	BTCDOWN	https://coinmarketcap.com//currencies/btcdown/	0.05
6679	Amun Bitcoin 3x Daily Short	BTC3S	https://coinmarketcap.com//currencies/amun-bitcoin-3x-daily-short/	0
6680	Basis Gold	BAG	https://coinmarketcap.com//currencies/bag/	0.58
6681	OKExChain	OKT	https://coinmarketcap.com//currencies/okexchain/	123.76
6682	PicaArtMoney	PICA	https://coinmarketcap.com//currencies/picaartmoney/	0.12
6683	VEROX	VRX	https://coinmarketcap.com//currencies/verox/	1134.23
6684	Electric Vehicle Zone	EVZ	https://coinmarketcap.com//currencies/electric-vehicle-zone/	0.04
6685	KINE	KINE	https://coinmarketcap.com//currencies/kine/	4.27
6686	ETHUP	ETHUP	https://coinmarketcap.com//currencies/ethup/	124.3
6687	EMOGI Network	LOL	https://coinmarketcap.com//currencies/emogi-network/	0
6688	New BitShares	NBS	https://coinmarketcap.com//currencies/new-bitshares/	0.03
6689	SXPUP	SXPUP	https://coinmarketcap.com//currencies/sxpup/	1.85
6690	Hoge Finance	HOGE	https://coinmarketcap.com//currencies/hoge-finance/	0
6691	MobileCoin	MOB	https://coinmarketcap.com//currencies/mobilecoin/	5.62
6692	bDollar	BDO	https://coinmarketcap.com//currencies/bdollar/	1.14
6693	ETHDOWN	ETHDOWN	https://coinmarketcap.com//currencies/ethdown/	0.01
6694	UnFederalReserve	ERSDL	https://coinmarketcap.com//currencies/unfederalreserve/	0.33
6695	3X Long Cardano Token	ADABULL	https://coinmarketcap.com//currencies/3x-long-cardano-token/	30462.35
6696	OASISBloc	OSB	https://coinmarketcap.com//currencies/oasisbloc/	0.55
6697	3X Long Bitcoin Token	BULL	https://coinmarketcap.com//currencies/3x-long-bitcoin-token/	77142.93
6698	sKLAY	SKLAY	https://coinmarketcap.com//currencies/sklay/	3.29
6699	Value Liquidity	VALUE	https://coinmarketcap.com//currencies/value-defi-protocol/	4.6
6700	XRPUP	XRPUP	https://coinmarketcap.com//currencies/xrpup/	3.19
6701	Hiblocks	HIBS	https://coinmarketcap.com//currencies/hiblocks/	0.01
6702	Polka City	POLC	https://coinmarketcap.com//currencies/polka-city/	1.42
6703	Australian Dollar Token	AUDT	https://coinmarketcap.com//currencies/australian-dollar-token/	0.77
6704	Basis Gold Share	BAGS	https://coinmarketcap.com//currencies/basis-gold-share/	226.63
6705	Polaris Share	POLA	https://coinmarketcap.com//currencies/polaris-share/	0.33
6706	SafeMoon	SAFEMOON	https://coinmarketcap.com//currencies/safemoon/	0
6707	Global AEX Token	GAT	https://coinmarketcap.com//currencies/global-aex-token/	0.02
6708	Dapp Token	DAPPT	https://coinmarketcap.com//currencies/dapp-token/	0.01
6709	Paypolitan Token	EPAN	https://coinmarketcap.com//currencies/paypolitan-token/	0.2
6710	ITAM Games	ITAM	https://coinmarketcap.com//currencies/itam-games/	0.07
6711	DigiCol	DGCL	https://coinmarketcap.com//currencies/digicol/	0.89
6712	Wifi Coin	WIFI	https://coinmarketcap.com//currencies/wifi-coin/	0.65
6713	Bitcoin Vault	BTCV	https://coinmarketcap.com//currencies/bitcoin-vault/	65.6
6714	Compound USDT	CUSDT	https://coinmarketcap.com//currencies/compound-usdt/	0.02
6715	300FIT NETWORK	FIT	https://coinmarketcap.com//currencies/300fit-network/	0
6716	BSCEX	BSCX	https://coinmarketcap.com//currencies/bscex/	13.76
6717	XRPDOWN	XRPDOWN	https://coinmarketcap.com//currencies/xrpdown/	0
6718	EduMetrix Coin	EMC	https://coinmarketcap.com//currencies/edumetrix/	16.9
6719	bDollar Share	SBDO	https://coinmarketcap.com//currencies/bdollar-share/	7686.05
6720	SHIBA INU	SHIB	https://coinmarketcap.com//currencies/shiba-inu/	0
6721	Beacon ETH	BETH	https://coinmarketcap.com//currencies/beacon-eth/	1720.03
6722	BNBUP	BNBUP	https://coinmarketcap.com//currencies/bnbup/	543.86
6723	Gold Coin Reserve	GCR	https://coinmarketcap.com//currencies/gold-coin-reserve/	1728.96
6724	FinexboxToken	FNB	https://coinmarketcap.com//currencies/finexboxtoken/	4.27
6725	PancakeBunny	BUNNY	https://coinmarketcap.com//currencies/pancakebunny/	128.81
6726	Mineral	MNR	https://coinmarketcap.com//currencies/mineral/	0.02
6727	Chain Guardians	CGG	https://coinmarketcap.com//currencies/chain-guardians/	3.24
6728	Dvision Network	DVI	https://coinmarketcap.com//currencies/dvision-network/	0.4
6729	LINKDOWN	LINKDOWN	https://coinmarketcap.com//currencies/linkdown/	0
6730	Rai Reflex Index	RAI	https://coinmarketcap.com//currencies/rai/	2.97
6731	Midas Dollar Share	MDS	https://coinmarketcap.com//currencies/midas-dollar-share/	17689.59
6732	Adappter Token	ADP	https://coinmarketcap.com//currencies/adappter-token/	0.04
6733	TRXDOWN	TRXDOWN	https://coinmarketcap.com//currencies/trxdown/	0.12
6734	Argon	ARGON	https://coinmarketcap.com//currencies/argon/	0.15
6735	Alchemix	ALCX	https://coinmarketcap.com//currencies/alchemix/	1655.72
6736	LINKUP	LINKUP	https://coinmarketcap.com//currencies/linkup/	11.77
6737	YFIII	YFIII	https://coinmarketcap.com//currencies/yfiii/	9.02
6738	DAOventures	DVG	https://coinmarketcap.com//currencies/daoventures/	3.51
6739	MinePlex	PLEX	https://coinmarketcap.com//currencies/mineplex/	0.46
6740	FILUP	FILUP	https://coinmarketcap.com//currencies/filup/	11.05
6741	QUEENBEE	QBZ	https://coinmarketcap.com//currencies/queenbee/	0.01
6742	Gera Coin	GERA	https://coinmarketcap.com//currencies/gera-coin/	0.7134
6743	ADAUP	ADAUP	https://coinmarketcap.com//currencies/adaup/	88.62
6744	SifChain	erowan	https://coinmarketcap.com//currencies/sifchain/	1.25
6745	DMD	DMD	https://coinmarketcap.com//currencies/dmd/	40.93
6746	LTCUP	LTCUP	https://coinmarketcap.com//currencies/ltcup/	27.22
6747	Global Utility Smart Digital Token	GUSDT	https://coinmarketcap.com//currencies/gusd-token/	25.32
6748	ADADOWN	ADADOWN	https://coinmarketcap.com//currencies/adadown/	0.000569
6749	SXPDOWN	SXPDOWN	https://coinmarketcap.com//currencies/sxpdown/	0.01109
6750	Tower token	TOWER	https://coinmarketcap.com//currencies/tower-token/	0.04242
6751	XTZUP	XTZUP	https://coinmarketcap.com//currencies/xtzup/	0.596
6752	Innovation Blockchain Payment	IBP	https://coinmarketcap.com//currencies/innovation-blockchain-payment/	0.07
6753	MeconCash	MCH	https://coinmarketcap.com//currencies/meconcash/	0.17
6754	BigBoys Industry	BBI	https://coinmarketcap.com//currencies/bigboys-industry/	852.48
6755	Idavoll Network	IDV	https://coinmarketcap.com//currencies/idavoll-network/	0.04
6756	Bearn	BFI	https://coinmarketcap.com//currencies/bearn/	0.44
6757	Soda Coin	SOC	https://coinmarketcap.com//currencies/soda-coin/	0.01
6758	OVR	OVR	https://coinmarketcap.com//currencies/ovr/	0.9
6759	FairGame	FAIR	https://coinmarketcap.com//currencies/fairgame/	0.01
6760	DeFi Pulse Index	DPI	https://coinmarketcap.com//currencies/defi-pulse-index/	433.78
6761	Golff	GOF	https://coinmarketcap.com//currencies/golff/	1.68
6762	WiBX	WBX	https://coinmarketcap.com//currencies/wibx/	0.04
6763	Pearl	PEARL	https://coinmarketcap.com//currencies/pearl/	240.73
6764	3X Long Ethereum Token	ETHBULL	https://coinmarketcap.com//currencies/3x-long-ethereum-token/	4576.57
6765	Trabzonspor Fan Token	TRA	https://coinmarketcap.com//currencies/trabzonspor-fan-token/	5.66
6766	DOTUP	DOTUP	https://coinmarketcap.com//currencies/dotup/	143.6
6767	Velo	VELO	https://coinmarketcap.com//currencies/velo/	1.26
6768	FILDOWN	FILDOWN	https://coinmarketcap.com//currencies/fildown/	0.11
6769	HOPR	HOPR	https://coinmarketcap.com//currencies/hopr/	0.78
6770	XLMUP	XLMUP	https://coinmarketcap.com//currencies/xlmup/	10.75
6771	Trading Membership Community	TMC	https://coinmarketcap.com//currencies/trading-membership-community/	0.01
6772	EDDASwap	EDDA	https://coinmarketcap.com//currencies/eddaswap/	4833.63
6773	Gather	GTH	https://coinmarketcap.com//currencies/gather/	0.57
6774	Compound Wrapped BTC	CWBTC	https://coinmarketcap.com//currencies/compound-wrapped-btc/	1120.83
6775	DOTDOWN	DOTDOWN	https://coinmarketcap.com//currencies/dotdown/	0
6776	UNIUP	UNIUP	https://coinmarketcap.com//currencies/uniup/	26.13
6777	3X Short Bitcoin Token	BEAR	https://coinmarketcap.com//currencies/3x-short-bitcoin-token/	0
6778	Modefi	MOD	https://coinmarketcap.com//currencies/modefi/	2.8
6779	MilliMeter	MM	https://coinmarketcap.com//currencies/millimeter/	1
6780	G999	G999	https://coinmarketcap.com//currencies/g999/	0.01
6781	Bat True Dollar	BTD	https://coinmarketcap.com//currencies/bat-true-dollar/	1.15
6782	MIMOSA	MIMO	https://coinmarketcap.com//currencies/mimosa/	1.69
6783	Wise	WISE	https://coinmarketcap.com//currencies/wise/	0.41
6784	FUTUREXCRYPTO	FXC	https://coinmarketcap.com//currencies/futurexcrypto/	1.02
6785	Bilaxy Token	BIA	https://coinmarketcap.com//currencies/bilaxy-token/	0.01
6786	TRXUP	TRXUP	https://coinmarketcap.com//currencies/trxup/	3.13
6787	Bitcoin File	BIFI	https://coinmarketcap.com//currencies/bitcoin-file/	0
6788	B20	B20	https://coinmarketcap.com//currencies/b20/	12.74
6789	Inverse Finance	INV	https://coinmarketcap.com//currencies/inverse-finance/	1220.34
6790	Launchpool	LPOOL	https://coinmarketcap.com//currencies/launchpool/	22.62
6791	Polkacover	CVR	https://coinmarketcap.com//currencies/polkacover/	0.91
6792	Zero Exchange	ZERO	https://coinmarketcap.com//currencies/zero-exchange/	0.29
6793	PAID Network	PAID	https://coinmarketcap.com//currencies/paid-network/	1.66
6794	SUSHIUP	SUSHIUP	https://coinmarketcap.com//currencies/sushiup/	91.11
6795	XTZDOWN	XTZDOWN	https://coinmarketcap.com//currencies/xtzdown/	0.11
6796	7up Finance	7UP	https://coinmarketcap.com//currencies/7up-finance/	4.48
6797	CyberFi Token	CFi	https://coinmarketcap.com//currencies/cyberfi/	24.09
6798	Kindcow Finance	KIND	https://coinmarketcap.com//currencies/kindcow-finance/	2.27
6799	Bat True Share	BTS	https://coinmarketcap.com//currencies/bat-true-share/	515.88
6800	Brother Music Platform	BMP	https://coinmarketcap.com//currencies/brother-music-platform/	0.04
6801	UniCrypt	UNCX	https://coinmarketcap.com//currencies/uncx/	236
6802	SocialGood	SG	https://coinmarketcap.com//currencies/socialgood/	3.78
6803	Yobit Token	YO	https://coinmarketcap.com//currencies/yobit-token/	2133.87
6804	10x.gg	XGG	https://coinmarketcap.com//currencies/10xgg/	0.01
6805	FastSwap	FAST	https://coinmarketcap.com//currencies/fastswap/	12.46
6806	Zeedex	ZDEX	https://coinmarketcap.com//currencies/zeedex/	0.99
6807	Alpha5	A5T	https://coinmarketcap.com//currencies/alpha5/	1.07
6808	AAVEUP	AAVEUP	https://coinmarketcap.com//currencies/aave-up/	102.01
6809	YFIUP	YFIUP	https://coinmarketcap.com//currencies/yfiup/	5.51
6810	Gas Token Two	GST2	https://coinmarketcap.com//currencies/gas-token-two/	373.4
6811	3X Long Chainlink Token	LINKBULL	https://coinmarketcap.com//currencies/3x-long-chainlink-token/	404.97
6812	ALP Coin	ALP	https://coinmarketcap.com//currencies/alp-coin/	0.26
6813	Monster Slayer Cash	MSC	https://coinmarketcap.com//currencies/monster-slayer-finance/	1.11
6814	TopChain	TOPC	https://coinmarketcap.com//currencies/topchain/	0.01
6815	Tessla Coin	TSLA	https://coinmarketcap.com//currencies/tessla-coin/	1.79
6816	3x Long XRP Token	XRPBULL	https://coinmarketcap.com//currencies/3x-long-xrp-token/	0.44
6817	Medican Coin	MCAN	https://coinmarketcap.com//currencies/medican-coin/	0.16
6818	Bitcicoin	BITCI	https://coinmarketcap.com//currencies/bitcicoin/	0.01
6819	BDCC Bitica COIN	BDCC	https://coinmarketcap.com//currencies/bdcc-bitica-coin/	0.52
6820	AAVEDOWN	AAVEDOWN	https://coinmarketcap.com//currencies/aave-down/	0
6821	EOSDOWN	EOSDOWN	https://coinmarketcap.com//currencies/eosdown/	0.25
6822	Dfinity (IOU)	ICP	https://coinmarketcap.com//currencies/dfinity/	96.94
6823	Panda Dao	PDAO	https://coinmarketcap.com//currencies/panda-dao/	0
6824	Hintchain	HINT	https://coinmarketcap.com//currencies/hintchain/	0.02
6825	Blank Wallet	BLANK	https://coinmarketcap.com//currencies/blank-wallet/	0.71
6826	Ethereum Lightning	ETLT	https://coinmarketcap.com//currencies/ethereumlightning/	6.11
6827	AOS	AOS	https://coinmarketcap.com//currencies/aos/	0.04
6828	Bitcoin God	GOD	https://coinmarketcap.com//currencies/bitcoin-god/	11.07
6829	3X Long Sushi Token	SUSHIBULL	https://coinmarketcap.com//currencies/3x-long-sushi-token/	1.16
6830	Portion	PRT	https://coinmarketcap.com//currencies/portion/	0.2
6831	Carnomaly	CARR	https://coinmarketcap.com//currencies/carnomaly/	0.88
6832	UNION Protocol Governance Token	UNN	https://coinmarketcap.com//currencies/union-protocol-governance-token/	0.1
6833	BSCstarter	START	https://coinmarketcap.com//currencies/bscstarter/	19.5
6834	BT.Finance	BT	https://coinmarketcap.com//currencies/bt-finance/	41.04
6835	King Swap	$KING	https://coinmarketcap.com//currencies/kingswap/	0.14
6836	DeltaHub Community	DHC	https://coinmarketcap.com//currencies/deltahub-community/	1.71
6837	Exeedme	XED	https://coinmarketcap.com//currencies/exeedme/	1.35
6838	Unit Protocol Duck	DUCK	https://coinmarketcap.com//currencies/unit-protocol-duck/	0.32
6839	Butterfly Protocol	BFLY	https://coinmarketcap.com//currencies/butterfly-protocol-2/	0.78
6840	HashBridge Oracle	HBO	https://coinmarketcap.com//currencies/hashbridge-oracle/	1.24
6841	THORChain (ERC20)	RUNE	https://coinmarketcap.com//currencies/thorchain-erc20/	5.55
6842	Butterfly Protocol	BFLY	https://coinmarketcap.com//currencies/butterfly-protocol-2/	0.7806
6843	FEGtoken	FEG	https://coinmarketcap.com//currencies/fegtoken/	0
6844	Pando	PANDO	https://coinmarketcap.com//currencies/pando/	2.25
6845	Lendhub	LHB	https://coinmarketcap.com//currencies/lendhub/	0.3939
6846	ROONEX	RNX	https://coinmarketcap.com//currencies/roonex/	0.0006187
6847	UNIDOWN	UNIDOWN	https://coinmarketcap.com//currencies/unidown/	0.00271
6848	ISTARDUST	ISDT	https://coinmarketcap.com//currencies/istardust/	0.0008385
6849	Tether Gold	XAUT	https://coinmarketcap.com//currencies/tether-gold/	1730.42
6850	Jasmy	JASMY	https://coinmarketcap.com//currencies/jasmy/	2.09
6851	All.me	ME	https://coinmarketcap.com//currencies/all-me/	0.01
6852	FolgoryUSD	USDF	https://coinmarketcap.com//currencies/folgoryusd/	1
6853	DEOR	DEOR	https://coinmarketcap.com//currencies/deor/	0.28
6854	Whole Network	NODE	https://coinmarketcap.com//currencies/whole-network/	0
6855	Lotto	LOTTO	https://coinmarketcap.com//currencies/lotto/	0.11
6856	GRAP	GRAP	https://coinmarketcap.com//currencies/grap/	0.45
6857	LTCDOWN	LTCDOWN	https://coinmarketcap.com//currencies/ltcdown/	0.01
6858	NFTL Token	NFTL	https://coinmarketcap.com//currencies/nftl-token/	0.27
6859	Governor DAO	GDAO	https://coinmarketcap.com//currencies/governor-dao/	2.78
6860	Consensus Cell Network	ECELL	https://coinmarketcap.com//currencies/consensus-cell-network/	0.05
6861	Swirge	SWG	https://coinmarketcap.com//currencies/swirge/	0.5
6862	Filda	FILDA	https://coinmarketcap.com//currencies/filda/	0.41
6863	POC Blockchain	POC	https://coinmarketcap.com//currencies/poc-blockchain/	9.17
6864	Monster Slayer Share	MSS	https://coinmarketcap.com//currencies/monster-slayer-share/	863.36
6865	QuickSwap	QUICK	https://coinmarketcap.com//currencies/quickswap/	219
6866	HAPY Coin	HAPY	https://coinmarketcap.com//currencies/hapy-coin/	0.01
6867	Lien	LIEN	https://coinmarketcap.com//currencies/lien/	60.9
6868	Tcbcoin	TCFX	https://coinmarketcap.com//currencies/tcbcoin/	1.29
6869	DxSale Network	SALE	https://coinmarketcap.com//currencies/dxsale-network/	0.22
6870	Betherchip	BEC	https://coinmarketcap.com//currencies/betherchip/	11.55
6871	FNK wallet	FNK	https://coinmarketcap.com//currencies/fnk-wallet/	18.56
6872	CNNS	CNNS	https://coinmarketcap.com//currencies/cnns/	0.01
6873	CafeSwap Token	BREW	https://coinmarketcap.com//currencies/cafeswap-token/	4.2
6874	BNBDOWN	BNBDOWN	https://coinmarketcap.com//currencies/bnbdown/	0
6875	Soteria	WSOTE	https://coinmarketcap.com//currencies/soteria/	2.25
6876	MobiePay	MBX	https://coinmarketcap.com//currencies/mobiepay/	0.02
6877	Freeliquid	FL	https://coinmarketcap.com//currencies/freeliquid/	7.33
6878	ETHA Lend	ETHA	https://coinmarketcap.com//currencies/etha-lend/	2.38
6879	Auric Network	AUSCM	https://coinmarketcap.com//currencies/auric-network/	0.32
6880	Crypto Holding Frank Token	CHFT	https://coinmarketcap.com//currencies/crypto-holding-frank-token/	1.08
6881	ORO	ORO	https://coinmarketcap.com//currencies/oro/	0.41
6882	ViteX Coin	VX	https://coinmarketcap.com//currencies/vitex-coin/	0.8
6883	Bananatok	BNA	https://coinmarketcap.com//currencies/bananatok/	0.01
6884	Peanut	NUX	https://coinmarketcap.com//currencies/peanut/	3.27
6885	Axial Entertainment Digital Asset	AXL	https://coinmarketcap.com//currencies/axial-entertainment-digital-asset/	0.03
6886	Marginswap	MFI	https://coinmarketcap.com//currencies/marginswap/	0.96
6887	Siren	SI	https://coinmarketcap.com//currencies/siren/	2.64
6888	Previse	PRVS	https://coinmarketcap.com//currencies/previse/	0.93
6889	Curate	XCUR	https://coinmarketcap.com//currencies/curate/	2.11
6890	IDK	IDK	https://coinmarketcap.com//currencies/idk/	0.07
6891	PoolTogether	POOL	https://coinmarketcap.com//currencies/pooltogether/	23.6
6892	Wault Finance	WAULT	https://coinmarketcap.com//currencies/wault-finance/	19.41
6893	Bispex	BPX	https://coinmarketcap.com//currencies/bispex/	0
6894	Medium	MDM	https://coinmarketcap.com//currencies/medium/	0.37
6895	DEUS Finance	DEUS	https://coinmarketcap.com//currencies/deus-finance/	9.5
6896	Haze Finance	HAZE	https://coinmarketcap.com//currencies/haze-finance/	8.46
6897	keyTango	TANGO	https://coinmarketcap.com//currencies/keytango/	1.83
6898	YFIDOWN	YFIDOWN	https://coinmarketcap.com//currencies/yfidown/	0
6899	Dexchain	DXC	https://coinmarketcap.com//currencies/mydexpay/	0.01
6900	QuiverX	QRX	https://coinmarketcap.com//currencies/quiverx/	0.08
6901	Fromm Car	FCR	https://coinmarketcap.com//currencies/fromm-car/	0
6902	Basis Share	BAS	https://coinmarketcap.com//currencies/basis-share/	45.86
6903	Fire Protocol	FIRE	https://coinmarketcap.com//currencies/fire-protocol/	1.08
6904	Combo	COMB	https://coinmarketcap.com//currencies/combo/	0.01
6905	Digital Reserve Currency	DRC	https://coinmarketcap.com//currencies/digital-reserve-currency/	0
6906	unilock.network	UNL	https://coinmarketcap.com//currencies/unilock-network/	2428.14
6907	Loon Network	LOON	https://coinmarketcap.com//currencies/loon-network/	0.04
6908	3x Long EOS Token	EOSBULL	https://coinmarketcap.com//currencies/3x-long-eos-token/	0.23
6909	Arianee	ARIA20	https://coinmarketcap.com//currencies/arianee-protocol/	3.64
6910	Coinzo Token	CNZ	https://coinmarketcap.com//currencies/coinzo-token/	0.11
6911	BambooDeFi	BAMBOO	https://coinmarketcap.com//currencies/bamboo-defi/	0.51
6912	TosDis	DIS	https://coinmarketcap.com//currencies/tosdis/	84.95
6913	KittenFinance	KIF	https://coinmarketcap.com//currencies/kittenfinance/	97.79
6914	Mandala Exchange Token	MDX	https://coinmarketcap.com//currencies/mandala-exchange-token/	0.15
6915	Apiary Fund Coin	AFC	https://coinmarketcap.com//currencies/apiary-fund-coin/	0
6916	pBTC35A	pBTC35A	https://coinmarketcap.com//currencies/pbtc35a/	124.18
6917	VKENAF	VKNF	https://coinmarketcap.com//currencies/vkenaf/	0.2
6918	MixTrust	MXT	https://coinmarketcap.com//currencies/mixtrust/	0.01
6919	Cross Finance	CRP	https://coinmarketcap.com//currencies/cross-finance/	0.01
6920	Axion	AXN	https://coinmarketcap.com//currencies/axion/	0
6921	Libartysharetoken	LST	https://coinmarketcap.com//currencies/libartysharetoken/	0.14
6922	ACryptoS	ACS	https://coinmarketcap.com//currencies/acryptos/	57.14
6923	dFuture	DFT	https://coinmarketcap.com//currencies/dfuture/	0.43
6924	Lightning	LIGHT	https://coinmarketcap.com//currencies/lightning/	0.02
6925	BKEX Token	BKK	https://coinmarketcap.com//currencies/bkex-token/	0.14
6926	e-Money	NGM	https://coinmarketcap.com//currencies/e-money-coin/	1.58
6927	GOGO.finance	GOGO	https://coinmarketcap.com//currencies/gogo-finance/	182.2
6928	Counos Coin	CCA	https://coinmarketcap.com//currencies/counos-coin/	1.28
6929	UBIX.Network	UBX	https://coinmarketcap.com//currencies/ubix-network/	0
6930	Rivex	RVX	https://coinmarketcap.com//currencies/rivex/	0.23
6931	Zyro	ZYRO	https://coinmarketcap.com//currencies/zyro/	0.04
6932	Tenet	TEN	https://coinmarketcap.com//currencies/tenet/	2.29
6933	BotOcean	BOTS	https://coinmarketcap.com//currencies/botocean/	0.29
6934	Elite Swap	ELT	https://coinmarketcap.com//currencies/elite-swap/	0
6935	Hydra	HYDRA	https://coinmarketcap.com//currencies/hydra/	14.43
6936	Mantis	MNTIS	https://coinmarketcap.com//currencies/mantis/	7.3
6937	Metis	MTS	https://coinmarketcap.com//currencies/metis/	1
6938	BeeKan	BKBT	https://coinmarketcap.com//currencies/beekan/	0
6939	MCDEX	MCB	https://coinmarketcap.com//currencies/mcdex/	15.66
6940	Starname	IOV	https://coinmarketcap.com//currencies/starname/	0.07
6941	Stream Protocol	STPL	https://coinmarketcap.com//currencies/stream-protocol/	0.04851
6942	MegaCryptoPolis	MEGA	https://coinmarketcap.com//currencies/megacryptopolis/	48.93
6943	Trism	TRISM	https://coinmarketcap.com//currencies/trism/	0.8988
6944	SaltSwap Finance	SALT	https://coinmarketcap.com//currencies/saltswap-finance/	3.28
6945	DeFi Wizard	DWZ	https://coinmarketcap.com//currencies/defi-wizard/	6.75
6946	ZCore Finance	ZEFI	https://coinmarketcap.com//currencies/zcore-finance/	1.55
6947	Tardigrades Finance	TRDG	https://coinmarketcap.com//currencies/tardigrades-finance/	0
6948	Dextoken	DEXG	https://coinmarketcap.com//currencies/dextoken/	330.4
6949	3X Short Ethereum Token	ETHBEAR	https://coinmarketcap.com//currencies/3x-short-ethereum-token/	5.438e-05
6950	Binance VND	BVND	https://coinmarketcap.com//currencies/binance-vnd/	4.145e-05
6951	YAM v3	YAM	https://coinmarketcap.com//currencies/yamv3/	3.22
6952	WaykiChain Governance Coin	WGRT	https://coinmarketcap.com//currencies/waykichain-governance-coin/	0.01
6953	Bidao	BID	https://coinmarketcap.com//currencies/bidao/	0.02
6954	Lux Bio Cell	LBXC	https://coinmarketcap.com//currencies/lux-bio-cell/	0
6955	StarLink	SLNV2	https://coinmarketcap.com//currencies/starlink/	0.03
6956	ProBit Token	PROB	https://coinmarketcap.com//currencies/probit-token/	0.45
6957	AGOV (ANSWER Governance)	AGOV	https://coinmarketcap.com//currencies/agov-answer-governance/	0.03
6958	The Luxury Coin	TLB	https://coinmarketcap.com//currencies/the-luxury-coin/	51.1
6959	DXdao	DXD	https://coinmarketcap.com//currencies/dxdao/	225.69
6960	XLMDOWN	XLMDOWN	https://coinmarketcap.com//currencies/xlmdown/	0.01
6961	Float Protocol	BANK	https://coinmarketcap.com//currencies/float-protocol/	704.65
6962	Proof Of Liquidity	POL	https://coinmarketcap.com//currencies/proof-of-liquidity/	0.11
6963	B21 Invest	B21	https://coinmarketcap.com//currencies/b21-invest/	0.37
6964	3x Long Bitcoin Cash Token	BCHBULL	https://coinmarketcap.com//currencies/3x-long-bitcoin-cash-token/	3.13
6965	Slime Finance	SLME	https://coinmarketcap.com//currencies/slime-finance/	6.39
6966	LinkArt	LAR	https://coinmarketcap.com//currencies/linkart/	0
6967	REDi	REDI	https://coinmarketcap.com//currencies/redi/	0
6968	Swapfolio	SWFL	https://coinmarketcap.com//currencies/swapfolio/	0.09
6969	HecoFi	HFI	https://coinmarketcap.com//currencies/hecofi/	0.04
6970	Mars	Mars	https://coinmarketcap.com//currencies/mars/	0.06
6971	Whirl Finance	WHIRL	https://coinmarketcap.com//currencies/whirl-finance/	0.01
6972	Themis	MIS	https://coinmarketcap.com//currencies/themis-oracle/	0.53
6973	Compound 0x	CZRX	https://coinmarketcap.com//currencies/compound-0x/	0.03
6974	EthicHub	ETHIX	https://coinmarketcap.com//currencies/ethichub/	0.54
6975	Decentralized Mining Exchange	DMC	https://coinmarketcap.com//currencies/decentralized-mining-exchange/	0.42
6976	RAKUN	RAKU	https://coinmarketcap.com//currencies/rakun/	0.02
6977	stakedXEM	STXEM	https://coinmarketcap.com//currencies/stakedxem/	0.37
6978	Yuan Chain Coin	YCC	https://coinmarketcap.com//currencies/yuan-chain-coin/	0.02
6979	BSC FARM	BSC	https://coinmarketcap.com//currencies/bsc-farm/	1.03
6980	MeetPle	MPT	https://coinmarketcap.com//currencies/meetple/	0.88
6981	Solomon Defi	SLM	https://coinmarketcap.com//currencies/solomon-defi/	0.04
6982	Polkazeck	ZCK	https://coinmarketcap.com//currencies/polkazeck/	0.09
6983	3x Long Litecoin Token	LTCBULL	https://coinmarketcap.com//currencies/3x-long-litecoin-token/	8
6984	Moola	MLA	https://coinmarketcap.com//currencies/moola/	1.35
6985	Marsan Exchange token	MRS	https://coinmarketcap.com//currencies/marsan-exchange-token/	0.21
6986	AllWin DeFi	ALLWIN	https://coinmarketcap.com//currencies/allwin-defi/	0.39
6987	UpBots	UBXT	https://coinmarketcap.com//currencies/upbots/	0.03
6988	Barkis Network	BKS	https://coinmarketcap.com//currencies/barkis-network/	0.81
6989	SmartKey	SKEY	https://coinmarketcap.com//currencies/smartkey/	0.36
6990	MiraQle	MQL	https://coinmarketcap.com//currencies/miraqle/	0.23
6991	DEGEN Index	DEGEN	https://coinmarketcap.com//currencies/degen-index/	6.56
6992	Compound Basic Attention Token	CBAT	https://coinmarketcap.com//currencies/compound-basic-attention-token/	0.02
6993	Brazilian Digital Token	BRZ	https://coinmarketcap.com//currencies/brz/	0.17
6994	Moderna tokenized stock FTX	MRNA	https://coinmarketcap.com//currencies/moderna-tokenized-stock-ftx/	155.9
6995	Mars Network	MARS	https://coinmarketcap.com//currencies/mars-network/	0.02
6996	DEUS Finance DEA	DEA	https://coinmarketcap.com//currencies/deus-finance-dea/	336.6
6997	Cap	CAP	https://coinmarketcap.com//currencies/cap/	38.4
6998	MCOBIT	MCT	https://coinmarketcap.com//currencies/mcobit/	0.14
6999	Alpaca City	ALPA	https://coinmarketcap.com//currencies/alpaca-city/	1.08
7000	Handy	HANDY	https://coinmarketcap.com//currencies/handy/	0.03
7001	Student Coin	STC	https://coinmarketcap.com//currencies/student-coin/	0.02
7002	Firdaos	FDO	https://coinmarketcap.com//currencies/firdaos/	0.2
7003	Katalyo	KTLYO	https://coinmarketcap.com//currencies/katalyo/	0.56
7004	3X Long Algorand Token	ALGOBULL	https://coinmarketcap.com//currencies/3x-long-algorand-token/	0
7005	Cryptoindex.com 100	CIX100	https://coinmarketcap.com//currencies/cryptoindex-com-100/	0.79
7006	UniPower	POWER	https://coinmarketcap.com//currencies/unipower/	1.36
7007	DEXTF Protocol	DEXTF	https://coinmarketcap.com//currencies/dextf-protocol/	1.58
7008	xDAI	xDAI	https://coinmarketcap.com//currencies/xdaistable/	1
7009	CSP DAO	NEBO	https://coinmarketcap.com//currencies/csp-dao/	17.83
7010	MyToken	MT	https://coinmarketcap.com//currencies/mytoken/	0
7011	CoFiX	COFI	https://coinmarketcap.com//currencies/cofix/	0.65
7012	protocol finance	PFI	https://coinmarketcap.com//currencies/protocol-finance/	101.94
7013	Echoin	EC	https://coinmarketcap.com//currencies/echoin/	0
7014	UBU	UBU	https://coinmarketcap.com//currencies/ubu/	0.08
7015	DigiFinexToken	DFT	https://coinmarketcap.com//currencies/digifinextoken/	0.16
7016	Armor NXM	arNXM	https://coinmarketcap.com//currencies/armor-nxm/	44.77
7017	Itchiro Games	JEM	https://coinmarketcap.com//currencies/itchiro-games/	52.98
7018	PROXI	CREDIT	https://coinmarketcap.com//currencies/proxi/	0.04
7019	Misbloc	MSB	https://coinmarketcap.com//currencies/misbloc/	0.11
7020	Zytara dollar	ZUSD	https://coinmarketcap.com//currencies/zytara-dollar/	1
7021	DefiCliq	CLIQ	https://coinmarketcap.com//currencies/deficliq/	0.12
7022	Bogged Finance	BOG	https://coinmarketcap.com//currencies/bogged-finance/	2.42
7023	Multiplier	BMXX	https://coinmarketcap.com//currencies/bmultiplier/	32.64
7024	OnX Finance	ONX	https://coinmarketcap.com//currencies/onx-finance/	2.38
7025	Bitcoiva	BCA	https://coinmarketcap.com//currencies/bitcoiva/	3.11
7026	ATTN	ATTN	https://coinmarketcap.com//currencies/attn/	0.08
7027	ethArt	ARTE	https://coinmarketcap.com//currencies/ethart/	21.24
7028	Scifi Finance	SPICE	https://coinmarketcap.com//currencies/scifi-finance/	1.32
7029	B26 Finance	B26	https://coinmarketcap.com//currencies/b26-finance/	265.75
7030	Blockcloud	BLOC	https://coinmarketcap.com//currencies/blockcloud/	0
7031	Quantbook	QTBK	https://coinmarketcap.com//currencies/quantbook/	0.01
7032	JustLiquidity Binance	JULB	https://coinmarketcap.com//currencies/justliquidity-binance/	179.32
7033	Chain Games	CHAIN	https://coinmarketcap.com//currencies/chain-games/	0.11
7034	Glitch	GLCH	https://coinmarketcap.com//currencies/glitch/	0.09
7035	Maxonrow	MXW	https://coinmarketcap.com//currencies/maxonrow/	0.03
7036	Yfi.mobi	YFIM	https://coinmarketcap.com//currencies/yfi-mobi/	68.37
7037	Govi	GOVI	https://coinmarketcap.com//currencies/govi/	3.31
7038	PayAccept	PAYT	https://coinmarketcap.com//currencies/payaccept/	0.09
7039	Exchange Union	XUC	https://coinmarketcap.com//currencies/exchange-union/	0.6267
7040	ClinTex CTi	CTI	https://coinmarketcap.com//currencies/clintex-cti/	0.04805
7041	JackPool.finance	JFI	https://coinmarketcap.com//currencies/jackpool-finance/	89.57
7042	Minds	MINDS	https://coinmarketcap.com//currencies/minds/	0.7448
7043	3x Short EOS Token	EOSBEAR	https://coinmarketcap.com//currencies/3x-short-eos-token/	0.03682
7044	KVI	KVI	https://coinmarketcap.com//currencies/kvi/	0.001287
7045	QiSwap	QI	https://coinmarketcap.com//currencies/qiswap/	0.6943
7046	Swop	SWOP	https://coinmarketcap.com//currencies/swop/	20.74
7047	Pteria	PTERIA	https://coinmarketcap.com//currencies/pteria/	3.87
7048	Flashstake	FLASH	https://coinmarketcap.com//currencies/flashstake/	0.9944
7049	Interest Bearing ETH	ibETH	https://coinmarketcap.com//currencies/ibeth/	1880.61
7050	CY Finance	CYF	https://coinmarketcap.com//currencies/cy-finance/	0
7051	3X Long Cosmos Token	ATOMBULL	https://coinmarketcap.com//currencies/3x-long-cosmos-token/	40.24
7052	TokenPocket	TPT	https://coinmarketcap.com//currencies/tokenpocket/	0.03
7053	Order of the Black Rose	OBR	https://coinmarketcap.com//currencies/order-of-the-black-rose/	294.69
7054	yAxis	YAX	https://coinmarketcap.com//currencies/yaxis/	17.43
7055	Fairum	FAI	https://coinmarketcap.com//currencies/fairum/	1.63
7056	Largo Coin	LRG	https://coinmarketcap.com//currencies/largo-coin/	9.38
7057	Banana.finance	BANANA	https://coinmarketcap.com//currencies/banana-finance/	374.28
7058	Culture Ticket Chain	CTC	https://coinmarketcap.com//currencies/culture-ticket-chain/	0.01
7059	MangoChain	MGP	https://coinmarketcap.com//currencies/mangochain/	0.07
7060	LBK	LBK	https://coinmarketcap.com//currencies/lbk/	0.01
7061	Connect Financial	CNFI	https://coinmarketcap.com//currencies/connect-financial/	0.39
7062	EXRT Network	EXRT	https://coinmarketcap.com//currencies/exrt-network/	0.02
7063	AC eXchange Token	ACXT	https://coinmarketcap.com//currencies/ac-exchange-token/	1.02
7064	CryptoBank	CBANK	https://coinmarketcap.com//currencies/cryptobank/	3.29
7065	Blockchain Quotations Index Token	BQT	https://coinmarketcap.com//currencies/blockchain-quotations-index-token/	0.01
7066	Kaleido	KAL	https://coinmarketcap.com//currencies/kaleido/	0.01
7067	Two Prime FF1 Token	FF1	https://coinmarketcap.com//currencies/two-prime-ff1-token/	1.09
7068	Buy-Sell	BSE	https://coinmarketcap.com//currencies/buy-sell/	1.4
7069	Behodler	EYE	https://coinmarketcap.com//currencies/beholder/	1.36
7070	ShineChain	SHE	https://coinmarketcap.com//currencies/shinechain/	0
7071	MileVerse	MVC	https://coinmarketcap.com//currencies/mileverse/	0.01
7072	GGDApp	GGTK	https://coinmarketcap.com//currencies/ggdapp/	0.41
7073	ARTH	ARTH	https://coinmarketcap.com//currencies/arth/	0.32
7074	Farmland Protocol	FAR	https://coinmarketcap.com//currencies/farmland-protocol/	0.45
7075	Natural Farm Union Protocol	NFUP	https://coinmarketcap.com//currencies/natural-farm-union-protocol/	0.03
7076	DeFiat	DFT	https://coinmarketcap.com//currencies/defiat/	24.05
7077	TOM Finance	TOM	https://coinmarketcap.com//currencies/tom-finance/	94.62
7078	Energy Ledger	ELX	https://coinmarketcap.com//currencies/energy-ledger/	0.01
7079	Engine	EGCC	https://coinmarketcap.com//currencies/engine/	0
7080	Gains Farm v2	GFARM2	https://coinmarketcap.com//currencies/gains-farm-v2/	260.69
7081	Octree	OCT	https://coinmarketcap.com//currencies/octree/	345.01
7082	Bankcoin	BANK	https://coinmarketcap.com//currencies/the-bank-coin/	0.01
7083	DeFiPie	PIE	https://coinmarketcap.com//currencies/defipie/	0.12
7084	SUP	SUP	https://coinmarketcap.com//currencies/sup/	67.83
7085	3X Long Matic Token	MATICBULL	https://coinmarketcap.com//currencies/3x-long-matic-token/	32.79
7086	Lung Protocol	L2P	https://coinmarketcap.com//currencies/lung-protocol/	0
7087	Tapmydata	TAP	https://coinmarketcap.com//currencies/tapmydata/	0.53
7088	TOZEX	TOZ	https://coinmarketcap.com//currencies/tozex/	0.32
7089	tBTC	TBTC	https://coinmarketcap.com//currencies/tbtc/	56521.9
7090	Unifty	NIF	https://coinmarketcap.com//currencies/unifty/	17.85
7091	Tesla tokenized stock FTX	TSLA	https://coinmarketcap.com//currencies/tesla-tokenized-stock-ftx/	706.2
7092	GameStop tokenized stock FTX	GME	https://coinmarketcap.com//currencies/gamestop-tokenized-stock-ftx/	193.9
7093	Akita Inu	AKITA	https://coinmarketcap.com//currencies/akita-inu/	0
7094	Treat DAO	TREAT	https://coinmarketcap.com//currencies/treat-dao/	1.62
7095	Roxe Cash	ROC	https://coinmarketcap.com//currencies/roxe-cash/	0.1
7096	Coreto	COR	https://coinmarketcap.com//currencies/coreto/	0.01
7097	3X Long BNB Token	BNBBULL	https://coinmarketcap.com//currencies/3x-long-bnb-token/	5745.81
7098	fry.world	FRIES	https://coinmarketcap.com//currencies/fryworld/	58.22
7099	CheesecakeSwap Token	CCAKE	https://coinmarketcap.com//currencies/cheesecakeswap-token/	1.57
7100	Oxbull.tech	OXB	https://coinmarketcap.com//currencies/oxbull-tech/	0.58
7101	nHBTC	N0001	https://coinmarketcap.com//currencies/nhbtc/	1.03
7102	Minty Art	MINTY	https://coinmarketcap.com//currencies/minty-art/	2.37
7103	BlackDragon	BDT	https://coinmarketcap.com//currencies/blackdragon/	14.04
7104	Voice Token	VOICE	https://coinmarketcap.com//currencies/nix-bridge-token/	75.03
7105	Sperax	SPA	https://coinmarketcap.com//currencies/sperax/	0.02
7106	Unifund	IFUND	https://coinmarketcap.com//currencies/unifund/	0.15
7107	HyruleSwap	RUPEE	https://coinmarketcap.com//currencies/hyruleswap/	0.87
7108	OneSwap DAO Token	ONES	https://coinmarketcap.com//currencies/oneswap-dao-token/	1.25
7109	Your Finance Decentralized	YFD	https://coinmarketcap.com//currencies/yfdfi-finance/	95.48
7110	REBIT	KEYT	https://coinmarketcap.com//currencies/rebit/	0.02
7111	Yearn Finance Passive Income	YFPI	https://coinmarketcap.com//currencies/yearn-finance-passive-income/	21.57
7112	CryptEx	CRX	https://coinmarketcap.com//currencies/cryptex/	31.94
7113	Phoswap	PHO	https://coinmarketcap.com//currencies/phoswap/	1.4
7114	Global Human Trust	GHT	https://coinmarketcap.com//currencies/global-human-trust/	43.07
7115	Axia Protocol	AXIA	https://coinmarketcap.com//currencies/axia-protocol/	1.18
7116	Vortex Defi	VTX	https://coinmarketcap.com//currencies/vortex-defi/	0.29
7117	JUST NETWORK	JUS	https://coinmarketcap.com//currencies/just-network/	0.01
7118	Sora Validator Token	VAL	https://coinmarketcap.com//currencies/sora-validator-token/	2.37
7119	UniFi Protocol	UP	https://coinmarketcap.com//currencies/unifi-protocol/	1.38
7120	Blue Baikal	BBC	https://coinmarketcap.com//currencies/blue-baikal/	0
7121	PRCY Coin	PRCY	https://coinmarketcap.com//currencies/prcy-coin/	0.1
7122	Yield	YLD	https://coinmarketcap.com//currencies/yield/	88.11
7123	EIDOS	EIDOS	https://coinmarketcap.com//currencies/eidos/	0
7124	VARC	VARC	https://coinmarketcap.com//currencies/varc/	0
7125	SpiderDAO	SPDR	https://coinmarketcap.com//currencies/spiderdao/	0.04
7126	COIN	$COIN	https://coinmarketcap.com//currencies/coin-defi/	0.29
7127	GIVLY Coin	GIV	https://coinmarketcap.com//currencies/givly-coin/	0.01
7128	YEARNYFI NETWORK	YNI	https://coinmarketcap.com//currencies/yearnyfi-network/	230.59
7129	Wealthlocks	WLT	https://coinmarketcap.com//currencies/wealthlocks/	41.75
7130	Channels	CAN	https://coinmarketcap.com//currencies/channels/	25.13
7131	Yearn Classic Finance	EARN	https://coinmarketcap.com//currencies/yearn-classic-finance/	90.6
7132	DACC	DACC	https://coinmarketcap.com//currencies/dacc/	0
7133	Mogu	MOGX	https://coinmarketcap.com//currencies/mogu/	0.01
7134	VeraSwap	VRAP	https://coinmarketcap.com//currencies/veraswap/	0.06
7135	Sashimi	SASHIMI	https://coinmarketcap.com//currencies/sashimi/	0.08
7136	Premia	PREMIA	https://coinmarketcap.com//currencies/premia/	1.4
7137	BuildUp	BUP	https://coinmarketcap.com//currencies/buildup/	0.53
7138	MoneySwap	MSWAP	https://coinmarketcap.com//currencies/moneyswap/	0.01
7139	Pluto	PLUT	https://coinmarketcap.com//currencies/pluto/	0.1711
7140	Wrapped CrescoFin	wCRES	https://coinmarketcap.com//currencies/wrapped-crescofin/	7.59
7141	Burency	BUY	https://coinmarketcap.com//currencies/burency/	0.1228
7142	Koinos	KOIN	https://coinmarketcap.com//currencies/koinos/	0.1365
7143	Intelligent Investment Chain	IIC	https://coinmarketcap.com//currencies/intelligent-investment-chain/	0.0002439
7144	Treecle	TRCL	https://coinmarketcap.com//currencies/treecle/	0.01584
7145	xToken	XTK	https://coinmarketcap.com//currencies/xtoken/	0.2224
7146	YearnAgnostic Finance	YFIAG	https://coinmarketcap.com//currencies/yearnagnostic-finance/	2.78
7147	Berry	BERRY	https://coinmarketcap.com//currencies/berry/	0.006764
7148	Latamcash	LMCH	https://coinmarketcap.com//currencies/latamcash/	0.004204
7149	Font	FONT	https://coinmarketcap.com//currencies/font/	18.03
7150	BitcoinVend	BCVT	https://coinmarketcap.com//currencies/bitcoinvend/	0.15
7151	Unique One	RARE	https://coinmarketcap.com//currencies/unique-one/	4.37
7152	Secret (ERC20)	WSCRT	https://coinmarketcap.com//currencies/secret-erc20/	3.06
7153	Hyper Pay	HPY	https://coinmarketcap.com//currencies/hyper-pay/	0
7154	Sovi Finance	SOVI	https://coinmarketcap.com//currencies/sovi-finance/	8.09
7155	EduCoin	EDU	https://coinmarketcap.com//currencies/edu-coin/	0
7156	TON Crystal	TON	https://coinmarketcap.com//currencies/ton-crystal/	0.67
7157	Warp Finance	WARP	https://coinmarketcap.com//currencies/warp-finance/	797.15
7158	Rake Finance	RAK	https://coinmarketcap.com//currencies/rake-finance/	35.28
7159	YFIONE	YFO	https://coinmarketcap.com//currencies/yfione/	2046.16
7160	Super Bitcoin	SBTC	https://coinmarketcap.com//currencies/super-bitcoin/	1.13
7161	Balloon-X	BLX	https://coinmarketcap.com//currencies/balloon-x/	0.01
7162	EHash	EHASH	https://coinmarketcap.com//currencies/ehash/	1.2
7163	DefiBox	BOX	https://coinmarketcap.com//currencies/defibox/	18.21
7164	WELL	WELL	https://coinmarketcap.com//currencies/well-token/	0.78
7165	UniWorld	UNW	https://coinmarketcap.com//currencies/uniworld/	0.04
7166	CircleSwap	CIR	https://coinmarketcap.com//currencies/circleswap/	1.96
7167	Mirrored Google	mGOOGL	https://coinmarketcap.com//currencies/mirrored-google/	2115.42
7168	Toshimon	TOSHI	https://coinmarketcap.com//currencies/toshimon/	113.55
7169	BlackFisk	BLFI	https://coinmarketcap.com//currencies/blackfisk/	37.3
7170	BSB Token	BSBT	https://coinmarketcap.com//currencies/bsb-token/	0.28
7171	BNS Token	BNS	https://coinmarketcap.com//currencies/bns-token/	0.07
7172	3X Long Dogecoin Token	DOGEBULL	https://coinmarketcap.com//currencies/3x-long-dogecoin-token/	4371
7173	WAXE	WAXE	https://coinmarketcap.com//currencies/waxe/	112.25
7174	BSYS	BSYS	https://coinmarketcap.com//currencies/bsys/	0
7175	FOMPOUND	FOMP	https://coinmarketcap.com//currencies/fompound/	35.96
7176	DefiDollar DAO	DFD	https://coinmarketcap.com//currencies/defidollar-dao/	0.67
7177	BUMO	BU	https://coinmarketcap.com//currencies/bumo/	0.01
7178	3x Long Bitcoin SV Token	BSVBULL	https://coinmarketcap.com//currencies/3x-long-bitcoin-sv-token/	0.03
7179	Tigerfinance	TIGER	https://coinmarketcap.com//currencies/tigerfinance/	0.62
7180	EA Token	EA	https://coinmarketcap.com//currencies/ea-token/	0
7181	MDUKEY	MDU	https://coinmarketcap.com//currencies/mdu/	0.06
7182	D Community	DILI	https://coinmarketcap.com//currencies/d-community/	0
7183	3X Short Cardano Token	ADABEAR	https://coinmarketcap.com//currencies/3x-short-cardano-token/	0
7184	BELIEVER	BLVR	https://coinmarketcap.com//currencies/believer/	0.76
7185	Force For Fast	FFF	https://coinmarketcap.com//currencies/force-for-fast/	0.01
7186	SparkPoint Fuel	SFUEL	https://coinmarketcap.com//currencies/sparkpoint-fuel/	0.16
7187	Biscuit Farm Finance	BCU	https://coinmarketcap.com//currencies/biscuit-farm-finance/	0.67
7188	Dcoin Token	DT	https://coinmarketcap.com//currencies/dcoin-token/	0.02
7189	Degen Protocol	DGN	https://coinmarketcap.com//currencies/degen-protocol/	4.3
7190	Relevant	REL	https://coinmarketcap.com//currencies/relevant/	2.2
7191	Ludos Protocol	LUD	https://coinmarketcap.com//currencies/ludos/	0
7192	Akropolis Delphi	ADEL	https://coinmarketcap.com//currencies/akropolis-delphi/	0.16
7193	Assemble Protocol	ASM	https://coinmarketcap.com//currencies/assemble-protocol/	0.04
7194	YouLive Coin	UC	https://coinmarketcap.com//currencies/youlive-coin/	0
7195	DeFi of Thrones	DOTX	https://coinmarketcap.com//currencies/deli-of-thrones/	0.38
7196	MM Token	MM	https://coinmarketcap.com//currencies/mm-token/	14.23
7197	BiLira	TRYB	https://coinmarketcap.com//currencies/bilira/	0.13
7198	Unique Photo	FOTO	https://coinmarketcap.com//currencies/unique-photo/	0.33
7199	CoinMeet	MEET	https://coinmarketcap.com//currencies/coinmeet/	0.01
7200	Tsunami finance	NAMI	https://coinmarketcap.com//currencies/tsunami/	517.42
7201	XT.com Token	XT	https://coinmarketcap.com//currencies/xtcom-token/	9.36
7202	DefiDollar	DUSD	https://coinmarketcap.com//currencies/defidollar/	1
7203	3x Short Bitcoin SV Token	BSVBEAR	https://coinmarketcap.com//currencies/3x-short-bitcoin-sv-token/	0.01
7204	3x Short XRP Token	XRPBEAR	https://coinmarketcap.com//currencies/3x-short-xrp-token/	0
7205	UniWhales	UWL	https://coinmarketcap.com//currencies/uniwhales/	0.69
7206	Block Duelers NFT Battles	BDT	https://coinmarketcap.com//currencies/block-duelers/	264.98
7207	QURAS	XQC	https://coinmarketcap.com//currencies/quras/	0.02
7208	Buzzshow	GLDY	https://coinmarketcap.com//currencies/buzzshow/	0.09
7209	The Famous Token	TFT	https://coinmarketcap.com//currencies/the-famous-token/	44.46
7210	YUI Token	YUI	https://coinmarketcap.com//currencies/yui-token/	21.91
7211	VROOMGO	VRGX	https://coinmarketcap.com//currencies/vroomgo/	0
7212	Aloha	ALOHA	https://coinmarketcap.com//currencies/aloha/	0.04
7213	Zugacoin	SZC	https://coinmarketcap.com//currencies/zugacoin/	89.74
7214	CoralFarm	CRL	https://coinmarketcap.com//currencies/coralfarm/	1.21
7215	Alaya	ATP	https://coinmarketcap.com//currencies/alaya/	5.53
7216	Hive Dollar	HBD	https://coinmarketcap.com//currencies/hive-dollar/	1.2
7217	Project Inverse	XIV	https://coinmarketcap.com//currencies/project-inverse/	0.22
7218	FINANCIAL INVESTMENT TOKEN	FIT	https://coinmarketcap.com//currencies/financial-investment-token/	11.86
7219	TOKOK	TOK	https://coinmarketcap.com//currencies/tokok/	0.01
7220	Jinbi Token	JNB	https://coinmarketcap.com//currencies/jinbi-token/	352.22
7221	Xrpalike Gene	XAG	https://coinmarketcap.com//currencies/xrpalike-gene/	0.05
7222	Dexfin	DXF	https://coinmarketcap.com//currencies/dexfin/	0.01
7223	ThingsOperatingSystem	TOS	https://coinmarketcap.com//currencies/thingsoperatingsystem/	0
7224	Baroin	BRI	https://coinmarketcap.com//currencies/baroin/	1.77
7225	DexMex	DEXM	https://coinmarketcap.com//currencies/dexmex/	0.1
7226	AINORI	AIN	https://coinmarketcap.com//currencies/ainori/	0
7227	TecraCoin	TCR	https://coinmarketcap.com//currencies/tecracoin/	0.25
7228	3X Long TomoChain Token	TOMOBULL	https://coinmarketcap.com//currencies/3x-long-tomochain-token/	0.18
7229	Black Diamond Rating	HZT	https://coinmarketcap.com//currencies/black-diamond-rating/	0.04
7230	Hey Bitcoin	HYBN	https://coinmarketcap.com//currencies/hey-bitcoin/	0.33
7231	DIESEL	DIESEL	https://coinmarketcap.com//currencies/diesel/	1.22
7232	Kulupu	KLP	https://coinmarketcap.com//currencies/kulupu/	0.22
7233	Glox Finance	GLOX	https://coinmarketcap.com//currencies/glox-finance/	102.47
7234	GSPI Shopping.io Governance	GSPI	https://coinmarketcap.com//currencies/gspi-governance/	9.59
7235	Niubi Swap	NIU	https://coinmarketcap.com//currencies/niubiswap/	0.27
7236	YFII Gold	YFIIG	https://coinmarketcap.com//currencies/yfii-gold/	7.88
7237	OpenAlexa Protocol	OAP	https://coinmarketcap.com//currencies/openalexa-protocol/	2.4
7238	YFII Gold	YFIIG	https://coinmarketcap.com//currencies/yfii-gold/	7.88
7239	Ludena Protocol	LDN	https://coinmarketcap.com//currencies/ludena-protocol/	1.06
7240	Alibaba tokenized stock FTX	BABA	https://coinmarketcap.com//currencies/alibaba-tokenized-stock-ftx/	228.55
7241	ReapChain	REAP	https://coinmarketcap.com//currencies/reapchain/	0.1578
7242	Tranche Finance	SLICE	https://coinmarketcap.com//currencies/tranche-finance/	0.7489
7243	Ternio-ERC20	TERN	https://coinmarketcap.com//currencies/ternio-erc20/	6.17
7244	WhaleRoom	WHL	https://coinmarketcap.com//currencies/whaleroom/	5.84
7245	KOK	KOK	https://coinmarketcap.com//currencies/keystone-of-opportunity-knowledge/	0.5
7246	ETG Finance	ETGF	https://coinmarketcap.com//currencies/etg-finance/	1.07
7247	Akoin	AKN	https://coinmarketcap.com//currencies/akoin/	0.4183
7248	Index Cooperative	INDEX	https://coinmarketcap.com//currencies/index-cooperative/	20.71
7249	Shardus	ULT	https://coinmarketcap.com//currencies/shardus/	0.11
7250	Basis Dollar Share	BSDS	https://coinmarketcap.com//currencies/basis-dollar-share/	1.29
7251	Fisco Coin	FSCC	https://coinmarketcap.com//currencies/fisco/	0.67
7252	ARK Innovation ETF tokenized stock FTX	ARKK	https://coinmarketcap.com//currencies/ark-innovation-etf-tokenized-stock-ftx/	130.18
7253	Tesra	TSR	https://coinmarketcap.com//currencies/tesra/	0.01
7254	BitUP Token	BUT	https://coinmarketcap.com//currencies/bitup-token/	0
7255	Rowan Token	RWN	https://coinmarketcap.com//currencies/rowan-token/	0.01
7256	Dipper Network	DIP	https://coinmarketcap.com//currencies/dipper-network/	0.01
7257	Metrix Coin	MRX	https://coinmarketcap.com//currencies/metrix-coin/	0
7258	YAMv2	YAMV2	https://coinmarketcap.com//currencies/yam-v2/	12.94
7259	Tamy Token	TMT	https://coinmarketcap.com//currencies/tamy-token/	0.03
7260	BlockNoteX	BNOX	https://coinmarketcap.com//currencies/blocknotex/	0.57
7261	Uniris	UCO	https://coinmarketcap.com//currencies/uniris/	0.16
7262	UniDex	UNIDX	https://coinmarketcap.com//currencies/unidex/	0.74
7263	Shrimp Capital	SHRMP	https://coinmarketcap.com//currencies/shrimp-capital/	0.03
7264	MP4	MP4	https://coinmarketcap.com//currencies/mp4/	0.01
7265	Kangal	KANGAL	https://coinmarketcap.com//currencies/kangal/	0
7266	Temtum	TEM	https://coinmarketcap.com//currencies/temtum/	0.83
7267	Vectorspace AI	VXV	https://coinmarketcap.com//currencies/vectorspace-ai/	0.75
7268	FANZY	FX1	https://coinmarketcap.com//currencies/fanzy/	0
7269	Pastel	PSL	https://coinmarketcap.com//currencies/pastel/	0
7270	QFinance	QFI	https://coinmarketcap.com//currencies/qfinance/	1.2
7271	DRC mobility	DRC	https://coinmarketcap.com//currencies/drc-mobility/	0.15
7272	Easticoin	ESTI	https://coinmarketcap.com//currencies/easticoin/	0.02
7273	BULLS	BULLS	https://coinmarketcap.com//currencies/bulls/	0.04
7274	STABLE ASSET	STA	https://coinmarketcap.com//currencies/stable-asset/	0.64
7275	3X Long TRX Token	TRXBULL	https://coinmarketcap.com//currencies/3x-long-trx-token/	4.12
7276	GrafSound	GSMT	https://coinmarketcap.com//currencies/grafsound/	0.33
7277	Chickenkebab Finance	CHIK	https://coinmarketcap.com//currencies/chickenkebab-finance/	0.17
7278	renFIL	RENFIL	https://coinmarketcap.com//currencies/renfil/	57.7
7279	BitcoinX	BCX	https://coinmarketcap.com//currencies/bitcoinx/	0
7280	RamenSwap	RAMEN	https://coinmarketcap.com//currencies/ramenswap/	2.67
7281	Exen Coin	EXEN	https://coinmarketcap.com//currencies/exen-coin/	1.78
7282	Keep3r BSC Network	KP3RB	https://coinmarketcap.com//currencies/keep3r-bsc-network/	5.39
7283	Earn Defi Coin	EDC	https://coinmarketcap.com//currencies/earn-defi/	15.79
7284	ARCS	ARX	https://coinmarketcap.com//currencies/arcs/	0.32
7285	SHD CASH	SHDC	https://coinmarketcap.com//currencies/shd-cash/	0.03
7286	Jetfuel Finance	FUEL	https://coinmarketcap.com//currencies/jetfuel-finance/	189.93
7287	CyberTime Finance Token	CTF	https://coinmarketcap.com//currencies/cybertime-finance-token/	49.04
7288	Stacker Ventures	STACK	https://coinmarketcap.com//currencies/stacker-ventures/	30.72
7289	Water Finance	WTR	https://coinmarketcap.com//currencies/water-finance/	93.1
7290	Oracle System	ORC	https://coinmarketcap.com//currencies/oracle-system/	0
7291	CoinLoan	CLT	https://coinmarketcap.com//currencies/coinloan/	8.35
7292	Kevacoin	KVA	https://coinmarketcap.com//currencies/kevacoin/	0
7293	Crow Finance	CROW	https://coinmarketcap.com//currencies/crow-finance/	274.36
7294	Mithril Share	MIS	https://coinmarketcap.com//currencies/mithril-share/	4.16
7295	Puriever	PURE	https://coinmarketcap.com//currencies/puriever/	0.55
7296	GoldFinX	GIX	https://coinmarketcap.com//currencies/goldfinx/	2.83
7297	Eight Hours	EHRT	https://coinmarketcap.com//currencies/eight-hours/	0
7298	Landbox	LAND	https://coinmarketcap.com//currencies/landbox/	7.55
7299	Dynamic Set Dollar	DSD	https://coinmarketcap.com//currencies/dynamic-set-dollar/	0.07
7300	Ndau	NDAU	https://coinmarketcap.com//currencies/ndau/	18.18
7301	GALAXY NETWORK	GNC	https://coinmarketcap.com//currencies/galaxy-network/	0.01
7302	TokenAsset	NTB	https://coinmarketcap.com//currencies/tokenasset/	0.42
7303	3X Long BitMax Token Token	BTMXBULL	https://coinmarketcap.com//currencies/3x-long-bitmax-token-token/	724.38
7304	PayYoda	YOT	https://coinmarketcap.com//currencies/payyoda/	0.07
7305	UTU Protocol	UTU	https://coinmarketcap.com//currencies/utu-protocol/	0.14
7306	Pofid Dao	PFID	https://coinmarketcap.com//currencies/pofid-dao/	28.24
7307	World Token	WORLD	https://coinmarketcap.com//currencies/world-token/	0.08
7308	SportX	SX	https://coinmarketcap.com//currencies/sportx/	0.37
7309	xFund	XFUND	https://coinmarketcap.com//currencies/xfund/	613.47
7310	Nyan Finance	NYAN	https://coinmarketcap.com//currencies/nyan-finance/	1.87
7311	The Hash Speed	THS	https://coinmarketcap.com//currencies/the-hash-speed/	0.22
7312	ReFork	EFK	https://coinmarketcap.com//currencies/refork/	0.02
7313	3X Short Algorand Token	ALGOBEAR	https://coinmarketcap.com//currencies/3x-short-algorand-token/	0
7314	CoinHe Token	CHT	https://coinmarketcap.com//currencies/coinhe-token/	0.07
7315	Cybereits	CRE	https://coinmarketcap.com//currencies/cybereits/	0
7316	Ballswap	BSP	https://coinmarketcap.com//currencies/ballswap/	0
7317	Mute	MUTE	https://coinmarketcap.com//currencies/mute/	0.28
7318	Index Chain	IDX	https://coinmarketcap.com//currencies/index-chain/	0
7319	FlourMix	FLO	https://coinmarketcap.com//currencies/flourmix/	0.71
7320	Promotion Coin	PC	https://coinmarketcap.com//currencies/promotion-coin/	0
7321	Serum Ecosystem Token	SECO	https://coinmarketcap.com//currencies/serum-ecosystem-token/	4.91
7322	PizzaSwap	PIZZA	https://coinmarketcap.com//currencies/pizzaswap/	8.09
7323	3x Short Litecoin Token	LTCBEAR	https://coinmarketcap.com//currencies/3x-short-litecoin-token/	0.13
7324	Global Gaming	GMNG	https://coinmarketcap.com//currencies/global-gaming/	0
7325	United Bitcoin	UBTC	https://coinmarketcap.com//currencies/united-bitcoin/	0.77
7326	YFi Management	YEFIM	https://coinmarketcap.com//currencies/yfi-management/	18.38
7327	Prophecy	PRY	https://coinmarketcap.com//currencies/prophecy/	0.05
7328	3X Short Cosmos Token	ATOMBEAR	https://coinmarketcap.com//currencies/3x-short-cosmos-token/	0
7329	UBU Finance	UBU	https://coinmarketcap.com//currencies/ubu-finance/	0.55
7330	Ti-Value	TV	https://coinmarketcap.com//currencies/ti-value/	0.01
7331	DFOhub	BUIDL	https://coinmarketcap.com//currencies/dfohub/	4.65
7332	V-Dimension	VOLLAR	https://coinmarketcap.com//currencies/v-dimension/	0.37
7333	Divert Finance	DEVE	https://coinmarketcap.com//currencies/divert-finance/	153.6
7334	Lukki Operating Token	LOT	https://coinmarketcap.com//currencies/lukki-operating-token/	0
7335	LCG	LCG	https://coinmarketcap.com//currencies/lcg/	0.02
7336	Basis Dollar	BSD	https://coinmarketcap.com//currencies/basis-dollar/	0.07057
7337	SIMBA Storage Token	SST	https://coinmarketcap.com//currencies/simba-storage-token/	0.4969
7338	Tcoin	TCO	https://coinmarketcap.com//currencies/tcoin-token/	0.0014
7339	ROCKI	ROCKS	https://coinmarketcap.com//currencies/rocki/	0.211
7340	Bitcashpay	BCP	https://coinmarketcap.com//currencies/bitcashpay/	0.01636
7341	Satopay Network	STOP	https://coinmarketcap.com//currencies/satopay-network/	0.002692
7342	Buxcoin	BUX	https://coinmarketcap.com//currencies/buxcoin/	0.09464
7343	Global Digital Content	GDC	https://coinmarketcap.com//currencies/global-digital-content/	0.004648
7344	GYEN	GYEN	https://coinmarketcap.com//currencies/gyen/	0.009173
7345	3X Long Midcap Index Token	MIDBULL	https://coinmarketcap.com//currencies/3x-long-midcap-index-token/	3275
7346	Shabu Shabu Finance	KOBE	https://coinmarketcap.com//currencies/shabu-shabu-finance/	4.88
7347	bZx Vesting Token	VBZRX	https://coinmarketcap.com//currencies/vbzrx/	0.16
7348	BitoPro Exchange Token	BITO	https://coinmarketcap.com//currencies/bitopro-exchange-token/	0.05
7349	Debase	DEBASE	https://coinmarketcap.com//currencies/debase/	0.5
7350	NNB Token	NNB	https://coinmarketcap.com//currencies/nnb-token/	0
7351	Soju Finance	SOJU	https://coinmarketcap.com//currencies/soju-finance/	0.45
7352	Gstcoin	GST	https://coinmarketcap.com//currencies/gstcoin/	0.35
7353	Golden Token	GOLD	https://coinmarketcap.com//currencies/golden-token/	0.09
7354	LONG COIN	LONG	https://coinmarketcap.com//currencies/long-coin/	0
7355	DegenVC	DGVC	https://coinmarketcap.com//currencies/degenvc/	1.26
7356	Freecash	FCH	https://coinmarketcap.com//currencies/freecash/	0.09
7357	CRYPTO20	C20	https://coinmarketcap.com//currencies/c20/	2.82
7358	USDFreeLiquidity	USDFL	https://coinmarketcap.com//currencies/usdfreeliquidity/	0.9
7359	Billibilli Inc tokenized stock FTX	BILI	https://coinmarketcap.com//currencies/billibilli-inc-tokenized-stock-ftx/	115.83
7360	APOyield	SOUL	https://coinmarketcap.com//currencies/apoyield/	0
7361	Ziktalk	ZIK	https://coinmarketcap.com//currencies/ziktalk/	0
7362	Cenfura Token	XCF	https://coinmarketcap.com//currencies/cenfura-token/	0.13
7363	Arcona	ARCONA	https://coinmarketcap.com//currencies/arcona/	0.29
7364	Libonomy	LBY	https://coinmarketcap.com//currencies/libonomy/	4.58
7365	Level01	LVX	https://coinmarketcap.com//currencies/level01/	0.02
7366	BAEPAY	BAEPAY	https://coinmarketcap.com//currencies/baepay/	0.4
7367	Space Cow Boy	SCB	https://coinmarketcap.com//currencies/space-cow-boy/	17.09
7368	Spore Finance	SPORE	https://coinmarketcap.com//currencies/spore/	14.87
7369	GYSR	GYSR	https://coinmarketcap.com//currencies/gysr/	0.67
7370	Narwhale.finance	NAWA	https://coinmarketcap.com//currencies/narwhale-finance/	0.09
7371	VidyX	VIDYX	https://coinmarketcap.com//currencies/vidyx/	0.17
7372	ZUSD	ZUSD	https://coinmarketcap.com//currencies/zusd/	1
7373	Bincentive	BCNT	https://coinmarketcap.com//currencies/bincentive/	0.01
7374	Web Coin Pay	WEC	https://coinmarketcap.com//currencies/web-coin-pay/	2.03
7375	Experty Wisdom Token	WIS	https://coinmarketcap.com//currencies/experty-wisdom-token/	0.04
7376	Swaprol	SWPRL	https://coinmarketcap.com//currencies/swaprol/	0.01
7377	Xion Finance	XGT	https://coinmarketcap.com//currencies/xion-finance/	0.24
7378	Elevate	ELE	https://coinmarketcap.com//currencies/elevate/	0.65
7379	QUSD	QUSD	https://coinmarketcap.com//currencies/qian-qusd/	0.97
7380	PHILLIPS PAY COIN	PPC	https://coinmarketcap.com//currencies/philips-pay-coin/	0
7381	One Share	ONS	https://coinmarketcap.com//currencies/one-share/	6.85
7382	CSPC	CSPC	https://coinmarketcap.com//currencies/cspc/	0
7383	Spockchain Network	SPOK	https://coinmarketcap.com//currencies/spockchain-network/	0.01
7384	Meter Stable	MTR	https://coinmarketcap.com//currencies/meter-stable/	0.84
7385	Anime Token	ANI	https://coinmarketcap.com//currencies/anime-token/	0.1
7386	DefHold	DEFO	https://coinmarketcap.com//currencies/defhold/	119.8
7387	NEXT	NET	https://coinmarketcap.com//currencies/next/	0.14
7388	Tutor's Diary	TUDA	https://coinmarketcap.com//currencies/tutors-diary/	0.01
7389	JSB FOUNDATION	JSB	https://coinmarketcap.com//currencies/jsb-foundation/	1.77
7390	BitDNS	DNS	https://coinmarketcap.com//currencies/bitdns/	0.41
7391	Rozeus	ROZ	https://coinmarketcap.com//currencies/rozeus/	0.01
7392	3X Short Chainlink Token	LINKBEAR	https://coinmarketcap.com//currencies/3x-short-chainlink-token/	0
7393	MiniSwap	MINI	https://coinmarketcap.com//currencies/miniswap/	0.05
7394	Coinbase Pre-IPO tokenized stock FTX	CBSE	https://coinmarketcap.com//currencies/coinbase-pre-ipo-tokenized-stock-ftx/	444.26
7395	Galaxy Wallet	GC	https://coinmarketcap.com//currencies/galaxy-wallet/	0.01
7396	Liquidity Dividends Protocol	LID	https://coinmarketcap.com//currencies/liquidity-dividends-protocol/	0.04
7397	Portal	PORTAL	https://coinmarketcap.com//currencies/portal/	0
7398	sETH	SETH	https://coinmarketcap.com//currencies/seth/	1762.27
7399	Wagerr	WGR	https://coinmarketcap.com//currencies/wagerr/	0.05
7400	VinDax Coin	VD	https://coinmarketcap.com//currencies/vindax-coin/	0.01
7401	LinkToken	LTK	https://coinmarketcap.com//currencies/linktoken/	0.02
7402	PayRue (Propel)	PROPEL	https://coinmarketcap.com//currencies/payrue-propel/	0
7403	Uberstate RIT 2.0	RIT20	https://coinmarketcap.com//currencies/uberstate-inc/	0.85
7404	BITCOINHEDGE	BTCHG	https://coinmarketcap.com//currencies/bitcoinhedge/	7.63
7405	CAPITAL X CELL	CXC	https://coinmarketcap.com//currencies/capital-x-cell/	0.05
7406	BUILD Finance	BUILD	https://coinmarketcap.com//currencies/build-finance/	33.45
7407	COVIR.IO	CVR	https://coinmarketcap.com//currencies/covir-io/	40.54
7408	3x Long Tezos Token	XTZBULL	https://coinmarketcap.com//currencies/3x-long-tezos-token/	35.66
7409	CAD Coin	CADC	https://coinmarketcap.com//currencies/cad-coin/	0.78
7410	Strudel Finance	$TRDL	https://coinmarketcap.com//currencies/strudel-finance/	2.62
7411	REALPAY	RAP	https://coinmarketcap.com//currencies/realpay/	0.56
7412	DSYS	DSYS	https://coinmarketcap.com//currencies/dsys/	0
7413	DoDreamChain	DRM	https://coinmarketcap.com//currencies/dodreamchain/	0.05
7414	JD Coin	JDC	https://coinmarketcap.com//currencies/jd-coin/	0.14
7415	Absorber Protocol	ABS	https://coinmarketcap.com//currencies/absorber-protocol/	5.47
7416	Interop	TROP	https://coinmarketcap.com//currencies/interop/	37.38
7417	Bankcoin Reserve	BCR	https://coinmarketcap.com//currencies/bankcoin-reserve/	12170.24
7418	CommunityToken	CT	https://coinmarketcap.com//currencies/communitytoken/	0
7419	BioNTech tokenized stock FTX	BNTX	https://coinmarketcap.com//currencies/biontech-tokenized-stock-ftx/	108.16
7420	ARMTOKEN	TARM	https://coinmarketcap.com//currencies/armtoken/	0.02
7421	CORN	CORN	https://coinmarketcap.com//currencies/corn/	10.27
7422	UNIFI DeFi	UNIFI	https://coinmarketcap.com//currencies/unifi-defi/	0.01
7423	HappinessToken	HPS	https://coinmarketcap.com//currencies/happinesstoken/	4.48
7424	N3RD Finance	N3RDz	https://coinmarketcap.com//currencies/n3rd-finance/	159.46
7425	CryptoKek	KEK	https://coinmarketcap.com//currencies/cryptokek/	0.08
7426	3X Long Huobi Token Token	HTBULL	https://coinmarketcap.com//currencies/3x-long-huobi-token-token/	446.9
7427	Crypto Heroes	HEROES	https://coinmarketcap.com//currencies/crypto-heroes/	1.02
7428	Blockchain Exchange Alliance	BXA	https://coinmarketcap.com//currencies/blockchain-exchange-alliance/	0
7429	3x Short Bitcoin Cash Token	BCHBEAR	https://coinmarketcap.com//currencies/3x-short-bitcoin-cash-token/	0.05
7430	MP3	MP3	https://coinmarketcap.com//currencies/mp3/	0.03
7431	R34P	R34P	https://coinmarketcap.com//currencies/r34p/	15.79
7432	CheeseSwap	CHS	https://coinmarketcap.com//currencies/cheeseswap/	0.59
7433	Punk	PUNK	https://coinmarketcap.com//currencies/punk/	156276.48
7434	Bit Financial	BFC	https://coinmarketcap.com//currencies/bit-financial/	0.02
7435	DEJAVE	DJV	https://coinmarketcap.com//currencies/dejave/	2025.01
7436	United	UTED	https://coinmarketcap.com//currencies/united/	0.006137
7437	ECOC Financial Growth	EFG	https://coinmarketcap.com//currencies/ecoc-financial-growth/	18.76
7438	FOCV	FOCV	https://coinmarketcap.com//currencies/focv/	0.009672
7439	YAS	YAS	https://coinmarketcap.com//currencies/yas/	0.03239
7440	Viking Swap	VIKING	https://coinmarketcap.com//currencies/viking-swap/	0.3271
7441	PieDAO Balanced Crypto Pie	BCP	https://coinmarketcap.com//currencies/piedao-balanced-crypto-pie/	3.25
7442	YMAX	YMAX	https://coinmarketcap.com//currencies/ymax/	9.89
7443	Transmute Protocol	XPB	https://coinmarketcap.com//currencies/transmute-protocol/	0.8131
7444	Daikicoin	DIC	https://coinmarketcap.com//currencies/daikicoin/	0.147
7445	SHD CASH	SHDC	https://coinmarketcap.com//currencies/shd-cash/	0.03
7446	Asia Reserve Currency Coin	ARCC	https://coinmarketcap.com//currencies/asia-reserve-currency-coin/	0.11
7447	K-Tune	KTT	https://coinmarketcap.com//currencies/k-tune/	0.13
7448	Cometh	MUST	https://coinmarketcap.com//currencies/cometh/	188.93
7449	Kiwi Finance	KIWI	https://coinmarketcap.com//currencies/kiwi-finance/	8.03
7450	DUO Network Token	DUO	https://coinmarketcap.com//currencies/duo-network-token/	0
7451	BitDEFi	BFI	https://coinmarketcap.com//currencies/bitdefi/	12.81
7452	Upper Dollar	USDU	https://coinmarketcap.com//currencies/upper-dollar/	1
7453	Deipool	DIP	https://coinmarketcap.com//currencies/deipool/	0.14
7454	Wrapped Celo	WCELO	https://coinmarketcap.com//currencies/wrapped-celo/	3.75
7455	AICON	AICO	https://coinmarketcap.com//currencies/aicon/	0.08
7456	KP2R.Network	KP2R	https://coinmarketcap.com//currencies/kp2r-network/	0.74
7457	LavaSwap	LAVA	https://coinmarketcap.com//currencies/lavaswap/	0.63
7458	YFIX Finance	YFIX	https://coinmarketcap.com//currencies/yfix-finance/	2.5
7459	Jubi Token	JT	https://coinmarketcap.com//currencies/jubi-token/	0.14
7460	Meter Governance mapped by Meter.io	eMTRG	https://coinmarketcap.com//currencies/meter-governance-mapped-by-meter-io/	1.24
7461	Jackpot	777	https://coinmarketcap.com//currencies/jackpot/	0.16
7462	KeyFi	KEYFI	https://coinmarketcap.com//currencies/keyfi/	2.07
7463	Extend Finance	EXF	https://coinmarketcap.com//currencies/extend-finance/	0
7464	PUML Better Health	PUML	https://coinmarketcap.com//currencies/puml-better-health/	0.02
7465	Grain	GRAIN	https://coinmarketcap.com//currencies/grain-token/	0.04
7466	Ulgen Hash Power	UHP	https://coinmarketcap.com//currencies/ulgen-hash-power/	0.05
7467	Goldblock	GBK	https://coinmarketcap.com//currencies/goldblock/	0
7468	LYFE	LYFE	https://coinmarketcap.com//currencies/lyfe/	0.04
7469	Xriba	XRA	https://coinmarketcap.com//currencies/xriba/	0.04
7470	Crudeoil Finance	OIL	https://coinmarketcap.com//currencies/crudeoil-finance/	11.62
7471	Yearn Loans Finance	YLFI	https://coinmarketcap.com//currencies/yearn-loans-finance/	15.37
7472	Interfinex	IFEX	https://coinmarketcap.com//currencies/interfinex/	0
7473	Smol	SMOL	https://coinmarketcap.com//currencies/smol/	31.15
7474	Gbrick	GBX	https://coinmarketcap.com//currencies/gbrick/	0.01
7475	Fashion Coin	FSHN	https://coinmarketcap.com//currencies/fashion-coin/	0
7476	bXIOT	BXIOT	https://coinmarketcap.com//currencies/bxiot/	2.69
7477	BLOCKMAX	OCB	https://coinmarketcap.com//currencies/blockmax/	0.04
7478	3X Long Shitcoin Index Token	BULLSHIT	https://coinmarketcap.com//currencies/3x-long-shitcoin-index-token/	6490
7479	Krypton Galaxy Coin	KGC	https://coinmarketcap.com//currencies/krypton-galaxy-coin/	0
7480	Cryptochrome	CHM	https://coinmarketcap.com//currencies/cryptochrome/	0.02
7481	Dogz	DOGZ	https://coinmarketcap.com//currencies/dogz/	0
7482	Libfx	LIBFX	https://coinmarketcap.com//currencies/libfx/	0.31
7483	MEX	MEX	https://coinmarketcap.com//currencies/mex/	0
7484	Naruto BSC	NARUTO2	https://coinmarketcap.com//currencies/naruto-bsc/	0.13
7485	YFTether	YFTE	https://coinmarketcap.com//currencies/yftether/	73.86
7486	Dymmax	DMX	https://coinmarketcap.com//currencies/dymmax/	1.72
7487	Crypto Price Index	CPI	https://coinmarketcap.com//currencies/crypto-price-index/	0.2
7488	X-Block	IX	https://coinmarketcap.com//currencies/x-block/	0
7489	JFIN	JFC	https://coinmarketcap.com//currencies/jfin/	0.17
7490	Dragonbit	DRGB	https://coinmarketcap.com//currencies/dragonbit/	0
7491	TUNE TOKEN	TUNE	https://coinmarketcap.com//currencies/tune-token/	0
7492	NanTrade	NAN	https://coinmarketcap.com//currencies/nantrade/	0.36
7493	Reserve	RSV	https://coinmarketcap.com//currencies/reserve/	1.01
7494	MGC Token	MGC	https://coinmarketcap.com//currencies/mgc-token/	0
7495	Grace Period Token	GPT	https://coinmarketcap.com//currencies/grace-period-token/	1015.87
7496	ACoconut	AC	https://coinmarketcap.com//currencies/acoconut/	2.34
7497	EtherBone	ETHBN	https://coinmarketcap.com//currencies/etherbone/	0
7498	YFOX FINANCE	YFOX	https://coinmarketcap.com//currencies/yfox-finance/	12.9
7499	CAT.trade Protocol	CATX	https://coinmarketcap.com//currencies/cat-trade-protocol/	0.11
7500	Bundles Finance	BUND	https://coinmarketcap.com//currencies/bundles/	22.33
7501	Upper Pound	GBPU	https://coinmarketcap.com//currencies/upper-pound/	1.38
7502	Polkainsure Finance	PIS	https://coinmarketcap.com//currencies/polkainsure-finance/	25.8
7503	AMC Entertainment Holdings tokenized stock FTX	AMC	https://coinmarketcap.com//currencies/amc-entertainment-holdings-tokenized-stock-ftx/	12.94
7504	SUP8EME	SUP8EME	https://coinmarketcap.com//currencies/sup8eme/	4.51
7505	Grayscale Bitcoin Trust tokenized stock FTX	GBTC	https://coinmarketcap.com//currencies/grayscale-bitcoin-trust-tokenized-stock-ftx/	50.53
7506	Candy Protocol	CAD	https://coinmarketcap.com//currencies/candy-protocol/	0.8
7507	CENTERCOIN	CENT	https://coinmarketcap.com//currencies/centercoin/	0
7508	Hydro	HYDRO	https://coinmarketcap.com//currencies/hydro/	0
7509	LemoChain	LEMO	https://coinmarketcap.com//currencies/lemochain/	0
7510	hodlearn	HODL	https://coinmarketcap.com//currencies/hodlearn-net/	0
7511	Alchemy	ACOIN	https://coinmarketcap.com//currencies/alchemy/	0.8
7512	WingShop	WING	https://coinmarketcap.com//currencies/wingshop/	0
7513	BeeSwap	BEE	https://coinmarketcap.com//currencies/beeswap/	5.85
7514	Ztranzit Coin	ZTNZ	https://coinmarketcap.com//currencies/ztranzit-coin/	7.81
7515	Bullswap Exchange	BVL	https://coinmarketcap.com//currencies/bullswap-exchange/	0
7516	DeFiSocial Gaming	DFSOCIAL	https://coinmarketcap.com//currencies/defisocial-gaming/	107.52
7517	3X Short BNB Token	BNBBEAR	https://coinmarketcap.com//currencies/3x-short-bnb-token/	0
7518	One Cash	ONC	https://coinmarketcap.com//currencies/one-cash/	0.44
7519	IterationSyndicate	ITS	https://coinmarketcap.com//currencies/iterationsyndicate/	45.4
7520	Bytus	BYTS	https://coinmarketcap.com//currencies/bytus/	1.23
7521	FIRST INTERCHANGEABLE TOKEN	FIT	https://coinmarketcap.com//currencies/first-interchangeable-token/	18.65
7522	Sphere	SPHR	https://coinmarketcap.com//currencies/sphere/	0.34
7523	Goldcoin	GLC	https://coinmarketcap.com//currencies/goldcoin/	0.18
7524	Camp	CAMP	https://coinmarketcap.com//currencies/camp/	0
7525	growth Root Token	GROOT	https://coinmarketcap.com//currencies/growth-root-token/	539.62
7526	3X Short Sushi Token	SUSHIBEAR	https://coinmarketcap.com//currencies/3x-short-sushi-token/	0
7527	FME	FME	https://coinmarketcap.com//currencies/fme/	0
7528	Carrot	CRT	https://coinmarketcap.com//currencies/carrot/	0.18
7529	UltrAlpha	UAT	https://coinmarketcap.com//currencies/ultralpha/	0.06
7530	ZYX	ZYX	https://coinmarketcap.com//currencies/zyx/	0.01
7531	PeerEx	PERX	https://coinmarketcap.com//currencies/peerex/	0
7532	intexcoin	INTX	https://coinmarketcap.com//currencies/intexcoin/	0
7533	Justdobet	JDB	https://coinmarketcap.com//currencies/justdobet/	1.23
7534	bitCEO	BCEO	https://coinmarketcap.com//currencies/bitceo/	0.36
7535	DOOS TOKEN	DOOS	https://coinmarketcap.com//currencies/doos-token/	0.07111
7536	Metric Exchange	METRIC	https://coinmarketcap.com//currencies/metric-exchange/	3.29
7537	Chintai	CHEX	https://coinmarketcap.com//currencies/chex-token/	0.004593
7538	Pokeball	POKE	https://coinmarketcap.com//currencies/pokeball/	0.1901
7539	Spice	SPICE	https://coinmarketcap.com//currencies/spice/	0.003735
7540	Groovy Finance	GVY	https://coinmarketcap.com//currencies/groovy-finance/	4.65
7541	KONJUNGATE	KONJ	https://coinmarketcap.com//currencies/konjungate/	0.007032
7542	1x Short Bitcoin Token	HEDGE	https://coinmarketcap.com//currencies/1x-short-bitcoin-token/	117.65
7543	Potentiam	PTM	https://coinmarketcap.com//currencies/potentiam/	0.05209
7544	RYI Platinum	RYIP	https://coinmarketcap.com//currencies/ryi-platinum/	3.67
7545	Spore Engineering	SPORE	https://coinmarketcap.com//currencies/spore-engineering/	5.48
7546	Lottonation	LNT	https://coinmarketcap.com//currencies/lottonation/	0.05
7547	Civitas Protocol	CVT	https://coinmarketcap.com//currencies/civitas-protocol/	217.48
7548	4ART Coin	4ART	https://coinmarketcap.com//currencies/4artechnologies/	0.18
7549	Moonday Finance	MOONDAY	https://coinmarketcap.com//currencies/moonday-finance/	52.12
7550	LUXOCHAIN	LUXO	https://coinmarketcap.com//currencies/luxochain/	0.19
7551	STPAY	STP	https://coinmarketcap.com//currencies/stpay/	5.89
7552	Release Project	REL	https://coinmarketcap.com//currencies/release-project/	0
7553	Nydronia	NIA	https://coinmarketcap.com//currencies/nydronia/	0
7554	Guapcoin	GUAP	https://coinmarketcap.com//currencies/guapcoin/	0.37
7555	MDsquare	TMED	https://coinmarketcap.com//currencies/mdsquare/	0
7556	Future1coin	F1C	https://coinmarketcap.com//currencies/future1coin/	0
7557	yRise Finance	YRISE	https://coinmarketcap.com//currencies/yrise-finance/	1.36
7558	HLand Token	HLAND	https://coinmarketcap.com//currencies/hland-token/	29.24
7559	Definex	DSWAP	https://coinmarketcap.com//currencies/definex/	1.79
7560	Digex	DIGEX	https://coinmarketcap.com//currencies/digex/	0
7561	BigBang Core	BBC	https://coinmarketcap.com//currencies/bigbang-core/	0.14
7562	XDAG	XDAG	https://coinmarketcap.com//currencies/xdag/	0.01
7563	Vera Cruz Coin	VCCO	https://coinmarketcap.com//currencies/vera-cruz-coin/	2.14
7564	IOEX	IOEX	https://coinmarketcap.com//currencies/ioex/	0.03
7565	Crypto Cricket Club	3Cs	https://coinmarketcap.com//currencies/crypto-cricket-club/	0.07
7566	My Crypto Play	MCP	https://coinmarketcap.com//currencies/my-crypto-play/	0.13
7567	Globalvillage Ecosystem	GVE	https://coinmarketcap.com//currencies/globalvillage-ecosystem/	0
7568	3X Long Altcoin Index Token	ALTBULL	https://coinmarketcap.com//currencies/3x-long-altcoin-index-token/	429.62
7569	Punk Basic	PUNK-BASIC	https://coinmarketcap.com//currencies/punk-basic/	36112.39
7570	OTCBTC Token	OTB	https://coinmarketcap.com//currencies/otcbtc-token/	0.01
7571	Grom	GR	https://coinmarketcap.com//currencies/grom/	3.73
7572	VKF Platform	VKF	https://coinmarketcap.com//currencies/vkf-platform/	0
7573	IZE	IZE	https://coinmarketcap.com//currencies/ize/	0.06
7574	1irstGold	1GOLD	https://coinmarketcap.com//currencies/1irstgold/	60.08
7575	Diamond	DMD	https://coinmarketcap.com//currencies/diamond/	2.53
7576	Social Rocket	ROCKS	https://coinmarketcap.com//currencies/social-rocket/	1.6
7577	Sav3Token	SAV3	https://coinmarketcap.com//currencies/sav3token/	0.05
7578	Tesla tokenized stock Bittrex	TSLA	https://coinmarketcap.com//currencies/tesla-tokenized-stock-bittrex/	704.72
7579	VersoView	VVT	https://coinmarketcap.com//currencies/versoview/	0.16
7580	TEAL	TEAT	https://coinmarketcap.com//currencies/teal/	0.14
7581	swiss.finance	SWISS	https://coinmarketcap.com//currencies/swiss-finance/	62.63
7582	Utile Network	UTL	https://coinmarketcap.com//currencies/utile-network/	0.05
7583	SERGS	SERGS	https://coinmarketcap.com//currencies/sergs/	0.48
7584	UNCL	UNCL	https://coinmarketcap.com//currencies/uncl/	12.22
7585	Ethanol	ENOL	https://coinmarketcap.com//currencies/ethanol/	162.35
7586	SWAG Finance	SWAG	https://coinmarketcap.com//currencies/swag-finance/	0.07
7587	Freedom Reserve	FR	https://coinmarketcap.com//currencies/freedom-reserve/	0
7588	YFFS Finance	YFFS	https://coinmarketcap.com//currencies/yffs/	1.56
7589	Zer-Dex	ZDX	https://coinmarketcap.com//currencies/zer-dex/	0
7590	dForce USDx	USDX	https://coinmarketcap.com//currencies/dforce-usdx/	1.02
7591	GSENetwork	GSE	https://coinmarketcap.com//currencies/gsenetwork/	0
7592	Joys Digital	JOYS	https://coinmarketcap.com//currencies/joys-digital/	0.01
7593	Dionpay	DION	https://coinmarketcap.com//currencies/dionpay/	0
7594	Crypto Rewards Studio	CRS	https://coinmarketcap.com//currencies/crypto-rewards-studio/	0.01
7595	KING OF DEFI	KODX	https://coinmarketcap.com//currencies/king-of-defi/	60.52
7596	Utopia Genesis Foundation	UOP	https://coinmarketcap.com//currencies/utopia-genesis-foundation/	0.41
7597	01coin	ZOC	https://coinmarketcap.com//currencies/01coin/	0
7598	Stronghold Token	SHX	https://coinmarketcap.com//currencies/stronghold-token/	0
7599	DoYourTip	DYT	https://coinmarketcap.com//currencies/doyourtip/	0.57
7600	Morpher	MPH	https://coinmarketcap.com//currencies/morpher/	0.04
7601	UniDexGas	UNDG	https://coinmarketcap.com//currencies/unidexgas/	190.72
7602	Mazzuma	MAZ	https://coinmarketcap.com//currencies/mazzuma/	0.73
7603	Yeld Finance	YELD	https://coinmarketcap.com//currencies/yeld-finance/	23.42
7604	Advanced Internet Blocks	AIB	https://coinmarketcap.com//currencies/advanced-internet-blocks/	0.02
7605	Thirm Protocol	THIRM	https://coinmarketcap.com//currencies/thirm-protocol/	10.9
7606	Bali Coin	BALI	https://coinmarketcap.com//currencies/bali-coin/	0.02
7607	Holyheld	HH	https://coinmarketcap.com//currencies/holyheld/	0.93
7608	Birthday Cake	BDAY	https://coinmarketcap.com//currencies/birthday-cake/	0.11
7609	BitTube	TUBE	https://coinmarketcap.com//currencies/bit-tube/	0.01
7610	Bancor Governance Token	VBNT	https://coinmarketcap.com//currencies/bancor-governance-token/	4.16
7611	Basix	BASX	https://coinmarketcap.com//currencies/basix/	0.69
7612	GM Holding	GM	https://coinmarketcap.com//currencies/gm-holding/	0.01
7613	Freight Trust & Clearing Network	EDI	https://coinmarketcap.com//currencies/freight-trust-clearing-network/	0
7614	Vanywhere	VANY	https://coinmarketcap.com//currencies/vanywhere/	0
7615	FANBI TOKEN	FBT	https://coinmarketcap.com//currencies/fanbi-token/	0
7616	Equilibria	XEQ	https://coinmarketcap.com//currencies/equilibria/	0.08
7617	AGAr	AGAR	https://coinmarketcap.com//currencies/agar/	390.82
7618	Dinastycoin	DCY	https://coinmarketcap.com//currencies/dinastycoin/	0
7619	Capital.Finance	CAP	https://coinmarketcap.com//currencies/capital-finance/	8.08
7620	X Infinity	XIF	https://coinmarketcap.com//currencies/x-infinity/	0.01
7621	SURF Finance	SURF	https://coinmarketcap.com//currencies/surf/	0.43
7622	Degens	DEGENS	https://coinmarketcap.com//currencies/degens/	3.44
7623	NewsToken	NEWOS	https://coinmarketcap.com//currencies/newstoken/	0
7624	SPDR S&P 500 ETF tokenized stock FTX	SPY	https://coinmarketcap.com//currencies/spdr-sp-500-etf-tokenized-stock-ftx/	397.26
7625	r3fi.finance	R3FI	https://coinmarketcap.com//currencies/r3fi-finance/	0.08
7626	XVIX	XVIX	https://coinmarketcap.com//currencies/xvix/	21.88
7627	Trade Butler Bot	TBB	https://coinmarketcap.com//currencies/trade-butler-bot/	495.21
7628	CashBackPro	CBP	https://coinmarketcap.com//currencies/cashbackpro/	0.32
7629	Revelation Finance	ADAO	https://coinmarketcap.com//currencies/revelation-finance/	4.91
7630	governance ZIL	GZIL	https://coinmarketcap.com//currencies/governance-zil/	307.75
7631	1X Short Ethereum Token	ETHHEDGE	https://coinmarketcap.com//currencies/1x-short-ethereum-token/	70.74
7632	BlockCDN	BCDN	https://coinmarketcap.com//currencies/blockcdn/	0
7633	Coral Swap	CORAL	https://coinmarketcap.com//currencies/coral-swap/	0.92
7634	BlockCDN	BCDN	https://coinmarketcap.com//currencies/blockcdn/	0.001829
7635	Coral Swap	CORAL	https://coinmarketcap.com//currencies/coral-swap/	0.9246
7636	gAsp	GASP	https://coinmarketcap.com//currencies/gasp/	0.4936
7637	SPDR S&P 500 ETF tokenized stock FTX	SPY	https://coinmarketcap.com//currencies/spdr-sp-500-etf-tokenized-stock-ftx/	397.26
7638	Bitsonic	BSC	https://coinmarketcap.com//currencies/bitsonic/	0.01529
7639	Degov	DEGOV	https://coinmarketcap.com//currencies/degov/	30.95
7640	Based Money	$BASED	https://coinmarketcap.com//currencies/based-money/	1.55
7641	DeFi100	D100	https://coinmarketcap.com//currencies/defi100/	0.1806
7642	MERCI	MERCI	https://coinmarketcap.com//currencies/merci/	0.000412
7643	Marblecoin	MBC	https://coinmarketcap.com//currencies/marblecoin/	0.8526
7644	Equilibria	XEQ	https://coinmarketcap.com//currencies/equilibria/	0.08
7645	Cofinex Coin	CNX	https://coinmarketcap.com//currencies/cofinex-coin/	0.03
7646	srnArt Gallery	SACT	https://coinmarketcap.com//currencies/srnartgallery/	0.07
7647	BuySell	BULL	https://coinmarketcap.com//currencies/buysell/	1.12
7648	Khipu Token	KIP	https://coinmarketcap.com//currencies/khipu-token/	0
7649	Midas	MIDAS	https://coinmarketcap.com//currencies/midas/	3.06
7650	HRDCOIN	HRD	https://coinmarketcap.com//currencies/hrdcoin/	0.01
7651	Taco Finance	TACO	https://coinmarketcap.com//currencies/taco-finance/	0.08
7652	Uber tokenized stock FTX	UBER	https://coinmarketcap.com//currencies/uber-tokenized-stock-ftx/	59.91
7653	TAMA EGG NiftyGotchi	TME	https://coinmarketcap.com//currencies/tama-egg-niftygotchi/	3907.24
7654	Xeonbit Token	XNS	https://coinmarketcap.com//currencies/xeonbit-token/	0.01
7655	Pasta Finance	PASTA	https://coinmarketcap.com//currencies/pasta-finance/	0.16
7656	Gadoshi	GADOSHI	https://coinmarketcap.com//currencies/gadoshi/	0
7657	City Coin	CITY	https://coinmarketcap.com//currencies/city-coin/	0.1
7658	Curio Governance	CGT	https://coinmarketcap.com//currencies/curio-governance/	0.43
7659	En-Tan-Mo	ETM	https://coinmarketcap.com//currencies/en-tan-mo/	0
7660	Bitpower	BPP	https://coinmarketcap.com//currencies/bitpower/	0.02
7661	Rug Proof	RPT	https://coinmarketcap.com//currencies/rug-proof/	0.39
7662	VeriBlock	VBK	https://coinmarketcap.com//currencies/veriblock/	0.01
7663	Tacos	TACO	https://coinmarketcap.com//currencies/tacos/	0.07
7664	EsportsPro	ESPRO	https://coinmarketcap.com//currencies/esportspro/	0.07
7665	Wrapped BIND	WBIND	https://coinmarketcap.com//currencies/wrapped-bind/	0.06
7666	Prime Whiterock Company	PWC	https://coinmarketcap.com//currencies/prime-whiterock-company/	0.02
7667	Receive Access Ecosystem	RAE	https://coinmarketcap.com//currencies/receive-access-ecosystem/	2.42
7668	Agoras Tokens	AGRS	https://coinmarketcap.com//currencies/agoras-tokens/	0.66
7669	A2A	A2A	https://coinmarketcap.com//currencies/a2a-50x-com/	0.02
7670	DEVA TOKEN	DEVA	https://coinmarketcap.com//currencies/deva-token/	1.03
7671	Midas Dollar	MDO	https://coinmarketcap.com//currencies/midas-dollar/	3.77
7672	3X Long Ethereum Classic Token	ETCBULL	https://coinmarketcap.com//currencies/3x-long-ethereum-classic-token/	62.8
7673	Securypto	SCU	https://coinmarketcap.com//currencies/securypto/	0.11
7674	Leading Coin 4 Entrepreneurs	LC4	https://coinmarketcap.com//currencies/leading-coin-4-entrepreneurs/	0.11
7675	AK12	AK12	https://coinmarketcap.com//currencies/ak12/	0.14
7676	Advanced Micro Devices tokenized stock FTX	AMD	https://coinmarketcap.com//currencies/advanced-micro-devices-tokenized-stock-ftx/	84.49
7677	SPACE-iZ	SPIZ	https://coinmarketcap.com//currencies/space-iz/	0
7678	SpankChain	SPANK	https://coinmarketcap.com//currencies/spankchain/	0.01
7679	AC Index	ACX	https://coinmarketcap.com//currencies/ac-index/	0.85
7680	PieDAO DEFI Small Cap	DEFI+S	https://coinmarketcap.com//currencies/piedao-defi-small-cap/	6.02
7681	XPToken.io	XPT	https://coinmarketcap.com//currencies/xptoken-io/	1.09
7682	Trendering	TRND	https://coinmarketcap.com//currencies/trendering/	6.96
7683	Ape Tools	NANA	https://coinmarketcap.com//currencies/ape-tools/	0.01
7684	APY Vision	VISION	https://coinmarketcap.com//currencies/apy-vision/	2.98
7685	Emanate	EMT	https://coinmarketcap.com//currencies/emanate/	0.01
7686	Healing Plus	HP	https://coinmarketcap.com//currencies/healing-plus/	0.05
7687	AI Network	AIN	https://coinmarketcap.com//currencies/ai-network/	0.07
7688	Unicap.finance	UCAP	https://coinmarketcap.com//currencies/unicap-finance/	1.01
7689	Excavo Finance	CAVO	https://coinmarketcap.com//currencies/excavo-finance/	273.41
7842	Terra SDT	SDT	https://coinmarketcap.com//currencies/terra-sdt/	1.53
7690	Bitsten Token	BST	https://coinmarketcap.com//currencies/bitsten-token/	0.03
7691	TTCRYPTO	TTC	https://coinmarketcap.com//currencies/ttcrypto/	1.57
7692	FRMx Token	FRMX	https://coinmarketcap.com//currencies/frmx-token/	9042.17
7693	Flits	FLS	https://coinmarketcap.com//currencies/flits/	0.99
7694	dXIOT	DXIOT	https://coinmarketcap.com//currencies/dxiot/	0.56
7695	snglsDAO	SGT	https://coinmarketcap.com//currencies/snglsdao/	0
7696	BNSD Finance	BNSD	https://coinmarketcap.com//currencies/bnsd-finance/	0.01
7697	ProperSix	PSIX	https://coinmarketcap.com//currencies/propersix/	0
7698	LifetionCoin	LTP	https://coinmarketcap.com//currencies/lifetioncoin/	0.01
7699	Filenet	FN	https://coinmarketcap.com//currencies/filenet/	0.01
7700	Tokemon	TKMN	https://coinmarketcap.com//currencies/tokemon/	146.57
7701	BitcoinSoV	BSOV	https://coinmarketcap.com//currencies/bitcoinsov/	0.22
7702	Wisdom Chain	WDC	https://coinmarketcap.com//currencies/wisdom-chain/	0.08
7703	Coinwaycoin	CAN	https://coinmarketcap.com//currencies/coinwaycoin/	0.68
7704	KEY	KEY	https://coinmarketcap.com//currencies/key/	0
7705	TheOvOrideA	OVOA	https://coinmarketcap.com//currencies/theovoridea/	1282.34
7706	Love Coin	LOVE	https://coinmarketcap.com//currencies/love-coin/	0
7707	Crypto Accept	ACPT	https://coinmarketcap.com//currencies/crypto-accept/	0
7708	Hybrid Bank Cash	HBC	https://coinmarketcap.com//currencies/hybrid-bank-cash/	0.01
7709	Everus	EVR	https://coinmarketcap.com//currencies/everus/	0.04
7710	Etherpay	ETHPY	https://coinmarketcap.com//currencies/etherpay/	0.02
7711	Counterparty	XCP	https://coinmarketcap.com//currencies/counterparty/	2.89
7712	Decentralized Nations	DENA	https://coinmarketcap.com//currencies/decentralized-nations/	6.51
7713	xscoin	XSC	https://coinmarketcap.com//currencies/xscoin/	0.49
7714	Bonkey dAPP	BNKY	https://coinmarketcap.com//currencies/bonkey-dapp/	0.01
7715	CORD.Finance	CORD	https://coinmarketcap.com//currencies/cord-finance/	16.75
7716	Yearn Finance DOT	YFDOT	https://coinmarketcap.com//currencies/yearn-finance-dot/	15.3
7717	Ubiner	UBIN	https://coinmarketcap.com//currencies/ubiner/	0
7718	Chicken	KFC	https://coinmarketcap.com//currencies/chicken/	5.22
7719	Talent Token	TTX	https://coinmarketcap.com//currencies/talent-token/	0.54
7720	LEVELG	LEVELG	https://coinmarketcap.com//currencies/levelg/	0.18
7721	VeraOne	VRO	https://coinmarketcap.com//currencies/veraone/	55.64
7722	BIOKKOIN	BKKG	https://coinmarketcap.com//currencies/biokkoin/	18.65
7723	Metacoin	MTC	https://coinmarketcap.com//currencies/metacoin/	0.09
7724	Asian-African Capital Chain	ACC	https://coinmarketcap.com//currencies/asian-african-capital-chain/	0.07
7725	TILWIKI	TLW	https://coinmarketcap.com//currencies/tilwiki/	4.53
7726	Alias	ALIAS	https://coinmarketcap.com//currencies/alias/	0.14
7727	Axioms	AXI	https://coinmarketcap.com//currencies/axioms/	0.05
7728	Giftedhands	GHD	https://coinmarketcap.com//currencies/giftedhands/	0
7729	Limestone Network	LIMEX	https://coinmarketcap.com//currencies/limestone-network/	0.01
7730	Mandi Token	MANDI	https://coinmarketcap.com//currencies/mandi-token/	0.02
7731	DeadSoul	DSL	https://coinmarketcap.com//currencies/deadsoul/	0.11
7732	COST COIN+	AKM	https://coinmarketcap.com//currencies/cost-coin/	0
7733	Ternio	TERN	https://coinmarketcap.com//currencies/ternio/	0.02
7734	Unobtanium	UNO	https://coinmarketcap.com//currencies/unobtanium/	53.71
7735	Aspire	ASP	https://coinmarketcap.com//currencies/aspire/	0.1541
7736	Fridn	EFAR	https://coinmarketcap.com//currencies/fridn/	0.2318
7737	Modex	MODEX	https://coinmarketcap.com//currencies/modex/	0.02179
7738	QChi Chain	QHC	https://coinmarketcap.com//currencies/qchi-chain/	0.007087
7739	Davion	DAVP	https://coinmarketcap.com//currencies/davion/	0.05303
7740	Davincij15 Token	DJ15	https://coinmarketcap.com//currencies/davincij15-token/	169.18
7741	WaterDrop	WDP	https://coinmarketcap.com//currencies/waterdrop/	33.65
7742	PalletOne	PTN	https://coinmarketcap.com//currencies/palletone/	0.001461
7743	YieldNyan	NYAN	https://coinmarketcap.com//currencies/yieldnyan/	0.0561
7744	Global Game Coin	GGC	https://coinmarketcap.com//currencies/global-game-coin/	1.22
7745	ThoreNext	THX	https://coinmarketcap.com//currencies/thorenext/	0.11
7746	Exchange Payment Coin	EXP	https://coinmarketcap.com//currencies/exchange-payment-coin/	0
7747	Pub Finance	PINT	https://coinmarketcap.com//currencies/pub-finance/	0.6
7748	Uranus	URAC	https://coinmarketcap.com//currencies/uranus/	0
7749	Kauri	KAU	https://coinmarketcap.com//currencies/kauri-crypto/	0.71
7750	Gomics	GOM	https://coinmarketcap.com//currencies/gomics/	0
7751	eosBLACK	BLACK	https://coinmarketcap.com//currencies/eosblack/	0
7752	Amazon tokenized stock FTX	AMZN	https://coinmarketcap.com//currencies/amazon-tokenized-stock-ftx/	3108.4
7753	MicroStrategy tokenized stock FTX	MSTR	https://coinmarketcap.com//currencies/microstrategy-tokenized-stock-ftx/	753.5
7754	Hyper Credit Network	HPAY	https://coinmarketcap.com//currencies/hyper-credit-network/	0.67
7755	LINA	LINA	https://coinmarketcap.com//currencies/lina/	0.01
7756	SaluS	SLS	https://coinmarketcap.com//currencies/salus/	14.44
7757	DWS	DWS	https://coinmarketcap.com//currencies/dws/	0
7758	Komet	KOMET	https://coinmarketcap.com//currencies/komet/	57.53
7759	BenePit Protocol	BNP	https://coinmarketcap.com//currencies/benepit-protocol/	0
7760	Dark.Build	DARK	https://coinmarketcap.com//currencies/darkbuild/	91.28
7761	FLAMA	FMA	https://coinmarketcap.com//currencies/flama/	0.01
7762	Stacy	STACY	https://coinmarketcap.com//currencies/stacy/	0
7763	Xdef Finance	XDEF2	https://coinmarketcap.com//currencies/xdef-finance/	0.21
7764	Dice	DICE	https://coinmarketcap.com//currencies/dice/	0
7765	Keysians Network	KEN	https://coinmarketcap.com//currencies/keysians-network/	1.17
7766	Rootkit Finance	ROOT	https://coinmarketcap.com//currencies/rootkit-finance/	5304.98
7767	Cipher Core Token	CIPHC	https://coinmarketcap.com//currencies/cipher-core-token/	6.67
7768	Derivex	DVX	https://coinmarketcap.com//currencies/derivex/	0
7769	HOLD	HOLD	https://coinmarketcap.com//currencies/hold/	0.01
7770	happy birthday coin	HBDC	https://coinmarketcap.com//currencies/happy-birthday-coin/	0.03
7771	Coupon Chain	CCT	https://coinmarketcap.com//currencies/coupon-chain/	1.64
7772	TideBit Token	TBT	https://coinmarketcap.com//currencies/tidebit-token/	0.06
7773	DragonFarm Finance	DRAGON	https://coinmarketcap.com//currencies/dragonfarm-finance/	0
7774	Trade.win	TWI	https://coinmarketcap.com//currencies/trade-win/	0.07
7775	Global Reserve System	GLOB	https://coinmarketcap.com//currencies/global-reserve-system/	23.92
7776	Bone	BONE	https://coinmarketcap.com//currencies/bone/	0.53
7777	LOA Protocol	LOA	https://coinmarketcap.com//currencies/loa-protocol/	0.01
7778	Psychic	PSY	https://coinmarketcap.com//currencies/psychic/	0.14
7779	EOX	EOX	https://coinmarketcap.com//currencies/eox/	0.03
7780	PREDIQT	PQT	https://coinmarketcap.com//currencies/prediqt/	0
7781	McBase Finance	MCBASE	https://coinmarketcap.com//currencies/mcbase-finance/	2.54
7782	CARAT	CARAT	https://coinmarketcap.com//currencies/carat/	0.27
7783	CloudCoin	CCE	https://coinmarketcap.com//currencies/cloudcoin/	0
7784	StakeHound Staked Ether	STETH	https://coinmarketcap.com//currencies/stakehound-staked-ether/	1636.55
7785	Ecoreal Estate	ECOREAL	https://coinmarketcap.com//currencies/ecoreal-estate/	0.17
7786	MEME CASH	MCH	https://coinmarketcap.com//currencies/meme-cash/	11.33
7787	VANCI FINANCE	VANCII	https://coinmarketcap.com//currencies/vanci-finance/	10.52
7788	Wrapped LEO	WLEO	https://coinmarketcap.com//currencies/wrapped-leo/	1.09
7789	ISALCOIN	ISAL	https://coinmarketcap.com//currencies/isalcoin/	0.16
7790	Vinci	VINCI	https://coinmarketcap.com//currencies/vinci/	8.21
7791	Bitalgo	ALG	https://coinmarketcap.com//currencies/bitalgo/	0.15
7792	LIQUID	LIQUID	https://coinmarketcap.com//currencies/liquidefi/	27.04
7793	Taiwan Semiconductor Mfg tokenized stock FTX	TSM	https://coinmarketcap.com//currencies/taiwan-semiconductor-mfg-tokenized-stock-ftx/	120.8
7794	Universa	UTNP	https://coinmarketcap.com//currencies/universa/	0
7795	DeFi Omega	DFIO	https://coinmarketcap.com//currencies/defi-omega/	6.07
7796	Electra Protocol	XEP	https://coinmarketcap.com//currencies/electra-protocol/	0
7797	Uniform Fiscal Object	UFO	https://coinmarketcap.com//currencies/uniform-fiscal-object/	0
7798	Combine.finance	COMB	https://coinmarketcap.com//currencies/combine-finance/	94.23
7799	BitherCash	BICAS	https://coinmarketcap.com//currencies/bithercash/	0.26
7800	Aureus Nummus Gold	ANG	https://coinmarketcap.com//currencies/aureus-nummus-gold/	0
7801	Value Set Dollar	VSD	https://coinmarketcap.com//currencies/value-set-dollar/	1.06
7802	SimpleChain	SIMPLE	https://coinmarketcap.com//currencies/simplechain/	0.18
7803	VELO Token	VLO	https://coinmarketcap.com//currencies/velo-token/	0
7804	LegalBlock	LBK	https://coinmarketcap.com//currencies/legalblock/	0
7805	3X Short Dogecoin Token	DOGEBEAR	https://coinmarketcap.com//currencies/3x-short-dogecoin-token/	0
7806	Astosch	ATC	https://coinmarketcap.com//currencies/astosch/	0.03
7807	YAM v1	YAM	https://coinmarketcap.com//currencies/yamv1/	3.33
7808	Skillchain	SKI	https://coinmarketcap.com//currencies/skillchain/	0.04
7809	Nyan V2	NYAN-2	https://coinmarketcap.com//currencies/nyan-v2/	42.15
7810	Pollux Coin	POX	https://coinmarketcap.com//currencies/pollux-coin/	0.61
7811	Medicalveda	MVEDA	https://coinmarketcap.com//currencies/medicalveda/	0.08
7812	DeFi Gold	DFGL	https://coinmarketcap.com//currencies/defi-gold/	2.32
7813	StakeCubeCoin	SCC	https://coinmarketcap.com//currencies/stakecubecoin/	1.35
7814	Contribute	TRIB	https://coinmarketcap.com//currencies/contribute/	1.71
7815	Dfinance	XFI	https://coinmarketcap.com//currencies/dfinance/	0.06
7816	BiTToken	BITT	https://coinmarketcap.com//currencies/bittoken/	0.19
7817	Global X Change Token	GXT	https://coinmarketcap.com//currencies/global-x-change-token/	0.16
7818	BioNTech tokenized stock Bittrex	BNTX	https://coinmarketcap.com//currencies/biontech-tokenized-stock-bittrex/	108.65
7819	Opennity	OPNN	https://coinmarketcap.com//currencies/opennity/	0
7820	Lykke	LKK	https://coinmarketcap.com//currencies/lykke/	0.01
7821	WHEN Token	WHEN	https://coinmarketcap.com//currencies/when-token/	0
7822	HebeBlock	HEBE	https://coinmarketcap.com//currencies/hebeblock/	0
7823	3X Short TRX Token	TRXBEAR	https://coinmarketcap.com//currencies/3x-short-trx-token/	0
7824	PrimeFinance	PFI	https://coinmarketcap.com//currencies/primefinance/	3.64
7825	Boltt Coin 	BOLTT	https://coinmarketcap.com//currencies/boltt-coin/	0.16
7826	Typhoon Cash	PHOON	https://coinmarketcap.com//currencies/typhoon-cash/	19.92
7827	Zoom Protocol	ZOM	https://coinmarketcap.com//currencies/zoom-protocol/	14.11
7828	SwapShip	SWSH	https://coinmarketcap.com//currencies/swapship/	4.17
7829	ModiHost	AIM	https://coinmarketcap.com//currencies/modihost/	0.16
7830	Robotina	ROX	https://coinmarketcap.com//currencies/robotina/	0.01
7831	Adelphoi	ADL	https://coinmarketcap.com//currencies/adelphoi/	0.08
7832	Pledge Coin	PLG	https://coinmarketcap.com//currencies/pledge-coin/	0.0009816
7833	Nio tokenized stock FTX	NIO	https://coinmarketcap.com//currencies/nio-tokenized-stock-ftx/	45.65
7834	T.OS	TOSC	https://coinmarketcap.com//currencies/t-os/	0.01056
7835	Emrals	EMRALS	https://coinmarketcap.com//currencies/emrals/	0.009652
7836	CURE Farm	CURE	https://coinmarketcap.com//currencies/cure-farm/	2.79
7837	ZUM TOKEN	ZUM	https://coinmarketcap.com//currencies/zum-token/	0.0001979
7838	Xank	XANK	https://coinmarketcap.com//currencies/xank/	0.04384
7839	Predictz	PRDZ	https://coinmarketcap.com//currencies/predictz/	31.44
7840	NAGA	NGC	https://coinmarketcap.com//currencies/naga/	0.02792
7841	DeCash	DESH	https://coinmarketcap.com//currencies/decash/	0.0007993
7843	YieldPanda Finance	yPANDA	https://coinmarketcap.com//currencies/yieldpanda-finance/	0
7844	IYF.finance	IYF	https://coinmarketcap.com//currencies/iyf-finance/	3.22
7845	Apple Finance	APLP	https://coinmarketcap.com//currencies/apple-finance/	0.09
7846	Neumark	NEU	https://coinmarketcap.com//currencies/neumark/	0.15
7847	Nectar	NEC	https://coinmarketcap.com//currencies/nectar/	0.16
7848	Super CoinView Token	SCV	https://coinmarketcap.com//currencies/super-coinview/	0
7849	YFWorld	YFW	https://coinmarketcap.com//currencies/yfworld/	26.66
7850	Defi	DEFI	https://coinmarketcap.com//currencies/defi/	0.02
7851	YFMoonBeam	YFMB	https://coinmarketcap.com//currencies/yfmoonbeam/	11.05
7852	Apple tokenized stock FTX	AAPL	https://coinmarketcap.com//currencies/apple-tokenized-stock-ftx/	126.67
7853	SoPay	SOP	https://coinmarketcap.com//currencies/sopay/	0
7854	Ubique Chain Of Things	UCT	https://coinmarketcap.com//currencies/ubique-chain-of-things/	0
7855	KVANT	KVNT	https://coinmarketcap.com//currencies/kvant/	0.12
7856	Semitoken	SEMI	https://coinmarketcap.com//currencies/semitoken/	0
7857	iOWN Token	iOWN	https://coinmarketcap.com//currencies/iown-token/	0.01
7858	Tratok	TRAT	https://coinmarketcap.com//currencies/tratok/	0.04
7859	BlackPearl Token	BPLC	https://coinmarketcap.com//currencies/blackpearl-chain/	0
7860	PieDAO Yearn Ecosystem Pie	YPIE	https://coinmarketcap.com//currencies/piedao-yearn-ecosystem-pie/	3.25
7861	zHEGIC	ZHEGIC	https://coinmarketcap.com//currencies/zhegic/	0.25
7862	Gasgains	GASG	https://coinmarketcap.com//currencies/gasgains/	0.45
7863	Decurian	ECU	https://coinmarketcap.com//currencies/decurian/	888.23
7864	Toshify.finance	YFT	https://coinmarketcap.com//currencies/toshify-finance/	4.63
7865	BitCherry	BCHC	https://coinmarketcap.com//currencies/bitcherry/	0.01
7866	ENTONE	ENTONE	https://coinmarketcap.com//currencies/entone/	0.03
7867	Koloop Basic	KPC	https://coinmarketcap.com//currencies/koloop-basic/	0.06
7868	ECOChain	ECOC	https://coinmarketcap.com//currencies/ecochain/	0.04
7869	Square tokenized stock FTX	SQ	https://coinmarketcap.com//currencies/square-tokenized-stock-ftx/	249.3
7870	The LoveChain	LOV	https://coinmarketcap.com//currencies/the-lovechain/	0.03
7871	Upper Euro	EURU	https://coinmarketcap.com//currencies/upper-euro/	1.2
7872	YFISCURITY	YFIS	https://coinmarketcap.com//currencies/yfiscurity/	0.98
7873	NewYorkCoin	NYC	https://coinmarketcap.com//currencies/newyorkcoin/	0
7874	United Traders Token	UTT	https://coinmarketcap.com//currencies/uttoken/	0.43
7875	The global index chain	TGIC	https://coinmarketcap.com//currencies/the-global-index-chain/	0
7876	Ecoin	ECOIN	https://coinmarketcap.com//currencies/ecoin-2/	0
7877	NDN Link	NDN	https://coinmarketcap.com//currencies/ndn-link/	0.01
7878	XUSD Stable	XUSD	https://coinmarketcap.com//currencies/xusd-stable/	0.96
7879	Marscoin	MARS	https://coinmarketcap.com//currencies/marscoin/	0.18
7880	Kimochi Finance	KIMOCHI	https://coinmarketcap.com//currencies/kimochi-finance/	0.04
7881	LinkCoin Token	LKN	https://coinmarketcap.com//currencies/linkcoin-token/	0
7882	RoboCalls	RC20	https://coinmarketcap.com//currencies/robocalls/	0
7883	Dune Network	DUN	https://coinmarketcap.com//currencies/dune-network/	0
7884	Infinity Economics	XIN	https://coinmarketcap.com//currencies/infinity-economics/	0
7885	VNDC	VNDC	https://coinmarketcap.com//currencies/vndc/	0
7886	FirstBlood	1ST	https://coinmarketcap.com//currencies/firstblood/	0.24
7887	Diamond Platform Token	DPT	https://coinmarketcap.com//currencies/diamond-platform-token/	4.21
7888	PlayChip	PLA	https://coinmarketcap.com//currencies/playchip/	0
7889	ODEM	ODE	https://coinmarketcap.com//currencies/odem/	0.03
7890	MINDOL	MIN	https://coinmarketcap.com//currencies/mindol/	0.25
7891	Hithotx	HITX	https://coinmarketcap.com//currencies/hithotx/	0
7892	BPOP	BPOP	https://coinmarketcap.com//currencies/bpop/	0
7893	GorillaYield	YAPE	https://coinmarketcap.com//currencies/gorillayield/	0
7894	Quasarcoin	QAC	https://coinmarketcap.com//currencies/quasarcoin/	0.01
7895	MetaWhale BTC	MWBTC	https://coinmarketcap.com//currencies/metawhale-btc/	0.23
7896	Narwhalswap	NAR	https://coinmarketcap.com//currencies/narwhalswap/	0.33
7897	WEBN token	WEBN	https://coinmarketcap.com//currencies/webn-token/	0
7898	Consentium	CSM	https://coinmarketcap.com//currencies/consentium/	0
7899	HeartBout Pay	HP	https://coinmarketcap.com//currencies/heartbout-pay/	0
7900	Simbcoin Swap	SMBSWAP	https://coinmarketcap.com//currencies/simbcoin-swap/	0.96
7901	CanYaCoin	CAN	https://coinmarketcap.com//currencies/canyacoin/	0.05
7902	1x Long Bitcoin Implied Volatility Token	BVOL	https://coinmarketcap.com//currencies/1x-long-bitcoin-implied-volatility-token/	1220.14
7903	MITH Cash	MIC	https://coinmarketcap.com//currencies/mith-cash/	0.13
7904	WallStreet.Finance	WSF	https://coinmarketcap.com//currencies/wallstreet-finance/	96.12
7905	AurusGOLD	AWG	https://coinmarketcap.com//currencies/aurusgold/	55.53
7906	Golden Ratio Coin	GOLDR	https://coinmarketcap.com//currencies/golden-ratio-coin/	12.97
7907	Twitter tokenized stock FTX	TWTR	https://coinmarketcap.com//currencies/twitter-tokenized-stock-ftx/	70.68
7908	NoLimitCoin	NLC2	https://coinmarketcap.com//currencies/nolimitcoin/	0.01
7909	DeVault	DVT	https://coinmarketcap.com//currencies/devault/	0
7910	XNODE	XNODE	https://coinmarketcap.com//currencies/xnode/	1.04
7911	3X Long OKB Token	OKBBULL	https://coinmarketcap.com//currencies/3x-long-okb-token/	1483.5
7912	Metawhale Gold	MWG	https://coinmarketcap.com//currencies/metawhale-gold/	0.57
7913	Tepleton	TEP	https://coinmarketcap.com//currencies/tepleton/	0.01
7914	DogDeFiCoin	DOGDEFI	https://coinmarketcap.com//currencies/dogdeficoin/	1.21
7915	YFIDapp	YFID	https://coinmarketcap.com//currencies/yfidapp/	0.68
7916	LimitSwap	LIMIT	https://coinmarketcap.com//currencies/limitswap/	8.82
7917	ASYAGRO	ASY	https://coinmarketcap.com//currencies/asyagro/	0.04
7918	POP Network Token	POP	https://coinmarketcap.com//currencies/pop-network-token/	0
7919	Yearn Finance Network	YFN	https://coinmarketcap.com//currencies/yearn-finance-network/	0.33
7920	Imsmart	IMT	https://coinmarketcap.com//currencies/imsmart/	0.01
7921	DeepCloud AI	DEEP	https://coinmarketcap.com//currencies/deepcloud-ai/	0.01
7922	Empow	EM	https://coinmarketcap.com//currencies/empow/	0.01
7923	AmazonasCoin	AMZ	https://coinmarketcap.com//currencies/amazonascoin/	0
7924	BitCore	BTX	https://coinmarketcap.com//currencies/bitcore/	0.49
7925	Electronero Pulse	ETNXP	https://coinmarketcap.com//currencies/electronero-pulse/	0
7926	ESR Coin	ESRC	https://coinmarketcap.com//currencies/esr-coin/	0.44
7927	Timers	IPM	https://coinmarketcap.com//currencies/timers/	0.04
7928	YFIEXCHANGE.FINANCE	YFIE	https://coinmarketcap.com//currencies/yfiexchange-finance/	2.38
7929	Honey	HNY	https://coinmarketcap.com//currencies/honey-token/	1251.98
7930	ALQO	XLQ	https://coinmarketcap.com//currencies/alqo/	0.09
7931	Kompass	KOMP	https://coinmarketcap.com//currencies/kompass/	0.01
7932	UCX FOUNDATION	UCX	https://coinmarketcap.com//currencies/ucx-foundation/	0.2354
7933	NVIDIA tokenized stock FTX	NVDA	https://coinmarketcap.com//currencies/nvidia-tokenized-stock-ftx/	537.7
7934	Lyra	LYR	https://coinmarketcap.com//currencies/lyra/	0.0004379
7935	AEN Smart Token	AENS	https://coinmarketcap.com//currencies/aen-smart-token/	0.01168
7936	Ixinium	XXA	https://coinmarketcap.com//currencies/ixinium/	0.1836
7937	EcoG9coin	EGC	https://coinmarketcap.com//currencies/ecog9coin/	0.0006894
7938	Mythic Finance	MYTHIC	https://coinmarketcap.com//currencies/mythic-finance/	26.93
7939	Asgard finance	THOR	https://coinmarketcap.com//currencies/asgard-finance/	0.002852
7940	YFBitcoin	YFBTC	https://coinmarketcap.com//currencies/yfbitcoin/	399.05
7941	Passive Income	PSI	https://coinmarketcap.com//currencies/passive-income/	122.56
7942	3X Long Dragon Index Token	DRGNBULL	https://coinmarketcap.com//currencies/3x-long-dragon-index-token/	1252
7943	Big Bang Game Coin	BBGC	https://coinmarketcap.com//currencies/big-bang-game-coin/	0
7944	Neeva Defi	NVA	https://coinmarketcap.com//currencies/neeva-defi/	25.84
7945	Algory Project	ALG	https://coinmarketcap.com//currencies/algory-project/	0.15
7946	Braziliex Token	BRZX	https://coinmarketcap.com//currencies/braziliexs-token/	0.03
7947	Facebook tokenized stock Bittrex	FB	https://coinmarketcap.com//currencies/facebook-tokenized-stock-bittrex/	270.02
7948	KingXChain	KXC	https://coinmarketcap.com//currencies/kingxchain/	0
7949	MyTVchain	MYTV	https://coinmarketcap.com//currencies/mytvchain/	0.01
7950	Torex	TOR	https://coinmarketcap.com//currencies/torex/	0.11
7951	YFIA	YFIA	https://coinmarketcap.com//currencies/yfia/	3.58
7952	NTON	NTON	https://coinmarketcap.com//currencies/nton/	0.01
7953	CryptoBharatCoin	CBC	https://coinmarketcap.com//currencies/cryptobharatcoin/	0.15
7954	Vodi X	VDX	https://coinmarketcap.com//currencies/vodi-x/	0
7955	Ultra Clear	UCR	https://coinmarketcap.com//currencies/ultra-clear/	0.05
7956	Amazon tokenized stock Bittrex	AMZN	https://coinmarketcap.com//currencies/amazon-tokenized-stock-bittrex/	3115.08
7957	Chainpay	CPAY	https://coinmarketcap.com//currencies/chainpay/	0.03
7958	LUCY	LUCY	https://coinmarketcap.com//currencies/lucy/	0.02
7959	Umbria Network	UMBR	https://coinmarketcap.com//currencies/umbria-network/	1.74
7960	LOLTOKEN	LOL	https://coinmarketcap.com//currencies/loltoken/	0.01
7961	Yearn Shark Finance	YSKF	https://coinmarketcap.com//currencies/yearn-shark-finance/	1.69
7962	Daiquilibrium	DAIQ	https://coinmarketcap.com//currencies/daiquilibrium/	0.12
7963	Luxurious Pro Network Token	LPNT	https://coinmarketcap.com//currencies/luxurious-pro-network-token/	1.75
7964	3X Short TomoChain Token	TOMOBEAR	https://coinmarketcap.com//currencies/3x-short-tomochain-token/	0
7965	Historia	HTA	https://coinmarketcap.com//currencies/historia/	0
7966	Bonorum	BONO	https://coinmarketcap.com//currencies/bonorum/	27.3
7967	Prophet	PROPHET	https://coinmarketcap.com//currencies/prophet/	0.1
7968	Fractal	FCL	https://coinmarketcap.com//currencies/fractal/	0.97
7969	Inverse Bitcoin Volatility Token	IBVOL	https://coinmarketcap.com//currencies/inverse-bitcoin-volatility-token/	5152.49
7970	BitcoinV	BTCV	https://coinmarketcap.com//currencies/bitcoinv/	0.02
7971	UCROWDME	UCM	https://coinmarketcap.com//currencies/ucrowdme/	0
7972	3x Short Tezos Token	XTZBEAR	https://coinmarketcap.com//currencies/3x-short-tezos-token/	0.01
7973	Blood	BLOOD	https://coinmarketcap.com//currencies/blood/	0
7974	IDall	IDALL	https://coinmarketcap.com//currencies/idall/	0.01
7975	Polybius	PLBT	https://coinmarketcap.com//currencies/polybius/	2.23
7976	Compound Augur	CREP	https://coinmarketcap.com//currencies/compound-augur/	0.6
7977	Wrapped Conceal	WCCX	https://coinmarketcap.com//currencies/wrapped-conceal/	0.2
7978	The Universal Settlement Coin	TUSC	https://coinmarketcap.com//currencies/the-universal-settlement-coin/	0
7979	Sparkster	SPRK	https://coinmarketcap.com//currencies/sparkster/	0
7980	DEONEX COIN	DON	https://coinmarketcap.com//currencies/deonex-coin/	0.85
7981	Universal Protocol Token	UPT	https://coinmarketcap.com//currencies/universal-protocol-token/	0
7982	BuckHathCoin	BHIG	https://coinmarketcap.com//currencies/buck-hath-coin/	0.06
7983	Profile Utility Token	PUT	https://coinmarketcap.com//currencies/profile-utility-token/	0
7984	Cherry Token	YT	https://coinmarketcap.com//currencies/cherry-token/	0.79
7985	Rich Maker	RICH	https://coinmarketcap.com//currencies/rich-maker/	2.74
7986	XFUEL	XFUEL	https://coinmarketcap.com//currencies/xfuel/	0
7987	Electra	ECA	https://coinmarketcap.com//currencies/electra/	0
7988	Digital Rand	DZAR	https://coinmarketcap.com//currencies/digital-rand/	0.06
7989	XCredit	XFYI	https://coinmarketcap.com//currencies/xcredit/	0.71
7990	indaHash	IDH	https://coinmarketcap.com//currencies/indahash/	0.01
7991	Blockmason Link	BLINK	https://coinmarketcap.com//currencies/blockmason-link/	0
7992	Google tokenized stock Bittrex	GOOGL	https://coinmarketcap.com//currencies/google-tokenized-stock-bittrex/	2073.81
7993	Bestay	BSY	https://coinmarketcap.com//currencies/bestay/	0.14
7994	FLEX	FLEX	https://coinmarketcap.com//currencies/flex/	0.26
7995	DogeYield	DOGY	https://coinmarketcap.com//currencies/dogeyield/	0
7996	Crex Token	CREX	https://coinmarketcap.com//currencies/crex-token/	3.45
7997	RIFT Token	RIFT	https://coinmarketcap.com//currencies/rift-token/	23.33
7998	Mega Lottery Services Global	MLR	https://coinmarketcap.com//currencies/mega-lottery-services-global/	0.02
7999	Reflector.Finance	RFCTR	https://coinmarketcap.com//currencies/reflector-finance/	0.01
8000	IDL Token	IDL	https://coinmarketcap.com//currencies/idl-token/	2.39
8001	Defi Shopping Stake	DSS	https://coinmarketcap.com//currencies/defi-shopping-stake/	0.01
8002	Gric Coin	GC	https://coinmarketcap.com//currencies/gric-coin/	0.16
8003	TheToken.Network	TTN	https://coinmarketcap.com//currencies/thetoken-network/	3579.21
8004	Auroracoin	AUR	https://coinmarketcap.com//currencies/auroracoin/	0.21
8005	BitCanna	BCNA	https://coinmarketcap.com//currencies/bitcanna/	0.08
8006	Native Utility Token	NUT	https://coinmarketcap.com//currencies/native-utility-token/	23.15
8007	EasySwap	ESWA	https://coinmarketcap.com//currencies/easyswap/	0.06
8008	ACE	ACE	https://coinmarketcap.com//currencies/ace-entertainment/	0
8009	Airbnb tokenized stock FTX	ABNB	https://coinmarketcap.com//currencies/airbnb-tokenized-stock-ftx/	208.15
8010	Sergey Save Link	SSL	https://coinmarketcap.com//currencies/sergey-save-link/	179.97
8011	Dequant	DEQ	https://coinmarketcap.com//currencies/dequant/	0.25
8012	DigiDinar	DDR	https://coinmarketcap.com//currencies/digidinar/	1.06
8013	DMme	DMME	https://coinmarketcap.com//currencies/dmme/	0
8014	Omega Protocol Money	OPM	https://coinmarketcap.com//currencies/omega-protocol-money/	0
8015	Cyclops Treasure	CYTR	https://coinmarketcap.com//currencies/cyclops-treasure/	373.85
8016	Facebook tokenized stock FTX	FB	https://coinmarketcap.com//currencies/facebook-tokenized-stock-ftx/	281.02
8017	Google tokenized stock FTX	GOOGL	https://coinmarketcap.com//currencies/google-tokenized-stock-ftx/	2099.7
8018	Sense	SENSE	https://coinmarketcap.com//currencies/sense/	0.01
8019	yplutus	YPLT	https://coinmarketcap.com//currencies/yplutus/	0
8020	SatoExchange Token	SATX	https://coinmarketcap.com//currencies/satoexchange-token/	0
8021	Bitbook Gambling	BXK	https://coinmarketcap.com//currencies/bitbook-gambling/	0.01
8022	MACH Project	MACH	https://coinmarketcap.com//currencies/mach-project/	0.02
8023	MONNOS	MNS	https://coinmarketcap.com//currencies/monnos/	0
8024	ITEN	ITEN	https://coinmarketcap.com//currencies/iten/	0
8025	Eska	ESK	https://coinmarketcap.com//currencies/eska/	0.01
8026	Volt	ACDC	https://coinmarketcap.com//currencies/volt/	0
8027	SPDR S&P 500 ETF tokenized stock Bittrex	SPY	https://coinmarketcap.com//currencies/spdr-sp-500-etf-tokenized-stock-bittrex/	395.05
8028	GazeCoin	GZE	https://coinmarketcap.com//currencies/gazecoin/	0.05
8029	Flash	FLASH	https://coinmarketcap.com//currencies/flash/	0
8030	STONK	STONK	https://coinmarketcap.com//currencies/stonk/	0
8031	Cyber Network Token	CNT	https://coinmarketcap.com//currencies/cyber-network-token/	0
8032	ZenSports	SPORTS	https://coinmarketcap.com//currencies/zensports/	4.975e-05
8033	Elxis	LEX	https://coinmarketcap.com//currencies/elxis/	6.5
8034	Samurai	SAM	https://coinmarketcap.com//currencies/samurai/	7.74
8035	Tronipay	TRP	https://coinmarketcap.com//currencies/tronipay/	0.001422
8036	BLOCKIDCOIN	BID	https://coinmarketcap.com//currencies/blockidcoin/	0.00278
8037	IOV BlockChain	IOV	https://coinmarketcap.com//currencies/iov-blockchain/	8.453e-05
8038	ZCore Token	ZCRT	https://coinmarketcap.com//currencies/zcore-token/	0.2817
8039	THECASH	TCH	https://coinmarketcap.com//currencies/thecash/	0.008839
8040	ORS Group	ORS	https://coinmarketcap.com//currencies/ors-group/	0.009392
8041	Taxi	TAXI	https://coinmarketcap.com//currencies/taxi/	0.4998
8042	Zippie	ZIPT	https://coinmarketcap.com//currencies/zippie/	0
8043	Delphi Chain Link	DCL	https://coinmarketcap.com//currencies/delphi-chain-link/	0.01
8044	EarnBet	BET	https://coinmarketcap.com//currencies/earnbet/	0.12
8045	XSwap	XSP	https://coinmarketcap.com//currencies/xsp/	0
8046	Endorsit	EDS	https://coinmarketcap.com//currencies/endorsit/	0
8047	Wrapped Shift	WSHIFT	https://coinmarketcap.com//currencies/wshift/	0.08
8048	PocketNode	NODE	https://coinmarketcap.com//currencies/pocketnode/	0
8049	Orient	OFT	https://coinmarketcap.com//currencies/orient/	0.7
8050	Wallet Plus X	WPX	https://coinmarketcap.com//currencies/wallet-plus-x/	0
8051	Belt	BELT	https://coinmarketcap.com//currencies/belt/	63.53
8052	XTRM COIN	XTRM	https://coinmarketcap.com//currencies/xtrm-coin/	0
8053	Zero Collateral Dai	ZAI	https://coinmarketcap.com//currencies/zero-collateral-dai/	0
8054	Experience Token	EXT	https://coinmarketcap.com//currencies/experience-token/	0
8055	zzz.finance v2	ZZZV2	https://coinmarketcap.com//currencies/zzz-finance-v2/	2.98
8056	EncrypGen	DNA	https://coinmarketcap.com//currencies/encrypgen/	0.08
8057	Netflix tokenized stock FTX	NFLX	https://coinmarketcap.com//currencies/netflix-tokenized-stock-ftx/	532.22
8058	sBTC	sBTC	https://coinmarketcap.com//currencies/softbtc/	0.01
8059	Five Star Coin	FSC	https://coinmarketcap.com//currencies/five-star-coin/	0.07
8060	BitOnyx	BTNYX	https://coinmarketcap.com//currencies/bitonyx/	0.02
8061	Xenon	XEN	https://coinmarketcap.com//currencies/xenon-xen/	0
8062	PayPal tokenized stock FTX	PYPL	https://coinmarketcap.com//currencies/paypal-tokenized-stock-ftx/	253.03
8063	Novo	NOVO	https://coinmarketcap.com//currencies/novo/	3.39
8064	SKINCHAIN	SKC	https://coinmarketcap.com//currencies/skinchain/	0
8065	DEFI Top 5 Tokens Index	DEFI5	https://coinmarketcap.com//currencies/defi-top-5-tokens-index/	102.71
8066	Vybe	VYBE	https://coinmarketcap.com//currencies/vybe/	0.09
8067	YFED.Finance	YFED	https://coinmarketcap.com//currencies/yfedfinance/	0.58
8068	Foresight	FORS	https://coinmarketcap.com//currencies/foresight/	0.02
8069	PlusOneCoin	PLUS1	https://coinmarketcap.com//currencies/plusonecoin/	0.01
8070	Apple tokenized stock Bittrex	AAPL	https://coinmarketcap.com//currencies/apple-tokenized-stock-bittrex/	124.75
8071	Newsolution	NST	https://coinmarketcap.com//currencies/newsolution/	0
8072	THENODE	THE	https://coinmarketcap.com//currencies/thenode/	0.01
8073	Pfizer tokenized stock FTX	PFE	https://coinmarketcap.com//currencies/pfizer-tokenized-stock-ftx/	35.63
8074	DeFi Insurance Protocol	DFIP	https://coinmarketcap.com//currencies/defi-insurance-protocol/	0
8075	Sogur	SGR	https://coinmarketcap.com//currencies/sogur/	2.32
8076	Documentchain	DMS	https://coinmarketcap.com//currencies/documentchain/	0.02
8077	qiibee	QBX	https://coinmarketcap.com//currencies/qiibee/	0.01
8078	Hub - Human Trust Protocol	HUB	https://coinmarketcap.com//currencies/hub-human-trust-protocol/	0.01
8079	Thorium	TORM	https://coinmarketcap.com//currencies/thorium/	0.01
8080	ColossusXT	COLX	https://coinmarketcap.com//currencies/colossusxt/	0
8081	Vision Network	VSN	https://coinmarketcap.com//currencies/vision-network/	0
8082	Golden Ratio Per Liquidity	GRPL	https://coinmarketcap.com//currencies/goldenratioperliquidity/	2.57
8083	Moon Juice	JUICE	https://coinmarketcap.com//currencies/moon-juice/	0.04
8084	Beyond Meat Inc tokenized stock Bittrex	BYND	https://coinmarketcap.com//currencies/beyond-meat-inc-tokenized-stock-bittrex/	147.42
8085	SymVerse	SYM	https://coinmarketcap.com//currencies/symverse/	0.16
8086	Hauteclere Shards	HAUT	https://coinmarketcap.com//currencies/hauteclere-shards/	7.59
8087	WOWswap	WOW	https://coinmarketcap.com//currencies/wowswap/	20.48
8088	Diagon	DGN	https://coinmarketcap.com//currencies/diagon/	0
8089	Ymen.Finance	YMEN	https://coinmarketcap.com//currencies/ymen-finance/	1.82
8090	Yearn Finance Bit2	YFB2	https://coinmarketcap.com//currencies/yearn-finance-bit2/	11.7
8091	TokenBacon	BAK	https://coinmarketcap.com//currencies/tokenbacon/	1.27
8092	BEAR Coin	BEAR	https://coinmarketcap.com//currencies/voytek-bear-coin/	0.01
8093	Aunite	AUNIT	https://coinmarketcap.com//currencies/aunite/	0.02
8094	ShareAt	XAT	https://coinmarketcap.com//currencies/shareat/	1.01
8095	Beyond Meat tokenized stock FTX	BYND	https://coinmarketcap.com//currencies/beyond-meat-tokenized-stock-ftx/	142.98
8096	Jointer	JNTR	https://coinmarketcap.com//currencies/jointer/	0.16
8097	Robonomics Web Services	RWS	https://coinmarketcap.com//currencies/robonomics-web-services/	59927.87
8098	Litecash	CASH	https://coinmarketcap.com//currencies/litecash/	0
8099	Fission Cash	FCX	https://coinmarketcap.com//currencies/fission-cash/	2.38
8100	MoonDayPlus	MD+	https://coinmarketcap.com//currencies/moondayplus/	41.8
8101	TranslateMe Network Token	TMN	https://coinmarketcap.com//currencies/translateme-network-token/	0
8102	BlackBerry tokenized stock FTX	BB	https://coinmarketcap.com//currencies/blackberry-tokenized-stock-ftx/	12.21
8103	VENJOCOIN	VJC	https://coinmarketcap.com//currencies/venjocoin/	0.54
8104	EzyStayz	EZY	https://coinmarketcap.com//currencies/ezystayz/	0
8105	Kepler Network	KMW	https://coinmarketcap.com//currencies/kepler-network/	0
8106	Yearn Finance Red Moon	YFRM	https://coinmarketcap.com//currencies/yearn-finance-red-moon/	5.54
8107	ROMToken	ROM	https://coinmarketcap.com//currencies/romtoken/	0
8108	YFE Money	YFE	https://coinmarketcap.com//currencies/yfe-money/	4.96
8109	Richlab Token	RLE	https://coinmarketcap.com//currencies/richlab-token/	0
8110	ABCC Token	AT	https://coinmarketcap.com//currencies/abcc-token/	0.01
8111	LivenPay	LVN	https://coinmarketcap.com//currencies/livenpay/	0
8112	Billibilli tokenized stock Bittrex	BILI	https://coinmarketcap.com//currencies/billibilli-tokenized-stock-bittrex/	119.44
8113	Codeo Token	CODEO	https://coinmarketcap.com//currencies/codeo-token/	0
8114	Alibaba tokenized stock Bittrex	BABA	https://coinmarketcap.com//currencies/alibaba-tokenized-stock-bittrex/	229.71
8115	EXOR	EXOR	https://coinmarketcap.com//currencies/exor/	0
8116	PIRANHAS	$PIR	https://coinmarketcap.com//currencies/piranhas/	2.31
8117	PeepCoin	PCN	https://coinmarketcap.com//currencies/peepcoin/	0
8118	DeFiato	DFO	https://coinmarketcap.com//currencies/defiato/	0.01
8119	3X Short Ethereum Classic Token	ETCBEAR	https://coinmarketcap.com//currencies/3x-short-ethereum-classic-token/	0.04
8120	CPUcoin	CPU	https://coinmarketcap.com//currencies/cpucoin/	0.07
8121	ChallengeDac	CHL	https://coinmarketcap.com//currencies/challengedac/	0
8122	KUN	KUN	https://coinmarketcap.com//currencies/qian-kun/	0.9
8123	MTI Finance	MTI	https://coinmarketcap.com//currencies/mti-finance/	0.01
8124	Demeter Chain	DMTC	https://coinmarketcap.com//currencies/demeter-chain/	0.11
8125	Rivermount	RM	https://coinmarketcap.com//currencies/rivermount/	0.01
8126	yffi finance	YFFI	https://coinmarketcap.com//currencies/yffi-finance/	5.75
8127	RUSH	RUC	https://coinmarketcap.com//currencies/rush/	0.39
8128	AurusDeFi	AWX	https://coinmarketcap.com//currencies/aurusdefi/	3.22
8129	weBloc	WOK	https://coinmarketcap.com//currencies/webloc/	0
8130	YieldWars	WAR	https://coinmarketcap.com//currencies/yieldwars-com/	0.03
8131	KingMoney	KIM	https://coinmarketcap.com//currencies/kingmoney/	1639.73
8132	DigiDinar Token	DDRT	https://coinmarketcap.com//currencies/digidinar-token/	2.41
8133	TKN Token	TKNT	https://coinmarketcap.com//currencies/tkn-token/	0.0085
8134	Netflix tokenized stock Bittrex	NFLX	https://coinmarketcap.com//currencies/netflix-tokenized-stock-bittrex/	531.62
8135	ECC	ECC	https://coinmarketcap.com//currencies/eccoin/	0.000556
8136	Everyonescrypto	EOC	https://coinmarketcap.com//currencies/everyonescrypto/	0.01566
8137	Usechain Token	USE	https://coinmarketcap.com//currencies/usechain-token/	0.0003951
8138	Rank Token	RANK	https://coinmarketcap.com//currencies/rank-token/	0.0006751
8139	Blurt	BLURT	https://coinmarketcap.com//currencies/blurt/	0.01056
8140	WeSing Coin	WSC	https://coinmarketcap.com//currencies/wesing-coin/	0.0009723
8141	0cash	ZCH	https://coinmarketcap.com//currencies/0cash/	0.0005555
8142	Netkoin	NTK	https://coinmarketcap.com//currencies/netkoin/	0
8143	Free Tool Box Coin	FTB	https://coinmarketcap.com//currencies/free-tool-box-coin/	0
8144	eToro Euro	EURX	https://coinmarketcap.com//currencies/etoro-euro/	1.19
8145	SHENG	SHENG	https://coinmarketcap.com//currencies/sheng/	0
8146	BigdataCash	BDCASH	https://coinmarketcap.com//currencies/bigdatacash/	0.01
8147	Fiscus.fyi	FFYI	https://coinmarketcap.com//currencies/fiscus-fyi/	3.89
8148	CitiOs	R2R	https://coinmarketcap.com//currencies/citios/	0
8149	AlphaLink	ANK	https://coinmarketcap.com//currencies/alphalink/	0.01
8150	Dragon Ball	DRAGON	https://coinmarketcap.com//currencies/dragon-ball/	0.09
8151	Secure Cash	SCSX	https://coinmarketcap.com//currencies/secure-cash/	0.12
8152	YFI CREDITS GROUP	YFICG	https://coinmarketcap.com//currencies/yfi-credits-group/	0.08
8153	GermanCoin	GCX	https://coinmarketcap.com//currencies/germancoin/	0
8154	Codex	CDEX	https://coinmarketcap.com//currencies/codex/	0
8155	Hanacoin	HANA	https://coinmarketcap.com//currencies/hanacoin/	0
8156	Harcomia	HCA	https://coinmarketcap.com//currencies/harcomia/	0.03
8157	Enzo	NZO	https://coinmarketcap.com//currencies/enzo/	0.14
8158	SaveToken	SAVE	https://coinmarketcap.com//currencies/savetoken/	0.01
8159	Pfizer tokenized stock Bittrex	PFE	https://coinmarketcap.com//currencies/pfizer-tokenized-stock-bittrex/	34.98
8160	BitKAM	KAM	https://coinmarketcap.com//currencies/bitkam/	0
8161	Zuplo	ZLP	https://coinmarketcap.com//currencies/zuplo/	0
8162	Voltz	VOLTZ	https://coinmarketcap.com//currencies/voltz/	0.01
8163	Markaccy	MKCY	https://coinmarketcap.com//currencies/markaccy/	0.01
8164	USDA	USDA	https://coinmarketcap.com//currencies/usda/	0.08
8165	Schilling-Coin	SCH	https://coinmarketcap.com//currencies/schilling-coin/	0
8166	Maggie	MAG	https://coinmarketcap.com//currencies/maggie/	0
8167	Bit Trust System	BIUT	https://coinmarketcap.com//currencies/bit-trust-system/	0
8168	HOMT	HOMT	https://coinmarketcap.com//currencies/homt/	0.01
8169	Dragon Token	DT	https://coinmarketcap.com//currencies/dragon-token/	2.76
8170	StockChain	SCC	https://coinmarketcap.com//currencies/stockchain/	0
8171	OceanChain	OC	https://coinmarketcap.com//currencies/oceanchain/	0
8172	FuturoCoin	FTO	https://coinmarketcap.com//currencies/futurocoin/	0.01
8173	NOIZ	NOIZ	https://coinmarketcap.com//currencies/noizchain/	0.07
8174	Resistance	RES	https://coinmarketcap.com//currencies/resistance/	0
8175	Petrachor	PTA	https://coinmarketcap.com//currencies/petrachor/	0.01
8176	Aerotoken	AET	https://coinmarketcap.com//currencies/aerotoken/	0.05
8177	SorachanCoin	SORA	https://coinmarketcap.com//currencies/sorachancoin/	0.04
8178	The Hustle App	HUSL	https://coinmarketcap.com//currencies/the-hustle-app/	0
8179	Ladder Network Token	LAD	https://coinmarketcap.com//currencies/ladder-network-token/	0
8180	Our Pay	OUR	https://coinmarketcap.com//currencies/our-pay/	0.01
8181	One DEX	ODEX	https://coinmarketcap.com//currencies/one-dex/	0
8182	TokenSwap	TOP	https://coinmarketcap.com//currencies/tokenswap/	0.08
8183	Super Running Coin	SRC	https://coinmarketcap.com//currencies/super-running-coin/	0
8184	7Eleven	7E	https://coinmarketcap.com//currencies/7eleven/	0
8185	VN Token	VN	https://coinmarketcap.com//currencies/vn-token/	0.07
8186	P2P	P2P	https://coinmarketcap.com//currencies/p2p/	0
8187	AgaveCoin	AGVC	https://coinmarketcap.com//currencies/agavecoin/	0.05
8188	Epanus	EPS	https://coinmarketcap.com//currencies/epanus/	0.2
8189	Infinite Ricks	RICK	https://coinmarketcap.com//currencies/infinite-ricks/	0
8190	Sudan Gold Coin	SGC	https://coinmarketcap.com//currencies/sudan-gold-coin/	0
8191	Altbet	ABET	https://coinmarketcap.com//currencies/altbet/	0.01
8192	Xtock	XTX	https://coinmarketcap.com//currencies/xtock/	0
8193	3X Long Tether Gold Token	XAUTBULL	https://coinmarketcap.com//currencies/3x-long-tether-gold-token/	6357.5
8194	Fanaticos Cash	FCH	https://coinmarketcap.com//currencies/fanaticos-cash/	0
8195	YFI3.money	YFI3	https://coinmarketcap.com//currencies/yfi3-money/	2.94
8196	New Year Bull	NYB	https://coinmarketcap.com//currencies/new-year-bull/	0
8197	Azzure	AZZR	https://coinmarketcap.com//currencies/azzure/	0
8198	CelCoin	CELC	https://coinmarketcap.com//currencies/celcoin/	0
8199	Mavro	MAVRO	https://coinmarketcap.com//currencies/mavro/	0.17
8200	Colibri Protocol	CLBR	https://coinmarketcap.com//currencies/colibri/	0
8201	Sombe	SBE	https://coinmarketcap.com//currencies/sombe/	0
8202	Bitcurate	BTCR	https://coinmarketcap.com//currencies/bitcurate/	0.01
8203	Davies	DVS	https://coinmarketcap.com//currencies/davies/	0.02
8204	Amoveo	VEO	https://coinmarketcap.com//currencies/amoveo/	79.87
8205	Bitpayer Token	BPT	https://coinmarketcap.com//currencies/bitpayer-token/	0.3
8206	Junsonmingchncoin	JMC	https://coinmarketcap.com//currencies/junsonmingchncoin/	0
8207	BlockStamp	BST	https://coinmarketcap.com//currencies/blockstamp/	0.1
8208	Wallabee	WLB	https://coinmarketcap.com//currencies/wallabee/	0.57
8209	SaveNode	SNO	https://coinmarketcap.com//currencies/savenode/	0
8210	HGH Token	HGH	https://coinmarketcap.com//currencies/hgh-token/	0
8211	QOOBER	QOOB	https://coinmarketcap.com//currencies/qoober/	0.03
8212	Hatch DAO	HATCH	https://coinmarketcap.com//currencies/hatch-dao/	0.01
8213	Colletrix	CIPX	https://coinmarketcap.com//currencies/colletrix/	0
8214	Tronx Coin	TRONX	https://coinmarketcap.com//currencies/tronx-coin/	0.04
8215	Decentralized Data Assets Management	DDAM	https://coinmarketcap.com//currencies/decentralized-data-assets-management/	0
8216	HondaisCoin	HNDC	https://coinmarketcap.com//currencies/hondaiscoin/	0
8217	MYCE	YCE	https://coinmarketcap.com//currencies/myce/	0.01
8218	CMITCOIN	CMIT	https://coinmarketcap.com//currencies/cmitcoin/	0
8219	Krosscoin	KSS	https://coinmarketcap.com//currencies/krosscoin/	0
8220	AladiEx	ALA	https://coinmarketcap.com//currencies/aladiex/	0.37
8221	ORBYT Token	ORBYT	https://coinmarketcap.com//currencies/orbyt-token/	0
8222	Ystar	YSR	https://coinmarketcap.com//currencies/ystar/	0.01
8223	Etheroll	DICE	https://coinmarketcap.com//currencies/etheroll/	0.65
8224	B ONE PAYMENT	B1P	https://coinmarketcap.com//currencies/b-one-payment/	0.01
8225	808TA	808TA	https://coinmarketcap.com//currencies/808ta/	0
8226	Lapis Chain	LPS	https://coinmarketcap.com//currencies/lapis-chain/	0
8227	EcoDollar	ECOS	https://coinmarketcap.com//currencies/ecodollar/	0.02
8228	Shift	SHIFT	https://coinmarketcap.com//currencies/shift/	1.45
8229	LibreFreelencer	LIBREF	https://coinmarketcap.com//currencies/librefreelencer/	0
8230	Master Coin Point	MACPO	https://coinmarketcap.com//currencies/macpo/	0
8231	FXPay	FXP	https://coinmarketcap.com//currencies/fxpay/	0
8232	GoalTime N	GTX	https://coinmarketcap.com//currencies/goaltime-n/	0.000278
8233	ROS Coin	ROS	https://coinmarketcap.com//currencies/ros-coin/	0.005144
8234	BLOCKCLOUT	CLOUT	https://coinmarketcap.com//currencies/blockclout/	7.189e-05
8235	VERA	VERA	https://coinmarketcap.com//currencies/vera/	0.000556
8236	French Digital Reserve	FDR	https://coinmarketcap.com//currencies/french-digital-reserve/	0.3086
8237	Valuto	VLU	https://coinmarketcap.com//currencies/valuto/	0.001112
8238	i9 Coin	I9C	https://coinmarketcap.com//currencies/i9-coin/	0.000556
8239	SpokLottery	SPKL	https://coinmarketcap.com//currencies/spoklottery/	6.53e-05
8240	LOOPREX	LOOP	https://coinmarketcap.com//currencies/looprex/	0.0003764
8241	Oracolxor	XOR	https://coinmarketcap.com//currencies/oracolxor/	0.000278
8242	BitcoinRegular	BTRL	https://coinmarketcap.com//currencies/bitcoinregular/	0.12
8243	MintCoin	MINT	https://coinmarketcap.com//currencies/mintcoin/	0
8244	Libera	LIB	https://coinmarketcap.com//currencies/libera/	0.56
8245	Nilu	NILU	https://coinmarketcap.com//currencies/nilu/	0
8246	OBIC	OBIC	https://coinmarketcap.com//currencies/obic/	0.31
8247	Centaure	CEN	https://coinmarketcap.com//currencies/centaure/	0
8248	Sumcoin	SUM	https://coinmarketcap.com//currencies/sumcoin/	773.73
8249	Touch Social	TST	https://coinmarketcap.com//currencies/touch-social/	0
8250	RabbitCoin	RBBT	https://coinmarketcap.com//currencies/rabbitcoin/	0
8251	HoboNickels	HBN	https://coinmarketcap.com//currencies/hobonickels/	0.01
8252	Karatgold Coin	KBC	https://coinmarketcap.com//currencies/karatgold-coin/	0
8253	Axiom	AXIOM	https://coinmarketcap.com//currencies/axiom/	0
8254	Social Send	SEND	https://coinmarketcap.com//currencies/social-send/	0.02
8255	Rubycoin	RBY	https://coinmarketcap.com//currencies/rubycoin/	0.14
8256	Clams	CLAM	https://coinmarketcap.com//currencies/clams/	1.59
8257	Quotient	XQN	https://coinmarketcap.com//currencies/quotient/	0
8258	Bubble	BUB	https://coinmarketcap.com//currencies/bubble/	0.01
8259	ClubCoin	CLUB	https://coinmarketcap.com//currencies/clubcoin/	0.02
8260	AvatarCoin	AV	https://coinmarketcap.com//currencies/avatarcoin/	0.05
8261	Francs	FRN	https://coinmarketcap.com//currencies/francs/	0.01
8262	Aces	ACES	https://coinmarketcap.com//currencies/aces/	0
8263	PROUD Money	PROUD	https://coinmarketcap.com//currencies/proud-money/	0
8264	InflationCoin	IFLT	https://coinmarketcap.com//currencies/inflationcoin/	0
8265	BlazerCoin	BLAZR	https://coinmarketcap.com//currencies/blazercoin/	0
8266	Moving Cloud Coin	MCC	https://coinmarketcap.com//currencies/moving-cloud-coin/	0
8267	XPA	XPA	https://coinmarketcap.com//currencies/xpa/	0
8268	MSD	MSD	https://coinmarketcap.com//currencies/msd/	0.02
8269	Hubii Network	HBT	https://coinmarketcap.com//currencies/hubii-network/	0.35
8270	MOAC	MOAC	https://coinmarketcap.com//currencies/moac/	0.39
8271	IDEX Membership	IDXM	https://coinmarketcap.com//currencies/idex-membership/	0
8272	ValueChain	VLC	https://coinmarketcap.com//currencies/valuechain/	0
8273	JET8	J8T	https://coinmarketcap.com//currencies/jet8/	0
8274	Pixie Coin	PXC	https://coinmarketcap.com//currencies/pixie-coin/	0
8275	Coni	CONI	https://coinmarketcap.com//currencies/coni/	0.04
8276	Wiki Token	WIKI	https://coinmarketcap.com//currencies/wiki-token/	2.29
8277	Insight Chain	INB	https://coinmarketcap.com//currencies/insight-chain/	1.63
8278	Obitan Chain	OBTC	https://coinmarketcap.com//currencies/obitan-chain/	0
8279	Twinkle	TKT	https://coinmarketcap.com//currencies/twinkle/	0
8280	YUKI	YUKI	https://coinmarketcap.com//currencies/yuki/	0
8281	DOWCOIN	DOW	https://coinmarketcap.com//currencies/dowcoin/	0
8282	Traceability Chain	TAC	https://coinmarketcap.com//currencies/traceability-chain/	0
8283	MobilinkToken	MOLK	https://coinmarketcap.com//currencies/mobilinktoken/	0
8284	TRONCLASSIC	TRXC	https://coinmarketcap.com//currencies/tronclassic/	0
8285	Digital Asset Guarantee Token	DAGT	https://coinmarketcap.com//currencies/digital-asset-guarantee-token/	0.14
8286	Wixlar	WIX	https://coinmarketcap.com//currencies/wixlar/	0.01
8287	Ondori	RSTR	https://coinmarketcap.com//currencies/ondori/	0
8288	AirWire	WIRE	https://coinmarketcap.com//currencies/airwire/	0.06
8289	Commerce Data Connection	CDC	https://coinmarketcap.com//currencies/commerce-data-connection/	0
8290	DEEX	DEEX	https://coinmarketcap.com//currencies/deex/	0
8291	Dark	D4RK	https://coinmarketcap.com//currencies/dark/	0
8292	MediBit	MEDIBIT	https://coinmarketcap.com//currencies/medibit/	0
8293	Bgogo Token	BGG	https://coinmarketcap.com//currencies/bgogo-token/	0
8294	Baer Chain	BRC	https://coinmarketcap.com//currencies/baer-chain/	0.08
8295	MoX	MOX	https://coinmarketcap.com//currencies/mox/	0
8296	EurocoinToken	ECTE	https://coinmarketcap.com//currencies/eurocoin-token/	0.08
8297	QUSD	QUSD	https://coinmarketcap.com//currencies/qusd/	0.01
8298	1SG	1SG	https://coinmarketcap.com//currencies/1sg/	3.94
8299	Jewel	JWL	https://coinmarketcap.com//currencies/jewel/	0.2
8300	Bitex Global XBX Coin	XBX	https://coinmarketcap.com//currencies/bitex-global-xbx-coin/	0.01
8301	Esportbits	HLT	https://coinmarketcap.com//currencies/esportbits/	0.16
8302	OOOBTC TOKEN	OBX	https://coinmarketcap.com//currencies/ooobtc-token/	0.01
8303	BIZKEY	BZKY	https://coinmarketcap.com//currencies/bizkey/	0
8304	Stellar Gold	XLMG	https://coinmarketcap.com//currencies/stellar-gold/	0
8305	Atlas Token	ATLS	https://coinmarketcap.com//currencies/atlas-token/	0
8306	Coineal Token	NEAL	https://coinmarketcap.com//currencies/coineal-token/	0.02
8307	Tratin	TRAT	https://coinmarketcap.com//currencies/tratin/	0
8308	B91	B91	https://coinmarketcap.com//currencies/b91/	0
8309	SpectreSecurityCoin	XSPC	https://coinmarketcap.com//currencies/spectre-security-coin/	0
8310	USDQ	USDQ	https://coinmarketcap.com//currencies/usdq/	0.61
8311	SPIN Protocol	SPIN	https://coinmarketcap.com//currencies/spin-protocol/	0
8312	Maya Preferred 223	MAPR	https://coinmarketcap.com//currencies/maya-preferred-223/	120.61
8313	Eco Value Coin	EVC	https://coinmarketcap.com//currencies/eco-value-coin/	0
8314	YottaChain	YTA	https://coinmarketcap.com//currencies/yottachain/	0.02
8315	GLOBEX	GEX	https://coinmarketcap.com//currencies/globex/	0
8316	Volume Network	VOL	https://coinmarketcap.com//currencies/volume-network/	0
8317	Xenoverse	XENO	https://coinmarketcap.com//currencies/xenoverse/	0
8318	Custody Token	CUST	https://coinmarketcap.com//currencies/custody-token/	0
8319	CITEX Token	CTT	https://coinmarketcap.com//currencies/citex-token/	0.01
8320	Aitheon	ACU	https://coinmarketcap.com//currencies/aitheon/	0
8321	Ritocoin	RITO	https://coinmarketcap.com//currencies/ritocoin/	0
8322	Bitscoin	BTCX	https://coinmarketcap.com//currencies/bitscoin/	0
8323	Aeryus	AER	https://coinmarketcap.com//currencies/aeryus/	0
8324	Korbot	KBOT	https://coinmarketcap.com//currencies/korbot/	0.07
8325	Electronero	ETNX	https://coinmarketcap.com//currencies/electronero/	0
8326	VENA	VENA	https://coinmarketcap.com//currencies/vena/	0
8327	LINK	LN	https://coinmarketcap.com//currencies/link/	85.57
8328	Tchain	TCH	https://coinmarketcap.com//currencies/tchain/	0
8329	STK Coin	STK	https://coinmarketcap.com//currencies/stk-coin/	0.08
8330	BOSCore	BOS	https://coinmarketcap.com//currencies/boscore/	0
8331	Sierracoin	SIERRA	https://coinmarketcap.com//currencies/sierracoin/	0
8332	Tokenize Xchange	TKX	https://coinmarketcap.com//currencies/tokenize-xchange/	4.37
8333	Versess Coin	VERS	https://coinmarketcap.com//currencies/versess-coin/	21.69
8334	INLOCK	ILK	https://coinmarketcap.com//currencies/inlock/	0.005049
8335	QPay	QPY	https://coinmarketcap.com//currencies/qpay/	1.795e-05
8336	Bancacy	BNY	https://coinmarketcap.com//currencies/bancacy/	0
8337	NOVA	NOVA	https://coinmarketcap.com//currencies/nova/	7.756e-05
8338	Dash Cash	DSC	https://coinmarketcap.com//currencies/dash-cash/	10
8339	BITFXT COIN	BXT	https://coinmarketcap.com//currencies/bitfxt-coin/	0
8340	Marshal Lion Group Coin	MLGC	https://coinmarketcap.com//currencies/marshal-lion-group-coin/	0.5555
8341	SOLBIT	SBT	https://coinmarketcap.com//currencies/solbit/	0.00156
8342	Bitcoin & Company Network	BITN	https://coinmarketcap.com//currencies/bitcoin-and-company-network/	0
8343	Bankroll Network	BNKR	https://coinmarketcap.com//currencies/bankroll-network/	0.18
8344	Bitgrin	XBG	https://coinmarketcap.com//currencies/bitgrin/	0
8345	Moozicore	MZG	https://coinmarketcap.com//currencies/moozicore/	0
8346	NairaX	NIRX	https://coinmarketcap.com//currencies/nairax/	0
8347	Sesameseed	SEED	https://coinmarketcap.com//currencies/sesameseed/	0.08
8348	Tradeplus	TDPS	https://coinmarketcap.com//currencies/tradeplus/	0.06
8349	Apple Network	ANK	https://coinmarketcap.com//currencies/apple-network/	0
8350	Building Cities Beyond Blockchain	BCB	https://coinmarketcap.com//currencies/building-cities-beyond-blockchain/	0.38
8351	CRYPTOBUCKS	CBUCKS	https://coinmarketcap.com//currencies/cryptobucks/	0.03
8352	VOMER	VMR	https://coinmarketcap.com//currencies/vomer/	0.59
8353	1AI Token	1AI	https://coinmarketcap.com//currencies/1ai-token/	0
8354	FRED Energy	FRED	https://coinmarketcap.com//currencies/fred-energy/	0
8355	AfroDex	AfroX	https://coinmarketcap.com//currencies/afrodex/	0
8356	Kryptofranc	KYF	https://coinmarketcap.com//currencies/kryptofranc/	0
8357	Bitcoffeen	BFF	https://coinmarketcap.com//currencies/bitcoffeen/	0.03
8358	ExtStock Token	XT	https://coinmarketcap.com//currencies/extstock-token/	0.07
8359	Kratscoin	KTC	https://coinmarketcap.com//currencies/kratscoin/	0.32
8360	Compound SAI	CSAI	https://coinmarketcap.com//currencies/compound-sai/	0.03
8361	Asac Coin	ASAC	https://coinmarketcap.com//currencies/asac-coin/	0.01
8362	The Tokenized Bitcoin	imBTC	https://coinmarketcap.com//currencies/the-tokenized-bitcoin/	55422.71
8363	SuperSkynet	SSN	https://coinmarketcap.com//currencies/superskynet/	0
8364	Ethereum eRush	EER	https://coinmarketcap.com//currencies/ethereum-erush/	0.01
8365	StellarPayGlobal	XLPG	https://coinmarketcap.com//currencies/stellarpayglobal/	0.14
8366	ODE	ODE	https://coinmarketcap.com//currencies/ode/	242.29
8367	SappChain	SAPP	https://coinmarketcap.com//currencies/sappchain/	0
8368	Charg Coin	CHG	https://coinmarketcap.com//currencies/charg-coin/	0.49
8369	Unknown Fair Object	UFO	https://coinmarketcap.com//currencies/unknown-fair-object/	0.11
8370	Cicoin	CICX	https://coinmarketcap.com//currencies/cicoin/	0.04
8371	Swapzilla	SWZL	https://coinmarketcap.com//currencies/swapzilla/	0
8372	pTokens BTC	PBTC	https://coinmarketcap.com//currencies/ptokens-btc/	56220.92
8373	Entherfound	ETF	https://coinmarketcap.com//currencies/entherfound/	0.04
8374	Metaprediction	METP	https://coinmarketcap.com//currencies/metaprediction/	0
8375	Binance KRW	BKRW	https://coinmarketcap.com//currencies/binance-krw/	0
8376	eToro Pound Sterling	GBPX	https://coinmarketcap.com//currencies/etoro-pound-sterling/	0
8377	eToro New Zealand Dollar	NZDX	https://coinmarketcap.com//currencies/etoro-new-zealand-dollar/	0
8378	Becaz	BCZ	https://coinmarketcap.com//currencies/becaz/	0.62
8379	BeeEx	BEE	https://coinmarketcap.com//currencies/beeex/	0.01
8380	Litecoin SV	LSV	https://coinmarketcap.com//currencies/litecoin-sv/	1.57
8381	VEHICLE DATA ARTIFICIAL INTELLIGENCE PLATFORM	VAIP	https://coinmarketcap.com//currencies/vehicle-data-artificial-intelligence-platform/	0.42
8382	Compound Ether	CETH	https://coinmarketcap.com//currencies/compound-ether/	34.9
8383	CryptoBet	CBET	https://coinmarketcap.com//currencies/cryptobet/	0
8384	EYES Protocol	EYES	https://coinmarketcap.com//currencies/eyes-protocol/	0.01
8385	UniDollar	UNIUSD	https://coinmarketcap.com//currencies/unidollar/	0
8386	Quish Coin	QTV	https://coinmarketcap.com//currencies/quish-coin/	0.05
8387	Golden Ratio Token	GRT	https://coinmarketcap.com//currencies/golden-ratio-token/	0.86
8388	PieDAO BTC++	BTC++	https://coinmarketcap.com//currencies/piedao-btc/	54569.17
8389	ChainZ Arena	SOUL	https://coinmarketcap.com//currencies/chainz-arena/	0.03
8390	IOOX System	IOOX	https://coinmarketcap.com//currencies/ioox-system/	0.02
8391	Aave TUSD	ATUSD	https://coinmarketcap.com//currencies/aave-tusd/	1
8392	Aave KNC	AKNC	https://coinmarketcap.com//currencies/aave-knc/	2.18
8393	Aave LINK	ALINK	https://coinmarketcap.com//currencies/aave-link/	27.78
8394	Aave SNX	ASNX	https://coinmarketcap.com//currencies/aave-snx/	19.02
8395	Aave MKR	AMKR	https://coinmarketcap.com//currencies/aave-mkr/	2036.23
8396	Aave BAT	ABAT	https://coinmarketcap.com//currencies/aave-bat/	0.82
8397	Aave BUSD	ABUSD	https://coinmarketcap.com//currencies/aave-busd/	1
8398	Aave DAI	ADAI	https://coinmarketcap.com//currencies/aave-dai/	1
8399	sBTC	SBTC	https://coinmarketcap.com//currencies/sbtc/	54698.99
8400	sADA	SADA	https://coinmarketcap.com//currencies/sada/	1.06
8401	Oikos	OKS	https://coinmarketcap.com//currencies/oikos/	0.05
8402	HelixNetwork	MHLX	https://coinmarketcap.com//currencies/helixnetwork/	0
8403	Kublaicoin	KUB	https://coinmarketcap.com//currencies/kublaicoin/	0.03
8404	Nokencoin	NOKN	https://coinmarketcap.com//currencies/nokencoin/	0.01
8405	AntiscamToken	AST	https://coinmarketcap.com//currencies/antiscamtoken/	0.05
8406	Bankroll Vault	VLT	https://coinmarketcap.com//currencies/bankroll-vault/	0.21
8407	sDEFI	SDEFI	https://coinmarketcap.com//currencies/sdefi/	13889.81
8408	sXAG	SXAG	https://coinmarketcap.com//currencies/sxag/	24.27
8409	SafePost	STAMP	https://coinmarketcap.com//currencies/safepost/	0
8410	Originate Coin	ORC	https://coinmarketcap.com//currencies/originate-coin/	0
8411	NEOBITCOIN	NBTC	https://coinmarketcap.com//currencies/neobitcoin/	5.5
8412	Unit Protocol	COL	https://coinmarketcap.com//currencies/unit-protocol/	0
8413	Union Fair Coin	UFC	https://coinmarketcap.com//currencies/union-fair-coin/	0.07
8414	Bitcoin True	BTCT	https://coinmarketcap.com//currencies/bitcoin-true/	0
8415	Cybercoin	CBR	https://coinmarketcap.com//currencies/cybercoin/	0
8416	Crypto User Base	CUB	https://coinmarketcap.com//currencies/crypto-user-base/	0.18
8417	CoinZoom	ZOOM	https://coinmarketcap.com//currencies/coinzoom/	0.07
8418	ZOM	ZOM	https://coinmarketcap.com//currencies/zom/	0.14
8419	DEMOS	DOS	https://coinmarketcap.com//currencies/demos/	0.04
8420	Unicrypt	UNC	https://coinmarketcap.com//currencies/unicrypt/	0.06
8421	DeFi Nation Signals DAO	DSD	https://coinmarketcap.com//currencies/defi-nation-signals-dao/	2.49
8422	PayFrequent USD	PUSD	https://coinmarketcap.com//currencies/payfrequent-usd-2/	1.19
8423	PowerBalt	PWRB	https://coinmarketcap.com//currencies/powerbalt/	0.15
8424	Kids Cash	KASH	https://coinmarketcap.com//currencies/kids-cash/	0.09
8425	CNY Tether	CNYT	https://coinmarketcap.com//currencies/cny-tether/	0.15
8426	Bit World Token	BWB	https://coinmarketcap.com//currencies/bw-token/	0.01
8427	FEX Token	FEX	https://coinmarketcap.com//currencies/fex-token/	0.33
8428	DigiMax DGMT	DGMT	https://coinmarketcap.com//currencies/digimax-dgmt/	0
8429	TOPBTC Token	TOPB	https://coinmarketcap.com//currencies/topb/	0.01
8430	LP-yCurve	YDAI+YUSDC+YUSDT+YTUSD	https://coinmarketcap.com//currencies/curve-fi-ydai-yusdc-yusdt-ytusd/	1.13
8431	888tron	888	https://coinmarketcap.com//currencies/888tron/	0.28
8432	888tron	888	https://coinmarketcap.com//currencies/888tron/	0.2797
8433	3X Short Matic Token	MATICBEAR	https://coinmarketcap.com//currencies/3x-short-matic-token/	1e-08
8434	Essek Tov	ETO	https://coinmarketcap.com//currencies/essek-tov/	0.003889
8435	KYSC Token	KYSC	https://coinmarketcap.com//currencies/kysc-token/	0.0001021
8436	Chess Coin	CHESS	https://coinmarketcap.com//currencies/chess-coin/	0.001945
8437	BTC Network Demand Set II	BYTE	https://coinmarketcap.com//currencies/btc-network-demand-set-ii/	710.43
8438	Chad Link Set	CHADLINK	https://coinmarketcap.com//currencies/chad-link-set/	529.11
8439	ETH 20 Day MA Crossover Set	ETH20SMACO	https://coinmarketcap.com//currencies/eth-20-day-ma-crossover-set/	1698.53
8440	ETH 20 Day MA Crossover Yield Set	ETHMACOAPY	https://coinmarketcap.com//currencies/eth-20-day-ma-crossover-yield-set/	1564.85
8441	ETH 26 EMA Crossover Yield II	ETHEMAAPY	https://coinmarketcap.com//currencies/eth-26-ma-crossover-yield-ii/	1212.84
8442	ETH/BTC RSI Ratio Trading Set	ETHBTCRSI	https://coinmarketcap.com//currencies/eth-btc-rsi-ratio-trading-set/	897.78
8443	ETH/LINK Price Action Candlestick Set	LINKETHPA	https://coinmarketcap.com//currencies/eth-link-price-action-candlestick-set/	1775.09
8444	ETH Price Action Candlestick Set	ETHPA	https://coinmarketcap.com//currencies/eth-price-action-candlestick-set/	1321.24
8445	ETH RSI 60/40 Crossover Set	ETHRSI6040	https://coinmarketcap.com//currencies/eth-rsi-60-40-crossover-set/	929.44
8446	ETH RSI 60/40 Yield Set	ETHRSIAPY	https://coinmarketcap.com//currencies/eth-rsi-60-40-yield-set/	922.82
8447	FlexETH/BTC Set	FLEXETHBTC	https://coinmarketcap.com//currencies/flexeth-btc-set/	931.64
8448	Intelligent Ratio Set	INTRATIO	https://coinmarketcap.com//currencies/intelligent-ratio-set/	1650.16
8449	LINK/ETH RSI Ratio Trading Set	LINKETHRSI	https://coinmarketcap.com//currencies/link-eth-rsi-ratio-trading-set/	514.17
8450	LINK Profit Taker Set	LINKPT	https://coinmarketcap.com//currencies/link-profit-taker-set/	712.5
8451	WhiteRockCasino	WRC	https://coinmarketcap.com//currencies/whiterockcasino/	0
8452	iETH	IETH	https://coinmarketcap.com//currencies/ieth/	1346.57
8453	sLINK	sLINK	https://coinmarketcap.com//currencies/slink/	27
8454	sXAU	SXAU	https://coinmarketcap.com//currencies/sxau/	1755.79
8455	sXTZ	SXTZ	https://coinmarketcap.com//currencies/sxtz/	4.06
8456	iBTC (Synthetix)	IBTC	https://coinmarketcap.com//currencies/ibtc-synthetix/	20247.41
8457	sBNB	SBNB	https://coinmarketcap.com//currencies/sbnb/	276.43
8458	Bityuan	BTY	https://coinmarketcap.com//currencies/bityuan/	0.5
8459	Diamond Voucher	DVS	https://coinmarketcap.com//currencies/diamond-voucher/	13.11
8460	3X Short Tether Gold Token	XAUTBEAR	https://coinmarketcap.com//currencies/3x-short-tether-gold-token/	3080
8461	Yield Farming Token	YFT	https://coinmarketcap.com//currencies/yield-farming-token/	5
8462	HL Chain	HL	https://coinmarketcap.com//currencies/hl-chain/	0
8463	DApp Evolution	EVO	https://coinmarketcap.com//currencies/dapp-evolution/	0.14
8464	Dark Energy Crystals	DEC	https://coinmarketcap.com//currencies/dark-energy-crystals/	0
8465	Play Royal	PLAY	https://coinmarketcap.com//currencies/play-royal/	0
8466	SCC DIGforIT	SCC	https://coinmarketcap.com//currencies/scc-digforit/	0.01
8467	AVCCOIN	AVC	https://coinmarketcap.com//currencies/avccoin/	0.05
8468	BTS Coin	BTSC	https://coinmarketcap.com//currencies/bts-coin/	0
8469	DTOP Token	DTOP	https://coinmarketcap.com//currencies/data-trade-on-demand-platform/	0
8470	Distributed Energy Coin	DEC	https://coinmarketcap.com//currencies/distributed-energy-coin/	0
8471	FunKeyPay	FNK	https://coinmarketcap.com//currencies/funkeypay/	0
8472	GLOBALTRUSTFUND TOKEN	GTF	https://coinmarketcap.com//currencies/globaltrustfund-token/	0
8473	Hemelios	HEM	https://coinmarketcap.com//currencies/hemelios/	0
8474	IDCM Token	IT	https://coinmarketcap.com//currencies/idcm-token/	0
8475	Galaxy Pool Coin	GPO	https://coinmarketcap.com//currencies/galaxy-pool-coin/	0.04
8476	Intelligence Quickly Chain	IQC	https://coinmarketcap.com//currencies/intelligence-quickly-chain/	0
8477	LoveHearts	LVH	https://coinmarketcap.com//currencies/lovehearts/	0
8478	TRONbetDice	DICE	https://coinmarketcap.com//currencies/tronbetdice/	0.03
8479	Aludra Network	ALD	https://coinmarketcap.com//currencies/aludra-network/	0
8480	UniversalEnergyChain	UENC	https://coinmarketcap.com//currencies/universalenergychain/	1
8481	Ripple Alpha	XLA	https://coinmarketcap.com//currencies/ripple-alpha/	0
8482	Frens Community	FRENS	https://coinmarketcap.com//currencies/frens-community/	1.08
8483	MONEY PARTY	PARTY	https://coinmarketcap.com//currencies/money-party/	0
8484	OWL	OWL	https://coinmarketcap.com//currencies/owl/	1.04
8485	Equus Mining Token	EQMT	https://coinmarketcap.com//currencies/equus-mining-token/	0
8486	Trump Wins Token	TRUMPWIN	https://coinmarketcap.com//currencies/trump-wins-token/	0.02
8487	Sendvibe	SVB	https://coinmarketcap.com//currencies/sendvibe/	0
8488	MYX Network	MYX	https://coinmarketcap.com//currencies/myx-network/	0
8489	cherry	CHERRY	https://coinmarketcap.com//currencies/cherry/	0.06
8490	Tokens of Babel	TOB	https://coinmarketcap.com//currencies/tokens-of-babel/	0.12
8491	Schain Wallet	SCHA	https://coinmarketcap.com//currencies/schain-wallet/	0
8492	Community Token	COM	https://coinmarketcap.com//currencies/community-token/	0.17
8493	Ethereum Vault	ETHV	https://coinmarketcap.com//currencies/ethereum-vault/	0.03
8494	Porkchop	CHOP	https://coinmarketcap.com//currencies/porkchop/	0.01
8495	Camouflage.eth	CAMO	https://coinmarketcap.com//currencies/camouflage-eth/	0
8496	COIL	COIL	https://coinmarketcap.com//currencies/coil/	0.78
8497	DACC2	DACC2	https://coinmarketcap.com//currencies/dacc2/	0
8498	MIKS COIN	MIKS	https://coinmarketcap.com//currencies/miks-coin/	0
8499	Orbicular	ORBI	https://coinmarketcap.com//currencies/orbicular/	0
8500	Newtonium	NEWTON	https://coinmarketcap.com//currencies/newtonium/	0.04
8501	OC Protocol	OCP	https://coinmarketcap.com//currencies/oc-protocol/	0.01
8502	JBOX	JBX	https://coinmarketcap.com//currencies/jboxcoin/	0.02
8503	Dextrust	DETS	https://coinmarketcap.com//currencies/dextrust/	0.04
8504	Blaze Network	BLZN	https://coinmarketcap.com//currencies/blaze-network/	0.5
8505	Wenburn	WENB	https://coinmarketcap.com//currencies/wenburn/	0.03
8506	Vision	VSN	https://coinmarketcap.com//currencies/vision/	0.39
8507	Port of DeFi Network	PDF	https://coinmarketcap.com//currencies/port-of-defi-network/	0.11
8508	Hands of Steel	STEEL	https://coinmarketcap.com//currencies/hands-of-steel/	0.01
8509	CEDARS	CEDS	https://coinmarketcap.com//currencies/cedars/	0.52
8510	YEP COIN	YEP	https://coinmarketcap.com//currencies/yep-coin/	29.5
8511	Fee Active Collateral Token	FACT	https://coinmarketcap.com//currencies/fee-active-collateral-token/	0.02
8512	GOT Token	GOT	https://coinmarketcap.com//currencies/got-token/	1.66
8513	Popcorn	CORN	https://coinmarketcap.com//currencies/popcorn/	0.08
8514	Bast	BAST	https://coinmarketcap.com//currencies/bast/	5.14
8515	Social Finance	SOFI	https://coinmarketcap.com//currencies/social-finance/	4.4
8516	Walnut.finance	WTF	https://coinmarketcap.com//currencies/walnut-finance/	0.6
8517	TAI	TAI	https://coinmarketcap.com//currencies/tai/	0.19
8518	SalmonSwap	SAL	https://coinmarketcap.com//currencies/salmonswap/	6.29
8519	YFIBusiness	YFIB	https://coinmarketcap.com//currencies/yfibusiness/	2.24
8520	yfBeta	YFBETA	https://coinmarketcap.com//currencies/yfbeta/	7.96
8521	TRONbetLive	LIVE	https://coinmarketcap.com//currencies/tronbetlive/	0.04
8522	SSS Finance	SSS	https://coinmarketcap.com//currencies/sss-finance/	2.76
8523	YFA Finance	YFA	https://coinmarketcap.com//currencies/yfa-finance/	2.5
8524	YFUEL	YFUEL	https://coinmarketcap.com//currencies/yfuel/	2.57
8525	junca Cash	JCC	https://coinmarketcap.com//currencies/junca-cash/	0.11
8526	Yearn Finance Protocol	YFP	https://coinmarketcap.com//currencies/yearn-finance-protocol/	0.28
8527	The Forms	FRMS	https://coinmarketcap.com//currencies/the-forms/	0.02
8528	Definitex	DFX	https://coinmarketcap.com//currencies/definitex/	0.46
8529	FlashSwap	FSP	https://coinmarketcap.com//currencies/flashswap/	0.02
8530	Token CashPay	TCP	https://coinmarketcap.com//currencies/token-cashpay/	0.03
8531	GOD KIMCHI	gKIMCHI	https://coinmarketcap.com//currencies/god-kimchi/	0
8532	YFIKING,FINANCE	YFIKING	https://coinmarketcap.com//currencies/yfiking-finance/	0.2211
8533	HBTC Captain Token	HBC	https://coinmarketcap.com//currencies/hbtc-token/	13.76
8534	Mafia.Network	MAFI	https://coinmarketcap.com//currencies/mafia-network/	0.004663
8535	Maki Finance	MAKI	https://coinmarketcap.com//currencies/maki-finance/	0.7891
8536	Predix Network	PRDX	https://coinmarketcap.com//currencies/predix-network/	0.3896
8537	YEARN2.FINANCE	YFI2	https://coinmarketcap.com//currencies/yearn2-finance/	4.66
8538	Candela Coin	CLA	https://coinmarketcap.com//currencies/candelacoin/	0.000296
8539	Farm House Finance	FHSE	https://coinmarketcap.com//currencies/farm-house-finance/	8.7
8540	SherLOCK Security	LOCK	https://coinmarketcap.com//currencies/sherlock-security/	0.4916
8541	OFIN Token	ON	https://coinmarketcap.com//currencies/ofin-token/	0.004957
8542	Momentum	XMM	https://coinmarketcap.com//currencies/momentum/	0
8543	Yield Farming Known as Ash	YFKA	https://coinmarketcap.com//currencies/yield-farming-known-as-ash/	15.35
8544	Tribute	TRBT	https://coinmarketcap.com//currencies/tribute/	0.29
8545	Atlantis Token	ATIS	https://coinmarketcap.com//currencies/atlantis-token/	0.05
8546	YFarmLand Token	YFARMER	https://coinmarketcap.com//currencies/yfarmland-token/	0.53
8547	Fame	FAME	https://coinmarketcap.com//currencies/fame/	0
8548	Va Na Su	VNS	https://coinmarketcap.com//currencies/va-na-su/	0.81
8549	Moon YFI	MYFI	https://coinmarketcap.com//currencies/moon-yfi/	7.08
8550	YFMoonshot	YFMS	https://coinmarketcap.com//currencies/yfmoonshot/	7.38
8551	ETF Dao	TFD	https://coinmarketcap.com//currencies/etf-dao/	0.01
8552	Resoper	RSP	https://coinmarketcap.com//currencies/resoper/	0
8553	Shill & Win	PoSH	https://coinmarketcap.com//currencies/shill-win/	0
8554	Livenodes Token	LNOT	https://coinmarketcap.com//currencies/livenodes-token/	64.03
8555	RI Token	RI	https://coinmarketcap.com//currencies/ri-token/	0.18
8556	Finswap	FNSP	https://coinmarketcap.com//currencies/finswap/	0.06
8557	Digital Currency Daily	DCD	https://coinmarketcap.com//currencies/digital-currency-daily/	0
8558	Gire Token	GET	https://coinmarketcap.com//currencies/gire-token/	0.06
8559	iCherry Finance	ICH	https://coinmarketcap.com//currencies/icherry-finance/	0.3
8560	UnityDAO	UTY	https://coinmarketcap.com//currencies/unitydao/	9.19
8561	Acuity Token	ACU	https://coinmarketcap.com//currencies/acuity-token/	0
8562	DeFi Firefly	DFF	https://coinmarketcap.com//currencies/defi-firefly/	0.25
8563	Safari	SFR	https://coinmarketcap.com//currencies/safari/	0
8564	UNII Finance	UNII	https://coinmarketcap.com//currencies/unii-finance/	0
8565	Buccaneer V2	BUCC	https://coinmarketcap.com//currencies/buccaneer-v2/	6.28
8566	CryptoWater	C2O	https://coinmarketcap.com//currencies/cryptowater/	1.18
8567	CXN Network	CXN	https://coinmarketcap.com//currencies/cxn-network/	0
8568	Yearn Land	YLAND	https://coinmarketcap.com//currencies/yearn-land/	14.01
8569	AmericanHorror.Finance	AHF	https://coinmarketcap.com//currencies/americanhorror-finance/	317.95
8570	Yoink	YNK	https://coinmarketcap.com//currencies/yoink/	0
8571	APEcoin	APE	https://coinmarketcap.com//currencies/apecoin/	0.03
8572	Pajama.Finance	PJM	https://coinmarketcap.com//currencies/pajama-finance/	4.3
8573	Yearn Finance Ecosystem	YFIEC	https://coinmarketcap.com//currencies/yearn-finance-ecosystem/	3.43
8574	YFET	YFET	https://coinmarketcap.com//currencies/yfet/	3
8575	Yearn4 Finance	YF4	https://coinmarketcap.com//currencies/yearn4-finance/	1.47
8576	ErcauX	RAUX	https://coinmarketcap.com//currencies/ercaux/	0.12
8577	ZAC Finance	ZAC	https://coinmarketcap.com//currencies/zac-finance/	0.29
8578	Bellevue Network	BLV	https://coinmarketcap.com//currencies/bellevue-network/	0.01
8579	YeaFinance	YEA	https://coinmarketcap.com//currencies/yeafinance/	8.09
8580	YFOS.finance	YFOS	https://coinmarketcap.com//currencies/yfos-finance/	4.06
8581	OWL Token (StealthSwap)	OWL	https://coinmarketcap.com//currencies/owl-token-stealthswap/	0.22
8582	YFPRO Finance	YFPRO	https://coinmarketcap.com//currencies/yfpro-finance/	2.98
8583	PieDAO DEFI Large Cap	DEFI+L	https://coinmarketcap.com//currencies/piedao-defi-large-cap/	4.64
8584	Crypxie	CPX	https://coinmarketcap.com//currencies/crypxie/	5.23
8585	Jiviz	JVZ	https://coinmarketcap.com//currencies/jiviz/	0
8586	BFis.Finance	BFI	https://coinmarketcap.com//currencies/bfis-finance/	1.16
8587	deCraft Finance	CRAFT	https://coinmarketcap.com//currencies/decraft-finance/	21.37
8588	Tavittcoin	TAVITT	https://coinmarketcap.com//currencies/tavittcoin/	0.08
8589	Load Network	LOAD	https://coinmarketcap.com//currencies/load-network/	0.03
8590	Payship	PSHP	https://coinmarketcap.com//currencies/payship/	33.69
8591	BonezYard	BNZ	https://coinmarketcap.com//currencies/bonezyard/	0
8592	Bitfinex Bitcoin Dominance Perps	BTCDOM	https://coinmarketcap.com//currencies/bitfinex-bitcoin-dominance-perps/	251.74
8593	PayUSD	PUSD	https://coinmarketcap.com//currencies/payusd/	1.42
8594	Payou Finance	PAYOU	https://coinmarketcap.com//currencies/payou-finance/	1.07
8595	McNetworkDefi	MCN	https://coinmarketcap.com//currencies/mcnetwork/	0.01
8596	Friends With Benefits	FWB	https://coinmarketcap.com//currencies/friends-with-benefits/	0.45
8597	BananoDOS	yBAN	https://coinmarketcap.com//currencies/bananodos/	119.23
8598	Naz Coin	NAZ	https://coinmarketcap.com//currencies/naz-coin/	0
8599	BSCswap	BSWAP	https://coinmarketcap.com//currencies/bscswap/	64.95
8600	Lumos	LMS	https://coinmarketcap.com//currencies/lumos/	0.02
8601	pxUSD Synthetic USD Expiring 1 April 2021	PXUSD_MAR2021	https://coinmarketcap.com//currencies/pxusd-synthetic-usd-expiring-1-april-2021/	2.14
8602	Tatcoin	TAT	https://coinmarketcap.com//currencies/tatcoin/	0.04
8603	HubDao	HD	https://coinmarketcap.com//currencies/hubdao/	0.11
8604	Bloody Token	BLOODY	https://coinmarketcap.com//currencies/bloody-token/	0
8605	Hoo Token	HOO	https://coinmarketcap.com//currencies/hoo-token/	0.3
8606	YYFI.Protocol	YYFI	https://coinmarketcap.com//currencies/yyfi-protocol/	0.38
8607	Hiz Finance	HIZ	https://coinmarketcap.com//currencies/hiz-finance/	0.41
8608	Gold BCR	GBCR	https://coinmarketcap.com//currencies/gold-bcr/	1918.85
8609	DotBased	xDOT	https://coinmarketcap.com//currencies/dotbased/	9.17
8610	$YEET	YEET	https://coinmarketcap.com//currencies/yeet/	3.18
8611	stakedFIRO	stFIRO	https://coinmarketcap.com//currencies/stakedfiro/	8.28
8612	Smpl foundation	SMPL	https://coinmarketcap.com//currencies/smplfoundation/	24.39
8613	zLOT Finance	ZLOT	https://coinmarketcap.com//currencies/zlot-finance/	1160.8
8614	Fiola	FLA	https://coinmarketcap.com//currencies/fiola/	0
8615	wormhole.finance	WHOLE	https://coinmarketcap.com//currencies/wormhole-finance/	1.15
8616	Future-Cash Digital	FCD	https://coinmarketcap.com//currencies/future-cash-digital/	0.02
8617	XFII	XFII	https://coinmarketcap.com//currencies/xfii/	4.36
8618	7Finance	SVN	https://coinmarketcap.com//currencies/7finance/	0.18
8619	Blueshare Token	BST1	https://coinmarketcap.com//currencies/blueshare-token/	0
8620	Volts.Finance	VOLTS	https://coinmarketcap.com//currencies/volts-finance/	11.71
8621	Kanva	KNV	https://coinmarketcap.com//currencies/kanva/	1.96
8622	PBS Chain	PBS	https://coinmarketcap.com//currencies/pbs-chain/	0
8623	Light Coin Exchange Token	LCT	https://coinmarketcap.com//currencies/light-coin-exchange-token/	0.01
8624	UnoSwap	UNOS	https://coinmarketcap.com//currencies/unoswap/	0.87
8625	Lock3r Network	LK3R	https://coinmarketcap.com//currencies/lk3r/	2.82
8626	Wrapped ZEC	WZEC	https://coinmarketcap.com//currencies/wrapped-zec/	132.27
8627	HoldToWin	7ADD	https://coinmarketcap.com//currencies/holdtowin/	1.02
8628	Tradebitpay	TBP	https://coinmarketcap.com//currencies/tradebitpay/	0.02
8629	LeLeFoodChain	LELE	https://coinmarketcap.com//currencies/lelefoodchain/	0.06
8630	Bitpumps Token	BPT	https://coinmarketcap.com//currencies/bitpumps-token/	0
8631	STVKE	STV	https://coinmarketcap.com//currencies/stvke-network/	0.01
8632	Money of Tomorrow, Today	MTTCOIN	https://coinmarketcap.com//currencies/money-of-tomorrow-today/	87.31
8633	PoolStake	PSK	https://coinmarketcap.com//currencies/poolstake/	0.1991
8634	Greenheart Punt	PUNT	https://coinmarketcap.com//currencies/greenheart-punt/	0.1119
8635	DGPayment	DGP	https://coinmarketcap.com//currencies/dgpayment/	0.1276
8636	WAV3	WAV3	https://coinmarketcap.com//currencies/wav3/	6.91
8637	Elynet	ELYX	https://coinmarketcap.com//currencies/elynet/	0.01984
8638	Sugarchain	SUGAR	https://coinmarketcap.com//currencies/sugarchain/	0.002821
8639	Yearn Ecosystem Token Index	YETI	https://coinmarketcap.com//currencies/yearn-ecosystem-token-index/	1.89
8640	ARVO	ARVO	https://coinmarketcap.com//currencies/arvo/	3.32
8641	502 Bad Gateway Token	Z502	https://coinmarketcap.com//currencies/z502-bad-gateway-token/	1.767e-05
8642	Venus XRP	vXRP	https://coinmarketcap.com//currencies/venus-xrp/	0.01
8643	Deflect	DEFLCT	https://coinmarketcap.com//currencies/deflect/	2.52
8644	Blaze DeFi	BNFI	https://coinmarketcap.com//currencies/blaze-defi/	0.36
8645	Trinity Protocol	TRI	https://coinmarketcap.com//currencies/trinity-protocol/	0.01
8646	MediconnectUk	MEDI	https://coinmarketcap.com//currencies/mediconnectuk/	0.02
8647	Refract	RFR	https://coinmarketcap.com//currencies/refract/	432.08
8648	DAPPCENTS	DPC	https://coinmarketcap.com//currencies/dappcents/	0
8649	3XT TOKEN	3XT	https://coinmarketcap.com//currencies/3xt-token/	243.94
8650	Diffract Finance	DFR	https://coinmarketcap.com//currencies/diffract-finance/	0.09
8651	Supreme Finance	HYPE	https://coinmarketcap.com//currencies/hype/	5.43
8652	LinkBased	LBD	https://coinmarketcap.com//currencies/linkbased/	3.17
8653	Zipmex	ZMT	https://coinmarketcap.com//currencies/zipmex/	2.41
8654	Basiscoin Cash	BCC	https://coinmarketcap.com//currencies/basiscoin/	0.03
8655	Basiscoin Share	BCS	https://coinmarketcap.com//currencies/basiscoin-share/	0.15
8656	Space Dollar	SPAD	https://coinmarketcap.com//currencies/space-dollar/	0
8657	FOX Token	FOX	https://coinmarketcap.com//currencies/fox-token/	0.41
8658	Venus Filecoin	vFIL	https://coinmarketcap.com//currencies/venus-filecoin/	1.18
8659	Vaultz	VAULTZ	https://coinmarketcap.com//currencies/vaultz/	6.32
8660	YFST.Protocol	YFST	https://coinmarketcap.com//currencies/yfst-protocol/	0
8661	Wrapped Monero	WXMR	https://coinmarketcap.com//currencies/wrapped-monero/	243.71
8662	Dynamic Supply Tracker	DSTR	https://coinmarketcap.com//currencies/dynamic-supply-tracker/	6.36
8663	Dynamic Supply	DST	https://coinmarketcap.com//currencies/dynamic-supply/	0.68
8664	PegsUSD	PUSD	https://coinmarketcap.com//currencies/pegs-usd/	0.19
8665	PegsShares	PEGS	https://coinmarketcap.com//currencies/pegsshares/	0.05
8666	Bitbot Protocol	BBP	https://coinmarketcap.com//currencies/bitbot-protocol/	23.55
8667	Xstable.Protocol	XST	https://coinmarketcap.com//currencies/xstable-protocol/	0.6
8668	Natus Vincere Fan Token	NAVI	https://coinmarketcap.com//currencies/natus-vincere-fan-token/	14.96
8669	Young Boys Fan Token	YBO	https://coinmarketcap.com//currencies/young-boys-fan-token/	5.12
8670	USDEX	USDEX	https://coinmarketcap.com//currencies/usdex/	1
8671	Universal Dollar	U8D	https://coinmarketcap.com//currencies/universal-dollar/	0.13
8672	Gladius Finance	GLAD	https://coinmarketcap.com//currencies/gladius-finance/	5.58
8673	Venus BETH	VBETH	https://coinmarketcap.com//currencies/venus-beth/	34.48
8674	YFLink Synthetic	sYFL	https://coinmarketcap.com//currencies/yflink-synthetic/	0.19
8675	Option Token	OT	https://coinmarketcap.com//currencies/option-token/	2.34
8676	Defla	DEFLA	https://coinmarketcap.com//currencies/defla/	0.57
8677	DeFiStarter	DFST	https://coinmarketcap.com//currencies/defistarter/	3.02
8678	SIMBA Storage	SIMBA	https://coinmarketcap.com//currencies/simba/	0
8679	mStable BTC	MBTC	https://coinmarketcap.com//currencies/mstable-btc/	57167.87
8680	Parsiq Boost	PRQBOOST	https://coinmarketcap.com//currencies/parsiq-boost/	0.03
8681	Quantfury Token	QTF	https://coinmarketcap.com//currencies/quantfury-token/	9.83
8682	ShuttleOne	wSZO	https://coinmarketcap.com//currencies/shuttleone/	0.26
8683	Aave Enjin	aENJ	https://coinmarketcap.com//currencies/aave-enjin/	2.62
8684	Weather Finance	WEATHER	https://coinmarketcap.com//currencies/weather-finance/	0.68
8685	wanSUSHI	WANSUSHI	https://coinmarketcap.com//currencies/wansushi/	8.17
8686	Lua Token	LUA	https://coinmarketcap.com//currencies/lua-token/	0.13
\.


--
-- Name: coin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.coin_id_seq', 8686, true);


--
-- Name: coin coin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coin
    ADD CONSTRAINT coin_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

