"""
https://gcorelabs.com/careers/
{'title': 'Backend Developer (Cloud Product)',
'town': 'Moscow / Perm / Remotely',
'url': 'https://gcorelabs.com/careers/backend-developer-cloud-product/'}
"""
import requests
from bs4 import BeautifulSoup
import csv

FIELDS = ('title', 'town', 'url')
CSV = 'gcore_hire.csv'
URL = 'https://gcorelabs.com/careers/'


def get_html(url):
    r = requests.get(url)
    return r.text


def csv_init(csv_name=CSV, fields_names=FIELDS):
    with open(csv_name, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fields_names)
        writer.writeheader()


def write_csv(data, csv_name=CSV, fields_names=FIELDS):
    with open(csv_name, 'a') as file:
        writer = csv.DictWriter(file, fieldnames=fields_names)

        writer.writerow(data)


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    vacancies_ul = soup.find('ul', {'class': 'list_vacances'})
    vacancies_li = vacancies_ul.find_all('li')
    for li in vacancies_li:
        url = li.find('a').get('href')
        title = li.find('div', class_='title_vac').text
        town = li.find('div', class_='town_vacance').text
        data = {'title': title, 'town': town, 'url': url}

        write_csv(data)


def main():
    html = get_html(URL)
    csv_init()
    get_page_data(html)


if __name__ == '__main__':
    main()
