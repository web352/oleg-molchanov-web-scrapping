import csv
from peewee import *
import os

# db = PostgresqlDatabase(database=os.environ['db_name'],
#                         user=os.environ['db_user'],
#                         password=os.environ['db_pw'], host='localhost')

db_name = '7.db'
if os.path.exists(db_name):
    os.remove(db_name)

db_path = f'{os.path.abspath(os.getcwd())}/{db_name}'

db = SqliteDatabase(db_path, pragmas={
    'journal_mode': 'wal',
    'cache_size': -1024 * 64})


class Coin(Model):
    title = CharField()
    symbol = CharField()
    link = TextField()
    currency = FloatField()

    class Meta:
        database = db


def read():
    db.connect()
    db.create_tables([Coin])

    with open('cmc.csv') as file:
        order = ['title', 'symbol', 'link', 'currency']
        reader = csv.DictReader(file, fieldnames=order)

        coins = list(reader)
        print(coins[0], type(coins[0]))

        # 1) Bad way to write data into DB
        # for row in coins:
        #     coin = Coin(name=row['title'], url=row['link'], price=row['currency'])
        #     coin.save()

        # for row in coins:
        #     print(row)

        # 2) Транзакция
        with db.atomic():
            for row in coins:
                try:
                    row['currency'] = float(row['currency'])

                # ValueError: could not convert string to float: ''

                except ValueError:
                    row['currency'] = 0
                Coin.create(**row)

        # 3) Запись слайсами
        with db.atomic():
            for index in range(0, len(coins), 100):
                Coin.insert_many(coins[index:index + 100]).execute()


if __name__ == '__main__':
    read()

"""
select * from coin where currency > 50 order by currency DESC limit 15;
"""
