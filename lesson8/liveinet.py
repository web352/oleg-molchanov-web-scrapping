import requests
import csv
import time


def get_answer(url):
    r = requests.get(url)
    return r.text


def write_csv(data):
    with open('liveinet.csv', 'a') as f:
        order = ['site_title', 'url', 'description', 'count']
        writer = csv.DictWriter(f, fieldnames=order)
        writer.writerow(data)


def main_1():
    pattern = 'https://www.liveinternet.ru/rating/ru//today.tsv?page={}'
    page_num = 1
    while True:
        url = pattern.format(page_num)
        page = get_answer(url).split('\n')

        # print(page[0], page[-1], sep='\n\n')
        try:
            for row in page[1:-2]:
                row = row.split('\t')
                site_title = row[0]
                url = row[1]
                description = row[2].strip()
                count = row[3]
                data = {'site_title': site_title, 'url': url, 'description': description, 'count': count}
                write_csv(data)

        except IndexError:
            break

        except Exception as e:
            print(e)
            """
            requests.exceptions.ConnectionError: HTTPSConnectionPool(host='www.liveinternet.ru', port=443): Max retries
            exceeded with url: /rating / ru // today.tsv?page = 2481 
            (Caused by NewConnectionError('<urllib3.connection.HTTPSConnection object at 0x1023e26d0>: 
            Failed to establish a new connection: [Errno 61] Connection refused'))
            """
            time.sleep(10)
            continue

        else:
            page_num += 1


def main_2():
    pattern = 'https://www.liveinternet.ru/rating/ru//today.tsv?page={}'

    for i in range(1, 5256+1):
        url = pattern.format(i)
        page = get_answer(url).split('\n')[1:-2]
        for row in page:
            row = row.split('\t')
            site_title = row[0]
            url = row[1]
            description = row[2].strip()
            count = row[3]
            data = {'site_title': site_title, 'url': url, 'description': description, 'count': count}
            write_csv(data)
            time.sleep(1)


if __name__ == '__main__':
    main_2()
