"""
Target: `https://wordpress.org/plugins/browse/blocks/`
`https://wordpress.org/plugins/browse/blocks/page/2/
1. Plugin title
2. Rating
  * Stars
  * Votes
3. Description
4. Author
5. Installations
6. WP version
7. Plugin url
"""
import requests
from bs4 import BeautifulSoup
import csv


def get_html(url):
    r = requests.get(url)
    return r.text


def rating_to_num(s):
    number = s.split()[0]
    return float(number.replace(',', '.'))


def calculate_pages_count(url='https://wordpress.org/plugins/browse/blocks/'):
    html = get_html(url)
    soup = BeautifulSoup(html, 'lxml')
    pages = int(soup.find_all('a', class_='page-numbers')[-2].text)  # last -> <Next> # next to last <number of pages>
    return pages

# Origin function for collect data into csv
# def write_csv(data):
#     with open('wp_plugins.csv', 'a') as f:
#         writer = csv.writer(f)
#         writer.writerow((data['title'], data['stars'], data['votes'], data['description'], data['author'],
#                          data['installations'], data['wp_version'], data['plugin_url']))


def csv_init(file_name, fields_names):
    """Problem here is that we need to add file_name and fields_name in bunch of methods."""
    with open(file_name, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fields_names)
        writer.writeheader()


def write_csv(data, file_name, fields_names):
    fields_names = fields_names
    with open(file_name, 'a') as f:
        writer = csv.DictWriter(f, fieldnames=fields_names)
        writer.writerow({fieldname: data[fieldname] for fieldname in fields_names})


def get_page_data(html, file_name, fields_names):
    soup = BeautifulSoup(html, 'lxml')
    blocks = soup.find("div", {"id": "primary"})
    articles = blocks.find_all('article')
    for article in articles:
        title = article.find('h3').text.strip()
        stars = float(article.find('div', class_='wporg-ratings').get('data-rating'))
        votes = rating_to_num(article.find('span', class_='rating-count').find('a').text)
        description = article.find('div', class_='entry-excerpt').text.strip()
        author = article.find('span', class_='plugin-author').text.strip()
        installations = article.find('span', class_='active-installs').text.strip()
        wp_version = article.find('span', class_='tested-with').text.strip()
        plugin_url = article.find('h3').find('a').get('href').strip()

        data = {'title': title, 'stars': stars, 'votes': votes, 'description': description, 'author': author,
                'installations': installations, 'wp_version': wp_version, 'plugin_url': plugin_url}
        write_csv(data, file_name, fields_names)


def main():
    file_name = 'wp_plugins.csv'
    fields_names = ['title', 'stars', 'votes', 'description', 'author', 'installations', 'wp_version', 'plugin_url']
    csv_init(file_name, fields_names)
    pattern = 'https://wordpress.org/plugins/browse/blocks/page/{page_num}/'
    pages = calculate_pages_count()
    for i in range(1, pages + 1):
        url = pattern.format(page_num=i)
        html = get_html(url)
        get_page_data(html, file_name, fields_names)


if __name__ == '__main__':
    main()
